<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'videogar_coindev');

/** MySQL database username */
define('DB_USER', 'videogar_devroot');

/** MySQL database password */
define('DB_PASSWORD', 'pass@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y[nRv!R6%qf,xO5X=URJm-jZdYnwMw[5nOU(, CiD@A6V:x0:}-.3X--],yKt5BR');
define('SECURE_AUTH_KEY',  'V9Pl#jYL?8m=<~4LnBp^||_Ot-krG.fmI{WUep`XcQuS1Vk5eB}TX)Kf& jG!6HJ');
define('LOGGED_IN_KEY',    ']# FcxTJS=(N]0BQY~GF37aw`Y]W8zhO)^5lj-[,i4}HK_tAcH`RxaC3ZlPeXxRL');
define('NONCE_KEY',        'f;ie_^q>d#<~C]_YEs?-|Xs0zuv,5o.6}!vYe4Gnyt3C<gr(T-qo[V3 nyq}0rjE');
define('AUTH_SALT',        'eT=6=-<k`c1llyV7R+qBL`R{QYAh.fdN6x0>!^6-mv?}zVozl?(E`3O3cL|L0s:{');
define('SECURE_AUTH_SALT', '@x>Bjx5%[?$wvf9tO.$<E=Y;db,fdRVFSac9(Pa&fL.N}PB|j!h7lm1YP B8]L(r');
define('LOGGED_IN_SALT',   '.XItP^*<sIA*:7!$Tb&J2GgrXw7QxS]4SdE[_K+G|~n?*5Q,m|.jN<Zt~8fo{oU{');
define('NONCE_SALT',       '#}8L k1RJ=KzNwjW+9]ZulO%dgoRvU}QZRCil0$]ts9v?noj#k[rx@0i|/mriR&e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'blogcoin_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
