<?php
/*
 * Template Name: contact us page
 * 
 */
?>
<?php
get_header(); ?>
<section>
  <div class="container">
    <div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="whitebg">
          <h1 class="mart0 weight400"><?php the_title(); ?></h1> 
         
          <?php echo do_shortcode('[contact-form-7 id="35" title="Contact us"]'); ?>
           </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <div class="whitebg">
      	<?php while(have_posts()) : the_post(); ?>
      		<?php the_content();?>
      	
        <?php endwhile; ?>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>