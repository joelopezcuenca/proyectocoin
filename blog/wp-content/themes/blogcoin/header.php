<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Coin Blog</title>

<!-- Bootstrap -->
<link href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php bloginfo('template_url');?>/css/font-awesome.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/isotope.pkgd.min.js"></script>
<script src="<?php bloginfo('template_url');?>/js/main.js"></script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header>
  <div class="container">
    <div class="row">
     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 list-inline">
      	<a href="http://blog.proyectocoin.com/"><img src="<?php bloginfo('template_url');?>/images/coin-logo.png" alt="Coin logo"></a>
      	<?php dynamic_sidebar('top-left-navigation'); ?> 
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right mart10">
      	<?php dynamic_sidebar('top-right-navigation'); ?>
        <!-- <ul class="list-inline">
          <li><a href="#">Log In -</a></li>
          <li><a href="#">Socios</a></li>
        </ul> -->
      </div>
     </div>
  </div>
</header>
<nav class="navbar navbar-default">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> CATEGORIES </button>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
	<?php
		$args = array(
			'theme_location'  => 'main_menu',  
			'menu_class'      => 'nav navbar-nav navbar-right',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu', 
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
		);
		
		wp_nav_menu($args);
	?>    	
      
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>