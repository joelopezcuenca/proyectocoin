<footer>
  <div class="topfoot">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
          <div class="row">
            <?php dynamic_sidebar('footer-script-area'); ?>
            <!-- <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
              <h3>Get Started Building Your Freedom Business <i class="fa fa-long-arrow-right"></i></h3>
              <p>Enter your emial address to get the freedom business quickstart guide</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mart30">
              <div class="yellow-but"> <a href="#" data-toggle="modal" data-target="#myModal">START HERE!</a></div>
            </div>   --> 
          </div>
        </div>
      </div>
    </div>
  </div><div class="mid-foot">
   <div class="container">
      <div class="row list-inline">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">			<img src="<?php bloginfo('template_url');?>/images/foot-logo.png" alt="Coin logo">  			 <?php dynamic_sidebar('footer-left-area'); ?>		</div>		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right mart6">				<?php dynamic_sidebar('footer-right-social-area'); ?> 		</div>
       </div>	</div></div>
  </div>
  <div class="foot">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 tc">
          <?php dynamic_sidebar('footer-bottom-copyright-area'); ?>
        </div>
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 tr list-inline">
          <?php dynamic_sidebar('footer-bottom-menu'); ?>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
<!--awbeber form js--> 
<script type="text/javascript">
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-719862157")) {
                document.getElementById("af-form-719862157").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-719862157")) {
                document.getElementById("af-body-719862157").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-719862157")) {
                document.getElementById("af-header-719862157").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-719862157")) {
                document.getElementById("af-footer-719862157").className = "af-footer af-quirksMode";
            }
        }
    })();
  
</script> 
<script type="text/javascript">document.getElementById('redirect_f98e0d273f98dca991d116c41e48a33b').value = document.location;</script> 

<!-- Modal -->
<div class="modal fade coin-form" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row text-center">
          <div class="col-md-12 col-sm-12 col-xs-12"><img src="<?php bloginfo('template_url'); ?>/images/popup-logo.png" alt=""></div>
          <div class="col-md-8 col-md-offst-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 border1"></div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <h1 class="weight300">EARN MONEY WITH COIN</h1>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <h4 class="weight300 line-height-normal">Nunca ha habido un mejor momento para conducir con Coin. Registrarse es fácil y usted podrá estar ganando dinero en poco tiempo .</h4>
          </div>
          <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12"> 
            <!-- Newsletter form -->
            <div id="newsletter" class="form-wrap">
              <form method="post" class="af-form-wrapper" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
                <div style="display: none;">
                  <input type="hidden" name="meta_web_form_id" value="719862157" />
                  <input type="hidden" name="meta_split_id" value="" />
                  <input type="hidden" name="listname" value="awlist3953336" />
                  <input type="hidden" name="redirect" value="" id="redirect_f98e0d273f98dca991d116c41e48a33b" />
                  <input type="hidden" name="meta_adtracking" value="Subscriber_form" />
                  <input type="hidden" name="meta_message" value="1" />
                  <input type="hidden" name="meta_required" value="email" />
                  <input type="hidden" name="meta_tooltip" value="" />
                </div>
                <div id="af-form-719862157" class="af-form"><!--<div id="af-header-719862157" class="af-header"><div class="bodyText"><p>&nbsp;</p></div></div>-->
                  <div id="af-body-719862157" class="af-body af-standards">
                    <div class="af-element width75">
                      <div class="af-textWrap">
                        <input class="text" id="awf_field-74991821" type="text" name="email" value="" tabindex="500" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                      </div>
                      <div class="af-clear"></div>
                    </div>
                    <div class="af-element buttonContainer width25">
                      <input name="submit" id="af-submit-image-719862157" type="submit" name="ENTRER" class="image" alt="Submit Form"  tabindex="501" />
                      <div class="af-clear"></div>
                    </div>
                    <div class="af-clear"></div>
                  </div>
                  <!--<div id="af-footer-719862157" class="af-footer"><div class="bodyText"><p>&nbsp;</p></div></div>--> 
                </div>
                <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=7IycHGxMjKzs" alt="" /></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body></html>