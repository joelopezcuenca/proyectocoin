<?php
get_header(); ?>


 <section>
  <div class="container">
    <div class="row">
    	
    	<?php
    	$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    	 while (have_posts()): the_post() ?>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <div class="whitebg">
          <h1 class="mart0 weight400"><?php the_title(); ?></h1>
          <img class="img-responsive imgwidth" src="<?php echo $image ?>" alt="">
          <div class="row mart20">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <ul class="list-inline setrater">
                <li><?php echo get_the_date('F d Y', true); ?></li>
                <li>Posted by <?php the_author(); ?></li>
              </ul>
            </div>
            </div>
          <div class="mart20">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
      <?php endwhile; ?>
      <?php get_sidebar('right'); ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>