<?php
############# main menu #################
function after_setup_theme_blogcoin() {
  register_nav_menus(   array('main_menu' => __( 'Main Menu' ),)  );
  
        register_sidebar(array(
                'name' => 'Main Left Sidebar',
                'id'   => 'main-left-sidebar',
                'description'   => 'Main Left Sidebar.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        ));				 register_sidebar(array(                'name' => 'Main Right Sidebar',                'id'   => 'main-right-sidebar',                'description'   => 'Main Left Sidebar.',                'before_widget' => '<div id="%1$s" class="widget %2$s">',                'after_widget'  => '</div>',                'before_title'  => '<h4>',                'after_title'   => '</h4>'        ));		
       register_sidebar(array(
                'name' => 'Footer Script Area',
                'id'   => 'footer-script-area',
                'description'   => 'This is a footer script.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        ));
		register_sidebar(array(
                'name' => 'Footer Left Area',
                'id'   => 'footer-left-area',
                'description'   => 'This is a footer script.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        ));
		register_sidebar(array(
                'name' => 'Footer Right Social Area',
                'id'   => 'footer-right-social-area',
                'description'   => 'This is a Footer Right Area.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        ));
		register_sidebar(array(
                'name' => 'Footer Bottom Copyright Area',
                'id'   => 'footer-bottom-copyright-area',
                'description'   => 'This is a Footer Buttom Content Area.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        ));
        register_sidebar(array(
                'name' => 'Footer Bottom Menu',
                'id'   => 'footer-buttom-menu',
                'description'   => 'This is a Footer Buttom Menu.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        ));
		 register_sidebar(array(
                'name' => 'Top Left Navigation',
                'id'   => 'top-left-navigation',
                'description'   => 'This is a Top Left Navigation.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        ));
        register_sidebar(array(
                'name' => 'Top Right Navigation',
                'id'   => 'top-right-navigation',
                'description'   => 'This is a Top Right Navigation.',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h4>',
                'after_title'   => '</h4>'
        )); 
add_theme_support( 'post-thumbnails' );  
}
add_action( 'after_setup_theme', 'after_setup_theme_blogcoin' ); 

################# widget#######################
function custom_enqueue_scripts() {

        wp_deregister_script( 'jquery' ); 

        wp_register_script( 'jquery', get_template_directory_uri() . '/bin/js/jquery-1.10.1.min.js', 'jquery', '1.10.1',TRUE);
        wp_register_script( 'fittext', get_template_directory_uri() . '/bin/js/jquery.fittext.js', 'jquery', '1.0',TRUE );
	 wp_enqueue_style( 'blogcoin-style', get_template_directory_uri() . '/style.css' );
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'fittext' );

}
add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts' );
?>
