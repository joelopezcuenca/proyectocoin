 <?php 
get_header(); 
?>

<section>
  <div class="container">
    <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 addtop">
      <?php dynamic_sidebar('main-left-sidebar'); ?>
    </div> 
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <div class="row left-contatainer" id="main-isotop">
        	
		<?php
    
        if( have_posts() ) : 
		$counter =1;
       while( have_posts()) :  the_post();    

        ?>         	
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?php
				$image_id = get_post_thumbnail_id();	
				$image_url = wp_get_attachment_image_src($image_id, 'full', true); 
				$image_url = $image_url=="" ? get_bloginfo('template_url').'/images/noImageAvailable.jpg' :$image_url[0];
			?>          	
            <div class="whitebg"> 
		<?php if(isset($image_id) && !empty($image_id))
	      { ?> 	
			<a href="<?php the_permalink(); ?>"><img class="img-responsive" src="<?php echo $image_url ?>" alt=""></a> 	    

	      <?php	 }else { ?>

		  <?php } ?>                  		
             <a href="<?php the_permalink(); ?>"> <h4><?php the_title(); ?></h4>
              </a>
              <?php
				$content = get_the_content();
				echo substr($content, 0, 210);
				?>              
			  <ul class="list-inline setrater">
                <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
                <li>Posted by <?php the_author(); ?> </li>
              </ul>
              <div class="row">
           
                <div class="col-md-8 col-sm-6 col-xs-12 category">
                	Categories 
                		<?php the_category(', '); ?>
                		</div>
                <div class="col-md-4 col-sm-6 col-xs-12 share text-right"><span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span></div>
              </div>
            </div>
          </div>
          
		<?php 
		if($counter==1){?>
             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 addvertise">
                        <?php dynamic_sidebar('main-left-sidebar'); ?>
            </div>
        <?php } $counter++;
		endwhile; ?>
        
		<?php endif; ?>            
          

          
        </div>
          <div class="paging"> <?php wp_pagenavi(); ?> </div>
      </div>
    </div>
  </div>
</section> 
<?php get_footer(); ?>
