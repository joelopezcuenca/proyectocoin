
		  
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
       <?php dynamic_sidebar('main-right-sidebar'); ?>
        <div>
        	
        	<?php
				$catquery = new WP_Query( 'category_name=Featured Article&posts_per_page=1' );
				while($catquery->have_posts()) : $catquery->the_post();
				
				$image_id = get_post_thumbnail_id();	
				$image_url = wp_get_attachment_image_src($image_id, 'full', true); 
				$image_url = $image_url=="" ? get_bloginfo('template_url').'/images/noImageAvailable.jpg' :$image_url[0];
			?>    
				
          
          <div class="whitebg">
          	<?php if(isset($image_id) && !empty($image_id))
	      { ?> 
          	 <a href="<?php the_permalink(); ?>"><img class="img-responsive" src="<?php echo $image_url ;?>" alt=""></a> <a href="#">
          	 <?php	 }else { ?>

		  <?php } ?>   
            <h4 class="weight400"> <h4><?php the_title(); ?></h4>
            </a>
            <p><?php
				$content = get_the_content();
				echo substr($content, 0, 210);
				?> </p>
            <ul class="list-inline setrater">
              <li><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></li>
              <li>Posted by <?php the_author(); ?></li>
            </ul>
            <div class="row">
            	
              <div class="col-md-12 col-sm-12 col-xs-12 category">
              	Categories <?php the_category(', '); ?>
              	</div>
              <div class="col-md-12 col-sm-12 col-xs-12 share"><span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span></div> 
            </div>
          </div>
           <?php endwhile; ?>
        </div>
      </div>
     