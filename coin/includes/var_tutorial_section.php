<?php  if($current_pageName == 'Afiliados'){ ?>
    <div class="boxes round round1 iunvitar">
    <div class="row-fluid">
      <div class="span12 heading">
	  <p><i class="fa fa-chevron-down Padd10"></i> Bienvenido a la sección de Invitaciones. Desde esta sección, podrás enviar invitaciones por mail a quien tu quieras.  <br/>

<font>Para más información, no olvides ver el tutorial</font></p>

	  <div class="video-a"> <a href="#video_tutorial" data-toggle="modal" >Ver tutorial</a> <img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> </div>
	  </div>
	   
      <div class="span6 offset3 MarT20">
    <div class="row-fluid">
    <div class="span4">
	<a id="loadmore" href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $client_id;?>&redirect_uri=<?php  echo $redirect_uri;?>&scope=https://www.google.com/m8/feeds/&response_type=code" class="hidethis"><img src="<?php echo IMAGE_URL; ?>/gmail-big.jpg"></a></div>
    
	<div class="span4"><a href="javascript:void(0)"><img src="<?php echo IMAGE_URL; ?>/outlock.jpg" id="import"></a><img src="<?php echo IMAGE_URL; ?>/bx_loader.gif" id="loading" style="display:none"></div>

    <!-- <div class="span4"><a href="javascript:void(0)" id="yahoo_click" ><img src="<?php echo IMAGE_URL; ?>/yahoo.jpg"></a></div> -->
    </div>
    </div>
      
    </div> 
  </div>
<?php } ?>

<?php  if($current_pageName == 'tutoriale'){ ?>
  <div class="boxes round round1 iunvitar">
    <div class="row-fluid">
      <div class="span12 heading"><i class="fa fa-chevron-down Padd10"></i>Al ayudar a otros a consegur sus primeros Coins, Tú estarás ganando muchos más Coins para cumplir tus sueños. Solo sigue estos 2 pasos:</div>
   </div>
   <div class="row-fluid MarT20">
     <div class="span10 text-center">
	 <form name="tutorial_search" id="tutorial_search" action="" method="GET">
       <div class="input-prepend">
         <span class="add-on"><i class="fa fa-search"></i></span>
         <input id="prependedInput" name="keywords" value="<?php echo $keywords;?>" type="text" placeholder="Buscar amigos">
       </div>
	   </form>
     </div>
     <div class="span2 text-right video-a"> <a href="#video_tutorial" data-toggle="modal" >Ver tutorial</a> <img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> </div>
     </div>
   </div>
<?php } ?>

<?php  if($current_pageName == 'Link'){ ?>
  <div class="boxes round round1 iunvitar">
    <div class="row-fluid">
      <div class="span12 heading"><i class="fa fa-chevron-down Padd10"></i>Bienvenido a tu zona de links. En esta sección podrás seleccionar el link que deseas promocionar. Cada link es diferente y llevará a tus invitados a ver diferentes páginas.<br/>
	  
</div>
   </div>
   <div class="row-fluid">
     <!-- <div class="span10 text-center">
       
     </div>-->
     <div class="video-a"> <a href="#video_tutorial" data-toggle="modal" >Ver tutorial</a> <img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> </div>
     </div>
   </div>
<?php } ?>

<?php  if($current_pageName == 'share_video'){ ?>
  <div class="boxes round round1 iunvitar">
    <div class="row-fluid">
      <div class="span12 heading"><i class="fa fa-chevron-down Padd10"></i> 
Bienvenido a la sección de videos. Aquí podrás compartir videos de tus 
experiencias o videos que te inspiren a ti o a los demás a soñar.<br/>
	  <!--<span style="color:#30BDFF">Para más información, no te olvides de ver el tutorial.</span> --> </div>    
    </div>   
     <div class="row-fluid PaddT101">
		<!-- <div class="span3 text-right">
			<div class="input-prepend irf">
			</div>
		</div> -->
      <div class="video-a"> <a href="#video_tutorial" data-toggle="modal" >Ver tutorial</a> <img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> </div>
    </div>   
  </div>
 
<?php } ?>

<?php  if($current_pageName == 'calculator'){ ?>
<span class="hide_metter">
  <div class="boxes iunvitar">
    <div class="row-fluid">
    	<div class="span12 text-center"><div class="smart new"><p><span class="text-green">S</span><span class="text-red">M</span><span class="text-skyblue">A</span><span class="text-yellow">R</span><span class="text-dark-red">T</span></p></div></div>
    </div>
    <div class="row-fluid">
    	<div class="span12">
        	<ul class="klc-nav hide_metter">
                <li class="<?php if($current_page=='calculator'){ ?> select <?php } ?> "><span><a href="javascript:void(0)" onclick="jQuery('#calculator_frm').submit()" >PASO 1</a></span></li>
                <li class="<?php if($current_page=='calculator2'){ ?> select <?php } ?>" ><span><a href="javascript:void(0)" onclick="jQuery('#calculator_frm').submit()" >PASO 2</a></span></li>
                <li class="<?php if($current_page=='calculator3'){ ?> select <?php } ?>" ><span><a href="javascript:void(0)" onclick="jQuery('#calculator_frm2').submit()" >PASO 3</a></span></li>
                <li class="<?php if($current_page=='calculator4'){ ?> select <?php } ?>" ><span><a href="javascript:void(0)" onclick="jQuery('#calculator_frm3').submit()" >PASO 4</a></span></li>
                 <li class="<?php if($current_page=='calculator5'){ ?> select <?php } ?>" ><span><a href="javascript:void(0)" onclick="jQuery('#calculator_frm4').submit()" >PASO 5</a></span></li> 
              </ul>
        </div> 
    </div>
	
	
	<?php

		$password_section_show_hide = 'none';
		if($current_page=='calculator' && empty($first_step_password)){
			$password_section_show_hide = 'block';
		}elseif($current_page=='calculator2' && empty($second_step_password))
		{
			$password_section_show_hide = 'block';
		}elseif($current_page=='calculator3' && empty($third_step_password)){
			$password_section_show_hide = 'block';
		}elseif($current_page=='calculator4' && empty($fourth_step_password)){
			$password_section_show_hide = 'block';
		}elseif($current_page=='calculator5' && empty($fifth_step_password)){
			$password_section_show_hide = 'block';
		}
		
	?>
	 <!-- Password section start -->
    <div class="row-fluid">
      <div id="program_lock" class="klc_page" style="display:<?php echo $password_section_show_hide;?>">
          <form name="calcular_unlocak_frm" id="calcular_unlocak_frm" method="post" action="">
            <input type="hidden" name="current_calculator" id="current_calculator" value="<?php echo $current_calculator;?>">
			<input type="hidden" name="calculator_password_id" id="calculator_password_id" value="<?php echo $calculator_password_id?>">
            <div>
              <div><img src="<?php echo IMAGE_URL; ?>/round.png"></div>
              <div>
                <h1>VIDEO BLOQUEADO</h1>
              </div>
              <div>Para desbloquear el siguiente paso, <a href="<?php echo MODULE_URL ; ?>/program/index.php?program=7 ">debes de ir al video</a><br>
                anterior e introducir la contraseña indicada aqui.</div>
              <div>
                <input type="password" required name="calculator_password" id="calculator_password">
                <img class="pop-arrow" src="<?php echo IMAGE_URL; ?>/pop-arrow.png">
				<span id="error_msg"><?php echo $_SESSION['ERROR_MSG']; $_SESSION['ERROR_MSG']='';?></span>
				<img src="<?php echo IMAGE_URL; ?>/small_loading.gif" id="small_loading" style="display:none"> </div>
              <div>
				<input type="hidden" name="action" value="submit_password" />
                <input type="submit" name="desbloquear" value="Desbloquear">
              </div>
            </div>
          </form>
      </div>
    </div>
    <!-- Password section end -->
    <div class="row-fluid">
    	<div class="span12 heading">
	<?php if($current_page=='calculator'){ ?>
		<h3 class="calculator_head">SITUACIÓN</h3>
	<?php } ?>
        <?php if($current_page=='calculator2'){ ?>
		<h3 class="calculator_head2">METAS</h3>
	<?php } ?>
	<?php if($current_page=='calculator3'){ ?>
		<h3 class="calculator_head3">ACTIVACIÓN RETICULAR</h3>
	<?php } ?>
	<?php if($current_page=='calculator4'){ ?>
		<h3 class="calculator_head4">REGLAS</h3>
	<?php } ?>
	<?php if($current_page=='calculator5'){ ?>
		<h3 class="calculator_head5">TARGET</h3>
	<?php } ?>
	
			
            <div class="video-a"> <a href="#video_tutorial" data-toggle="modal" >Ver tutorial</a> <img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> </div> 
        </div>
    </div>  
  </div>
</span>  
  <span class="show_metter" style="display:none;" >
    <div class="boxes round round1 iunvitar" >
    <div class="row-fluid">
       <div class="span12 heading text-center" style="width:100%">
	    <table class="text-center" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" class="span10 text-center"><img src="<?php echo IMAGE_URL; ?>/logo.png"></td>
			</tr>
			<tr>
				<td align="center" class="span12 heading">
				Conevta y comparte tus metas con otros Coiners. Descubre nuevas aventuras y<br>conoce nuevas personas que comparten los mismos intereses que tu.</td>
			</tr>
	    </table>
	   </div>    
    </div>      
  </div>
 </span>  
  
<?php } ?>
<?php  if($current_pageName == 'Mis Ganancias'){ ?>
  <div class="boxes round round1">
    <div class="row-fluid">
	  <div class="span1"></div>
      <div class="span10 heading">
	  <?php if(isset($_GET) && $_GET['event'] =='mis_prospectos') { 
	  echo 'En esta sección puedes revisar cuantas personas se interesaron<br/> En Fondear tu Sueño y/o crear el suyo. Ayúdalos a tomar el gran paso.<br/>Envíales un correo e inspirarlos a cumplir sus Sueños.'; } elseif(isset($_GET) && $_GET['event'] =='mis_pagos'){
		  echo 'En esta sección puedes revisar las estadísticas de como se ha ido<br/>Fondeando tu Sueño';
	  } else{ ?>
	  En esta sección puedes revisar cuanto llevas acumulado y quién fue <br>la persona que te ayudó a fondear tus Sueños.
	  <?php } ?></div>
	  <div class="span1"></div>
    </div>
    <div class="row-fluid PaddT101">
		<div class="span3 text-right">
			<div class="input-prepend irf">
			</div>
		</div>
      <div class="span4 text-right video-a"> <a href="#video_tutorial" data-toggle="modal" >Ver tutorial</a> <img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> </div>
    </div>
  </div>
<?php } ?>
<?php  if($current_pageName == 'payment'){ ?>
  <div class="boxes round round1">
    <div class="row-fluid">
     <div class="span7 text-right"><h4>FORMA DE PAGO</h4></div>
	 <!-- <div class="span5 text-right video-a"> <a style="color:#222;" href="#video_tutorial" data-toggle="modal" >Ver tutorial</a> <img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> </div> -->
    </div>
    
  </div>
<?php } ?>

<!--<div id="video_tutorial" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="add_goal_form_common" action="<?php // echo MODULE_URL ; ?>/home/code/goal_code.php" method="post" name="goal form" enctype="multipart/form-data"   >
    <div class="modal-header">
     <a href="javascript:void(0)" class="close video_close" data-dismiss="modal" aria-hidden="true" > <img class="" src="<?php echo IMAGE_URL?>/close_video.png" /> </a>
    </div>
    <div class="modal-body">
    <?php // echo $var_tutorial_result['video'];?>
    </div>
  </form>
</div>-->

<div class="fadediv">
  <div class="popup_div">
    <img src="<?php echo IMAGE_URL?>/close_video.png" alt="close" class="close_img"  onclick=" jQuery('.fadediv').hide();" />
  	<div class="popup_body"><?php echo $var_tutorial_result['video'];?></div> 
  </div>
</div>

<script>
function printContent(el){
	 jQuery('.hide_metter').hide();
	 jQuery('.show_metter').show();
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
	setTimeout(function(){ jQuery('.hide_metter').show();
  jQuery('.show_metter').hide(); }, 50);
}
$(window).resize(function(){
  jQuery('.hide_metter').show();
  jQuery('.show_metter').hide();
});
</script>

<script>
$( document ).ready(function() {
   $(".video-a").click(function(){
    $(".fadediv").show();
});
$(".close_img").click(function(){
    $(".fadediv").fadeOut();
});
});

</script>
