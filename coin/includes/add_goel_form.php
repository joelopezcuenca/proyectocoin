<?php
include('headercode.php') ;
include('code/navigation_code.php') ;
$page_name=getMyPageName(); //for getting page Name called a function 
?>
<?php
if($payment_page_url=='dopayment.php'){
	$lock_it = 1 ;
	$payment_page_url=MODULE_URL.'/home/dopayment.php';
}
if($payment_page_url=='prouser.php'){
	$lock_it = 0 ;
	$payment_page_url=MODULE_URL.'/payment/php_files/prouser.php';
}

?>

<div id="goal_categories" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="add_goal_form_common" action="<?php echo MODULE_URL ; ?>/home/code/goal_code.php" method="post" name="goal form" enctype="multipart/form-data"   >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
    </div>
    <div class="modal-body">
      <h1 id="myModalLabel"><!--<i class="fa fa-cog"></i>--> ¿Qué Sueño quieres Cumplir?</h1>
      <h4 id="myModalLabel" class="text-center gray">Se lo más detallado posible</h4>
      <div>
        <div class="select_file">
          <input class="validate[required]" data-prompt-position="bottomRight:-100" placeholder="Titulo de tu Sueño"  name="goal_name" type="text">
        </div>
        <div class="select_file">
          <select class="validate[required]" name="category_id" id="category_id" style="width:100%;" data-prompt-position="bottomRight:-100">
            <option value="" >Selecciona una categoría</option>
            <?php foreach($allrecord_category as $categories){ ?>
            <option value="<?php echo $categories['id']; ?>"><?php echo $categories['title']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="inp select_file">
          <textarea rows="5" class="validate[required]" data-prompt-position="bottomRight:-100" placeholder="¿Qué te inspiro a querer lograr este sueño?"  name="goal_description" id="goal_description"></textarea>
        </div>
        <div>
          <div class="name"> <span style="color: red;"></span>¿Qué tan feliz te haría?: </div>
          <div class="select_file starpop">
            <?php $ttlrating=5; ?>
            <div id="predefined-star"  data-score="<?php echo round($ttlrating/0); ?>" ></div>
            <input type="hidden" name="ratting" id="rattingscore" value="">
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="PaddT20">
        <div class="row-fluid">
          <div class="span5">
            <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancelar</button>
          </div>
          <div class="span7">
            <div class="row-fluid text-right">
              <div class="span7">
                <button type="button" data-toggle="modal" data-target="#des-experiance" onclick="markAsComplete()" class="btn btn-default">
                <input type="checkbox" checked="checked">
                Marcar como Logrado
                </button>
              </div>
              <div class="span5">
                <button class="btn btn-success" style="font-weight: bold;"> + Agregar Sueño</button>
              </div>
            </div>
          </div>
        </div>
        
        <!--<div class="fr">
                  <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                    <label class="onoffswitch-label" for="myonoffswitch">
                      <span class="onoffswitch-inner"></span>
                      <span class="onoffswitch-switch"></span>
                      </label>
                    </div>
                  </div>-->
        <input name="action" type="hidden" value="add_goal" />
        <input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/>
      </div>
    </div>
  </form>
</div>

<!-- Modal -->
<div id="des-experiance" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <div class="row-fluid">
      <div class="span8">
        <form action="#" class="dropzone" id="my-dropzone">
          <div class="fallback">
            <input name="file" type="file" multiple />
          </div>
		  <div id="error_msg_imge" style="color:RED; display:none">*Tienes que subir por lo menos una foto.</div>
        </form>
      </div>
      <div class="span4">
        <h5>Describe tu experiencia</h5>
        <form name="complete_goal" id="complete_goal" action="<?php echo DEFAULT_URL.'/modules/home/code/goal_code.php'; ?>" method="post">
		<input type="hidden" id="userid_image" value="<?php echo $_SESSION['AD_user_id'];?>" /> 
		<input type="hidden" id="site_url" value="<?php echo DEFAULT_URL.'/modules/home/ajax/';?>" /> 
		<input type="hidden" name="unlock_files" id="upload_image_arr" value="" /> 
		<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id'];?>" />		
		<input name="action" type="hidden" value="unlock_goal">
          <fieldset>
		     <input type="text" name="short_des" id="short_des" placeholder="Where did you unlock this goal ?">
            <textarea name="goal_full_description" id="goal_full_description" rows="5" placeholder="How was your experience?How did you do it?"></textarea>
         
           <!-- <input type="date" name="date" placeholder="Date"> -->
           <!-- <input type="text" name="youtube"  placeholder="Youtube URL"> -->
            <!-- <p><strong>Share on:</strong></p> -->
            <div class="row-fluid">
              <!-- <div class="span4"> <a target="_blank" href="#"><img src="<?php echo IMAGE_URL;?>/facebook.png"></a> <a target="_blank" href="#" class="twitter-share-button"><img src="<?php echo IMAGE_URL;?>/twit.png"></a> </div>-->
			  
              <!-- <div class="span6 text-right"><div class="onoffswitch">
    				<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
    				<label class="onoffswitch-label" for="myonoffswitch">
    				<span class="onoffswitch-inner"></span>
    				<span class="onoffswitch-switch"></span>
    				</label>
    			</div></div> -->
            </div>
            <button type="button" class="btn btn-warning MarT201" onclick="return complete_goal_form()"><i class="icon-ok"></i> Desbloquear Logro</button>
          </fieldset>
		  <input type="hidden" name="complete_goal_title" id="complete_goal_title" value="" />
		   <input type="hidden" name="complete_goal_category" id="complete_goal_category" value="" />
		    <input type="hidden" name="complete_goal_description" id="complete_goal_description" value="" />
			 <input type="hidden" name="complete_goal_periority" id="complete_goal_periority" value="" />
			 <input type="hidden" name="bucket_id" id="bucket_id" value="" />
        </form>
      </div>
    </div>
    <link href="<?php echo CSS_URL; ?>/dropzone.css" type="text/css" rel="stylesheet" />
    <script src="<?php echo JS_URL; ?>/dropzone.js"></script> 
    <script>
		jQuery(document).ready(function(){
			jQuery("#add_goal_form_common").validationEngine();
		});

		Dropzone.options.myAwesomeDropzone = {
				addRemoveLinks: true,
				dictCancelUploadConfirmation: true
		}
		
	

	
		Dropzone.options.myDropzone = {
			init: function() {
			  this.on("addedfile", function(file) {
				var removeButton = Dropzone.createElement("<span><i class='icon-remove'></i></span>");
				//var removeButton = Dropzone.createElement("<button>Remove file</button>");
				var _this = this;
				removeButton.addEventListener("click", function(e) {
				  e.preventDefault();
				  e.stopPropagation();
				  _this.removeFile(file);
				  
				  /* var file_path = <?php echo SITE_ROOT.'/goal_img/';?>+file;
					file_path.remove(); */
				});
				file.previewElement.appendChild(removeButton);
			  });
			}
		  };		
		
		function  markAsComplete(){
			var complete_goal_title_val = jQuery('#form-validation-field-0').val();
			var complete_goal_description_val = jQuery('#goal_description').val();
			jQuery('#short_des').val(complete_goal_title_val);
			jQuery('#goal_full_description').val(complete_goal_description_val);
		}
		function complete_goal_form()
		{
			var complete_goal_title_val = jQuery('#form-validation-field-0').val();
			var complete_goal_category_val = jQuery('#category_id').val();
			var complete_goal_description_val = jQuery('#goal_description').val();
			var complete_goal_rating_val = jQuery('#rattingscore').val();
			
			if(complete_goal_title_val==''){
			  alert('Please insert goal title.');
				return false;
			}
			if(complete_goal_category_val==''){
			  alert('Please select category.');
				return false;
			}
			if(complete_goal_description_val==''){
			  alert('Please insert goal description.');
				return false;
			}
			
			jQuery('#complete_goal_title').val(complete_goal_title_val);
			jQuery('#complete_goal_category').val(complete_goal_category_val);
			jQuery('#complete_goal_description').val(complete_goal_description_val);
			jQuery('#complete_goal_periority').val(complete_goal_rating_val);
			
			var selectedImg = jQuery('#upload_image_arr').val();
			var goal_full_description_val = jQuery('#goal_full_description').val();
			if(selectedImg==''){
			  jQuery('#error_msg_imge').show();
				return false;
			}
			if(goal_full_description_val==''){
			  alert('Please insert description.');
			  jQuery('#goal_full_description').focus();
				return false;
			}
			jQuery('#complete_goal').submit();
		}
		</script> 
		<script src="<?php echo DEFAULT_URL; ?>/js/jquery.raty.min.js"></script>
		<script>
$(document).ready(function() {
 	$('#predefined-star').raty({
		path: '<?php echo DEFAULT_URL; ?>/img',
		half: true,
		targetKeep: true,
		click: function(score, evt) {
			$('#rattingscore').val(score)
		},
		score: function()
		{     return $(this).attr('data-score');
		}
	}); 
	 $('.predefined').raty({
		path: '<?php echo DEFAULT_URL; ?>/img',
		half: true,
		readOnly:  true,
		targetKeep: true,
		score: function()
		{     return $(this).attr('data-score');
		}
	}); 
	setInterval(oprationFunc,4000);
		function oprationFunc() {
			$('.sussMsgSet').fadeOut();
		}
});
</script>
  </div>
</div>
<?php
if (!empty($_FILES)) {
	//echo "<pre>"; print_r($_FILES); die;
    $tempFile = $_FILES['file']['tmp_name']; 
    $targetPath = SITE_ROOT.'/goal_img/'; 
    $targetFile =  $targetPath.$_SESSION['AD_user_id'].$_FILES['file']['name']; 
    move_uploaded_file($tempFile,$targetFile);
	
	//echo "<pre>"; print_r($_SESSION['images']);	
}

 ?>