<!-- Here Main Navigation Inside Home Page is Defined Here --> 
<!-- Navigation On Home Pages --> 

<?php
include('code/navigation_code.php') ;

?>
<?php $page_name=getMyPageName(); //for getting page Name called a function ?>

<?php if($payment_page_url=='dopayment.php')
{
	$lock_it = 1 ;
	$payment_page_url=MODULE_URL.'/home/dopayment.php';
}
if($payment_page_url=='prouser.php')
	{
		$lock_it = 0 ;
		$payment_page_url=MODULE_URL.'/payment/php_files/prouser.php';
	}

?>
<div class="nav-collapse1">
  <div class="navbar">
    <div class="navbar-inner">
      <ul class="nav">
        <li <?php if(($page_name=='home.php')||($page_name=='mi_prefil.php') || ($page_name=='digitel_passport.php') ||($page_name=='bucket_list.php')||($page_name=='leaderboard.php')){?>class="active"<?php } ?>><a href="<?php echo MODULE_URL.'/home/home.php';?>">Inicio</a></li>
        
        <?php /*?><li <?php if(($page_name=='missions.php')||($page_name=='share_the_idea.php')||($page_name=='howothersdoit.php')){?>class="active"<?php } ?>>
        <a href="<?php echo MODULE_URL.'/home/missions.php';?>">Misiones</a></li><?php */?>
        
        <li class="dropdown" <?php if(($page_name=='missions.php')||($page_name=='share_the_idea.php')||($page_name=='howothersdoit.php')){?>class="active"<?php } ?>>
        <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo MODULE_URL.'/home/missions.php';?>">Programas</a>
			<ul class="dropdown-menu">
			<?php $main = 1; ?>
			<?php foreach($allrecord_lession as $programs){ ?>
			<?php if($main == '1'){ ?>
				<li>
					<a href="<?php echo MODULE_URL.'/program/index.php?program='.$programs['id'];?>"><?php echo ucfirst($programs['title']); ?></a>
				</li>
			<?php }else{ ?>	
				<li>
					<a href="javascript:void(0);"><?php echo ucfirst($programs['title']); ?></a>
				</li>
			<?php } ?>
			<?php $main++; } ?>	
			</ul>
        </li>
        
       <li <?php if(($page_name=='UserLevelEarning.php')||($page_name=='UserLevelForum.php')){?>class="active"<?php } ?>><a href="<?php echo MODULE_URL.'/home/UserLevelEarning.php';?>">Mis Coins</a></li>
        <li <?php if(($page_name=='dopayment.php')||($page_name=='prouser.php')||($page_name=='paypal_email.php')||($page_name=='calculator.php')){?>class="active"<?php } ?>>
        	<a href="<?php echo $payment_page_url;?>">Mis Ganancias&nbsp;<?php if($lock_it==1){ ?><img src="<?php echo IMAGE_URL ; ?>/lock-sm.png" alt="icon"> <?php } ?></a></li>
      </ul>
    </div>
  </div>
</div>
<div id="goal_categories" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<form id="add_goal_form" action="<?php echo MODULE_URL ; ?>/home/code/goal_code.php" method="post" name="goal form" enctype="multipart/form-data" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					x
				</button> 
				<h3 id="myModalLabel">What Inspired You?</h3>
			</div>
			<div class="modal-body">
				<div class="popup">
					<h5 id="myModalLabel">What inspired you to pursue this goal?</h5>
					<div class="select_file">
						<input class="validate[required]" data-prompt-position="bottomRight:-50" placeholder="Title of your goal"  name="goal_name" type="text">
					</div>
					<div class="inp select_file">
						<textarea class="" data-prompt-position="bottomRight:-50" placeholder="What inspired you to pursue this goal?"  name="goal_description"></textarea>
					</div>
					<div class="select_file">
						<div class="select_file">
							<select class="validate[required]" name="category_id">
								<option value="">Please select a category</option>
								<?php foreach($allrecord_category as $categories){ ?>
								<option value="<?php echo $categories['id']; ?>"><?php echo $categories['title']; ?></option>
								<?php } ?>	
							</select>
						</div>
					</div>
					<div class="name">
						<span style="color: red;"></span>Priority:
					</div>					
					<div class="select_file">
					<div class="select_file">
						<?php $ttlrating=5; ?>
						<div id="predefined-star"  data-score="<?php echo round($ttlrating/0); ?>" ></div> 
						<input type="hidden" name="ratting" id="rattingscore" value="">
					</div>					
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">
				cancel
				</button>

				<div class="fr"><button class="btn btn-success">Guardar en mi Lista</button></div>
                <div class="fr">
					<div class="onoffswitch">
						<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
						<label class="onoffswitch-label" for="myonoffswitch">
						<span class="onoffswitch-inner"></span>
						<span class="onoffswitch-switch"></span>
						</label>
					</div>
				</div>
				<input name="action" type="hidden" value="add_goal" />
				<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/> 
			</div>	
		</form>
</div>