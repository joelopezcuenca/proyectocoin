<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Coin</title>
		<link href="<?php echo CSS_URL; ?>/style.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/font-awesome.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/bootstrap.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/bootstrap-responsive.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/jquery.mCustomScrollbar.css" rel="stylesheet">
	     <link href="<?php echo CSS_URL; ?>/colorbox.css" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo IMAGE_URL; ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo IMAGE_URL; ?>/favicon.ico" type="image/x-icon">
		<!-- <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> -->
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery-1.8.3.min.js"></script>
		<link href="<?php echo CSS_URL; ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/bootstrap.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine-en.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/query.countdown.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/HomePageAjax_Jquery_functions.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_LeadersboardPage_functions.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_DiscussionForumPage_function.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_bucketlistPage_function.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/GeneralDiscussion_function.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/countdown.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.flexslider.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.colorbox.js"></script> 
        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/coin.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/calculator/calculator.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/CustomFunction/generalfunction.js"></script>
		 <!-- <script src="http://connect.facebook.net/en_US/all.js"></script> -->
</head>
<body>
