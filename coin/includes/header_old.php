<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Coin</title>
		<meta property="og:image" content="http://www.comuniadcoin.com/coin/images/coin_fb_image.jpg" />
		<meta property="og:description" content="Descubre como Vivir una Vida llena de Libertad, Diversión y Aventura. Únete al movimiento! " />
		<link href="<?php echo CSS_URL; ?>/style.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/font-awesome.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/bootstrap.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/bootstrap-responsive.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/bootstrap-responsive.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/jquery.bxslider.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/jquery.mCustomScrollbar.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/jcarousel.basic.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/flexslider.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/colorbox.css" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo IMAGE_URL; ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo IMAGE_URL; ?>/favicon.ico" type="image/x-icon">
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery-1.8.3.min.js"></script>
		<link href="<?php echo CSS_URL; ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/bootstrap.js"></script>

		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine-en.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/query.countdown.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/HomePageAjax_Jquery_functions.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_LeadersboardPage_functions.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_DiscussionForumPage_function.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_bucketlistPage_function.js"></script> 
		
		
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/countdown.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.flexslider.js"></script> 
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.colorbox.js"></script> 
	</head>
	<body>
		<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] ||
				function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o), m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-48279747-1', 'comunidadcoin.com');
			ga('send', 'pageview');

		</script>

