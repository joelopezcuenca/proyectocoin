<?php
include(MODULE_PATH . "/home/code/account_setting_code.php") ;
include(INCLUDE_PATH . "/user_level_define.php") ;
?>
<div class="boxes round">
	<?php if($userdetails['profile_pic'] != ''){ ?>
		<div class="profilediv">
			<img class="profile-img" src="<?php echo IMAGE_ADMIN_PROFILE_URL."/".$userdetails['profile_pic']; ?>">
			<?php ?> </div>
	<?php }else{ ?>
		<div style="position:relative;"><img class="profile-img" src="<?php echo IMAGE_URL; ?>/adminuser.png">
			</div>		
	<?php } ?>
	<h4><?php echo $userdetails['name'] ; ?></h4>
	<!--<div id="addphoto">
		<a data-toggle="modal" role="button" href="#edit_profile">Cambiar foto</a>
	</div>-->
	<div id="edit_profile" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">Cambiar foto</h3>
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>
		<form id="updatephoto" action="<?php echo MODULE_URL ; ?>/home/code/account_setting_code.php" method="post" name="f1" enctype="multipart/form-data" >
			<div class="modal-body">
				<div class="popup">
					<div class="name">
						Subir imagen :
					</div>
					<div class="select_file">
						<input class="validate[required]" name="file" type="file">
					</div>					
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success">Guardar cambios</button>
				<input name="action" type="hidden" value="updatephotoforuser" />
				<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/> 
		</form>
		<? /*
			 * End of Modal
			 */
		?>
	</div>
</div>
<?php
/* This code use for profile progress start */
$profile_fields = 0;
if(!empty($allUserDetails['profile_pic'])){
	$profile_fields +=	1;
}
if(!empty($allUserDetails['bio'])){
	$profile_fields +=	1;
}

if($totalGoal>0){
	$profile_fields +=	1;
}
 /* if(!empty($calculationResult)){ 
	$profile_fields +=	1;
 } */
 
if(!empty($userDetailresult)) {
	$profile_fields +=	1;
}

$width = $profile_fields * 25;
/* This code use for profile progress end */
?>

<div>


<?php

//echo 'Width: '.$width;

 if($width<100) { ?>
<div class="row-fluid">
  <div class="span8 text-left">Completa tu perfil</div>
  <div class="span4 text-right red"><?php echo $width; ?>%</div>
  <div class="clearfix"></div>
</div>

<div class="progress">
  <div class="bar" style="width: <?php echo $width; ?>%;"></div>
</div>

<div>
<div class="row-fluid">
  <div class="span2 text-left">
	<?php if(!empty($allUserDetails['profile_pic'])){ ?>
		<img src="<?php echo IMAGE_URL; ?>/check-circle-green-16.png">
	<?php }else{ ?>
		<img src="<?php echo IMAGE_URL; ?>/check-circle-gray-16.png">
	<?php } ?>
  </div>
	<div class="span10 text-left MarL0">
		<a href="<?php echo MODULE_URL.'/home/account-setting.php';?>">
			Ponle cara a tu perfil
		</a>
	</div>
  <div class="clearfix"></div>
</div>
<div class="row-fluid MarT2">
	<div class="span2 text-left">
		<?php if(!empty($allUserDetails['bio'])){ ?>
			<img src="<?php echo IMAGE_URL; ?>/check-circle-green-16.png">
		<?php }else{ ?>
			<img src="<?php echo IMAGE_URL; ?>/check-circle-gray-16.png">
		<?php } ?>
	</div>
  <div class="span10 text-left MarL0">
		<a href="<?php echo MODULE_URL.'/home/account-setting.php';?>">
			Cuéntanos de ti
		</a>
  </div>
  <div class="clearfix"></div>
</div>
<!--<div class="row-fluid MarT2">
	<div class="span2 text-left">
	 <?php if(!empty($calculationResult)){ ?>
		<img src="<?php echo IMAGE_URL; ?>/check-circle-green-16.png">
	 <?php } else{?>
		<img src="<?php echo IMAGE_URL; ?>/check-circle-gray-16.png">
	 <?php } ?>
	</div>
  <div class="span10 text-left MarL0">
	<a href="<?php echo MODULE_URL.'/affiliates/calculator.php?event=calculator';?>">
		Editar tu Proyecto
	</a>
	</div>
  <div class="clearfix"></div>
</div> -->

<div class="row-fluid MarT2">
	<div class="span2 text-left">
		<?php if($totalGoal>0){ ?>
			<img src="<?php echo IMAGE_URL; ?>/check-circle-green-16.png">
		<?php }else{ ?>
			<img src="<?php echo IMAGE_URL; ?>/check-circle-gray-16.png">
		<?php } ?>
	</div>
  <div class="span10 text-left MarL0">
	<a href="#goal_categories" data-toggle="modal">
		Escribe tu primer sueño
	</a>	
	</div>
  <div class="clearfix"></div>
</div>
<div class="row-fluid MarT2">
	<div class="span2 text-left">
	<?php if(!empty($userDetailresult)) { ?>
		<img src="<?php echo IMAGE_URL; ?>/check-circle-green-16.png">
	<?php } else{ ?>
		<img src="<?php echo IMAGE_URL; ?>/check-circle-gray-16.png">
	<?php } ?>	
	</div>
  <div class="span10 text-left MarL0">
	<a href="<?php echo MODULE_URL.'/mis_ganancias/preferencias.php?event=preferencias';?>">
		Información de pago
	</a>
	</div>
  <div class="clearfix"></div>
</div>
</div>
<?php } ?>
</div>
</div>
<div class="boxes MarT5">
	<?php $pageName = explode("/", $_SERVER['REQUEST_URI']);

	?>
	 <ul class="tutorial">
		
		<a href="<?php echo MODULE_URL ; ?>/home/leaderboard.php">
		<li <?php if(in_array("leaderboard.php", $pageName)){ ?>class="active"<?php } ?>>
			Mi Comunidad
		</li></a>
		
		
		
		<a href="<?php echo MODULE_URL ; ?>/home/home.php">
		<li <?php if(in_array("home.php", $pageName) || $current_pageName == 'calculator'){ ?>class="active"<?php } ?>>
			Mi Sueño #1
		</li></a>
		
		<a href="<?php echo MODULE_URL ; ?>/home/bucket_list.php">
		<li <?php if(in_array("bucket_list.php", $pageName)){ ?>class="active"<?php } ?>>
			Mi Lista</li></a>
		<!-- <a href="<?php echo MODULE_URL ; ?>/home/digitel_passport.php">
		<li <?php if(in_array("digitel_passport.php", $pageName)){ ?>class="active"<?php } ?>>
			Logros
		</li></a> -->
		<a href="<?php echo MODULE_URL ; ?>/home/mi_prefil.php">
		<li <?php if(in_array("mi_prefil.php", $pageName)){ ?>class="active"<?php } ?>>
			Mi Perfil</li></a>
		
		</ul>
			<div class="cls"></div>
			<?php /*?><ul>
				<?php if($_SERVER["REQUEST_URI"]=="/coin/modules/home/dopayment.php"){ ?> 
				<a href="#tutorial_video1" data-toggle="modal">
					<?php }else{ ?>
					<a href="#tutorial_video" data-toggle="modal">	
						<?php } ?>
		<img src="<?php echo IMAGE_URL ; ?>/tutorialvideo.png">
		</a>
				</ul><?php */?>
</div>
<div id="tutorial_video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">					      
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">&nbsp;</h3>
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>		
			<div class="modal-body">
				<div class="popup">
					<div>
					<iframe src="//fast.wistia.net/embed/iframe/zq0xdcrp7i" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="360"></iframe>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
	</div>
</div>
<div id="tutorial_video1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			
		      
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">&nbsp;</h3>
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>		
			<div class="modal-body">
				<div class="popup">
					<div>
					<iframe src="//fast.wistia.net/embed/iframe/ik26kpyvw4" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="360"></iframe>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">				
	</div>
</div>
<script type="text/javascript">
	function submitform()
	{
	  $('#updatephoto').submit();
      }
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#updatephoto").validationEngine('attach', {promptPosition : "centerRight", scroll: false});
	});
</script>
