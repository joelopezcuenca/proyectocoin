<!-- Here Main Navigation Inside Home Page is Defined Here -->
<!-- Here Main Navigation Inside Home Page is Defined Here -->
<!-- Navigation On Home Pages -->

<?php
include('code/navigation_code.php') ;
$page_name=getMyPageName(); //for getting page Name called a function 
?>
<?php
if($payment_page_url=='dopayment.php'){
	$lock_it = 1 ;
	$payment_page_url=MODULE_URL.'/home/dopayment.php';
}
if($payment_page_url=='prouser.php'){
	$lock_it = 0 ;
	$payment_page_url=MODULE_URL.'/payment/php_files/prouser.php';
}

?>

<div class="nav-collapse1">
  <div class="navbar">
    <div class="navbar-inner">
      <ul class="nav">
        <li <?php if(($page_name=='home.php')||($page_name=='mi_prefil.php') || ($page_name=='digitel_passport.php') ||($page_name=='bucket_list.php') ||($page_name=='profilefollow.php')||($page_name=='leaderboard.php') || ($current_pageName=="calculator") || ($page_name=="account-setting.php") ){?>class="active"<?php } ?>><a href="<?php echo MODULE_URL.'/home/home.php';?>">Inicio</a></li>
      
        <li class="dropdown <?php if(($pageName=="program1")){ echo "active"; } ?>" > <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo MODULE_URL.'/home/missions.php';?>">Programas</a>
          <ul class="dropdown-menu">
            <?php $main = 1; ?>
            <?php foreach($allrecord_lession as $programs){ ?>
            <?php if($main == '1'){ ?>
            <li> <a href="<?php echo MODULE_URL.'/program/index.php?program='.$programs['id'];?>"><img src="<?php echo $programs['image']?>" /> <?php echo ucfirst($programs['title']); ?></a> </li>
            <?php }else{ ?>
            <li><a href="<?php echo MODULE_URL.'/program/index.php?program='.$programs['id'].'&main_url='.$main;?>"><img src="<?php echo $programs['image']?>" /> <?php echo ucfirst($programs['title']); ?> <i class="fa fa-lock"></i></a>  </li>
            <?php } ?>
            <?php $main++; } ?>
          </ul>
        </li>
     
        <li <?php if(($current_pageName=="Afiliados"  || $current_pageName=="share_video" || $current_pageName=="tutoriale" || $current_pageName=="Link" )){ echo "class='active'"; } ?>> <a href="<?php echo MODULE_URL.'/affiliates/index.php?event=amigos';?>">Herramientas</a> </li>
		
        <li <?php if(($current_pageName=='Mis Ganancias' || $current_pageName=='payment')){?>class="active"<?php } ?>> <a href="<?php echo MODULE_URL.'/mis_ganancias/index.php?event=mis_ganancias';?>"> Fondeo&nbsp;
          <?php if($lock_it==1){ ?>
          <!-- <img src="<?php echo IMAGE_URL ; ?>/lock-sm.png" alt="icon"> -->
          <?php } ?>
          </a> </li>
      </ul>
    </div>
  </div>
</div>
<div id="goal_categories" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="add_goal_form_common" action="<?php echo MODULE_URL ; ?>/home/code/goal_code.php" method="post" name="goal form" enctype="multipart/form-data"   >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
    </div>
    <div class="modal-body">
      <h1 id="myModalLabel"><!--<i class="fa fa-cog"></i>--> ¿Qué Sueño quieres Cumplir?</h1>
      <h4 id="myModalLabel" class="text-center gray">Se lo más detallado posible</h4>
      <div>
        <div class="select_file">
          <input class="validate[required]" data-prompt-position="bottomRight:-100" placeholder="Titulo de tu Sueño"  name="goal_name" type="text">
        </div>
        <div class="select_file">
          <select class="validate[required]" name="category_id" id="category_id" style="width:100%;" data-prompt-position="bottomRight:-100">
            <option value="" >Selecciona una categoría</option>
            <?php foreach($allrecord_category as $categories){ ?>
            <option value="<?php echo $categories['id']; ?>"><?php echo $categories['title']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="inp select_file">
          <textarea rows="5" class="validate[required]" data-prompt-position="bottomRight:-100" placeholder="¿Qué te inspiro a querer lograr este sueño?"  name="goal_description" id="goal_description"></textarea>
        </div>
		
		
		<div class="select_file">
          <input type="file" name="incomplete_goel_image"  class="validate[required]" id="incomplete_goel_image" data-prompt-position="bottomRight:-100"   />
        </div>
		<div class="select_file">&nbsp;</div>
		
		
        <div>
          <div class="name"> <span style="color: red;"></span>¿Qué tan feliz te haría?: </div>
          <div class="select_file starpop">
            <?php $ttlrating=5; ?>
            <div id="predefined-star"  data-score="<?php echo round($ttlrating/0); ?>" ></div>
            <input type="hidden" name="ratting" id="rattingscore" value="">
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="PaddT20">
        <div class="row-fluid">
          <div class="span5">
            <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancelar</button>
          </div>
          <div class="span7">
            <div class="row-fluid text-right">
              <div class="span7">
                <button type="button" data-toggle="modal" data-target="#des-experiance" onclick="markAsComplete()" class="btn btn-default">
                <input type="checkbox" checked="checked">
                Marcar como Logrado
                </button>
              </div>
              <div class="span5">
                <button class="btn btn-success" style="font-weight: bold;">+ Agregar Sueño</button>
              </div>
            </div>
          </div>
        </div>
        
        <!--<div class="fr">
                  <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                    <label class="onoffswitch-label" for="myonoffswitch">
                      <span class="onoffswitch-inner"></span>
                      <span class="onoffswitch-switch"></span>
                      </label>
                    </div>
                  </div>-->
        <input name="action" type="hidden" value="add_goal" />
        <input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/>
      </div>
    </div>
  </form>
</div>

<!-- Modal -->
<div id="des-experiance" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <div class="row-fluid">
      <div class="span8">
        <form action="#" class="dropzone" id="my-dropzone">
          <div class="fallback">
            <input name="file" type="file" multiple />
          </div>
		  <div id="error_msg_imge" style="color:RED; display:none">*Tienes que subir por lo menos una foto.</div>
        </form>
      </div>
      <div class="span4">
        <h5>Describe tu experiencia</h5>
        <form name="complete_goal" id="complete_goal" action="<?php echo DEFAULT_URL.'/modules/home/code/goal_code.php'; ?>" method="post">
		<input type="hidden" id="userid_image" value="<?php echo $_SESSION['AD_user_id'];?>" /> 
		<input type="hidden" id="site_url" value="<?php echo DEFAULT_URL.'/modules/home/ajax/';?>" /> 
		<input type="hidden" name="unlock_files" id="upload_image_arr" value="" /> 
		<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id'];?>" />		
		<input name="action" type="hidden" value="unlock_goal">
          <fieldset>
		     <input type="text" name="short_des" id="short_des" placeholder="Titulo de tu Sueño">
            <textarea name="goal_full_description" id="goal_full_description" rows="5" placeholder="Cuéntanos como fue…
¿Qué sentiste al lograr tu sueño?"></textarea>
         
            <!-- <input type="date" name="date" placeholder="Date">-->
            <!--<input type="text" name="youtube"  placeholder="Youtube URL">-->
            <!--<p><strong>Share on:</strong></p> -->
            <div class="row-fluid">
             <!-- <div class="span4"> <a target="_blank" href="#"><img src="<?php echo IMAGE_URL;?>/facebook.png"></a> <a target="_blank" href="#" class="twitter-share-button"><img src="<?php echo IMAGE_URL;?>/twit.png"></a> </div> -->
			  
              <!-- <div class="span6 text-right"><div class="onoffswitch">
    				<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
    				<label class="onoffswitch-label" for="myonoffswitch">
    				<span class="onoffswitch-inner"></span>
    				<span class="onoffswitch-switch"></span>
    				</label>
    			</div></div> -->
            </div>
            <button type="button" class="btn btn-warning MarT201" onclick="return complete_goal_form()"><i class="icon-ok"></i> Desbloquear Logro</button>
          </fieldset>
		  <input type="hidden" name="complete_goal_title" id="complete_goal_title" value="" />
		   <input type="hidden" name="complete_goal_category" id="complete_goal_category" value="" />
		    <input type="hidden" name="complete_goal_description" id="complete_goal_description" value="" />
			 <input type="hidden" name="complete_goal_periority" id="complete_goal_periority" value="" />
			 <input type="hidden" name="bucket_id" id="bucket_id" value="" />
        </form>
      </div>
    </div>
    <link href="<?php echo CSS_URL; ?>/dropzone.css" type="text/css" rel="stylesheet" />
    <script src="<?php echo JS_URL; ?>/dropzone.js"></script> 
    <script>
		jQuery(document).ready(function(){
			jQuery("#add_goal_form_common").validationEngine();
		});

		Dropzone.options.myAwesomeDropzone = {
				addRemoveLinks: true,
				dictCancelUploadConfirmation: true
		}
		
	

	
		Dropzone.options.myDropzone = {
			init: function() {
			  this.on("addedfile", function(file) {
				var removeButton = Dropzone.createElement("<span><i class='icon-remove'></i></span>");
				//var removeButton = Dropzone.createElement("<button>Remove file</button>");
				var _this = this;
				removeButton.addEventListener("click", function(e) {
				  e.preventDefault();
				  e.stopPropagation();
				  _this.removeFile(file);
				  
				  /* var file_path = <?php echo SITE_ROOT.'/goal_img/';?>+file;
					file_path.remove(); */
				});
				file.previewElement.appendChild(removeButton);
			  });
			}
		  };		
		
		function  markAsComplete(){
			var complete_goal_title_val = jQuery('#form-validation-field-0').val();
			var complete_goal_description_val = jQuery('#goal_description').val();
			jQuery('#short_des').val(complete_goal_title_val);
			jQuery('#goal_full_description').val(complete_goal_description_val);
		}
		function complete_goal_form()
		{
			var complete_goal_title_val = jQuery('#form-validation-field-0').val();
			var complete_goal_category_val = jQuery('#category_id').val();
			var complete_goal_description_val = jQuery('#goal_description').val();
			var complete_goal_rating_val = jQuery('#rattingscore').val();
			
			if(complete_goal_title_val==''){
			  alert('Please insert goal title.');
				return false;
			}
			if(complete_goal_category_val==''){
			  alert('Please select category.');
				return false;
			}
			/* if(complete_goal_description_val==''){
			  alert('Please insert goal description.');
				return false;
			} */
			
			jQuery('#complete_goal_title').val(complete_goal_title_val);
			jQuery('#complete_goal_category').val(complete_goal_category_val);
			jQuery('#complete_goal_description').val(complete_goal_description_val);
			jQuery('#complete_goal_periority').val(complete_goal_rating_val);
			
			var selectedImg = jQuery('#upload_image_arr').val();
			var goal_full_description_val = jQuery('#goal_full_description').val();
			if(selectedImg==''){
			  jQuery('#error_msg_imge').show();
				return false;
			}
			if(goal_full_description_val==''){
			  alert('Please insert description.');
			  jQuery('#goal_full_description').focus();
				return false;
			}
			jQuery('#complete_goal').submit();
		}
		</script> 
  </div>
</div>
<?php
if (!empty($_FILES)) {
	//echo "<pre>"; print_r($_FILES); die;
    $tempFile = $_FILES['file']['tmp_name']; 
    $targetPath = SITE_ROOT.'/goal_img/'; 
    $targetFile =  $targetPath.$_SESSION['AD_user_id'].$_FILES['file']['name']; 
    move_uploaded_file($tempFile,$targetFile);
	
	//echo "<pre>"; print_r($_SESSION['images']);	
}

 ?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5VSQRN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5VSQRN');</script>
<!-- End Google Tag Manager -->