<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Coin</title>
        <link href="<?php echo CSS_URL; ?>/style.css" rel="stylesheet">
        <link href="<?php echo CSS_URL; ?>/style_new.css" rel="stylesheet">
        <link href="<?php echo CSS_URL; ?>/font-awesome.css" rel="stylesheet">
        <link href="<?php echo CSS_URL; ?>/bootstrap.css" rel="stylesheet">
        <link href="<?php echo CSS_URL; ?>/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo CSS_URL; ?>/jquery.mCustomScrollbar.css" rel="stylesheet">


        <link rel="shortcut icon" href="<?php echo IMAGE_URL; ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo IMAGE_URL; ?>/favicon.ico" type="image/x-icon">
                <!-- <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> -->
        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery-1.8.3.min.js"></script>
        <link href="<?php echo CSS_URL; ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine.js"></script>
        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/bootstrap.js"></script>
        <!--<script type="text/javascript" src="<?php // echo JS_URL; ?>/jquery-pack.js"></script>-->
        <!--<script type="text/javascript" src="<?php // echo JS_URL; ?>/jquery.imgareaselect.min.js"></script>-->
        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine-en.js"></script>
        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo JS_URL; ?>/jquery.countdownTimer.js"></script>
        <script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/HomePageAjax_Jquery_functions.js"></script> 
        <script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_LeadersboardPage_functions.js"></script> 
        <script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_DiscussionForumPage_function.js"></script> 



        <script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/Ajax_bucketlistPage_function.js"></script> 
        <script type="text/javascript" src="<?php echo JS_URL; ?>/Ajax_js_function/GeneralDiscussion_function.js"></script> 

        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.colorbox.js"></script> 
        <script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/coin.js"></script> 
        <script type="text/javascript" src="<?php echo JS_URL; ?>/calculator/calculator.js"></script>
        <script type="text/javascript" src="<?php echo JS_URL; ?>/CustomFunction/generalfunction.js"></script>
         <!-- <script src="http://connect.facebook.net/en_US/all.js"></script> -->
		 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72193600-1', 'auto');
  ga('send', 'pageview');

</script>


<!-- Hotjar Tracking Code for http://www.proyectocoin.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:127787,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>

    </head>
    <body class="loading">
