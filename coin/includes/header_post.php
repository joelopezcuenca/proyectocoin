<?php // print_r($_SESSION);?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Coin</title>
		<meta name="description" content="Gracias a la Comunidad Coin he logrado tomar el control de mi Libertad Financiera! Hazlo tu tambien." />
		<!-- <meta property="og:image" content="http://phpdemo.internetbusinesssolutionsindia.com/coin/images/coin_fb_image.jpg" />
	 -->	
	  <meta property="og:description" content="" />
   	    <link rel="stylesheet" href="<?php echo CSS_URL; ?>/style.css">
		<link href="<?php echo CSS_URL; ?>/bootstrap.css" rel="stylesheet">
		<link href="<?php echo CSS_URL; ?>/bootstrap-responsive.css" rel="stylesheet">

		<!-- <link rel="stylesheet" href="<?php echo CSS_URL;?>/shadow.css"> -->
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/jquery-1.8.3.min.js"></script>
		<link href="<?php echo CSS_URL; ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="<?php echo JS_URL; ?>/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/jquery.validationEngine-en.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/query.countdown.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/basicfunctions.js"></script> 
	</head>
	<body>
		<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] ||
				function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o), m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-48279747-1', 'comunidadcoin.com');
			ga('send', 'pageview');

		</script>

