<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>My Organisational Demo</title>
		<style>
			#nav {position: fixed; bottom: 20px; left: 50%; margin-left: -50px;}
			#nav input {padding: 5px; font-size: 15px; cursor: pointer;}
			#chart {font-size: 10px;}
			
			.spanplus
			{
				color:#FFF;
			}
			.spanminus
			{
				color:#FFF;
				display: none;
			}
			.brand{
				
				color:#FFF;
			}
			
			.jOrgChart .node {
				
				font-weight:bold!important;
			}
		</style>
		
		
		<link rel="stylesheet" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" href="css/jquery.jOrgChart.css"/>
		<link rel="stylesheet" href="css/custom.css"/>
		<link href="css/prettify.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="prettify.js"></script>
		<!-- jQuery includes -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
		<script src="jquery.jOrgChart.js"></script>
		<script>
			jQuery(document).ready(function() {
				$("#org").jOrgChart({
					chartElement : '#chart',
					dragAndDrop : true
				});
			});
			
	$(function() {
    $('#nav input').on('click', function() {
        var scale  = parseInt($('#chart').css('font-size'),10);
            nScale = $(this).index()===0 ? scale+1 : scale-1;
        $('#chart').stop(true,true).animate({  fontSize: nScale }, {
            step: function(now,fx) {
                $(this).css('transform','scale('+parseFloat(now/10)+')');
            },
            duration: 300
        },'linear');
   });     
});

</script>
	</head>
	<body onload="prettyPrint();">
		
		<div class="topbar">
			<div class="topbar-inner">
				<div class="container">
					<h3 style="color:#FFF;text-align:center;">Dynamic Organizational structure hierarchy wise</h3>
				</div>
			</div>
		</div>
		<ul id="org" style="display:none">
			<li style="color:#fff;">
				Department
				<ul>
					<li id="beer">
						Dept1
					</li>
					<li>
						Dept2
					</li>
					<li class="fruit">
						Dept3
					</li>
					<li>
						UserId:#04000<br />
						Name :John 
						<br/>
						Position:H.O.D
						<br/>
						
						<span class="spanplus" id="1">(+)</span>
											
						<ul>
							<li>
						UserId:#04100<br />
						Name :John 
						<br/>
						Position:H.O.D
						<br/>
							<span class="spanplus" id="2">(+)</span>
							<ul>
							<li>
						UserId:#04110<br />
						Name :John 
						<br/>
						Position:H.O.D
						<br/>
									</li>
									<li>
										Unit2
									</li>
									<li>
										Unit3
									</li>
								</ul>
							</li>
							<li>
						UserId:#04200<br />
						Name :Joe 
						<br>
						Position:S.O.D
						<br>
							</li>
							<li>
						UserId:#04300<br />
						Name :Joe 
						<br>
						Position:S.O.D
						<br>
								<ul>
									<li>
										Unit1
									</li>
									<li>
										Unit2
									</li>
									<li>
						UserId:#04330<br/>
						Name:Steyln 
						<br>
						Position:M.O.D
						<br>
						
									</li>
								</ul>
								<span class="spanplus" href="javascript:void(0);" id="3">(+)</span>
								<span class="spanminus" href="javascript:void(0);" id="3">(-)</span>
							</li>
						</ul>
					</li>
				</ul>
				
			</li>
		</ul>
		<div id="nav">
    <input type="button" value="Zoom in" />
    <input type="button" value="Zoom out" />
</div>
		<div id="chart" class="orgChart">
		</div>
	</body>
</html>