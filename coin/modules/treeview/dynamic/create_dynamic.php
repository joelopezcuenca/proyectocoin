<html>
	<head>
		<style>
.myButton {
	-moz-box-shadow: 0px 1px 0px 0px #1c1b18;
	-webkit-box-shadow: 0px 1px 0px 0px #1c1b18;
	box-shadow: 0px 1px 0px 0px #1c1b18;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #eae0c2), color-stop(1, #ccc2a6));
	background:-moz-linear-gradient(top, #eae0c2 5%, #ccc2a6 100%);
	background:-webkit-linear-gradient(top, #eae0c2 5%, #ccc2a6 100%);
	background:-o-linear-gradient(top, #eae0c2 5%, #ccc2a6 100%);
	background:-ms-linear-gradient(top, #eae0c2 5%, #ccc2a6 100%);
	background:linear-gradient(to bottom, #eae0c2 5%, #ccc2a6 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#eae0c2', endColorstr='#ccc2a6',GradientType=0);
	background-color:#eae0c2;
	-moz-border-radius:15px;
	-webkit-border-radius:15px;
	border-radius:15px;
	border:2px solid #333029;
	display:inline-block;
	cursor:pointer;
	color:#505739;
	font-family:arial;
	font-size:14px;
	font-weight:bold;
	padding:12px 16px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ccc2a6), color-stop(1, #eae0c2));
	background:-moz-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-webkit-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-o-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-ms-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:linear-gradient(to bottom, #ccc2a6 5%, #eae0c2 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ccc2a6', endColorstr='#eae0c2',GradientType=0);
	background-color:#ccc2a6;
}
.myButton:active {
	position:relative;
	top:1px;
}
</style>
		
	</head>
<title>Create Dynamic Structure</title>
</head>
<body style="background:#CCC;">
<div style="text-align: center;">
	<h1 style="color:green;background: #DDD3B5;"><u>Dynamic Organizational structure</u></h1>
<form action="create_dynamic_ready.php">
	
	<div class="widtha">Department ID</div> 
	<div class="widthal">#0<input type="text" name="department_id" placeholder="Only single digit"/></div>
	<br/>
	<div class="widtha">Unit ID</div>
	<div class="widthal">
		<input type="text" name="unit_id" placeholder="Only number"/></div>
	<br/>
	<div class="widtha">Section ID</div>
	<div class="widthal"><input type="text" name="section_id" placeholder="Please enter only Number"/></div>
	<br/>
	<div class="widtha">Rank</div>
	<div class="widthal"><input type="text" name="rank" placeholder="Please enter only Number"/></div>
	<br/>
	
<h1 style="color:green;background: #DDD3B5;">Background Information</h1>	

    <div class="widtha">Name of user</div>
	<div class="widthal"><input type="text" name="u_name" placeholder="username please" /></div>
	<br/>
	<div class="widtha">Position of user</div>
	<div class="widthal"><input type="text" name="position" placeholder="position of user"/></div>
	<br/>
	<br/>
	<input class="myButton" type="submit" name="submit" value="Submit Information"/>
</form>
	</div>

	</body>
	
	</html>
