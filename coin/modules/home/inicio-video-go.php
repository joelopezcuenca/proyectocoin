<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Coin</title>
<link href="http://www.proyectocoin.com/coin/css/style.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/style_new.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/font-awesome.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/bootstrap.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<link rel="shortcut icon" href="http://www.proyectocoin.com/coin/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.proyectocoin.com/coin/images/favicon.ico" type="image/x-icon">
<!-- <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> -->
<link href="http://www.proyectocoin.com/coin/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.validationEngine.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/bootstrap.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/jquery.countdownTimer.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/HomePageAjax_Jquery_functions.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_LeadersboardPage_functions.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_DiscussionForumPage_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_bucketlistPage_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/GeneralDiscussion_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.colorbox.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/coin.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/calculator/calculator.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/CustomFunction/generalfunction.js"></script>

<!-- <script src="http://connect.facebook.net/en_US/all.js"></script> -->

</head>
<body>
<header class="inicio-video">
  <div class="container">
    <div class="row-fluid">
      <div class="span12"> <a href="#"><img width="66" src="http://www.proyectocoin.com/coin/images/landing_logo_icon.png"></a> </div>
    </div>
  </div>
</header>

<section id="top_video" class="inicio-go">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
      	<div class="embed-responsive embed-responsive-16by9">
        	<iframe class="embed-responsive-item" src="http://www.socioscoin.com.usrfiles.com/html/525b74_4b519b06dfbbf8605f81439db4cee12c.html?rel=0" allowfullscreen=""></iframe>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="comprar_page" class="inicio-go-page">
  <div class="container">
    <div class="row-fluid comprar_top_part">
      <div class="span6"><img src="http://www.proyectocoin.com/coin/images/comprar-lock.png"> <h4>Portal de Pago Seguro&nbsp;y Verificado&nbsp;™</h4></div>
      <div class="span6 text-right"><img src="http://www.proyectocoin.com/coin/images/comprar-phone.png"> <h2>(55) 84212603</h2></div>
    </div>
    <div class="row-fluid comprar_Payment_part">
      <div class="white_wrapper">
      	<div class="span12 text-center"><img src="http://www.proyectocoin.com/coin/images/steps.png"></div>
        <div class="row-fluid">
          <div class="span8">
            <div class="main-co">
              <h5>1.- CORREO ELECTRÓNICO<span class="red">*</span></h5>
              <input class="fully" name="" type="text" placeholder="getsettera@hotmail.com">
              <h5>2.- NOMBRE COMPLETO<span class="red">*</span></h5>
              <input class="halfly" name="" type="text" placeholder="Nombre"> <input class="halfly" name="" type="text" placeholder="Apellido">
              <h5>3.- FORMA DE PAGO<span class="red">*</span> <img src="http://www.proyectocoin.com/coin/images/comprar-lock-yellow.png"></h5>
              <div class="input-radio">
                <input name="a" type="radio" value=""> <h4>Pagar con PayPal</h4> <i class="fa fa-angle-down"></i><img src="http://www.proyectocoin.com/coin/images/comprar-paypal-master.png" style="display: inline-block;margin-right: 10px;">
                </div>
              <div class="input-radio">
                <div class="checkboxes"> <input name="" type="checkbox" value="">He leído y acepto los términos y condiciones que se indican anteriomente, incluidas la Política de reembolsos la Política de afiliados.</div>
                <div class="checkboxes"> <input name="" type="checkbox" value="">Acepto los terminos de mi suscripcion.</div>
                <div class="ENVIAR"><a href="#"><img src="http://www.proyectocoin.com/coin/images/enviar-but-1.jpg"></a></div>
                </div>
              </div>
            </div>
          <div class="span4">
            <div class="comprar_producto_part" id="producto_part">
              <h4>Resumen de Orden</h4>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:1px solid #d5d5d5">
                <tr style="border-top:1px solid #d5d5d5; border-bottom:1px solid #d5d5d5">
                  <th><img src="http://www.proyectocoin.com/coin/images/comprar-cart.png"></th>
                  <th>Productos: 3</th>
                  <th>Total: $9ºº</th>
                  </tr>
                <tr>
                  <td><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon.png"></td>
                  <td><img src="http://www.proyectocoin.com/coin/images/comprar-lapi.png"></td>
                  <td>Px27 Coin - Programa de 27 pasos</td>
                  </tr>
                <tr>
                  <td><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon.png"></td>
                  <td><img src="http://www.proyectocoin.com/coin/images/comprar-lapi.png"></td>
                  <td>Px27 Coin - Programa de 27 pasos</td>
                  </tr>
                <tr>
                  <td><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon.png"></td>
                  <td><img src="http://www.proyectocoin.com/coin/images/comprar-lapi.png"></td>
                  <td>Acceso de manera mensual a la Plataforma ATLAS OS</td>
                  </tr>
                </table>
              </div>
            
            <div class="comprar_total_part">
              <div>
                <h4>Resumen de Compra</h4>
                <h1>$5<sup>USD</sup></h1>
                <p>Suscripción mensual del Sistema de Coaching Coin XP27  $5ºº USD por el primer mes. Luego $47.00 cada 30 días; puedes cancelar en cualquier momento enviando un correo a admin@proyectocoin.com o llamando al (55) 84212603</p>
              </div>
              </div>
            <div class="yellow-box-copmpar">
              <img src="http://www.proyectocoin.com/coin/images/compar-guarantee.png">
              <h5>GARANTÍA TOTAL<br>TE DEVOLVEREMOS<br>EL 100% TU DINERO</h5>
              <p>Completa tu pedido hoy mismo y, si por cualquier motivo, no te enamoras de este producto en los próximos 14 días, te reembolsaremos el precio de compra,<br>SIN PREGUNTAS.<br>Es tan simple como enviar un correo a admin@proyectocoin.com y te develveremos el 100% de tu dinero</p>      
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<footer class="inicio-video-foot">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
      	<p>NOTICE: The income examples given on this page are extraordinary. They are not intended to serve as a guarantee of income. They're designed to give you an idea of what's possible. Success in this business - as with anything - requires leadership skills, effort, commitment, and dedication. Our goal is to help you make informed decisions. That's why we go above and beyond with our income disclosure document.</p>
        <a href="#">Terms & Conditions</a> | <a href="#">Privacy Policy</a> | <a href="#">Refund Policy</a> | © Copyright 2015 Proyecto Coin. All Rights Reserved.
      </div>
    </div>
  </div>	
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.timeout.interval.idle.js"></script> 
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.countdown.counter.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		(function($){
			$(window).load(function(){
				$("#content-6").mCustomScrollbar({
					axis:"x",
					theme:"light-3",
					advanced:{autoExpandHorizontalScroll:true}
				});
			});
		})(jQuery);
	</script>
<script type="text/javascript">
     CounterInit();
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>