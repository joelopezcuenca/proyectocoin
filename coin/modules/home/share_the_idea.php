<?php ob_start();
include ("../../conf/config.inc.php");
include (INCLUDE_PATH . "/header.php");
//include (INCLUDE_PATH . "/fbacess.php");
$user_post_obj = new user_post();

//to get count of notifications for user
$user_id = $_SESSION['AD_user_id'];
unset($_SESSION['share_url']) ;
include_once (INCLUDE_PATH . "/info_header.php");
include_once (INCLUDE_PATH . "/navigation.php");
include_once (INCLUDE_PATH . "/recursive_coin_function.php");

if (!isset($_SESSION['AD_LoggedIn']) && $_SESSION['AD_LoggedIn'] == false) {
	header("location: " . MODULE_URL . "/home/index.php?to=login");
	exit ;
} else {
	include ("code/share_the_idea_code.php");

?>
<div class="inner">
	<?php
	include_once (MODULE_PATH . "/home/form/share_the_idea_form.php");
	?>
	<div class="cls"></div>
</div>

<?php
include_once (INCLUDE_PATH . "/footer.php");
}
?>