<?php
include ("../../../conf/config.inc.php");
extract($_POST);
extract($_GET);
$leader_search_obj = new leaderboard_search();
$userfollowobj = new userfollow();
$goalObj = new goal();
$objUser = new user();
if ($action == "most_popular") {
	$popular_result = $leader_search_obj -> search_most_followed();
	$html = "";
	$html .= '<div class="span12">';
	for ($i = 0; $iend = count($popular_result), $i < $iend; $i++) {
		$condition = "Followers_Id='" . $popular_result[$i]['id'] . "'";
		$following_var = $userfollowobj -> selectAllFollowData($condition);
		/*
		 * Code for count follwings,etc
		 * as well as multipurpose according to
		 * condition......................
		 */
		$conditionfollowing = "UsertoFollow_Id='" . $popular_result[$i]['id'] . "' and Followers_Id='".$_SESSION['AD_user_id']."'";
		$follow = $userfollowobj -> selectAllFollowData($conditionfollowing);
		$condition = "user_id='" . $popular_result[$i]['id'] . "' and is_unlock='1'";
		$usergoalcount = $goalObj -> User_Count_Achievements_Second($condition);
		$condition1 = "id='" . $popular_result[$i]['id'] . "'";
		$populardata1 = $objUser -> getOnGivenusername($condition1);

		if ($populardata1['id'] != $_SESSION['AD_user_id']) {
			$html .= '<div id="leaderboard_div' . $i . '" class="follow">';
			$html .= '<div class="icon">';
			if ($populardata1['profile_pic'] != '') {
				$html .= '<img src="' . IMAGE_ADMIN_PROFILE_URL . '/' . $populardata1['profile_pic'] . '" alt="icon">';
			} else {
				$html .= '<img src="' . IMAGE_URL . '/adminuser.png" alt="icon">';
			}
			$html .= '</div>';
			$html .= '<div class="info-follow">';
			$html .= '<h3><a href=' . MODULE_URL . '/home/profilefollow.php?id=' . $popular_result[$i]['id'] . '>' . $popular_result[$i]['name'] . '</a></h3>';
			$html .= '<p>';
			$html .= '<em>Seguidores:' . count($follow) . ' | Siguiendo:' . count($following_var) . ' | Sueños logrados : ' . count($usergoalcount) . '</em>';
			$html .= '</p>';
			$html .= '</div>';
			$html .= '<div class="follow-but">';
			if (count($follow) < 1)
				$html .= '<a id="follow'.$i.'"  href="javascript:void(0);" onclick="follow_someone(' .$popular_result[$i]['id'] . ',' . $_SESSION['AD_user_id'] .','.$i.');">Seguir</a>';
			else
				$html .= '<a id="unfollow'.$i.'"  href="javascript:void(0);" onclick="unfollow_someone(' . $popular_result[$i]['id'] . ',' . $_SESSION['AD_user_id'] .','.$i.');">Dejar de Seguir</a>';
			$html .= '</div>';
			$html .= '<div class="cls"></div>';
			$html .= '</div>';
		}
	}

	if (count($popular_result) > 5) {

		$html .= '<div class="loadmore-but">';
		$html .= '<a id="loadmore" class="hidethis" href="javascript:void(0);">Ver más sueños</a>';
		$html .= '</div>';

	}
	$html .= '<input type="hidden" name="leaderboard_count" id="leaderboard_count" value="' . count($popular_result) . '">';
	$html .= '</div>';

	echo $html;
}
if ($action == "top_coiners") {

	$condition1 = "Admin_Role!='Superadmin' and Admin_Role!='admin' order by level DESC";
	$topcoiner_result = $leader_search_obj -> selectAllData($condition1);
	$html = "";
	$html .= '<div class="span12">';
	for ($i = 0; $iend = count($topcoiner_result), $i < $iend; $i++) {
		$condition = "Followers_Id='" . $topcoiner_result[$i]['id'] . "'";
		$following_var = $userfollowobj -> selectAllFollowData($condition);
		/*
		 * Code for count follwings,etc
		 * as well as multipurpose according to
		 * condition......................
		 */
		$conditionfollowing = "UsertoFollow_Id='" . $topcoiner_result[$i]['id'] . "' and Followers_Id='".$_SESSION['AD_user_id']."'";
		$follow = $userfollowobj -> selectAllFollowData($conditionfollowing);

		$condition = "user_id='" . $topcoiner_result[$i]['id'] . "' and is_unlock='1'";
		$usergoalcount = $goalObj -> User_Count_Achievements_Second($condition);

		$condition1 = "id='" . $topcoiner_result[$i]['id'] . "'";
		$populardata1 = $objUser -> getOnGivenusername($condition1);

		if ($populardata1['id'] != $_SESSION['AD_user_id']) {

			$html .= '<div id="leaderboard_div' . $i . '" class="follow">';
			$html .= '<div class="icon">';
			if ($populardata1['profile_pic'] != '') {
				$html .= '<img src="' . IMAGE_ADMIN_PROFILE_URL . '/' . $populardata1['profile_pic'] . '" alt="icon">';
			} else {
				$html .= '<img src="' . IMAGE_URL . '/adminuser.png" alt="icon">';
			}
			$html .= '</div>';
			$html .= '<div class="info-follow">';
			$html .= '<h3><a href=' . MODULE_URL . '/home/profilefollow.php?id=' . $topcoiner_result[$i]['id'] . '>' . $topcoiner_result[$i]['name'] . '</a></h3>';
			$html .= '<p>';
			$html .= '<em>Seguidores:' . count($follow) . ' | Siguiendo:' . count($following_var) . ' | Sueños logrados : ' . count($usergoalcount) . '</em>';
			$html .= '</p>';
			$html .= '</div>';
			$html .= '<div class="follow-but">';
			if (count($follow) < 1)
				$html .= '<a id="follow'.$i.'" class="" href="javascript:void(0);" onclick="follow_someone(' . $topcoiner_result[$i]['id'] . ',' . $_SESSION['AD_user_id'] . ','.$i.');">Seguir</a>';
			else
				$html .= '<a id="unfollow'.$i.'" class="" href="javascript:void(0);" onclick="unfollow_someone(' . $topcoiner_result[$i]['id'] . ',' . $_SESSION['AD_user_id'] . ','.$i.');">Dejar de Seguir</a>';
			$html .= '</div>';
			$html .= '<div class="cls"></div>';
			$html .= '</div>';
		}
	}
	if (count($topcoiner_result) > 5) {

		$html .= '<div class="loadmore-but">';
		$html .= '<a id="loadmore" class="hidethis" href="javascript:void(0);">Ver más sueños</a>';
		$html .= '</div>';

	}
	$html .= '<input type="hidden" name="leaderboard_count" id="leaderboard_count" value="' . count($topcoiner_result) . '">';
	$html .= '</div>';
	echo $html;
}

if ($action == "most_achieved") {
	$popular_result = $leader_search_obj -> search_most_goal_achieved();
	$html = "";
	$html .= '<div class="span12">';
	for ($i = 0; $iend = count($popular_result), $i < $iend; $i++) {
		$condition = "Followers_Id='" . $popular_result[$i]['id'] . "'";
		$following_var = $userfollowobj -> selectAllFollowData($condition);
		/*
		 * Code for count follwings,etc
		 * as well as multipurpose according to
		 * condition......................
		 */
		$conditionfollowing = "UsertoFollow_Id='" . $popular_result[$i]['id'] . "'and Followers_Id='".$_SESSION['AD_user_id']."'";
		$follow = $userfollowobj -> selectAllFollowData($conditionfollowing);
		$condition = "user_id='" . $popular_result[$i]['id'] . "' and is_unlock='1'";
		$usergoalcount = $goalObj -> User_Count_Achievements_Second($condition);
		$condition1 = "id='" . $popular_result[$i]['id'] . "'";
		$populardata1 = $objUser -> getOnGivenusername($condition1);

		if ($populardata1['id']!= $_SESSION['AD_user_id']) {

			$html .= '<div id="leaderboard_div' . $i . '" class="follow">';
			$html .= '<div class="icon">';
			if ($populardata1['profile_pic']!= '') {
				$html .= '<img src="' . IMAGE_ADMIN_PROFILE_URL . '/' . $populardata1['profile_pic'] . '" alt="icon">';
			} else {
				$html .= '<img src="' . IMAGE_URL . '/adminuser.png" alt="icon">';
			}
			$html .= '</div>';
			$html .= '<div class="info-follow">';
			$html .= '<h3><a href=' . MODULE_URL . '/home/profilefollow.php?id=' . $popular_result[$i]['id'] . '>' . $popular_result[$i]['name'] . '</a></h3>';
			$html .= '<p>';
			$html .= '<em>Sueños logrados : ' . count($usergoalcount) . ' | Seguidores:' . count($follow) . ' | Siguiendo:' . count($following_var) . ' </em>';
			$html .= '</p>';
			$html .= '</div>';
			$html .= '<div class="follow-but">';
			if (count($follow) < 1)
				$html .= '<a id="follow'.$i.'" class="" href="javascript:void(0);" onclick="follow_someone(' . $popular_result[$i]['id'] . ',' . $_SESSION['AD_user_id'] . ','.$i.');">Seguir</a>';
			else
				$html .= '<a id="unfollow'.$i.'" class="" href="javascript:void(0);" onclick="unfollow_someone(' . $popular_result[$i]['id'] . ','.$_SESSION['AD_user_id'] .','.$i.');">Dejar de Seguir</a>';
			$html .= '</div>';
			$html .= '<div class="cls"></div>';
			$html .= '</div>';
		}
	}
	if (count($popular_result) > 5) {

		$html .= '<div class="loadmore-but">';
		$html .= '<a id="loadmore" class="hidethis" href="javascript:void(0);">Ver más sueños</a>';
		$html .= '</div>';

	}
	$html .= '<input type="hidden" name="leaderboard_count" id="leaderboard_count" value="' . count($popular_result) . '">';
	$html .= '</div>';
	echo $html;
}
?>