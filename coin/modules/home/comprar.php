<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Coin</title>
<link href="http://www.proyectocoin.com/coin/css/style.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/style_new.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/font-awesome.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/bootstrap.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<link rel="shortcut icon" href="http://www.proyectocoin.com/coin/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.proyectocoin.com/coin/images/favicon.ico" type="image/x-icon">
<!-- <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> -->
<link href="http://www.proyectocoin.com/coin/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.validationEngine.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/bootstrap.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/jquery.countdownTimer.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/HomePageAjax_Jquery_functions.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_LeadersboardPage_functions.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_DiscussionForumPage_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_bucketlistPage_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/GeneralDiscussion_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.colorbox.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/coin.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/calculator/calculator.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/CustomFunction/generalfunction.js"></script>
<!-- <script src="http://connect.facebook.net/en_US/all.js"></script> -->
</head>
<body style="background:#e8e8e8;">
<header class="comprar">
  <div class="container">
    <div class="row-fluid">
      <div class="span12"> <a href="#"><img width="50" src="http://www.proyectocoin.com/coin/images/landing_logo_icon.png"></a> </div>
    </div>
  </div>
</header>

<section id="comprar_page">
  <div class="container">
    <div class="row-fluid comprar_top_part">
      <div class="span6"><img src="http://www.proyectocoin.com/coin/images/comprar-lock.png"> <h4>Portal de Pago Seguro&nbsp;y Verificado&nbsp;™</h2></div>
      <div class="span6 text-right"><img src="http://www.proyectocoin.com/coin/images/comprar-phone.png"> <h2>(55) 84212603</h2></div>
    </div>
    <div class="row-fluid">
      <div class="white_wrapper">
      	<div class="span3">
        	<h1>Proyecto COIN</h1>
            <h5>PLATAFORMA: COIN.OS</h5>
            <img class="comprar-coin" src="http://www.proyectocoin.com/coin/images/comprar-coin.png">
            <a class="Acceso_Coin" href="#producto_part">Acceso a Coin</a>
            <p class="dicen">Lo que dicen los miembros <br>de nuestra Comunidad.</p>
            
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="http://placehold.it/100">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Ricardo Martinez</p>
                        <img class="comprar-star" src="http://www.proyectocoin.com/coin/images/comprar-star.png">
						<p>La Plataforma esta buenísima!  No tuve problemas en entender como usarla y todo es super claro y desde la primer semana ya había cumplido 1 de mis metas.</p>
                    </div>
				</div>
			</div>
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="http://placehold.it/100">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Ricardo Martinez</p>
                        <img class="comprar-star" src="http://www.proyectocoin.com/coin/images/comprar-star.png">
						<p>La Plataforma esta buenísima!  No tuve problemas en entender como usarla y todo es super claro y desde la primer semana ya había cumplido 1 de mis metas.</p>
                    </div>
				</div>
			</div>
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="http://placehold.it/100">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Ricardo Martinez</p>
                        <img class="comprar-star" src="http://www.proyectocoin.com/coin/images/comprar-star.png">
						<p>La Plataforma esta buenísima!  No tuve problemas en entender como usarla y todo es super claro y desde la primer semana ya había cumplido 1 de mis metas.</p>
                    </div>
				</div>
			</div>
            
        </div>
      	<div class="span9">
        	<div class="top-right-text"><p>Tu orden esta asegurada 100%?<br>en nuestros servidores</p> <img class="comprar-star" src="http://www.proyectocoin.com/coin/images/comprar-lock-yellow.png"></div>
            <div class="co_part">
            	<h4>Descripción</h4>
                <p>Upgrade your Mac to OS X Yosemite and you'll get an elegant design that's both fresh and familiar. The apps you use every day will have powerful new features. And your Mac, iPhone, and iPad will work together in amazing new ways. You'll also get the latest technology and the strongest security. It's like getting a whole new Mac — for free.</p>
                <div class="grayish">
                	<h4>Ejemplos del Producto</h4>
                    <div class="screenshotz"><img src="http://www.proyectocoin.com/coin/images/screenshot.jpg"></div>
                </div>
                <div class="logos-part">
                	<span class="comprar-right-icon"><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon.png"></span>
                    <p>Como lo viste en:</p>
                    <span class="comprar-logos-image"><img src="http://www.proyectocoin.com/coin/images/comprar-logos-image.png"></span>
                </div>
            </div>
        </div>
        <div class="row-fluid comprar_producto_part" id="producto_part">
          <div class="span12">
            <h4>Resumen de Orden</h4>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:1px solid #d5d5d5">
            	<tr style="border-top:1px solid #d5d5d5; border-bottom:1px solid #d5d5d5">
                    <th width="60px"><img src="http://www.proyectocoin.com/coin/images/comprar-cart.png"></th>
                    <th width="120px">Productos: 3</th>
                    <th></th>
                    <th width="150px">Total: $9ºº</th>
                </tr>
                <tr>
                    <td><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon.png"></td>
                    <td><img src="http://www.proyectocoin.com/coin/images/comprar-lapi.png"></td>
                    <td>Px27 Coin - Programa de 27 pasos</td>
                    <td><del>&nbsp;$155&nbsp;</del> $0ºº </td>
                </tr>
                <tr>
                    <td><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon.png"></td>
                    <td><img src="http://www.proyectocoin.com/coin/images/comprar-lapi.png"></td>
                    <td>Px27 Coin - Programa de 27 pasos</td>
                    <td> $0ºº </td>
                </tr>
                <tr>
                    <td><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon.png"></td>
                    <td><img src="http://www.proyectocoin.com/coin/images/comprar-lapi.png"></td>
                    <td>Acceso de manera mensual a la Plataforma ATLAS OS</td>
                    <td><del>&nbsp;$155&nbsp;</del> $9ºº </td>
                </tr>
            </table>
          </div>
        </div>
        <div class="row-fluid comprar_total_part">
          <div class="span9">
            <h4>Resumen de Compra</h4>
            <p>Suscripción mensual del Sistema de Coaching Coin XP27  $5ºº USD por el primer mes. Luego $47.00 cada 30 días; puedes cancelar en cualquier momento enviando un correo a admin@proyectocoin.com o llamando al (55) 84212603</p>
          </div>
          <div class="span3 text-center">
            <h4>Monto Total</h4>
            <h1>$5ººUSD</h1>
          </div>
        </div>
        <div class="row-fluid comprar_Payment_part">
          <div class="span8">
            <div class="main-co">
                <h5>FORMA DE PAGO<span class="red">*</span> <img src="http://www.proyectocoin.com/coin/images/comprar-lock-yellow.png"></h5>
                <div class="input-radio">
                    <input name="a" type="radio" value="" /> <h4>Tarjeta de Crédito/Débito</h4> <i class="fa fa-angle-up"></i> <img src="http://www.proyectocoin.com/coin/images/comprar-visa-master.png" style="display: inline-block;">
                   <div> 
                     <h6>Información de la Tarjeta &nbsp; &nbsp; &nbsp;<img style="display: inline-block;width:7px; margin: -3px 0 0 0;" src="http://www.proyectocoin.com/coin/images/lock-sm.png" /> &nbsp;SEGURO</h6>
                   </div>
                   <div>
                     <label>Nombre</label><input name="" type="text" pleaceholder="Titular de la tarjeta" />
                   </div>
                   <div>
                     <label>Tarjeta</label><input name="" type="text" pleaceholder="Numero de la tarjeta" /><img src="http://www.proyectocoin.com/coin/images/comprar-visa-master.png" style="display: inline-block;opacity:.4;margin: -15px 0 0 10px;">
                   </div>
                   <div>
                     <label>Expiración</label><input class="MM" name="" type="text" placeholder="MM" /> &nbsp;/&nbsp; <input class="AA" name="" type="text" placeholder="AA" /> &nbsp;CVC &nbsp;&nbsp;<input class="ccv" name="" type="text" placeholder="CVC" /> &nbsp;<img src="http://www.proyectocoin.com/coin/images/green-faq.jpg" style="display: inline-block;margin: 0 0 0 0;">
                   </div>
                   <div class="checkboxes"> <input name="" type="checkbox" value="">He leído y acepto los términos y condiciones que se indican anteriomente, incluidas la Política de reembolsos la Política de afiliados.</div>
                   <div class="checkboxes"> <input name="" type="checkbox" value="">Acepto los terminos de mi suscripcion.</div>
                   <div class="ENVIAR"><a href="#"><img src="http://www.proyectocoin.com/coin/images/enviar-but.jpg"></a></div>
                </div>
                <div class="input-radio">
                <input name="a" type="radio" value="" /> <h4>American Express</h4> <img src="http://www.proyectocoin.com/coin/images/comprar-express.png" style="display: inline-block;margin-right: 10px;">
                </div>
                <div class="input-radio">
                <input name="a" type="radio" value="" /> <h4>Pagar con PayPal</h4> <i class="fa fa-angle-down"></i><img src="http://www.proyectocoin.com/coin/images/comprar-paypal-master.png" style="display: inline-block;margin-right: 10px;">
                </div>
             </div>
          </div>
          <div class="span4 text-center">
            <div class="yellow-box-copmpar">
            	<img src="http://www.proyectocoin.com/coin/images/compar-guarantee.png">
                <h5>Tenemos la mejor<br>Garantía del Mundo</h5>
                <p>Completa tu pedido hoy mismo y, si por cualquier motivo, no te enamoras de este producto en los próximos 14 días, te reembolsaremos el precio de compra,<br>SIN PREGUNTAS.<br>Es tan simple como enviar un correo a admin@proyectocoin.com y te develveremos el 100% de tu dinero</p>      
            </div>
            <img src="http://www.proyectocoin.com/coin/images/intruck.png">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>

</body>
</html>