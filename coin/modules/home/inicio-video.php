<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Coin</title>
<link href="http://www.proyectocoin.com/coin/css/style.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/style_new.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/font-awesome.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/bootstrap.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="http://www.proyectocoin.com/coin/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<link rel="shortcut icon" href="http://www.proyectocoin.com/coin/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.proyectocoin.com/coin/images/favicon.ico" type="image/x-icon">
<!-- <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> -->
<link href="http://www.proyectocoin.com/coin/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.validationEngine.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/bootstrap.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/jquery.countdownTimer.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/HomePageAjax_Jquery_functions.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_LeadersboardPage_functions.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_DiscussionForumPage_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/Ajax_bucketlistPage_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/Ajax_js_function/GeneralDiscussion_function.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.colorbox.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/coin.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/calculator/calculator.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/CustomFunction/generalfunction.js"></script>

<!-- <script src="http://connect.facebook.net/en_US/all.js"></script> -->

</head>
<body style="background:#f7f7f7;">
<header class="inicio-video">
  <div class="container">
    <div class="row-fluid">
      <div class="span12"> <a href="#"><img width="66" src="http://www.proyectocoin.com/coin/images/landing_logo_icon.png"></a> </div>
    </div>
  </div>
</header>

<section id="top_video">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
      	<div class="embed-responsive embed-responsive-16by9">
        	<iframe class="embed-responsive-item" src="http://www.socioscoin.com.usrfiles.com/html/525b74_4b519b06dfbbf8605f81439db4cee12c.html?rel=0" allowfullscreen=""></iframe>
        </div>
        <div class="timer_sec">
        	<span>Comienza a vivir y trabajar bajo tus propios términos en...</span>
            <div id="centered_div">
            <div id="counter">
            <div id="counter_item1" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            <div id="counter_item2" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            
            <div id="counter_item4" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            <div id="counter_item5" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="comprar_page" class="inicio-video-page">
  <div class="container">
    <div class="row-fluid comprar_top_part">
      <div class="span12">
      	<div class="logos-part">
            <span class="comprar-right-icon"><img src="http://www.proyectocoin.com/coin/images/comprar-right-icon-1.png"></span>
            <p>Como lo viste en:</p>
            <span class="comprar-logos-image"><img src="http://www.proyectocoin.com/coin/images/comprar-logos-image-1.png"> <img src="http://www.proyectocoin.com/coin/images/comprar-logos-image-2.png"></span>
        </div>
      </div>
    </div>
    
    <div class="row-fluid">
      <div class="white_wrapper">
      	<div class="span3">
        	<h1>Proyecto COIN</h1>
            <h5>PLATAFORMA: COIN.OS</h5>
            <img class="comprar-coin" src="http://www.proyectocoin.com/coin/images/comprar-coin.png">
            <a class="Acceso_Coin" href="#producto_part">Acceso a Coin</a>
            <p class="dicen">Lo que dicen los miembros <br>de nuestra Comunidad.</p>
            
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="http://placehold.it/100">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Ricardo Martinez</p>
                        <img class="comprar-star" src="http://www.proyectocoin.com/coin/images/comprar-star.png">
						<p>La Plataforma esta buenísima!  No tuve problemas en entender como usarla y todo es super claro y desde la primer semana ya había cumplido 1 de mis metas.</p>
                    </div>
				</div>
			</div>
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="http://placehold.it/100">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Ricardo Martinez</p>
                        <img class="comprar-star" src="http://www.proyectocoin.com/coin/images/comprar-star.png">
						<p>La Plataforma esta buenísima!  No tuve problemas en entender como usarla y todo es super claro y desde la primer semana ya había cumplido 1 de mis metas.</p>
                    </div>
				</div>
			</div>
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="http://placehold.it/100">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Ricardo Martinez</p>
                        <img class="comprar-star" src="http://www.proyectocoin.com/coin/images/comprar-star.png">
						<p>La Plataforma esta buenísima!  No tuve problemas en entender como usarla y todo es super claro y desde la primer semana ya había cumplido 1 de mis metas.</p>
                    </div>
				</div>
			</div>
            
        </div>
      	<div class="span9">
        	<div class="co_part">
            	<h4>Descripción</h4>
                <p>Upgrade your Mac to OS X Yosemite and you'll get an elegant design that's both fresh and familiar. The apps you use every day will have powerful new features. And your Mac, iPhone, and iPad will work together in amazing new ways. You'll also get the latest technology and the strongest security. It's like getting a whole new Mac — for free.</p>
                <div class="grayish">
                	<h4>Ejemplos del Producto</h4>
                    <div class="screenshotz">
                    	<div id="content-6" class="content horizontal-images">
                            <ul>
                                <li><img src="http://www.proyectocoin.com/coin/images/screenshot.jpg" /></li>
                                <li><img src="http://www.proyectocoin.com/coin/images/screenshot.jpg" /></li>
                                <li><img src="http://www.proyectocoin.com/coin/images/screenshot.jpg" /></li>
                                <li><img src="http://www.proyectocoin.com/coin/images/screenshot.jpg" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="guarantee_part">
                	<p><img src="http://www.proyectocoin.com/coin/images/compar-guarantee.png"> Complete your order today and if for any reason you don't absolutely love this product within the next 14 days, we'll refund you NO QUESTIONS ASKED. It's as simple as sending us a refund request at admin@proyectocoin.com</p>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<footer class="inicio-video-foot">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
      	<p>NOTICE: The income examples given on this page are extraordinary. They are not intended to serve as a guarantee of income. They're designed to give you an idea of what's possible. Success in this business - as with anything - requires leadership skills, effort, commitment, and dedication. Our goal is to help you make informed decisions. That's why we go above and beyond with our income disclosure document.</p>
        <a href="#">Terms & Conditions</a> | <a href="#">Privacy Policy</a> | <a href="#">Refund Policy</a> | © Copyright 2015 Proyecto Coin. All Rights Reserved.
      </div>
    </div>
  </div>	
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.timeout.interval.idle.js"></script> 
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.countdown.counter.js"></script>
<script type="text/javascript" src="http://www.proyectocoin.com/coin/js/All_Js_library/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		(function($){
			$(window).load(function(){
				$("#content-6").mCustomScrollbar({
					axis:"x",
					theme:"light-3",
					advanced:{autoExpandHorizontalScroll:true}
				});
			});
		})(jQuery);
	</script>
<script type="text/javascript">
     CounterInit();
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>