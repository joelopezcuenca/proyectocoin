<?php ob_start();
include("../../conf/config.inc.php");
include(INCLUDE_PATH."/header.php") ;
//include_once(INCLUDE_PATH."/info_header.php");

?>
<div id="Containt1">
    <section>
      <div class="basicfont font40 white MarT25">POLITICAS DE PRIVACIDAD</div>
      <div class="MarT50">
        <div class="whiteshadow whiteshad">
          <div class="whiteshadow-line"></div>
        </div>
      </div>
    </section>
  </div>
  <div id="Containt5" >
    <section>
      <div class="Mar20-10 tj colorgray">
        <div>Con fundamento en los artículos 15 y 16 de la Ley Federal de Protección de DatosPersonales en Posesión de Particulareshacemos de suconocimientoque la comunidadsisomo (www.redsisomo.com - www.comunidadsisomo.com ó www.sisomo-fls.in) son responsable de recabarsusdatospersonales, del usoque se le dé a los mismos y de suprotección.
Su información personal seráutilizadapara: Saber exactamente lo quebuscanparamandarinformación de calidad, comunicarcambios en los mismos, evaluar la calidad del servicio, mandarboletines e informaciónpersonalizada de los serviciosrequeridos.
Para lasfinalidades antes mencionadas, requerimosobtener los siguientesdatospersonales:
<ul style="margin-top:-10px;margin-bottom:10px;">
	<li>Nombrecompleto</li>
	<li>Correo(s) electrónico(s)</li>
</ul>

Esimportanteinformarlequeustedtienederecho al Acceso, Rectificación y Cancelación de susdatospersonales, aOponerse al tratamiento de los mismos o a revocar el consentimientoqueparadicho fin noshayaotorgado.  Para ello, esnecesarioquediriga la solicitud en los términosquemarca la Ley en su Art.29 a Departamento de Protección de DatosPersonales, responsable de nuestroDepartamento de Protección de DatosPersonales, víacorreoelectrónico a comunidad@sisomo-fls.in, el cualsolicitamosconfirmesucorrectarecepción.
Porotra parte, hacemos de suconocimientoque la comunidadsisomo en ningúnmomentotransfiereinformación personal a terceros.
En caso de que no obtengamossuoposiciónexpresaparaquesusdatospersonalesseantransferidos en la forma y términos antes descrita, entenderemosque ha otorgadosuconsentimiento en forma tácitaparaello.
En caso de que no desee de recibirmensajespromocionales de nuestra parte, puedeenviarnossusolicitudpormedio de la direcciónelectrónica: comunidad@sisomo-fls.in
</div>
        
      </div>
    </section>
  </div>

<?php include_once(INCLUDE_PATH."/footer.php"); ?>