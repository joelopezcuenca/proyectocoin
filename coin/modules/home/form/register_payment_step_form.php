<!--| Content Start |-->

<div class="login-video payment-step">
  <div class="container">
    <div class="row-fluid">
      <div class="span6 offset3">
        <div class="payment-white-box">
          <div class="login-logo"> <img src="<?php echo IMAGE_URL?>/logo.png" alt="logo"> </div>
          <form action="" method="POST" id="card-form"  class="form-horizontal">
            <span class="card-errors card-red"></span>
            <div class="control-group form-row">
              <label class="control-label"><span>Customer's Name</span></label>
              <div class="controls">
                <input type="text" name="name" required  size="20" data-conekta="card[name]"/>
              </div>
            </div>
            <div class="control-group form-row">
              <label class="control-label"><span>Customer's Email</span></label>
              <div class="controls">
                <input type="text" name="email"  required size="20" data-conekta="card[email]"/>
              </div>
            </div>
            <div class="control-group form-row">
              <label class="control-label"><span>Customer's Phone</span></label>
              <div class="controls">
                <input type="text" name="phone_no"  required  size="20" data-conekta="card[phone]"/>
              </div>
            </div>
            <div class="control-group form-row">
              <label class="control-label"><span>Credit Card Number</span></label>
              <div class="controls">
                <input type="text" card="card_no"  required size="20" data-conekta="card[number]"/>
              </div>
            </div>
            <div class="control-group form-row">
              <label class="control-label"><span>CVC</span></label>
              <div class="controls">
                <input type="text" size="4" required data-conekta="card[cvc]"/>
              </div>
            </div>
            <div class="control-group form-row">
              <label class="control-label"><span>Expiration Date (MM/AAAA)</span></label>
              <div class="controls">
                <select class="span6" id="" name="expiration_month" required data-conekta="card[exp_month]">
                  <?php for($i=1;$i<=12;$i++){
							if($i<10)
							{
								$i= '0'.$i;
							}
							?>
                  <option value="<?php echo $i; ?>" ><?php echo $i;?></option>
                  <?php } ?>
                </select>
                <select class="span6" name="expiration_year" required data-conekta="card[exp_year]" >
                  <?php
					for($i=date('Y'); $i<=2070; $i++) {
						$selected = '';
						if ($birthdayYear == $i) $selected = ' selected="selected"';
						print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
					}
					?>
                </select>
              </div>
            </div>
            <div class="control-group form-row">
              <label class="control-label"><span>Amount</span></label>
              <div class="controls">
                <input type="text" readonly value="$5"   size="20" />
                <p>$5 for the first month afterwards it would be $25/Month.</p> </div>
            </div>
            <input type="hidden" name="action" value="charge_card" />
            <button type="submit">Subscribe now</button>
          </form>
          <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
          <script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script> 
          <script type="text/javascript">
 
 // Conekta Public Key
  Conekta.setPublishableKey('key_AAAbCKb2VV4NdcgV7DvvUSA');
 
 // ...
 jQuery(function($) {
  $("#card-form").submit(function(event) {
    var $form;
    $form = $(this);

    
/* Prevent the button from submitting multiple times */

    $form.find("button").prop("disabled", true);
    Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);

    
/* Prevent form information from being sent to the server */

    return false;
  });
});
 var conektaSuccessResponseHandler;
conektaSuccessResponseHandler = function(token) {
  var $form;
  $form = $("#card-form");

  
/* Insert the token_id into the form so it gets submitted to the server */

  $form.append($("<input type=\"hidden\" name=\"conektaTokenId\" />").val(token.id));

  
/* and submit */

  $form.get(0).submit();
};
var conektaErrorResponseHandler;
conektaErrorResponseHandler = function(response) {
  var $form;
  $form = $("#card-form");

  
/* Show the errors on the form */

  $form.find(".card-errors").text(response.message);
  $form.find("button").prop("disabled", false);
};
</script>
          <div class="login-logo-bottom" style="background:none;"> <img width="70" src="<?php echo IMAGE_URL?>/logo.png" alt="logo"> </div>
        </div>
      </div>
    </div>
  </div>
</div>
