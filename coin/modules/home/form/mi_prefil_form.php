﻿<style type="text/css">
    .profile-image2 {
        padding: 20px 0;
        width: 200px;
    }
</style>
<div class="left_part">
    <?php
    include_once (INCLUDE_PATH . "/home_left_navigation.php");
    ?>
</div>
<div class="right_part">
    <div class="boxes round mi-prefil">
        <?php
        //echo "<pre>"; print_r($userdetails);die;	
        $text = 0;
        if ($userdetails['profile_pic'] != '') {

            $text = $text + 25;
        }
        if ($userdetails['description'] != '') {
            $text = $text + 25;
        }
        if (count($checking_for_goal) > 0) {
            $text = $text + 25;
        }
        if (count($checking_for_follow_user) > 0) {
            $text = $text + 25;
        }
        ?>
        <!--    <div class="text-left Padd15N name">
                        
        <?php
//			if($current_pageName =='digitalPassport'){ 
//			echo '<i class="fa fa-chevron-down Padd10"></i>';
//				echo "Metas Logradas : ".count($goal_array_achieveds);
//			}elseif($current_pageName =='profileFollow'){ 
//			echo '<a href="'.MODULE_URL.'/home/leaderboard.php"><i class="fa fa-chevron-left Padd10"></i></a>';
//				echo "Regresar a la comunidad";
//			}elseif($current_pageName =='miProfile'){
//				echo '<i class="fa fa-chevron-down Padd10"></i>';
//				echo "Mi Perfil";			
//			}
        ?>
                </div>-->
        <div class="cls"></div>
        <?php if (isset($_SESSION['image_error'])) { ?>
            <span style="color: red"><b><?php echo $_SESSION['image_error']; ?></b></span>
            <div class="cls"></div>
            <?php
            unset($_SESSION['image_error']);
        }
        ?>
        <?php if ($current_pageName == 'miProfile' || $current_pageName == 'profileFollow') { ?>
            <div class="cover-page">
                <?php if ($userInfo['background_pic'] != '') { ?>
                    <img alt="image" src="<?php echo IMAGE_ADMIN_BACKGROUND_URL . "/" . $userInfo['background_pic']; ?>" class="cover-image">
                <?php } else { ?>
                    <img  alt="image" src="<?php echo IMAGE_URL; ?>/profile-back-pic.jpg" class="cover-image">
                <?php } ?>
                <?php if ($userInfo['profile_pic'] != '') { ?>
                    <img class="profile-image" alt="image" src="<?php echo IMAGE_ADMIN_PROFILE_URL . "/" . $userInfo['profile_pic']; ?>">
                <?php } else { ?>
                    <img class="profile-image" alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png">
                <?php } ?>
                <h1 class="name2"><?php echo $userInfo['name']; ?></h1>
                <h3 class="name2"><?php echo $userInfo['username']; ?></h3>


                <?php
                $Idname = 'followmee';
                if ($current_pageName != 'profileFollow' && empty($id)) {
                    $Idname = '';
                }
                ?>
                <div id="<?php echo $Idname; ?>">
                    <img src="<?php echo IMAGE_URL; ?>/bx_loader.gif" id="follow_loading2" style="display:none;" />
                    <?php if ($current_pageName == 'profileFollow') { ?>
                        <?php
                        // Check if a User already follwed to someone or not to show unfollow button there
                        if (count($CheckDataFollows) > 0) {
                            ?>
                            <a id="unfollow" class="" href="javascript:void(0);" onclick="unfollow_someone('<?php echo $userInfo['id']; ?>', '<?php echo $_SESSION['AD_user_id']; ?>');"><i class="fa fa-user-plus gold paddR5"></i> Dejar de Seguir</a>
                        <?php } else { ?>
                            <a id="follow" class="" href="javascript:void(0);" onclick="follow_someone('<?php echo $userInfo['id']; ?>', '<?php echo $_SESSION['AD_user_id']; ?>');"><i class="fa fa-user-plus gold paddR5"></i> Seguir</a>


                        <?php } ?>
                    <?php } ?>	
                    <?php if ($current_pageName != 'profileFollow' && empty($id)) { ?>
                        <a class="change-bg" data-toggle="modal" role="button" href="<?php echo MODULE_URL . '/home/account-setting.php'; ?>">Editar mi Perfil</a> 
                    <?php } ?>
                </div>
            </div>
            <div class="MarB20">
                <div class="Font11N left160 text-left">				
                    <p><?php echo $userInfo['bio']; ?> </p>
                    <p ><a class="blue" href="<?php echo $userInfo['website']; ?>" target="_blank" ><?php echo $userInfo['website']; ?></a></p>
                </div>
                <div class="rightbox">
                    <ul>
                        <li><a href="javascript:void(0)"><?php echo count($userachievedgoalcount) ?> <br/><span>Logros</span></a></li>
                        <li>
                            <!--<a href="javascript:void(0)" onclick="dreams('<?php echo $userInfo['id']; ?>')">-->
                            <a href="<?php echo MODULE_URL?>/home/bucket_list.php"  >
                                <?php
                                if ($totalGoal_counting > 0) {
                                    echo $totalGoal_counting;
                                } else {
                                    echo '0';
                                }
                                ?><br/> <span>Sueños</span>
                            </a></li>

                        <li><a href="javascript:viod(0)"><?php echo count($follow) ?><br/> <span>Seguidores</span></a></li>
                        <li><a href="javascript:viod(0)"><?php echo count($following_var) ?> <br/><span>Siguiendo</span></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php } ?>
        <hr class="Mar0" />

        <div class="round profile_images" id="accordion2">
			<?php 
		
			if(empty($goal_array_achieveds) && $_SESSION['show_goal_noti']==1) { ?>
		  <div class="profile_img-bg profile-gray-remove">
       		   <h4>Aún no has guardado ninguna meta lograda.<br>¡Comienza escribiendo tus sueños!</h4>
               <img class="profile-bg-img" alt="image" src="<?php echo IMAGE_URL; ?>/mi-form-icon1.png">
                <img class="profile-img-arrow" alt="image" src="<?php echo IMAGE_URL; ?>/mi-form-arrows.png">
            </div>
            <div class="clear-fix"></div>
			<?php  	$_SESSION['show_goal_noti'] = '';	 } ?>
			
			
            <ul class="miprefil">
                <?php
                foreach ($goal_array_achieveds as $goal_vals) {
                    $goalImages = $goalobj->select_all_user_goal_images($goal_vals['user_id'], $goal_vals['id']);
                    ?>
                    <li id="goel_li_<?php echo $goal_vals['id']; ?>">
					    <div class="relative">
                           
							<?php if($goal_vals['user_id']==$_SESSION['AD_user_id']){ ?>
						   <span><a href="javascript:void(0)" onclick="delete_complete_goel('<?php echo $goal_vals['id']; ?>')"><img style="width:16px; height:16px;" src="<?php echo IMAGE_URL; ?>/trashcan.png" /></a></span>
							<?php } ?>
							
							
                            <a href="#" class="prefilimg">
                                <img style="width:231px; height:215px;" alt="image" src="<?php echo DEFAULT_URL; ?>/goal_img/<?php echo $goal_vals['bucket_image']; ?>">
                            </a>
							
                            <div class="showdiv" href="#myModal<?php echo $goal_vals['id']; ?>" role="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" >
							
                                <div class="padd40">
								
                                    <h2><?php echo substr($goal_vals['bucket_name'], 0, 20); ?></h2>
                                    <p><?php
                                        if (strlen($goal_vals['bucket_mdesc_l']) > 50) {
                                            echo substr($goal_vals['bucket_mdesc_l'], 0, 50) . '...';
                                        } else {
                                            echo substr($goal_vals['bucket_mdesc_l'], 0, 50);
                                        }
                                        ?></p>
                                    <div class="width125">
                                        <div class="like ">
                                            <a href="javascript:void(0)">
                                                <i class="fa fa-heart-o"></i>
                                                <?php
                                                $conditions = "achieved_goal_id = " . $goal_vals['id'];
                                                $likes = $goalObj->goal_count_likes($conditions);
                                                $total_count = count($likes);

                                                $conditions = "achieved_goal_id = " . $goal_vals['id'] . " and ip='" . $_SERVER['REMOTE_ADDR'] . "' ";
                                                $ip_like_arr = $goalObj->goal_count_likes($conditions);
                                                $total_count = count($likes);
                                                $total_count_ip = count($ip_like_arr);


                                                echo '<span class"hart_like_' . $goal_vals[id] . '">' . $total_count . '</span>'
                                                ?> LIKE 
                                            </a>
                                        </div>

                                        <div class="cls"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!-- Modal start -->
                    <div id="myModal<?php echo $goal_vals['id']; ?>"  class="modal hide fade current_popup" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
                        <a class="pop-close" href="javascript:void(0)" onclick="close_popup('<?php echo $goal_vals['id']; ?>')">x</a>

                        <?php // echo $goal_vals['id'];    ?>
                        <a class="pop-left" href="javascript:void(0)" onclick="previous_goal('myModal<?php echo $goal_vals['id']; ?>')" ><img alt="icon" src="<?php echo IMAGE_URL; ?>/left-pop-left.png"></a>  
                        <a class="pop-right" href="javascript:void(0)" onclick="next_goal('myModal<?php echo $goal_vals['id']; ?>')" ><img alt="icon" src="<?php echo IMAGE_URL; ?>/left-pop-left.png"></a>
                        <div class="modal-body">
                            <div class="row-fluid">
                                <div class="span7">
                                    <div class="slides">
                                        <ul class="pgwSlideshow">
                                            <?php
                                            $imageCounter = 1;
                                            foreach ($goalImages as $value2) {
                                                if ($imageCounter == 1)
                                                    $classActive = 'active';
                                                else
                                                    $classActive = '';
                                                $imageCounter++;
                                                ?>
                                                <li><img alt="image" src="<?php echo DEFAULT_URL; ?>/goal_img/<?php echo $value2['images']; ?>"></li>
                                            <?php } ?>
                                        </ul>
                                    </div>							
                                </div>
                                <div class="span5 PaddTB10">
                                    <div class="row-fluid">
                                        <div class="PaddLR10"> 
                                            <div class="span6">
                                                <div class="width40">
                                                    <?php
                                                    $conditions = "id = " . $goal_vals['user_id'];
                                                    $user_infos = $objUser->SelectAll_User($conditions);
                                                    //echo "<pre>"; print_r($user_infos);
                                                    ?>

                                                    <!-- There fields are use for comments section start  -->	
                                                    <input type="hidden" id="userId" value="<?php echo $_SESSION['AD_user_id']; ?>">
                                                    <input type="hidden" id="goalId" value="<?php echo $goal_vals['id']; ?>">
                                                    <!-- There fields are use for comments section end  -->	

                                                    <?php if (empty($user_infos['profile_pic'])) { ?>
                                                        <img alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png" class="img-circle" />
                                                    <?php } else { ?>
                                                        <img alt="image" src="<?php echo DEFAULT_URL . '/profile_pic/' . $user_infos['profile_pic']; ?>" class="img-circle"/>
                                                    <?php } ?>
                                                </div>
                                                <div class="porfile-name">
                                                    <h5><?php echo $user_infos['name']; ?></h5>
                                                    <p><?php echo agoFun($user_infos['registration_date']); ?></p>
                                                    <p>
                                                        <i class="fa fa-thumbs-up"></i>
                                                        <span id="likeCount_<?php echo $goal_vals['id']; ?>"><?php echo $total_count; ?></span>
                                                        Likes
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="span6 add_goal_btn">

                                                <?php
                                                if ($goal_vals['user_id'] != $_SESSION['AD_user_id']) {
                                                    $condition = " share_bucket_id= " . $goal_vals['id'] . " and user_id=" . $_SESSION['AD_user_id'] . " ";
                                                    $shareDetail = $goalObj->search_child_parent($condition);
                                                    if (empty($shareDetail)) {
                                                        ?>
                                                        <img src="<?php echo IMAGE_URL; ?>/bx_loader.gif" class="goal_loding" style="display:none; width:25px" />
                                                        <span id="copy_button_<?php echo $goal_vals['id'] ?>">
                                                            <button class="btn" type="button" onclick="copy_goal('<?php echo $goal_vals['id'] ?>', '<?php echo $_SESSION['AD_user_id'] ?>')" ><img alt="image" src="<?php echo IMAGE_URL; ?>/user.png" />Quiero hacerlo</button>

                                                        </span>
                                                    <?php } else { ?>
                                                        <button class="btn goal-aded" type="button"><i class="fa fa-check"></i>Agregado</button> 
                                                    <?php } ?>  
                                                <?php } ?>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="PaddLR10">
                                        <h3><span style="color:#5cb85c">Lo Logré!:</span> <?php echo $goal_vals['bucket_name']; ?></h3>
                                    </div>
                                    <hr />
                                    <div class="text-left autoscroll"><?php echo $goal_vals['bucket_desc']; ?> 
									<?php if($goal_vals['youtube']!='')
										  {
											echo '<p><a style="color:#0088cc" href="'.$goal_vals['youtube'].'" target="_blank">'.$goal_vals['youtube'].'</a></p>';
										  }									
									?>   

                                        <div class="">
                                            <?php
                                            $condition = "goal_id = " . $goal_vals['id'];
                                            $goal_all_comments = $goalObj->get_all_goal_comments($condition);
                                            foreach ($goal_all_comments as $comment) {
                                                $condition_user = "id = " . $comment['user_id'];
                                                $user_details = $objUserDetail->SelectDetailUserById($condition_user);
                                                ?>

                                                <div class="row-fluid">
                                                    <div class="PaddLR10">
                                                        <div class="span12">
                                                            <div class="width30">
                                                                <?php if ($user_details['profile_pic']) { ?>
                                                                    <img alt="image" src="<?php echo DEFAULT_URL; ?>/profile_pic/<?php echo $user_details['profile_pic']; ?>" class="img-circle" />
                                                                <?php } else { ?>
                                                                    <img alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png" class="img-circle" />
                                                                <?php } ?>
                                                            </div>
                                                            <div class="porfile-name">
                                                                <p ><a class="blue" href="<?php echo MODULE_URL ?>/home/profilefollow.php?id=<?php echo $user_details['id'] ?>"><?php echo $user_details['name']; ?></a></p>
                                                                <p><?php echo $comment['comment']; ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            <?php } ?></div></div>
                                    <div id="commentDiv<?php echo $goal_vals['id']; ?>">
                                    </div>
                                    <div class="row-fluid">
                                        <div id="likedgoal" class="PaddLR10">
                                            <div class="leftbut like_cls_<?php echo $goal_vals['id']; ?>">
                                                <a href="javascript:void(0)" <?php if ($total_count_ip <= '0') { ?> onclick="goalLike(<?php echo $goal_vals['id']; ?>,<?php echo $_SESSION['AD_user_id']; ?>)" <?php } ?> class="btn">
                                                    <span id="likes<?php echo $goal_vals['id']; ?>">
                                                        <i style="margin-top: 6px !important" class="fa fa-thumbs-up fa-lg"></i>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="leftinp">
                                                <input required class="input-xlarge" maxlength="50" id="commentsId_<?php echo $goal_vals['id']; ?>" type="text" placeholder="Comenta algo agradable" onkeypress="return goalComment(event, '<?php echo $goal_vals['id']; ?>')">
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- Modal end -->
            </ul>
        </div>



        <div class="cls"></div>
        <form id="account_name" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="f1" enctype="multipart/form-data" >
            <div id="NameEditContainer" style="display: none">
                <div class="width25">Cambia tu nombre:</div>
                <div class="width75">
                    <input name="name" type="text" value="<?php echo $userInfo['name']; ?>">
                </div>
                <input name="action" type="hidden" value="NameSave" />
                <div class="bdr-ir cls">
                    <button onclick="submitForm2()" class="btn btn-success">Guardar cambios</button>
                </div>
            </div>
        </form>

        <div class="cls"></div>
        <div class="cls"></div>
        <form id="description_name" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="f2" enctype="multipart/form-data" >
            <div id="DescriptionEditContainer" style="display: none">
                <div class="width25">Editar descripción:</div>
                <div class="width75">
                    <textarea name="description"><?php echo $userInfo['description']; ?></textarea>
                </div>
                <input name="action" type="hidden" value="descriptionsave" />
                <div class="bdr-ir cls">
                    <button class="btn btn-success">Guardar cambios</button>
                </div>
            </div>
        </form>
        <div class="cls"></div>
    </div>
</div>

<!-- Modal Modified By Irfaan sir 18th June 2014--> 
<!-- Modal -->
<div id="myModal" class="modal ir hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="login-video">
        <div class="container">
            <div class="row-fluid">
                <div class="span12 step">
                    <div class="login-logo"> <img alt="logo" src="<?php echo IMAGE_URL; ?>/logo.png"> </div>
                    <div class=" PdTop40 ft16"> Creando tu Perfil... </div>
                    <div class="progress progress-striped">
                        <div class="bar" style="width: 75%;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal2" class="modal ir hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="login-video">
        <div class="container">
            <div class="row-fluid">
                <div class="span12 step">
                    <div class="login-logo"> <img alt="logo" src="<?php echo IMAGE_URL; ?>/logo.png"> </div>
                    <div class=" PdTop40 ft16"> Creando tu Perfil... </div>
                    <div class="PdTop20"> <a href="succes.html"> <img alt="icon" src="<?php echo IMAGE_URL; ?>/checkmark.png"></a> </div>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- to share unlocked goal -->

<div id="share_unocked_goal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button onclick="location.href = '<?php echo MODULE_URL; ?>/home/mi_prefil.php'" type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×
        </button>
        <h3 id="myModalLabel">&nbsp;</h3>
    </div>
    <? /*
     * Modal for Profile Photo
     */
    ?>
    <form id="" action="<?php echo MODULE_URL; ?>/home/mi_prefil.php" method="post" name="f2" enctype="multipart/form-data" >
        <div class="modal-body">
            <div class="popup">
                <div>
                    <div class="share-goal-image"><img alt="image" src="<?php echo IMAGE_ADMIN_GOAL_URL . "/" . $achieved_goal_img['images']; ?>" class=""></div>
                    <div class="share-goal-info">
                        <div><h1><?php echo $userdata1['name']; ?> Comparte tu Logro!</h1></div>
                        <p>Al compartirlo con tus Amigos
                            Ganarás más Coins, los cuáles
                            te permitirán seguir ganando Dinero
                            Para lograr más aventuras
                        </p>
                        <p><img src="<?php echo IMAGE_URL; ?>/coin-group.jpg" alt="icon"></p>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <div class="fl">
                <a href="<?php echo MODULE_URL; ?>/home/mi_prefil.php">No quiero ganar Coins </a></div>
            <div class="share-but-ir">
                <span>
                    <a target="_blank" href="https://twitter.com/intent/tweet?&text=<?php echo "Logre cumplir mi sueño! de " . $share_popup_unlocked_goal[0]['bucket_name'] . " en Proyectocoin " . $_SESSION['User_url'] . "?gid=".$bucket_share_id ?>">
                        <img src="<?php echo IMAGE_URL; ?>/twitter.png" alt="icon">
                    </a> &nbsp;
                    <?php
                    $send_array = array(
                        "goal" => $bucket_share_id,
                        "user_id" => $user_id,
                        "user_url" => $_SESSION['User_url']
                    );
                    $s_send_array = serialize($send_array);
                    $a_send_array = str_replace('"', "--", $s_send_array);
                    ?>
                    <a id="fbclick" target="_blank" href="http://www.facebook.com/share.php?u=<?php echo MODULE_URL ?>/home/share_unlockedbucket_fb.php?goal=<?php echo $bucket_share_id; ?>&cur_time=<?php echo time()?>">
                        <img src="<?php echo IMAGE_URL; ?>/fb.png" alt="icon" id="share_button"> 
                    </a>
                </span>
            </div>
    </form>
    <? /*
     * End of Modal
     */
    ?>
</div>
</div>



<?php
/** This code use for comment putting * */
$profilePic = $userCommentPuter = "";
if (!empty($id)) {
    $profilePic = $user_details['profile_pic'];
    $userCommentPuter = $user_details['name'];
} else {
    $profilePic = $userInfo['profile_pic'];
    $userCommentPuter = $userInfo['name'];
}
?>


<script src='https://cdn.rawgit.com/Pagawa/PgwSlideshow/master/pgwslideshow.min.js'></script>
<link rel='stylesheet' href='https://cdn.rawgit.com/Pagawa/PgwSlideshow/master/pgwslideshow.min.css'>
<script>
            $(document).ready(function () {
                $('.pgwSlideshow').pgwSlideshow({
                    autoSlide: true,
                    intervalDuration: 5000
                });
            });
</script>


<script type="text/javascript">


//    function dreams(id) {
//
//        $.ajax({
//            type: "POST",
//            url: "<?php // echo DEFAULT_URL . '/modules/home/ajax/get_dreams_ajax.php'; ?>",
//            data: {user_id: id},
//            success: function (data) {
//
//                console.log(data);
//                alert(data);
//                var d = $.trim(data);
//                $("#accordion2").html(d);
//
//            }
//        });
//    }

    function goalComment(e, goalId) {
        var commentsVal, userId;
        if (e.keyCode == '13') {
            commentsVal = $('#commentsId_' + goalId).val();
            if (commentsVal == '') {
                return false;
            } else {
                userId = $("#userId").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo DEFAULT_URL . '/modules/home/ajax/goal_comment_ajax.php'; ?>",
                    data: {goal_id: goalId, user_id: userId, comment: commentsVal},
                    success: function (data) {
						
                        $('#commentsId_' + goalId).val("");
                        $('#commentDiv' + goalId).append('<div class="row-fluid"><div class="PaddLR10"><div class="span12"><div class="width30"><?php if ($profilePic) { ?><img alt="image" src="<?php echo DEFAULT_URL; ?>/profile_pic/<?php echo $profilePic; ?>"><?php } else { ?><img alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png"><?php } ?></div><div class="porfile-name"><p class="blue"><?php echo $userCommentPuter; ?></p><p>' + commentsVal + '</p></div></div></div></div>');
                    }
                });
                return false;
            }
        }
    }

    function goalLike(goal_id, user_id) {
        $.ajax({
            type: "POST",
            url: "<?php echo DEFAULT_URL . '/modules/home/ajax/goal_achived_like_ajax.php'; ?>",
            data: {goal_id: goal_id, user_id: user_id},
            success: function (data) {
                var totalLike = jQuery('#likeCount_' + goal_id).html();
                totalLike = parseInt(totalLike);
                jQuery('.like_cls_' + goal_id).html('<a href="javascript:void(0)" class="btn"><span><i class="fa fa-thumbs-up fa-lg\"\></i></span></a>');

                jQuery('#likeCount_' + goal_id).html(totalLike + 1);
            }
        });
    }

    function copy_goal(goal_id, user_id) {

        jQuery('.goal_loding').show();
        $.ajax({
            type: "POST",
            url: "<?php echo DEFAULT_URL . '/modules/home/code/goal_code.php'; ?>",
            data: {goalid: goal_id, user_id: user_id, action: 'add_goal_in_my_list'},
            success: function (data) {

                jQuery('.goal_loding').hide();
                jQuery('#copy_button_' + goal_id).html('<button class="btn goal-aded" type="button"><i class="fa fa-check"></i> Agregado</button>');
            }
        });

    }
    function show_name_form() {
        $('#NameEditContainer').show();
    }
    function descrip_name_form() {
        $('#DescriptionEditContainer').show();
    }
    function submitForm1() {
        $("#account_name").submit();
    }
    function submitForm2() {
        $("#description_name").submit();
    }
    function submitForm3() {
        $("#background_image_form_name").submit();
    }

    function next_goal(val)
    {
        //alert('ffff: '+val);
        //alert('Data:  '+$('#'+val).nextAll('.current_popup:first').attr('id'));
        var data = '';
        var nextId = '';
        if ($('#' + val).nextAll('.current_popup:first').attr('id'))
            nextId = $('#' + val).nextAll('.current_popup:first').attr('id');

        //alert(nextId);
        data = $('#' + val);
        if (nextId != '')
        {
            //alert(nextId);

            $('.current_popup').hide();
            $('.current_popup').prevAll('.current_popup:first').removeClass("in");


            $('.current_popup').prevAll('.current_popup:first').hide();
            $('#' + val).attr('aria-hidden', 'true');
            data.nextAll('.current_popup:first').attr('aria-hidden', 'false');
            data.nextAll('.current_popup:first').addClass("in");
            data.nextAll('.current_popup:first').show();
        } else {
            $('.current_popup').hide();
            $('.current_popup').nextAll('.current_popup:first').removeClass("in");
            $('.current_popup').nextAll('.current_popup:first').hide();
            $('#' + val).attr('aria-hidden', 'true');
            data.prev('.current_popup').attr('aria-hidden', 'false');
            $('.current_popup:first').addClass("in");
            $('.current_popup:first').show();
        }

    }
    function previous_goal(val)
    {
        //alert('ffff: '+val);
        //alert($('#'+val).prevAll('.current_popup:first').attr('id'));
        var data = '';
        data = $('#' + val);

        if (data.prevAll('.current_popup:first').attr('id') != undefined) {
            //alert(nextId);
            //alert('ssss');		
            $('.current_popup').hide();
            $('.current_popup').nextAll('.current_popup:first').removeClass("in");
            $('.current_popup').nextAll('.current_popup:first').hide();
            $('#' + val).attr('aria-hidden', 'true');
            data.prevAll('.current_popup:first').attr('aria-hidden', 'false');
            data.prevAll('.current_popup:first').addClass("in");
            data.prevAll('.current_popup:first').show();
        } else {
            $('.current_popup').hide();
            data.prevAll('.current_popup:first').removeClass("in");
            data.prevAll('.current_popup:first').hide();
            $('#' + val).attr('aria-hidden', 'true');
            data.nextAll('.current_popup:first').attr('aria-hidden', 'false');
            $('.current_popup:last').addClass("in");
            $('.current_popup:last').show();
        }

    }


    function close_popup(val)
    {
        //alert('dd');
        $('.modal-backdrop').remove( );
        $('#myModal' + val).hide();
    }

	function delete_complete_goel(val)
	{
		if (confirm('Are you sure you want to delete this goal.')) {
			$.ajax({
				type: "POST",
				url: "<?php echo DEFAULT_URL . '/modules/home/code/delete_goel.php'; ?>",
				data: {goalid: val, action: 'delete_goel'},
				success: function (data) {
					//alert(data);
					jQuery('#goel_li_'+val).hide();
				}
			});
		}
	}
	
</script> 


<style>
    .slides {
        width: 96%;
        margin:10px auto 0;
    }

    .slides ul {
        list-style: inside none disc;
        margin: 0;
        padding: 0;
    }
    .pgwSlideshow .ps-current {
        height: 340px !important;
    }
    .pgwSlideshow .ps-current>ul>li img {
        display: block;
        max-width: 100%;
        margin: auto;
        height: auto;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }
    span.ps-caption {
        display: none !important;
    }
    .pgwSlideshow .ps-list li .ps-item {
        margin: 0;
    }
    .pgwSlideshow .ps-list li .ps-item.ps-selected {
        border: inherit;
    }
    .pgwSlideshow .ps-list li .ps-item.ps-selected img {
        margin:auto;
    }
    .pgwSlideshow {
        background: none;
    }
    .profile_images .slides ul li {
        padding: 0;
        height: 340px !important;
        position: relative;
    }
    .slides .pgwSlideshow .ps-list li {
        float: left;
        padding: 0;
        margin: 0;
        width: 100px !important;
        height: 100px !important;
        position: relative;
    }
    .pgwSlideshow .ps-list {
        border-top: none;
        box-shadow: none;
        background: none;
        margin-top: 10px;
        height: 100px;
    }
    .pgwSlideshow.narrow .ps-list li img {
        width: 100px;
        height: auto;
        border:none;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        margin:auto;
    }
    a.pop-left {
        position: absolute;
        top: 43%;
        left: -60px;
        transform: rotate(180deg);
    }
    a.pop-right {
        position: absolute;
        top: 43%;
        right: -60px;
    }
    a.pop-close {
        right: 5px;
        font-size: 20px;
        position: absolute;
        font-weight: bold;
        z-index: 999;
    }
    .profile_images ul.miprefil li{ height:215px; position:relative}
    .profile_images ul.miprefil li a.prefilimg{ display:inline-block; width:100%; height:100%}
    .profile_images ul.miprefil li img {
        width: 100% !important;
        /*  height: auto !important; */
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        margin: auto;
    }
    .profile_images ul.miprefil li img {
        width: 100% !important;
        /*  height: auto !important; */
    }
    .profile_images ul.miprefil li .relative {
        border: 1px solid #eee;
    }
</style>
<?php
include_once(SITE_ROOT . "/js_php/bucket_list.js.php");
