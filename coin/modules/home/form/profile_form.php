<style>
	body {
		background: #ffffff;
	}

</style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#form").validationEngine('attach');
	}); 
</script>

<script type="text/javascript">
	function submitForm() {
		$("#form").submit();
	}
</script>

<header class="white_bg">
	<div class="container">
		<div class="row-fluid">
			<div class="span12 logo-profile">
				<img src="<?php echo IMAGE_URL; ?>/logo.png">
			</div>
		</div>
	</div>
</header>
<!--| Header End |-->
<div class="main_bg testone" style="display:block;" id="mainbg">
	<img style="width:100%; height:100%;" src="<?php echo IMAGE_URL; ?>/profile-bg.jpg" alt="">
</div>
<div class="profil_bg">

	<?php $userDetails = $objRegister -> getOnId($_SESSION['AD_user_id']);
	$email = $userDetails['username'];
?>
	<div class="container po_absolute">
		<div class="row-fluid">
			<div class="span12">
				<div class="main">
					<div class="video-frame">
						<div class="video-container">
							<iframe src="//fast.wistia.net/embed/iframe/7cw3wn2urj" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="360"></iframe>
						</div>
					</div>
					<div id="countdown1" style="color:#8EC324;font-size:19px;font-weight:bold;">
						<!-- Para unirte a la Comunidad espera a que termine el video -->
						<div id="showtimer"></div>
						<div class="cls"></div>
						<div>

							<div class="cls"></div>
						</div>
						<div class="cls"></div>
					</div>

					<?php if(isset($_SESSION['Msg']) && $_SESSION['Msg']!=''){?>
					<span id="error" style="color:red;padding-left:0px;"> <?php echo $_SESSION['Msg']; ?> </span>

					<?php } ?>

					<div class="profile_form"  id="profile">
						<form id="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="f1" enctype="multipart/form-data" >
							<div class="left">
								<img src="<?php echo IMAGE_URL; ?>/left_arrow.png">
							</div>
							<div class="right">
								<img src="<?php echo IMAGE_URL; ?>/down_arrow.png">
							</div>
							<div class="pro_head" style="color:#8EC324;">
								<span class="blue"><strong></strong> <?php echo $email; ?><
									br/> </span> Para unirte a la Comunidad espera a que termine el video
							</div>
							<div class="input_field" style="display:none;">
								<input type="text" class="search-query inplogin validate[required]" value="" name="username" placeholder="nombre de usuario">
								<input type="password" class="search-query inplogin validate[required]" value="" name="password"  placeholder="contraseña">
								<input name="action" type="hidden" value="add" />
								<input type="submit" onkeypress="return submitForm()"  style="display: none;">
								<a href="javascript:void(0)" onclick="return submitForm();"> <span> &nbsp; </span></a>
							</div>
							<?php if(isset($_SESSION['passmsg']) && $_SESSION['passmsg']!='')
								{
							?>
							<span id="error1" style="color:red;padding-left:0px;"> <?php echo $_SESSION['passmsg']; ?> </span>
							<?php } ?>
							<div class="text">
								<!-- “Tu contraseña debe tener 8 caracteres mínimo” -->
							</div>
							<div class="cls"></div>
							<input name="action" type="hidden" value="update" />
							<input name="email" type="hidden" value="<?php echo $email; ?>" />
						</form>
					</div>
					<div class="cls"></div>
					</body>
					</html>

					<?php unset($_SESSION['passmsg']); ?>
					<?php unset($_SESSION['Msg']);