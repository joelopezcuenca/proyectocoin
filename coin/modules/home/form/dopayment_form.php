﻿<div class="left_part">
		<?php include_once (INCLUDE_PATH . "/home_left_navigation.php");?>
	</div>
	<div class="right_part">     
		<div class="boxes round mis-gana">
              <div><img src="<?php echo IMAGE_URL;?>/co.png" alt="icon"></div>
              <div class="mis-banner">
                <!-- <img src="<?php echo IMAGE_URL;?>/mis-banner.jpg" alt="banner"> -->
                
                <img src="<?php echo IMAGE_URL;?>/xmis-banner.jpg" alt="banner">
                
                <div class="banner-info">
                  <!-- <h1>Activa el valor de tus Coins <span>y únete a una comunidad de personas que ya están cumpliendo sus sueños</span></h1>
                 --> <a class="orange-but" href="<?php echo MODULE_URL ;?>/payment/php_files/make_payment.php?userid=<?php echo $_SESSION['AD_user_id']?>">Solo $5 dolares</a>
                  <!--<h2>PayPal</h2>-->
                  <div class="cls"></div>
                </div>
                <!-- <img class="white-arrow" src="<?php echo IMAGE_URL;?>/white-arrow.png" alt="icon"> -->
              </div>
              
              <div class="mis-yellow height67">
              	<img class="medal" src="<?php echo IMAGE_URL;?>/medal.png" alt="icon">
                <p><strong>Nuestra Garantía</strong>:<br>Sí en <strong>15 días</strong> no has generado ninguna ganancia, te devolvemos tu dinero.</p>
              </div>
              
              <h1 class="green-head">Tal vez te preguntes...</h1>
              <h2 class="mis-grey-text">¿Qué beneficios recibo a cambio de pagar $5usd al mes?</h2>
              
              <div class="mis-discription">
                <div class="mis-width25"><img src="<?php echo IMAGE_URL;?>/mis-money.png" alt="icon"></div>
                <div class="mis-width75">1.- Al final de cada mes podrás cambiar  todos tus Coins por dinero real para poder cumplir los sueños de tu lista.</div>
              </div>
              <div class="mis-discription">
                <div class="mis-width25"><img src="<?php echo IMAGE_URL;?>/mis-paypal.png" alt="icon"></div>
                <div class="mis-width75">2.- Todas tus ganancias serán enviadas y depositadas de manera segura a través de PayPal mes con mes.</div>
              </div>
              <div class="mis-discription">
                <div class="mis-width25"><img src="<?php echo IMAGE_URL;?>/mis-comment.png" alt="icon"></div>
                <div class="mis-width75">3.- Tendrás acceso a los foros de la Comunidad Coin, en donde podrás conectar con personas que tengan sueños similares a los tuyos y compartir experiencias o tal vez lograr sueños en conjunto.</div>
              </div>
              <div class="mis-discription">
                <div class="mis-width25"><img src="<?php echo IMAGE_URL;?>/mis-ticket.png" alt="icon"></div>
                <div class="mis-width75">4.- Tendrás acceso exclusivo para asistir al Coin Fest 2015 evento que  llevaremos acabo en a finales de año en un lugar secreto. En este evento podrás conectar con otros miembros de la comunidad, presenciar las platicas de nuestros speakers de talla internacional y por supuesto mucha diversión.</div>
              </div>
              
                <div class="mis-yellow">
                  <a href="#misganacias_backup" data-toggle="modal"><img class="mis-play" src="<?php echo IMAGE_URL;?>/play-mis.png" alt="icon"></a>
                  <span>Mira lo que otros miembros de la Comunidad tienen que decir</span>
                  <img class="mis-one" src="<?php echo IMAGE_URL;?>/mis-one.png" alt="icon">
                  <div class="cls"></div>
                </div>
                
                <div class="mis-green">
                  <p><strong>El mejor beneficio de todos</strong><br> Es que finalmente podrás cumplir cualquier sueño que te propongas...</p>
                  <p class="mrb10">Debemos aclarar que cuando mencionamos cumplir cualquier sueño, nos referimos a aquellos sueños en donde el dinero sea una limitante.<br><em>*No aplican sueños como: Ser futbolista del Real Madrid :)</em></p>
                  <img src="<?php echo IMAGE_URL;?>/mis-coin.png" alt="icon">
                  <a href="<?php echo MODULE_URL ;?>/payment/php_files/make_payment.php?userid=<?php echo $_SESSION['AD_user_id']?>" class="red-but">Comenzar</a>
                  <div class="cls"></div>
                </div>
                <div id="misganacias_backup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			
		      
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">&nbsp;</h3>
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>
		
			<div class="modal-body">
				<div class="popup">
					<div>
	<iframe src="//fast.wistia.net/embed/iframe/ik26kpyvw4" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="360"></iframe>
</div>
					
				</div>
			</div>
			<div class="modal-footer">
				
	</div>
</div>
                <div class="cls"></div>
            </div>
	</div>
<div class="cls"></div>

