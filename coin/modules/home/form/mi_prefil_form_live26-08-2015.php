﻿<style type="text/css">
.profile-image2 {
	padding: 20px 0;
	width: 200px;
}
</style>
<div class="left_part">
  <?php
	include_once (INCLUDE_PATH . "/home_left_navigation.php");
	?>
</div>
<div class="right_part">
  <div class="boxes round mi-prefil">
    <?php
	//echo "<pre>"; print_r($userdetails);die;	
		$text = 0;
		if ($userdetails['profile_pic'] != '') {
			
			$text = $text + 25;
		}
		if ($userdetails['description'] != '') {
			$text = $text + 25;
		}
		if(count($checking_for_goal)>0){
			$text = $text + 25;
		}
		if(count($checking_for_follow_user)>0){
			$text = $text + 25;
		}
 ?>
    <div class="text-left Padd15N name">
		<i class="fa fa-chevron-down Padd10"></i>
		<?php 
			if($current_pageName =='digitalPassport'){ 
				echo "Metas Logradas : ".count($goal_array_achieveds);
			}elseif($current_pageName =='profileFollow'){ 
				echo "Regresar a la comunidad";
			}elseif($current_pageName =='miProfile'){
				echo "Mi Perfil";			
			}
		?>
	</div>
    <div class="cls"></div>
		<?php if(isset($_SESSION['image_error'])) { ?>
			<span style="color: red"><b><?php echo $_SESSION['image_error'] ; ?></b></span>
			<div class="cls"></div>
		<?php unset($_SESSION['image_error']) ;}  ?>
		<?php if($current_pageName == 'miProfile' || $current_pageName == 'profileFollow'){ ?>
			<div class="cover-page">
				<?php if($userInfo['background_pic'] != ''){ ?>
				<img alt="image" src="<?php echo IMAGE_ADMIN_BACKGROUND_URL . "/" . $userInfo['background_pic']; ?>" class="cover-image">
				<?php }else{ ?>
				<img  alt="image" src="<?php echo IMAGE_URL; ?>/profile-back-pic.jpg" class="cover-image">
				<?php } ?>
				<?php if($userInfo['profile_pic'] != ''){ ?>
				<img class="profile-image" alt="image" src="<?php echo IMAGE_ADMIN_PROFILE_URL . "/" . $userInfo['profile_pic']; ?>">
				<?php }else{ ?>
				<img class="profile-image" alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png">
				<?php } ?>
				<h1 class="name2"><?php echo $userInfo['name']; ?></h1>
				<h3 class="name2"><?php echo $userInfo['username']; ?></h3>
				<?php 
				$Idname = 'followmee';
				if($current_pageName != 'profileFollow' && empty($id)){ 
					$Idname = '';
				 } ?>
					<div id="<?php echo $Idname;?>">
						<?php if($current_pageName=='profileFollow'){ ?>
							<?php 
							// Check if a User already follwed to someone or not to show unfollow button there
							if(count($CheckDataFollows)>0){?>
								<a id="unfollow" class="" href="javascript:void(0);" onclick="unfollow_someone('<?php echo $userInfo['id'];?>','<?php echo $_SESSION['AD_user_id'];?>');"><i class="fa fa-user-plus gold paddR5"></i> Dejar de Seguir</a>
							<?php } else {  ?>
								<a id="follow" class="" href="javascript:void(0);" onclick="follow_someone('<?php echo $userInfo['id'];?>','<?php echo $_SESSION['AD_user_id'];?>');"><i class="fa fa-user-plus gold paddR5"></i> Seguir</a>
							<?php } ?>
						<?php } ?>	
						<?php if($current_pageName != 'profileFollow' && empty($id)){ ?>
							<a class="change-bg" data-toggle="modal" role="button" href="<?php echo MODULE_URL.'/home/account-setting.php';?>">Edit  profile</a> 
						<?php } ?>
					</div>
			</div>
			<div class="MarB20">
			  <div class="Font11N left160 text-left">
				<p>Build a Billion-Dollar Legacy</p>
				<p>Inquiries@TheBillionairesClubInc.com </p>
				<p ><a class="blue" href="<?php echo $userInfo['website'];?>"><?php echo $userInfo['website'];?></a></p>
			  </div>
			  <div class="rightbox">
				<ul>
				  <li><?php if($totalGoal>0){ echo $totalGoal; }else{ echo '0'; }?> <span>Sueños</span></li>
				  <li><?php echo count($userachievedgoalcount) ?> <span>Logros</span></li>
				  <li><?php echo count($follow) ?> <span>followers</span></li>
				  <li><?php echo count($following_var) ?> <span>following</span></li>
				</ul>
			  </div>
			  <div class="clearfix"></div>
			</div>
	<?php } ?>
    <hr class="Mar0" />
	
    <div class="round profile_images">
		<ul>
		<?php foreach($goal_array_achieveds as $goal_vals){ 		
			$goalImages = $goalobj->select_all_user_goal_images($goal_vals['user_id'],$goal_vals['id']); 				
			?>
		
			<li href="#myModal<?php echo $goal_vals['id']; ?>" role="button" data-toggle="modal">
				<div class="relative">
					<a href="#">
						<img style="width:231px; height:215px;" alt="image" src="<?php echo DEFAULT_URL; ?>/goal_img/<?php echo $goal_vals['bucket_image']; ?>">
					</a>
					<div class="showdiv">
						<div class="padd40">
							<h2><?php echo substr($goal_vals['bucket_name'],0,20); ?></h2>
							<p><?php 
							
							if(strlen($goal_vals['bucket_mdesc_l'])>50){
								echo substr($goal_vals['bucket_mdesc_l'],0,50).'...';
							}else{
								echo substr($goal_vals['bucket_mdesc_l'],0,50);
							}	

							?></p>
							<div class="width125">
								<div class="like">
									<a href="#">
										<i class="fa fa-heart-o"></i>
										<?php 
										$conditions = "achieved_goal_id = ".$goal_vals['id'];
										$likes = $goalObj->goal_count_likes($conditions);
										$total_count = count($likes);
										echo $total_count;	?> LIKE
									</a>
								</div>
								
								<div class="cls"></div>
							</div>
						</div>
					</div>
				</div>
			</li>
			<!-- Modal start -->
			<div id="myModal<?php echo $goal_vals['id']; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-body">
					<div class="row-fluid">
						<div class="span7">
							
							<div id="myCarousel" class="carousel slide">
							  <!-- Carousel items -->
							  <div class="carousel-inner">
							  <?php
								$imageCounter = 1;
							  foreach($goalImages as $value2){
								  if($imageCounter==1)
										$classActive = 'active';
								   else	
									    $classActive = '';
									
								$imageCounter++;	
								  
								  ?>
								<div class=" <?php echo $classActive;?> item"><img alt="image" src="<?php echo DEFAULT_URL; ?>/goal_img/<?php echo $value2['images']; ?>"></div>
							  <?php } ?>
							  </div>
							  <!-- Carousel nav -->
							  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
							  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
							</div>
							
						</div>
						<div class="span5 PaddTB10">
							<div class="row-fluid">
								<div class="PaddLR10">
									<div class="span6">
										<div class="width40">
											<?php 
												$conditions = "id = ".$goal_vals['user_id'];
												$user_infos = $objUser->SelectAll_User($conditions);
												//echo "<pre>"; print_r($user_infos);
											?>

											<!-- There fields are use for comments section start  -->	
											<input type="hidden" id="userId" value="<?php echo $_SESSION['AD_user_id']; ?>">
											<input type="hidden" id="goalId" value="<?php echo $goal_vals['id']; ?>">
											<!-- There fields are use for comments section end  -->	
											
											<?php if(empty($user_infos['profile_pic'])){ ?>
												<img alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png">
											<?php }else{ ?>
												<img alt="image" src="<?php echo DEFAULT_URL.'/profile_pic/'.$user_infos['profile_pic']; ?>">
											<?php } ?>
										</div>
										<div class="porfile-name">
											<h5><?php echo $user_infos['name']; ?></h5>
											<p><?php echo agoFun($user_infos['registration_date']); ?></p>
											<p>
												<i class="fa fa-thumbs-up"></i>
													<span id="likeCount_<?php echo $goal_vals['id']; ?>"><?php echo $total_count;	?></span>
												Likes
											</p>
										</div>
									</div>
									
									<div class="span6">
									
									<?php if($goal_vals['user_id']!=$_SESSION['AD_user_id']){ 
									$condition = " share_bucket_id= ".$goal_vals['id']." and user_id=".$_SESSION['AD_user_id']." ";
									$shareDetail = $goalObj->search_child_parent($condition);
									if(empty($shareDetail)){
									?>
									<img src="<?php echo IMAGE_URL; ?>/bx_loader.gif" class="goal_loding" style="display:none; width:25px" />
									<span id="copy_button_<?php echo $goal_vals['id']?>">
										<button class="btn" type="button" onclick="copy_goal('<?php echo $goal_vals['id']?>','<?php echo $_SESSION['AD_user_id']?>')" ><img alt="image" src="<?php echo IMAGE_URL; ?>/user.png" /> Add Goal</button>
										
										</span>
									<?php } else{ ?>
                                     <button class="btn goal-aded" type="button"><i class="fa fa-check"></i> Goal Added</button> 
									<?php } ?>  
									<?php } ?>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<hr/>
							<div class="PaddLR10">
							  <h3><span>Goal:</span> <?php echo $goal_vals['bucket_name']; ?></h3>
							</div>
							<hr />
							<div class="text-left"><?php echo $goal_vals['bucket_desc']; ?>    
							</div>
							<div class="height310">
							<?php 
								$condition = "goal_id = ".$goal_vals['id'];
								$goal_all_comments = $goalObj->get_all_goal_comments($condition);
								foreach($goal_all_comments as $comment){ 
									$condition_user = "id = ".$comment['user_id'];
									$user_details =	$objUserDetail -> SelectDetailUserById($condition_user);
								?>
                                
								<div class="row-fluid">
								<div class="PaddLR10">
									<div class="span12">
										<div class="width30">
											<?php if($user_details['profile_pic']){ ?>
												<img alt="image" src="<?php echo DEFAULT_URL; ?>/profile_pic/<?php echo $user_details['profile_pic']; ?>">
											<?php }else{ ?>
												<img alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png">
											<?php } ?>
										</div>
										<div class="porfile-name">
											<p class="blue"><?php echo $user_details['name'] ;?></p>
											<p><?php echo $comment['comment']; ?></p>
										</div>
									</div>
								</div>
								</div>
                                
							<?php }  ?></div>
							<div id="commentDiv<?php echo $goal_vals['id']; ?>">
							</div>
							<div class="row-fluid">
								<div id="likedgoal" class="PaddLR10">
									<div class="leftbut">
										<a href="javascript:void(0)" <?php if($total_count <= '0'){ ?> onclick="goalLike(<?php echo $goal_vals['id']; ?>,<?php echo $_SESSION['AD_user_id']; ?>)" <?php } ?> class="btn">
											<span id="likes<?php echo $goal_vals['id']; ?>">
												<i class="fa fa-thumbs-up fa-lg"></i>
											</span>
										</a>
									</div>
									<div class="leftinp">
										<input required class="input-xlarge" maxlength="50" id="commentsId_<?php echo $goal_vals['id']; ?>" type="text" placeholder="Enter your comment" onkeypress="return goalComment(event,'<?php echo $goal_vals['id']; ?>')">
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
			<!-- Modal end -->
		</ul>
    </div>

	
	
  <div class="cls"></div>
  <form id="account_name" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="f1" enctype="multipart/form-data" >
    <div id="NameEditContainer" style="display: none">
      <div class="width25">Cambia tu nombre:</div>
      <div class="width75">
        <input name="name" type="text" value="<?php echo $userInfo['name']; ?>">
      </div>
      <input name="action" type="hidden" value="NameSave" />
      <div class="bdr-ir cls">
        <button onclick="submitForm2()" class="btn btn-success">Guardar cambios</button>
      </div>
    </div>
  </form>
  
  <div class="cls"></div>
  <div class="cls"></div>
  <form id="description_name" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="f2" enctype="multipart/form-data" >
    <div id="DescriptionEditContainer" style="display: none">
      <div class="width25">Editar descripción:</div>
      <div class="width75">
        <textarea name="description"><?php echo $userInfo['description']; ?></textarea>
      </div>
      <input name="action" type="hidden" value="descriptionsave" />
      <div class="bdr-ir cls">
        <button class="btn btn-success">Guardar cambios</button>
      </div>
    </div>
  </form>
  <div class="cls"></div>
</div>
</div>

<!-- Modal Modified By Irfaan sir 18th June 2014--> 
<!-- Modal -->
<div id="myModal" class="modal ir hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="login-video">
    <div class="container">
      <div class="row-fluid">
        <div class="span12 step">
          <div class="login-logo"> <img alt="logo" src="<?php echo IMAGE_URL; ?>/logo.png"> </div>
          <div class=" PdTop40 ft16"> Creando tu Perfil... </div>
          <div class="progress progress-striped">
            <div class="bar" style="width: 75%;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="myModal2" class="modal ir hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="login-video">
    <div class="container">
      <div class="row-fluid">
        <div class="span12 step">
          <div class="login-logo"> <img alt="logo" src="<?php echo IMAGE_URL; ?>/logo.png"> </div>
          <div class=" PdTop40 ft16"> Creando tu Perfil... </div>
          <div class="PdTop20"> <a href="succes.html"> <img alt="icon" src="<?php echo IMAGE_URL; ?>/checkmark.png"></a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
/** This code use for comment putting **/
	$profilePic = $userCommentPuter = "";
	if(!empty($id)){
		$profilePic = $user_details['profile_pic'];
		$userCommentPuter = $user_details['name'];
	}else{
		$profilePic = $userInfo['profile_pic'];
		$userCommentPuter = $userInfo['name'];
	}


 ?>


<script type="text/javascript">

	function goalComment(e,goalId){
		var commentsVal,userId;
		if(e.keyCode == '13'){ 		
			commentsVal = $('#commentsId_'+goalId).val();
			if(commentsVal==''){
				return false;
			}else{
				userId = $( "#userId").val();
				$.ajax({
				  type: "POST",
				  url: "<?php echo DEFAULT_URL.'/modules/home/ajax/goal_comment_ajax.php'; ?>",
				  data: { goal_id : goalId , user_id : userId , comment : commentsVal },
				  success: function(data) {
					$('#commentsId_'+goalId).val("");
					$('#commentDiv'+goalId).append('<div class="row-fluid"><div class="PaddLR10"><div class="span12"><div class="width30"><?php if($profilePic){ ?><img alt="image" src="<?php echo DEFAULT_URL; ?>/profile_pic/<?php echo $profilePic; ?>"><?php }else{ ?><img alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png"><?php } ?></div><div class="porfile-name"><p class="blue"><?php echo $userCommentPuter ;?></p><p>'+commentsVal+'</p></div></div></div></div>');
				  }
				});
				return false;
			}
		}
	}

	function goalLike(goal_id,user_id){
		$.ajax({
		  type: "POST",
		  url: "<?php echo DEFAULT_URL.'/modules/home/ajax/goal_achived_like_ajax.php'; ?>",
		  data: { goal_id : goal_id , user_id : user_id },
		  success: function(data) {
			  var totalLike = jQuery('#likeCount_'+goal_id).html();
			  totalLike = parseInt(totalLike);
			 
			jQuery('#likeCount_'+goal_id).html(totalLike+1);
		  }
		});
	}

	function copy_goal(goal_id,user_id){

		jQuery('.goal_loding').show();
		$.ajax({
		  type: "POST",
		  url: "<?php echo DEFAULT_URL.'/modules/home/code/goal_code.php'; ?>",
		  data: { goalid : goal_id , user_id : user_id ,action:'add_goal_in_my_list'},
		  success: function(data) {
			 
			  jQuery('.goal_loding').hide();
				jQuery('#copy_button_'+goal_id).html('<button class="btn goal-aded" type="button"><i class="fa fa-check"></i> Goal Added</button>');
		  }
		});
		
	}	
	function show_name_form() {
		$('#NameEditContainer').show();
	}
	function descrip_name_form() {
		$('#DescriptionEditContainer').show();
	}
	function submitForm1() {
		$("#account_name").submit();
	}
	function submitForm2() {
		$("#description_name").submit();
	}
	function submitForm3() {
		$("#background_image_form_name").submit();
	}
</script> 
