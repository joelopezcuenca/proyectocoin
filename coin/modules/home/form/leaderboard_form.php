﻿<div class="left_part">
    <?php
    include_once (INCLUDE_PATH . "/home_left_navigation.php");
    $user_id = $_SESSION["AD_user_id"];
    ?>
</div>
<div class="right_part">
    <div class="boxes round pasport">
        <div class="cover-page3">
            <!-- <div class="round2">
                <div class="row-fluid">
                    <div class="span4 offset8 text-right">Ver tutorial <img alt="image" src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"></div>
                </div>
            </div> -->
            <a class="fp-center" href="https://www.facebook.com/groups/proyectocoin/" target="blank"><img alt="image" src="<?php echo IMAGE_URL; ?>/facebookgroup.jpg"></a>
            <p> Conecta y comparte tus sueños con otros Coiners. Descubre nuevas aventuras y conoce personas que comparten los mismos intereses que tú. </p>
        </div>
        <div class="greybar Bor1padd5">
            <div class="row-fluid">
                <div class="span5">
                    <div class="input-prepend irf">
                        <form>
                            <span class="add-on"> <img alt="image" src="<?php echo IMAGE_URL; ?>/1404990268_67.png"></span>
                            <input type="text" class="span2" id="searchbox" value=""  onkeyup="suggest(this.value);" placeholder="Buscar amigos" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Buscar amigos'" / >
                        </form>
                    </div>
                </div>
                <div class="span7">
                    <ul>
                        <li class="le active"> <a style="color: black;" href="javascript:void(0);" onclick="categorize_leaderboard('top_coiners')">Top Coiners</a> </li>
                        <li class="po"> <a style="color: black;"  href="javascript:void(0);" onclick="categorize_leaderboard('most_popular')">Popular</a> </li>
                        <li class="mo"> <a style="color: black;"  href="javascript:void(0);" onclick="categorize_leaderboard('most_achieved')">Metas Logradas</a> </li>
                    </ul>
                </div>
            </div>
            <div id="display"></div>
        </div>
        <div class="row-fluid" id="leaderboard_data">
            <div class="span12">
                <?php
                //Only fetched five(5) record					
                for ($i = 0; $i < count($userdatam); $i++) {
					
					if($userdatam[$i]['username']=='')
						 continue;
					
                    /*
                     * Code for disabling,count follwers,etc
                     * as well as multipurpose according to
                     * condition......................
                     */
                    //$condition="UsertoFollow_Id='".$userdatam[$i]['id']."' and Followers_Id='".$_SESSION['AD_user_id']."'";
                    $condition = "Followers_Id='" . $userdatam[$i]['id'] . "'  ";
                    $following_var = $userfollowobj->selectAllFollowData($condition);
                    /*
                     * Code for count follwings,etc
                     * as well as multipurpose according to
                     * condition......................
                     */

                    $conditionfollowing = "UsertoFollow_Id='" . $userdatam[$i]['id'] . "'";
                    $conditionfollowing_peruser = "UsertoFollow_Id='" . $userdatam[$i]['id'] . "' and Followers_Id='" . $_SESSION['AD_user_id'] . "'";

                    $follow_count_foruser = $userfollowobj->selectAllFollowData($conditionfollowing_peruser);
                    $follow = $userfollowobj->selectAllFollowData($conditionfollowing);
                    $conditiongoal = "user_id='" . $userdatam[$i]['id'] . "' and is_unlock='1'";

                    //$variable=1;
                    $usergoalcount = $goalObj->User_Count_Achievements_Second($conditiongoal);
                    $condition1 = "id='" . $userdatam[$i]['id'] . "' ";
                    $userdata1 = $objUser->getOnGivenusername($condition1);
                    ?>
                    <?php 
//                    if ($userdatam[$i]['id'] != $_SESSION['AD_user_id']) {
                        ?>
                        <div id="<?php echo "leaderboard_div" . $i; ?>" class="follow">
                            <div class="icon">
                                <?php if ($userdata1['profile_pic'] != '') { ?>
                                    <img src="<?php echo IMAGE_ADMIN_PROFILE_URL; ?>/<?php echo $userdatam[$i]['profile_pic'] ?>" alt="icon">
                                <?php } else { ?>
                                    <img src="<?php echo IMAGE_URL; ?>/adminuser.png" alt="icon">
        <?php } ?>
                            </div>
                            <div class="info-follow">
                                <h3><a href="<?php echo MODULE_URL . "/home/profilefollow.php?id=" . $userdatam[$i]['id'] ?>"><span class="profile_name"><?php echo ucfirst($userdatam[$i]['name']); ?></span> @ <?php echo ucfirst($userdatam[$i]['username']); ?></a></h3>
                                <p> <em>Seguidores:<?php echo count($follow); ?> | Siguiendo:<?php echo count($following_var); ?> | Sueños logrados: <?php echo count($usergoalcount); ?></em> </p>
                            </div>

                            <div class="follow-but" id="follow_but<?php echo $i; ?>">
                                <?php
                                if (count($follow_count_foruser) < 1) {
                                    ?>
                                    <a id="follow<?php echo $i; ?>" class="" href="javascript:void(0);" onclick="follow_someone_leaderboard('<?php echo $userdatam[$i]['id']; ?>', '<?php echo $_SESSION['AD_user_id']; ?>', '<?php echo $i; ?>');">Seguir</a>
                                <?php
                                } else {
                                    ?>
                                    <a id="unfollow<?php echo $i; ?>" class="leader_unfollow_btn" href="javascript:void(0);" onclick="unfollow_someone_leaderboard('<?php echo $userdatam[$i]['id']; ?>', '<?php echo $_SESSION['AD_user_id']; ?>',<?php echo $i; ?>);">Dejar de Seguir</a>
                                <?php } ?>
                            </div>
                            <div class="cls"></div>
                        </div>
                    <?php } ?>
                <?php // } ?>
                <?php // if (count($userdatam) > 5) { ?>
                    <!--<div Cclass="loadmore-but"> <a id="loadmoreleaderboard" class="hidethis" href="javascript:void(0);">Ver más Coiners</a> </div>-->
                <?php // } ?>
                <!--<input type="hidden" name="leaderboard_count" id="leaderboard_count" value="<?php // echo count($userdatam); ?>">-->
            </div>
        </div>
        <div class="cls"></div>
    </div>
</div>
