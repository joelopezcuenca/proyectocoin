<style>
.popup .name2 {
    float: left;
    font-size: 12px;
    font-weight: bold;
    text-align: left;
    width: 25%;
    margin-bottom: 15px;
}
	
.popup .discription {
    width: 75%;
    margin-bottom: 15px;
}
.popup.detail {
    width: 100%;
}
.popup .images {
    width: 100%;
    text-align: left;
}
.popup .images img{
    width: 120px;
}
.popup .images img {
    height: 110px;
    margin: 10px 0 0 10px;
    width: 118px;
}
.positioning {
	float:none; !important
}
.irf-new-popup{padding:10px; background:#fff; float: left; width: 95%;}	
	
</style>
<?php //echo "<pre>";print_r($level);?>
<div class="boxes">
	<?php //echo $userdata['level'];?>
	<div class="cover-page">
		<?php if($userdata['background_pic'] != ''){ ?>
			<img alt="image" src="<?php echo IMAGE_ADMIN_BACKGROUND_URL . "/" . $userdata['background_pic']; ?>" class="cover-image">
			<?php }else{ ?>
				<img  alt="image" src="<?php echo IMAGE_URL; ?>/cover-page.jpg" class="cover-image">
				<?php } ?>
			<?php if($userdata['profile_pic'] != ''){ ?>
    		<img class="profile-image" alt="image" src="<?php echo IMAGE_ADMIN_PROFILE_URL . "/" . $userdata['profile_pic'];?>">
    		<?php }else{ ?>
    			<img class="profile-image" alt="image" src="<?php echo IMAGE_URL; ?>/adminuser.png">
    			<?php } ?>
             <div class="nevels">
             	
             	<?php $levelmm = $levelobj -> levelcheckm($userdata['level']);?>
			<div class="nevel-ir <?php echo $levelmm ; ?>"><?php echo $userdata['level'];?></div>
                NIVEL
            </div>
    	<h1 class="name2"><?php echo $userdata['name'];?></h1>
    </div>
    <div class="green-text"><?php echo $userdata['description'];?></div>
    <div><img src="<?php echo IMAGE_URL?>/right-arrow.jpg" alt="icon"></div>
    <div class="PdTop10 font18">Ganancias al mes: <br />
    	<span class="green">$<?=$totalincome;?></span>
    	<div class="irf-follow">
    		<div class ="follow-but positioning" id="followmee">
			<?php 
    		// Check if a User already follwed to someone or not to show unfollow button there
    		if($variables['msg']!="followed"){?>
				<a id="follow" class="" href="javascript:void(0);" onclick="follow_someone('<?php echo $userdata['id'];?>','<?php echo $_SESSION['AD_user_id'];?>');">Seguir</a>
  		    <?php }else{  ?>
				<a id="unfollow" class="" href="javascript:void(0);" onclick="unfollow_someone('<?php echo $userdata['id'];?>','<?php echo $_SESSION['AD_user_id'];?>');">Dejar de Seguir</a>
			<?php } ?>
			</div>
    </div>
    <div class="grey-bar"><span><?php echo count($userachievedgoalcount) ?> Logradas</span> <span><?php echo count($usergoalcount);?>&nbsp;Sueños</span><span><?php echo count($follow);?>&nbsp;Seguidores</span>  <span><?php echo count($following_var);?>&nbsp; Siguiendo</span></div>
   <div class="row-fluid">
        	<div class="span12">
        		<ul class="slides">
        		<?php
        		$achieved_goal_count = count($goal_array) ;
        		 for($i=0;$i<$achieved_goal_count;$i++)
				{
					$bucket_id =  $goal_array[$i]['id'] ;
				// to select single image to be dispalyed on digital passport section
				$achievement_img	=  $goalObj->select_visible_user_goal_image($userdata['id'],$bucket_id);
				// to select all images for particular achievement for detail section 
				$all_goal_img_array	=  $goalObj->select_all_user_goal_images($userdata['id'],$bucket_id);
					?>
					<li>
            	<div class="" id="<?php echo "achieve_div_".$i ?>">
                	<img src="<?php echo IMAGE_ADMIN_GOAL_URL; ?>/<?php echo $achievement_img['images'] ?>" alt="tour">
                    <h3><?php echo substr($goal_array[$i]['bucket_name'],0,50) ?></h3>
                    <p><?php echo substr($goal_array[$i]['bucket_mdesc_l'],0,60) ?></p>
                    <a class="inline" href="#view_detail_<?php echo $i ; ?>"  style="text-align: center">See More...</a>
        
           	    </div>
           	    </li>
           	    <div style='display:none'>
<div id='view_detail_<?php echo $i ; ?>' class='irf-new-popup'>
<div class="popup detail">
					<div class="name2">
						Logro Nombre :
					</div>
					<div class="discription">
					<?php	echo $goal_array[$i]['bucket_name'] ; ?>
					</div>
					<div class="name2">
						al respecto:
					</div>
					<div class="discription">
						<?php	echo $goal_array[$i]['bucket_mdesc_l'] ; ?>
					</div>
					<div class="name2">
						Imágenes :
					</div>
					<div class="images">
						<?php for($j=0;$jend=count($all_goal_img_array),$j<$jend;$j++)
						{
							?>
						<img src="<?php echo IMAGE_ADMIN_GOAL_URL; ?>/<?php echo $all_goal_img_array[$j]['images'] ?>" alt="goal Image">&nbsp;
						<?php } ?>
					</div>
				</div>
</div>
</div>
           	    <?php } ?>
               </ul>
               <?php if(count($goal_array)>5) { ?>
               
		<div class="loadmore-but">
        <a id="loadmore" class="hidethis" href="javascript:void(0);">Ver más</a>
       </div>
		<div class="cls"></div>
		<?php } ?>
               <input type="hidden" name="achievement_count" id="achievement_count" value="<?php echo $achieved_goal_count ; ?>">
            </div>
        </div>
    <div class="cls"></div>
</div>
<div class="cls"></div>
</div>
<div class="cls"></div>
<script type="text/javascript">
$(document).ready(function(){
	$(".inline").colorbox({inline:true, width:"50%"});
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	
	$bucket_count = $('#achievement_count').val() ;
		for($i=8;$i<$bucket_count;$i++)
		{
			$('#achieve_div_'+$i).hide() ;
		}
		$i =8 ;
		$('#loadmore').click(function(){
			$bucket_count = $('#achievement_count').val() ;
			for($j=$i;$j<($i+8);$j++)
			{
				$('#achieve_div_'+$j).show("75000") ;
			}
			$i = $i + 8 ;
			if($i>=$bucket_count)
			{
				$('.hidethis').hide() ;
			}
		});
});
</script>