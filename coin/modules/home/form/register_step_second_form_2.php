<script>
var ticksCount = 30;
</script>
<header class="inicio-video">
  <div class="container">
    <div class="row-fluid">
      <div class="span12"> <a href="<?php echo DEFAULT_URL?>"><img width="66" src="<?php echo IMAGE_URL?>/landing_logo_icon.png"></a> </div>
    </div>
  </div>
</header>

<section id="top_video" class="inicio-2">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
      	<div class="embed-responsive embed-responsive-16by9">
        	<iframe class="embed-responsive-item" src="http://www.socioscoin.com.usrfiles.com/html/525b74_4b519b06dfbbf8605f81439db4cee12c.html?rel=0" allowfullscreen=""></iframe>
        </div>
        <div class="timer_sec">
        	<span><a href="<?php echo MODULE_URL?>/home/register_second_step_3.php">ENTRAR AHORA >>   $5 ºº</a> <h4>Tu Descuento del 75% expira en:</h4></span>
            <div id="centered_div">
            <div id="counter">
            <div id="counter_item1" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            <div id="counter_item2" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            
            <div id="counter_item4" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            <div id="counter_item5" class="counter_item">
            <div class="front"></div>
            <div class="digit digit0"></div>
            </div>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="comprar_page" class="inicio-video-page">
  <div class="container">
    <div class="row-fluid comprar_top_part">
      <div class="span12">
      	<div class="logos-part">
            <span class="comprar-right-icon"><img src="<?php echo IMAGE_URL?>/comprar-right-icon-1.png"></span>
            <p>Como lo viste en:</p>
            <span class="comprar-logos-image"><img src="<?php echo IMAGE_URL?>/comprar-logos-image-1.png"> <img src="<?php echo IMAGE_URL?>/comprar-logos-image-2.png"></span>
        </div>
      </div>
    </div>
    
    <div class="row-fluid">
      <div class="white_wrapper">
      	<div class="span3">
        	<h1>Proyecto COIN</h1>
            <h5>PLATAFORMA: COIN.OS</h5>
            <img class="comprar-coin" src="<?php echo IMAGE_URL?>/comprar-coin.png">
         
            <p class="dicen">Lo que dicen los miembros <br>de nuestra Comunidad.</p>
            
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="<?php echo IMAGE_URL?>/member_1.png">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Ricardo Martinez</p>
                        <img class="comprar-star" src="<?php echo IMAGE_URL?>/comprar-star.png">
						<p>La Plataforma esta buenisima! No
						tuve problemas en entender como
						usarla y todo es super claro y 
						desde la primer samana ya habia
						cumplido 1 de mis metas.</p>
                    </div>
				</div>
			</div>
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="<?php echo IMAGE_URL?>/member_2.png">
				</div>
				<div class="span9">
					<div class="caption">
						<p class="text-info lead adjust2">Sergio Andres</p>
                        <img class="comprar-star" src="<?php echo IMAGE_URL?>/comprar-star.png">
						<p>Tengo 19 anos y no tenia idea de
							que queria estudiar. ni a que
							dedicarme. la plataforma solita.
							me ayudo aclarar mis metas...lo
							mejor fue que mientras aprendia 
							comenze a generar ingresos.</p>
                    </div>
				</div>
			</div>
            
            <div class="thumbnail adjust1">
				<div class="span3">
					<img class="media-object img-rounded img-responsive" src="<?php echo IMAGE_URL?>/member_3.png">
				</div>
				<div class="span9">
						<div class="caption">
						<p class="text-info lead adjust2">Jorge Herrera</p>
                        <img class="comprar-star" src="<?php echo IMAGE_URL?>/comprar-star.png">
						<p>Este programa es lo MAXIMO!!! mi
						hobby y mis suenos era ser
						paracaidista profesional...esto me
						a ayudado a liberar tanto de mi
						tiempo y a generar los ingresos
						suficientes. que ya voy por mi
						salto numero 30!!!</p>
                    </div>
				</div>
			</div>
            
        </div>
      	<div class="span9">
        	<div class="co_part">
            	<h4>Descripción</h4>
                <p>Upgrade your Mac to OS X Yosemite and you'll get an elegant design that's both fresh and familiar. The apps you use every day will have powerful new features. And your Mac, iPhone, and iPad will work together in amazing new ways. You'll also get the latest technology and the strongest security. It's like getting a whole new Mac — for free.</p>
                <div class="grayish">
                	<h4>Ejemplos del Producto</h4>
                    <div class="screenshotz">
                    	<div id="content-6" class="content horizontal-images">
                            <ul>
                                <li><img src="<?php echo IMAGE_URL?>/screenshot.jpg" /></li>
                                <li><img src="<?php echo IMAGE_URL?>/screenshot.jpg" /></li>
                                <li><img src="<?php echo IMAGE_URL?>/screenshot.jpg" /></li>
                                <li><img src="<?php echo IMAGE_URL?>/screenshot.jpg" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="guarantee_part">
                	<p><img src="<?php echo IMAGE_URL?>/compar-guarantee.png"> Complete your order today and if for any reason you don't absolutely love this product within the next 14 days, we'll refund you NO QUESTIONS ASKED. It's as simple as sending us a refund request at admin@proyectocoin.com</p>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>