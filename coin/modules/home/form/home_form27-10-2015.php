﻿<?php /*
 * User Level Updation When User LogggedIn Automatic
 */
?>
<script type="text/javascript">

	var cur_year = '<?php echo date('Y',$userdata1['modified_date'])?>';
	var cur_month = '<?php echo date('m',$userdata1['modified_date'])+1?>';
	var cur_day = '<?php echo date('d',$userdata1['modified_date'])?>';
	var lussy = parseInt(22);
var launchDay = new Date(2015, 9-parseInt(22), 1);
	function set_user_input(val)
	{
		jQuery('.option_class').hide();
		jQuery('#'+val).show();
		$('#text_video').prop('required', false);
		$('#text_image').prop('required', false);
		$('#text_'+val).prop('required', true);
	}
</script>
 <script>
 
	$(function(){
		$('#timer').countdowntimer({
			dateAndTime : ""+cur_year+"/"+cur_month+"/"+cur_day+" 00:00:00",
			size : "lg",
			regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
			regexpReplaceWith: "$1 dias  $2 hrs $3 min"
		});
	});
</script>
<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part new-changes">
	<?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
		<span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
 <?php unset($_SESSION['image_ext_error']);
			}
	?>
	<div class="boxes round">
    <div class="row-fluid">
		<div class="span6 text-left">
		<a href="<?php echo MODULE_URL.'/affiliates/calculator.php?event=calculator';?>"> <img src="<?php echo IMAGE_URL; ?>/pencil.png" /></a></div>
	<div class="span6 text-right">
		<a href="<?php echo MODULE_URL.'/program/index.php?program='.$allrecord_lession[0]['id'];?>" >Ver tutorial</a>
		<img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png" />
	</div>
</div>
<div class="row-fluid">
<?php if(!empty($calculationResult)){ ?>
  <div class="span12 heading">
  <h1>Proyecto: "Hawaii"</h1>
  <?php if($calculationResult['payment_type']=='Un pago'){
	  $totalEarning = $calculationResult['total']/30;
  }else{
	   $totalEarning = $calculationResult['total'];
  }?>
  
  
   <p>"Earn $<?php echo number_format($totalEarning,0); ?> <?php echo $calculationResult['description']; ?> <?php echo $calculationResult['time_period']; ?> Months" </p>
 </div>
<?php } else{ ?>
	<div class="span12 heading">
  <h1>Proyecto: "Hawaii"</h1>
  <p>En 3 meses Quiero cumplir mi meta de vivir 6 meses en Hawai<br>Para aprender a surfear y tener las mejorres vistas del mundo.</p>
</div>
<?php } ?>
</div>

<hr class="mr0">
<div class="row-fluid">
  <div class="span7">
      <div class="video-container" style=" border:none;">
        <?php if($userdata1['user_wallpaper']==''){ ?>
        <!--<iframe  src="<?php  //echo $home_wallpaper?>?controls=0" frameborder="0" allowfullscreen></iframe>-->
        <img src="<?php echo IMAGE_URL?>/videoimg1.jpg" alt="" />
        <?php } else{
        	if($userdata1['video_image_type']=='0')
			{
				echo '<img src="'.USER_WALLPAPER.'/'.$userdata1['user_wallpaper'].'" />';
			}else{
				
				$video_url =  parseVideos($userdata1['user_wallpaper']);
				echo '<iframe  src="'.$video_url[0]['url'].'?controls=0" frameborder="0" allowfullscreen></iframe>';
			}
        }?>
      </div>
      <!--<p class="text-left MarT5"><a href="javascript:void(0)" onclick="jQuery('#user_data').show();$('#text_video').prop('required', true);">Change video/image</a></p>-->    
      <div style="display: none;" id="user_data">
      	<div class="popupN">
      	<form name="user_input" method="post" action="" enctype="multipart/form-data">
      		<div class="crosspopup"><img src="<?php echo IMAGE_URL?>/cross.png" onclick="jQuery('#user_data').hide();" style="cursor: pointer" /></div>
	      	<div>
		      	<div><h3>Set Wallpaper: </h3></div>
		      	<div class="MarT5">
                <div class="radioleft">
                  <div class="popleftradio"><input type="radio" name="video_image_type" id="video_image_type" value="1" checked="checked" onclick="set_user_input('video')" /></div>
                  <div class="popleftrext">Video</div>
                  <div class="clearfix"></div>
                </div>              
                <div class="radioleft">
                  <div class="popleftradio"><input class="MarL10" type="radio" name="video_image_type" id="video_image_type" value="0" onclick="set_user_input('image')" /></div>
                  <div class="popleftrext">Image</div>
                  <div class="clearfix"></div>
                </div>
		      	 <div class="clearfix"></div>		      	 
		      	</div>		      
		      	<div id="video" class="option_class MarT10"> Video: <input type="text" name="video" id="text_video" placeholder="Video url" /></div>
		      	<div id="image" class="option_class MarT10" style="display: none;"> Image: <input type="file"  name="image" id="text_image" /></div>
		      	<input type="hidden" name="action" value="upload_userinput" />
		      	<div class="MarT10 text-right"><input class="btn btn-primary" type="submit" name="submit" value="Submit" /></div>
	      	</div>
	      	</form>
	      	</div>
      	</div>
</div>
 
  
  <?php if(!empty($calculationResult)){ ?>
  <div class="span5 graybg " style="background:#ececec;">
 <h1>$<?php echo number_format($totalEarning,0);?></h1>
  <p>Goal: $<?php echo number_format($calculationResult['total'],0)?> USD per month</p>
  <h1 class="MarT30"><?php echo $date_diff?></h1>
  
  <p>Of <?php echo ($calculationResult['time_period']*30)-$date_diff?> days to complete</p>
  <?php  
  $earningParsentage =  round(($totalEarning / $calculationResult['total'] ) * 100) .'%';
  ?>
  <div class="progress progress1">
  <div class="bar" style="width: <?php echo $earningParsentage?>;"></div>
</div>
<?php if($earningParsentage>'100%') {
	$total_presentage = '100%';
}else{
	$total_presentage = $earningParsentage;
}?>
  <h4 class="text-center"><?php echo $total_presentage?></h4>
  
  </div>

  <?php } else{ ?>
    <!--<div class="span5 graybg new-timer">
  <h6>Necesita:</h6>
  <h1>$50,000</h1>
  <p>Goal: $0,000 USD per month</p>
  <h6>Tiempo restante:</h6>
  <div id="timer"></div>
  <p>Of 0 days to complete</p>
  </div>-->
  <div class="span5 graybg " style="background:#ececec;">
  <h1>$0</h1>
  <p>Meta: $10,000 USD al mes</p>
  <h1 class="MarT20">0</h1>
  <p>de 168 para cumplir la mi proyecto</p>
  <div class="progress">
    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
      <span class="sr-only">&nbsp;</span>
    </div>
  </div>
  <h4 class="text-center">0%</h4>
</div>
   <?php } ?>
  
</div>


    
    <div class="row-fluid MarT20">
 	 <div class="span3">
     <p>MCN</p>
     <div class="clicks"><?php echo number_format($calculationResult['sales_needed'],0);?> Clicks</div>
     </div>
 	 <div class="span3">
     <p>DCN</p>
     <div class="clicks"><?php echo number_format($calculationResult['sales_needed']/30,0);?> Clicks</div>
     </div>
     <div class="span3">
     <p>TMI</p>
     <div class="clicks"><?php echo number_format($calculationResult['total'],0);?> ºº USD</div>
     </div>
     <div class="span3">
     <p>TDI</p>
     <div class="clicks"><?php echo number_format($calculationResult['total']/30,0);?> ºº USD</div>
     </div>
     <div class="clearfix"></div>
	</div>
    
    
<div id="see_video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">Coin Video</h3>
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>
		<form id="" action="" method="post" name="f1" enctype="multipart/form-data" >
			<div class="modal-body">
				<div class="popup">
					<div class="video-container">
						<iframe src="//fast.wistia.net/embed/iframe/acvm86vx2i" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="360">
							</iframe>
							</div>
				</div>
			</div>
            </form>
			<div class="modal-footer">
            	
				
	</div>
</div>
<div style='display:none'>
<div id='view_msg' class='irf-new-popup'>
<div class="popup detail">
		        <div>
		        	&nbsp;
		        	</div>
		         <div style="text-align:center;">
					<span style="text-align: center;">
						<b>Su pago está siendo procesado....</b>
						</span>
						</div>
						 <div>
					<span>
						<!-- <img src="<?php echo IMAGE_URL ; ?>/paypalwait.png" /> -->
						<a href="http://www.comunidadcoin.org/#!kitdebienvenida/c1n6i" target="_blank">
						<img src="<?php echo IMAGE_URL; ?>/slide10.jpg" />
						</a>
						</span>
						</div>
						  <div>
		        	&nbsp;
		        	</div>
				</div>
</div>
</div>
		<div class="cls"></div>
	</div>
    <hr />
    <div class="row-fluid new-add-sec">
    	<div class="span6 font-size15 Open_sans">
        	<div class="tc"> <img src="<?php echo IMAGE_URL ; ?>/list-icon.png" /> &nbsp;MIS CRITERIOS: </div>
            <ul>
            	<li>1.- First Criteria</li>
                <li>2.- Second Criteria</li>
                <li>3.- Third Criteria</li>
                <li>4.- Fourth Criteria</li>
                <li>5.- Fifth Criteria</li>
            </ul>
        </div>
        <div class="span6 font-size15 Open_sans">
        	<div class="tc"> <img src="<?php echo IMAGE_URL ; ?>/star-round.png" /> &nbsp;MI DIA PERFECTO: </div>
            <p class="tc">"Mi día perfecto comienza en Playa del Carmen en donde me levanto a las 10 de la mañana para salir a meditar al balcon de mi departamento el cuál tiene vista al mar.</p>
            <p class="tc">Después salgo a correr durante 40 minutos para regresar, bañarme y desayunar. una vez listo salgo al cafe de la esquina para iniciar mi jornada de trabajo de 2 horas :)  , En donde reviso correos, mi asistente me pone al día acerca de mi negocio y reviso mis ganancias todo para encontrarme con la sorpresa de que mientras dormía gane dinero en piloto automático.</p>
        </div>
    </div>
</div>
