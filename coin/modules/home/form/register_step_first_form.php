<!--| Content Start |-->
<div class="login-video">
	<div class="container">
		<div class="row-fluid">
			<div class="span12 step posi_rela">
				<div class="login-logo"><img src="<?php echo IMAGE_URL; ?>/logo.png" alt="logo">
				</div>
				<p class="grey-text">
					Al registrate Gratis a la Comunidad!
					<br/>
					te estarás uniendo a un grupo de personas que en tan solo de 3 a 5 meses han logrado cambiar su vida y llenarla de Libertad, Diversión y Aventura <img class="step-image" src="<?php echo IMAGE_URL; ?>/step1.jpg" alt="image">
				</p>
				<div class="amount"><img src="<?php echo IMAGE_URL; ?>/amount.png" alt="image">
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row-fluid">
			<div class="span12">
				<div class="posi_rela width440">
					<div class="progress">
						<div style="width: 100%;" class="bar"></div>
						<div class="progress-icon">
							&nbsp;
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span6 PdTop40" style="color:red; text-align: right">
		<?php
		if (($variables['error']) != '') {
			echo $variables['error'];
		}
		?>
	</div>
	<form name="targetform" id="targetform" action="<?php $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
		<div class="container">
			<div class="row-fluid">
				<div class="span12  PdTop20 generate">
					<div class="span6 offset3">
						<span>I want to Generate <b>$</b></span>
						<input name="targetamount" type="text" placeholder="$354,000" value=""  onfocus="this.placeholder = ''" onblur="this.placeholder ='$354,000'">
						<span>In</span>
						<input name="targetmonth" type="text" placeholder="5" value=""  onfocus="this.placeholder = ''" onblur="this.placeholder ='5'">
						<span>Months</span>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<div class=" PdTop40">
						<input type="hidden" name="action" value="targetachieve" />
						<button class="btn btn-success" onclick="submitForm();">
							<em>NEXT</em>
						</button>
					</div>
				</div>
			</div>
		</div>

	</form>

	<div class="container">
		<div class="row-fluid">
			<div class="span12">
				<div class="login-logo-bottom">
					<img width="70" src="<?php echo IMAGE_URL; ?>/logo.png" alt="logo">
				</div>
			</div>
		</div>
	</div>

</div>

<!--| Content End |-->
<script type="text/javascript">
	function submitForm() {
		$("#targetform").submit();

	}

</script>