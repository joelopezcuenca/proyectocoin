﻿<?php /*
 * User Level Updation When User LogggedIn Automatic
 */
?>
<script type="text/javascript">
	function set_user_input(val)
	{
		jQuery('.option_class').hide();
		jQuery('#'+val).show();
		$('#text_video').prop('required', false);
		$('#text_image').prop('required', false);
		$('#text_'+val).prop('required', true);
	}
</script>
<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part">
	<?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
		<span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
 <?php unset($_SESSION['image_ext_error']);
			}
	?>
	<div class="boxes round round1">
    <div class="row-fluid">
		<div class="span6 text-left"><img src="<?php echo IMAGE_URL; ?>/flag.png"> Mi Mision</div>
	<div class="span6 text-right">
		<a href="<?php echo MODULE_URL.'/program/index.php?program='.$allrecord_lession[0]['id'];?>" >Ver tutorial</a>
		<img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png">
	</div>
</div>
<div class="row-fluid">
<?php if(!empty($calculationResult)){ ?>
  <div class="span8 offset2 heading"><h1>"Earn $<?php echo number_format($calculationResult['total'],0); ?> <?php echo $calculationResult['description']; ?> <?php echo $calculationResult['time_period']; ?> Months"</h1></div>
<?php } else{ ?>
	<div class="span8 offset2 heading"><h1>"Earn $0ºº usd per month to
 ___________________"</h1></div>
<?php } ?>
</div>
<hr class="mr0">
<div class="row-fluid MarT20">
  <div class="span7">
  <div class="video-container">
        <?php if($userdata1['user_wallpaper']==''){ ?>
        <iframe  src="<?php echo $home_wallpaper?>?controls=0" frameborder="0" allowfullscreen></iframe>
        <?php } else{
        	if($userdata1['video_image_type']=='0')
			{
				echo '<img src="'.USER_WALLPAPER.'/'.$userdata1['user_wallpaper'].'" />';
			}else{
				
				echo '<iframe  src="'.youtubeVideo($userdata1['user_wallpaper']).'?controls=0" frameborder="0" allowfullscreen></iframe>';
			}
        }?>
      </div>
      <p class="text-left MarT5"><a href="javascript:void(0)" onclick="jQuery('#user_data').show();$('#text_video').prop('required', true);">Change video/image</a></p>    
      <div style="display: none;" id="user_data">
      	<div class="popupN">
      	<form name="user_input" method="post" action="" enctype="multipart/form-data">
      		<div class="crosspopup"><img src="<?php echo IMAGE_URL?>/cross.png" onclick="jQuery('#user_data').hide();" style="cursor: pointer" /></div>
	      	<div>
		      	<div><h3>Set Wallpaper: </h3></div>
		      	<div class="MarT5">
                <div class="radioleft">
                  <div class="popleftradio"><input type="radio" name="video_image_type" id="video_image_type" value="1" checked="checked" onclick="set_user_input('video')" /></div>
                  <div class="popleftrext">Video</div>
                  <div class="clearfix"></div>
                </div>              
                <div class="radioleft">
                  <div class="popleftradio"><input class="MarL10" type="radio" name="video_image_type" id="video_image_type" value="0" onclick="set_user_input('image')" /></div>
                  <div class="popleftrext">Image</div>
                  <div class="clearfix"></div>
                </div>
		      	 <div class="clearfix"></div>		      	 
		      	</div>		      
		      	<div id="video" class="option_class MarT10"> Video: <input type="text" name="video" id="text_video" placeholder="Youtube url" /></div>
		      	<div id="image" class="option_class MarT10" style="display: none;"> Image: <input type="file"  name="image" id="text_image" /></div>
		      	<input type="hidden" name="action" value="upload_userinput" />
		      	<div class="MarT10 text-right"><input class="btn btn-primary" type="submit" name="submit" value="Submit" /></div>
	      	</div>
	      	</form>
	      	</div>
      	</div>
</div>
  <div class="span5 graybg">
  <h1>$3,450</h1>
  <p>Goal: $10,000 USD per month</p>
  <h1 class="MarT30"><?php echo $date_diff?></h1>
  <p>Of <?php echo ($calculationResult['time_period']*30)-$date_diff?> days to complete</p>
  
  <div class="progress progress1">
  <div class="bar" style="width: 60%;"></div>
</div>
  <h4 class="text-center">33%</h4>
  
  </div>
</div>


    
    <div class="row-fluid MarT20">
 	 <div class="span3">
     <div class="clicks"><?php echo number_format($calculationResult['sales_needed'],0);?> Clicks</div>
     <div>Monthly Clicks Needed</div>
     <p>(MCN)</p>
     </div>
 	 <div class="span3">
     <div class="clicks"><?php echo number_format($calculationResult['sales_needed']/30,0);?> Clicks</div>
     <div>Daily Clicks Needed</div>
     <p>(DCN)</p>
     </div>
     <div class="span3">
     <div class="clicks"><?php echo number_format($calculationResult['total'],0);?> ºº USD</div>
     <div>Target Monthly Income</div>
     <p>(TMI)</p>
     </div>
     <div class="span3">
     <div class="clicks"><?php echo number_format($calculationResult['total']/30,0);?> ºº USD</div>
     <div>Target Daily Income</div>
     <p>(TDI)</p>
     </div>
     <div class="clearfix"></div>
	</div>
    
    
<div id="see_video" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel">Coin Video</h3>
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>
		<form id="" action="" method="post" name="f1" enctype="multipart/form-data" >
			<div class="modal-body">
				<div class="popup">
					<div class="video-container">
						<iframe src="//fast.wistia.net/embed/iframe/acvm86vx2i" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="640" height="360">
							</iframe>
							</div>
				</div>
			</div>
			<div class="modal-footer">
            	
				
	</div>
</div>
<div style='display:none'>
<div id='view_msg' class='irf-new-popup'>
<div class="popup detail">
		        <div>
		        	&nbsp;
		        	</div>
		         <div style="text-align:center;">
					<span style="text-align: center;">
						<b>Su pago está siendo procesado....</b>
						</span>
						</div>
						 <div>
					<span>
						<!-- <img src="<?php echo IMAGE_URL ; ?>/paypalwait.png" /> -->
						<a href="http://www.comunidadcoin.org/#!kitdebienvenida/c1n6i" target="_blank">
						<img src="<?php echo IMAGE_URL; ?>/slide10.jpg" />
						</a>
						</span>
						</div>
						  <div>
		        	&nbsp;
		        	</div>
				</div>
</div>
</div>
		<div class="cls"></div>
	</div>
</div>