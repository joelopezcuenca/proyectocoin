<div class="login-video">
<div class="Invite-friendstopbg">
  <div class="container">
    <div class="row-fluid">
      <div class="span12 step posi_rela">
        <div class="login-logo"><img src="<?php echo IMAGE_URL; ?>/logo.png" alt="logo"></div>
      </div>
    </div>
  </div>
</div>
<div class="Greenbg">
  <div class="container">
    <div class="row-fluid">
      <div class="span12 ">
        <div class="Width500">
          <div class="invited-photo"><img src="<?php echo IMAGE_ADMIN_PROFILE_URL ; ?>/<?php echo $user_info['profile_pic']  ; ?>" alt=""></div>
          <div class="blue ft16 MarT5"><?php echo $user_info['name']  ; ?></div>
          <div class="ft16 MarT5">Te ha invitado a que te unas a:</div>
          <div class="blue ft16 MarT5"><?php echo $goal_detail[0]['bucket_name'] ; ?></div>
          <!-- <div class="Font15"><?php echo $goal_detail[0]['bucket_name'] ; ?></div> -->
         <form id="invite_friend" action="<?php echo $_SERVER['PHP_SELF'] ; ?>" method="post" enctype="multipart/form-data">
          <div class="MarT40">
          	<?php 
          	if(isset($variables['error']))
			{ ?>
				<span style="color: red;"><b><?php echo $variables['error'] ; ?></b></span>
			<?php }
          	?>
            <div class="input_field">
              <input type="text" name="username" placeholder="Nombre"  class="search-query validate[required]" data-prompt-position="topLeft:-7">
            </div>
            <div class="input_field">
            	<?php if(isset($_SESSION['DuplicatEmail']))
				{ ?>
            	<span style="color: red;"><b><?php echo $_SESSION['DuplicatEmail'] ; ?></b></span>
            	<?php }
				unset($_SESSION['DuplicatEmail']) ;
				 ?>
              <input type="email" name="email" placeholder="Correo electronico" class="search-query validate[required,custom[email]]">
            </div>
            <div class=" PdTop20 comenzar">
            	<input type="hidden" name="action" value="invite_bucket_friend">
            	<input type="hidden" name="bucket" value="<?php echo $bucket ; ?>">
            	<input type="hidden" name="send_id" value="<?php echo $send_id ; ?>">
            	
              <input  name="" type="submit" value="Accept">
            </div>
          </div>
         </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="Invite-friendstopbg">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
        <div class="login-logo-bottom"><img width="70" src="<?php echo IMAGE_URL; ?>/logo.png" alt="logo"></div>
      </div>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
	$("#invite_friend").validationEngine();
});
</script>