<header class="inicio-video">
  <div class="container">
    <div class="row-fluid">
      <div class="span12"> <a href="#"><img width="66" src="<?php echo IMAGE_URL?>/landing_logo_icon.png"></a> </div>
    </div>
  </div>
</header>
<?php if(!isset($private_sales) && $private_sales!='pago'){ ?>
<section id="top_video" class="inicio-go">
<div class="bg_change"></div>
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
      	<div class="embed-responsive embed-responsive-16by9_new">
        	<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_embed wistia_async_7gk66tuuon" style="height:329px;width:585px">&nbsp;</div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>
<section id="comprar_page" class="inicio-go-page">
  <div class="container">
    <div class="row-fluid comprar_top_part">
      <div class="span6"><img src="<?php echo IMAGE_URL?>/comprar-lock.png"> <h4>Portal de Fondeo Seguro&nbsp;y Verificado&nbsp;™</h4></div>
      <div class="span6 text-right smart new2"><span class="text-green">S</span><span class="text-red">M</span><span class="text-skyblue">A</span><span class="text-yellow">R</span><span class="text-dark-red">T</span></div>
    </div>
    <div class="row-fluid comprar_Payment_part">
      <div class="white_wrapper">
      	<div class="span12 text-center"><img src="<?php echo IMAGE_URL?>/steps.png"></div>
        <div class="row-fluid">

		 <form style="margin:0" id="sales_finel" action="<?php echo $_SERVER['PHP_SELF']; ?>?current_id=<?php echo $current_id?>" method="post" name="f1" enctype="multipart/form-data">
          <div class="span8">
            <div class="main-co">
              <h5>1.- CORREO ELECTRÓNICO<span class="red">*</span></h5>
              <input class="fully validate[required]" name="email" value="<?php echo $userdatal_funel['email']?>" type="text" placeholder="tu@correo.com" data-prompt-position="bottomLeft">
			  
			  <?php if($_SESSION['DuplicatEmail']!=''){
				echo '<h5 style="color:#FF0000">'.$_SESSION['DuplicatEmail'].'</h5>';
				$_SESSION['DuplicatEmail'] ='';
			  }?>
			  
              <h5>2.- NOMBRE COMPLETO<span class="red">*</span></h5>
              <input class="halfly validate[required]" name="first_name" type="text" placeholder="Nombre" data-prompt-position="bottomLeft"> <input class="halfly validate[required]" name="last_name" type="text" placeholder="Apellido" data-prompt-position="bottomLeft">
              <h5>3.- FORMA DE PAGO<span class="red">*</span> <img src="<?php echo IMAGE_URL?>/comprar-lock-yellow.png"></h5>
              <div class="input-radio">
                <input name="payment_type" type="radio" value="1" checked="checked"> <h4>Pagar con PayPal</h4> <i class="fa fa-angle-down"></i><img src="<?php echo IMAGE_URL?>/comprar-paypal-master.png" style="display: inline-block;margin-right: 10px;">
                </div>
              <div class="input-radio">
                <div class="checkboxes a_black"> <input name="tearm_1" type="checkbox" value="1" checked="checked" class="validate[required]" data-prompt-position="bottomLeft">He leído y acepto los <a href="#getTerms" data-toggle="modal">términos y condiciones</a> que se indican anteriomente, incluidas la <a href="#getAffiliate" data-toggle="modal">Política de reembolsos</a> la <a href="#getRefund" data-toggle="modal">Política de afiliados</a></div>
                <div class="checkboxes"> <input name="tearm_2" type="checkbox" value="1" checked="checked" class="validate[required]" data-prompt-position="bottomLeft">Acepto los terminos de mi suscripcion.</div>
                <div class="ENVIAR"><a href="javascript:void(0)" onclick="jQuery('#sales_finel').submit()"><img src="<?php echo IMAGE_URL?>/enviar-but-1.jpg"></a></div>
				<div class="text-center"><img src="<?php echo IMAGE_URL?>/yoplant.jpg"></div>
                </div>
              </div>
            </div>
			
			<?php if(isset($private_sales) && $private_sales=='pago'){ ?>
			<input type="hidden" name="action"  value="aweberform"/>
			<input type="hidden" name="private_sales"  value="<?php echo $private_sales?>"/>
			<input type="hidden" name="ref"  value="<?php echo $_GET['ref']?>"/>
			<?php }else { ?>
			<input type="hidden" name="action" value="sales_funel_from" />
			<input type="hidden" name="userid_form" value="<?php echo $current_get_id?>" />
			<?php } ?>
			</form>
			
			
          <div class="span4">
            <div class="comprar_producto_part" id="producto_part">
              <h4>Resumen de Orden</h4>
              <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:1px solid #d5d5d5">
                <tr style="border-top:1px solid #d5d5d5; border-bottom:1px solid #d5d5d5">
                  <th><img src="<?php echo IMAGE_URL?>/comprar-cart.png" style=" height:23px"></th>
                  <th>Productos: 3</th>
                  <th>Total: <span style="color:#bfdd82; font-weight:bold">$5usd</span></th>
                  </tr>
               <!-- <tr>
                  <td><img src="<?php echo IMAGE_URL?>/check_img_home.png"></td>
                  <td><img src="<?php echo IMAGE_URL?>/plant.png"></td>
                  <td>Plantaremos un árbol Mientras Fondeas tu sueño</td>
                  </tr> -->
     		<tr>
                  <td><img src="<?php echo IMAGE_URL?>/check_img_home.png"></td>
                  <td><img src="<?php echo IMAGE_URL?>/first_payment_page_product.png"></td>
                  <td>Membresía SMART</td>
                  </tr>
                <tr>
                  <td><img src="<?php echo IMAGE_URL?>/comprar-right-icon.png"></td>
                  <td><img src="<?php echo IMAGE_URL?>/second_payment_page_product.png"></td>
                  <td>Fondea tu Sueño</td>
                  </tr>
                <tr>
                  <td><img src="<?php echo IMAGE_URL?>/comprar-right-icon.png"></td>
                  <td><img src="<?php echo IMAGE_URL?>/third_payment_page_product.png"></td>
                  <td>Acceso a la Comunidad Privada durante 1 mes.</td>
                  </tr>
                </table>
              </div>
            
          <!--  <div class="comprar_total_part">
              <div>
                <h4>Resumen de Compra</h4>
                <h1>$5<sup>USD</sup></h1>
                <p>Suscripción mensual a la Comunidad de Proyecto Coin. Por promoción obtendré un descuento de más del 95% por lo que en mi primer mes solo pagaré $5USD para poder comprobar que el programa y la plataforma si funcionan, después de 30 días pagaré $50 USD cada 30 días (Lo ideal es que sigas el programa SMART al pie de la letra para que el siguiente mes, tus ganancias cubran este costo). RECUERDA: que siempre  podrás cancelar tu membresía en cualquier momento enviando un correo a admin@proyectocoin.com .</p>
              </div>
              </div>-->
            <div class="yellow-box-copmpar">
              <img src="<?php echo IMAGE_URL?>/compar-guarantee.png">
              <h5>GARANTÍA TOTAL<br/>TE DEVOLVEREMOS<br/>EL 100% TU DINERO</h5>
              <p>Completa tu pedido hoy mismo y, si por cualquier motivo, no te enamoras de este producto en los próximos 14 días, te reembolsaremos el precio de compra, SIN PREGUNTAS.</p>      
              </div>
          </div> 
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
	
	var image = $('.bg_change');
    image.fadeOut(5000, function () {
        image.css("background", "url(<?php echo IMAGE_URL?>/login-beach2.jpg)");
        image.fadeIn(7000);
    });
	
	$("#sales_finel").validationEngine();
	
jQuery.extend(jQuery.validator.messages, {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});	
	
});
</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3Z0fAUOl7Bx07I6X7o3P8aJWSQX2MfRN";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
