﻿<?php  
if((isset($_SESSION['ref']) && $_SESSION['ref']!='') && (!isset($private_sales) && $private_sales!='inicio') ){ 

 $imp_array = array("time" => time(), "user_url" => $enroller_userdata['url']);
$temp_imp = serialize($imp_array);
$imp_string = str_replace('"', "--", $temp_imp);

?>
<header class="landing-page">
  <div class="container">
    <div class="row-fluid">
    <div class="sopan6 mobile-show mobile-logo"><img src="<?php echo IMAGE_URL?>/mobile-logo.jpg" alt="" /></div>
		<div class="span6 offset6 mobile-fun"> <strong><a class=" my_target" href="#landing-page-bottom_ankesh">¿Cómo funciona?</a></strong> <span><a href="<?php echo MODULE_URL; ?>/home/index.php?to=login">Entrar</a></span></div>
    </div>
  </div>
</header>
<?php
$landing_class = '';
 if(isset($vid) && $vid!=''){ 
 $landing_class = 'landing-1';
 } else if(isset($gid) && $gid!=''){ 
  $landing_class = 'landing-2';
 }?>

<div class="container <?php echo $landing_class?>" id="landing-page">
	<div class="row-fluid">
		<div class="span12">
			<div class="home_bg">
				<div class="inner"> 
                ﻿  
				  
				  <?php if((!isset($vid) && $vid=='') && (!isset($gid) && $gid=='')){ 
				  $enroller_totalEarning = number_format($enroller_calculationResult['total']+$enroller_calculationResult['total_calculation2'],0);
				  ?>
                  <div class="right_part">
                    <div class="boxes round round1">
                      <div class="row-fluid showdesktop">
                        <div class="span12 heading">
						<?php if($enroller_calculationResult['home_heading']!=''){
							$heading = $enroller_calculationResult['home_heading'];
						}else{ 	$heading = 'Hawaii'; }?>
                          <h1><a class="my_target2" href="#landing-page-bottom_ankesh">Mi Sueño:</a> "<?php echo $heading;?>"</h1>
						  
						  
						   <p>Quiero lograr mi meta de ganar  $<?php echo number_format($enroller_totalEarning,0); ?> USD al mes para <?php echo $enroller_calculationResult['user_type'] ?> <?php echo $enroller_calculationResult['description']; ?> y sueño con que se cumpla en tan solo <?php echo $enroller_calculationResult['time_period']; ?> Meses</p>
                         
                        </div>
                      </div>
                      <hr class="mr0">
                      <div class="row-fluid grayboxn">
                        <div class="span7">
                          <div class="video-container" style=" border:none;">
						       <?php if($enroller_userdata['user_wallpaper']==''){ ?>
								<!--<iframe  src="<?php  //echo $home_wallpaper?>?controls=0" frameborder="0" allowfullscreen></iframe>-->
								<img src="<?php echo IMAGE_URL?>/videoimg1.jpg" alt="" />
								<?php } else{
									if($enroller_userdata['video_image_type']=='0')
									{
										echo '<img src="'.USER_WALLPAPER.'/'.$enroller_userdata['user_wallpaper'].'" />';
									}else{
										
										$video_url =  parseVideos($enroller_userdata['user_wallpaper']);
										echo '<iframe  src="'.$video_url[0]['url'].'?controls=0" frameborder="0" allowfullscreen></iframe>';
									}
								}?>
						  
                          </div>
                        </div>
                        
                        
                        <div class="row-fluid showmobile">
                        <div class="span12 heading">
						<?php if($enroller_calculationResult['home_heading']!=''){
							$heading = $enroller_calculationResult['home_heading'];
						}else{ 	$heading = 'Hawaii'; }?>
                          <h1>Mi Sueño: "<?php echo $heading;?>"</h1>
						  
						  
						   <p>Quiero lograr mi meta de ganar  $<?php echo number_format($enroller_totalEarning,0); ?> USD al mes para <?php echo $enroller_calculationResult['user_type'] ?> <?php echo $enroller_calculationResult['description']; ?> y sueño con que se cumpla en tan solo <?php echo $enroller_calculationResult['time_period']; ?> Meses</p>
                         
                        </div>
                      </div>
                        
                        
						<?php  if(!empty($enroller_calculationResult)){ 
						$target_monthly_income = number_format($enroller_calculationResult['total']+$enroller_calculationResult['total_calculation2'],0);
						?>
                        <div class="span5 graybg " style="background:#ececec;">
						<div class="showdesktop">
                          <h1>$<?php echo number_format($totalEarning2,0);?></h1>
                          <p>Meta: $<?php echo $target_monthly_income;?> USD al mes</p>
                          <h1 class="MarT20">día: <?php echo $enroller_date_diff?></h1>
                         <p>De <?php echo ($enroller_calculationResult['time_period']*30)-$enroller_date_diff?> dias para lograrlo</p>
						</div>
						<div class="showmobile marb10">
						<div class="meta-days-fl">
                          <h1>$<?php echo number_format($totalEarning2,0);?></h1>
                          <p>Meta: $<?php echo $target_monthly_income;?> USD al mes</p>
						</div>
						<div class="meta-days-fl">
                          <h1 class="MarT20">día: <?php echo $enroller_date_diff?></h1>
                          <p>De <?php echo ($enroller_calculationResult['time_period']*30)-$enroller_date_diff?> dias para lograrlo</p>
						 </div>
						</div>
						
						  <?php

						  $enroller_totalEarning  = str_replace(',','',$enroller_totalEarning);
						  $target_monthly_income  = str_replace(',','',$target_monthly_income);
						  $enroller_earningParsentage =  round(($totalEarning2 / $target_monthly_income ) * 100);  

						  if($enroller_earningParsentage>'100') {
							$enroller_total_presentage = '100';
						}else{
							$enroller_total_presentage = $enroller_earningParsentage;
						}?>
												  
						  
                          <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $enroller_total_presentage?>%">
                              <span class="sr-only">&nbsp;</span>
                            </div>
                          </div>
                          <h4 class="text-center"><?php echo $enroller_total_presentage?>%</h4>
                        </div>
						<?php } else{ ?>
						<div class="span5 graybg ">
                          <div class="showdesktop">
						  <h1>$<?php echo $totalEarning2?></h1>
                          <p>Meta: 0 USD al mes</p>
                          <h1 class="MarT20">día: 0</h1>
                          <p>Of -0 days to complete</p>
						  </div>
						  <div class="showmobile marb10">
						  <div class="meta-days-fl">
						  <p>Meta: 0 USD al mes</p>
						  <h1>$<?php echo $totalEarning2?></h1>
						  </div>
						  <div class="meta-days-fl">
						  <p>Of -0 days to complete</p>
						  <h1 class="MarT20">día: 0</h1>
						  </div>
						  <div class="cls"></div>
						  </div>
						  
                          <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                              <span class="sr-only">&nbsp;</span>
                            </div>
                          </div>
                          <h4 class="text-center">0%</h4>
                        </div>
						
						<?php } ?>
                      </div>
                      <div class="cls"></div>
                      </div>
                    </div>
				  <?php } else if(!isset($gid) && $gid==''){ ?>
				    <div class="right_part">
                    <div class="boxes round round1">
                      <div class="row-fluid">
                        <div class="span12">
                          <div class="video-container" style=" border:none;">
                            <iframe src="<?php echo $video_result[0]['url']; ?>" frameborder="0" allowfullscreen=""></iframe>
                          </div>
                          <!-- <p><?php // echo $video_detail['topic']?></p>-->
                        </div>
                      </div>
                      </div>
                    </div>
				
				  <?php } else { ?>
				    <div class="right_part">
                    <div class="boxes round round1">
                      <div class="row-fluid">
                        <div class="span12">
						
                          <img style="width:100%; height:370px;" src="<?php echo DEFAULT_URL?>/goal_img/<?php echo $unlocked_img;?>">
                          <div class="goal-descripyion">
                          	<div class="landing_goal">
                            	<img class="showdesktop" src="<?php echo DEFAULT_URL?>/goal_category_img/<?php echo $bucket_icon; ?>" style="width:109px; height:100px;" />
								
								<?php if($enroller_userdata['profile_pic'] != ''){ ?>
		
					<img class="profile-img showmobile" src="<?php echo IMAGE_ADMIN_PROFILE_URL."/".$enroller_userdata['profile_pic']; ?>">
					<?php ?> 
			<?php }else{ ?>
				<img class="profile-img showmobile" src="<?php echo IMAGE_URL; ?>/adminuser.png">
					
			<?php } ?>
								
								
                            </div>
                            <div class="landing_goal_info">
                            	<h3>Sueño con: <?php echo substr($goal_array_t[0]['bucket_name'],0,50)?></h3>
                                <img src="<?php echo IMAGE_URL?>/star-rating.png">
                                <p><?php echo $goal_array_t[0]['bucket_desc']?></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>
				  
				  <?php } ?>
				  
				  <div class="left_part">
                    <div class="boxes round">
                      <!-- <div style="padding:10px 0 20px"><img src="<?php echo IMAGE_URL?>/green-full-logo.png"> </div> -->
                      <div style="position:relative;">
					  <?php if($enroller_userdata['profile_pic'] != ''){ ?>
		
			<img class="profile-img" src="<?php echo IMAGE_ADMIN_PROFILE_URL."/".$enroller_userdata['profile_pic']; ?>">
			<?php ?> 
	<?php }else{ ?>
		<img class="profile-img" src="<?php echo IMAGE_URL; ?>/adminuser.png">
			
	<?php } ?>
				</div>
                      <h4><?php echo $enroller_userdata['username']?></h4>
                      <ul id="Conunter">
					 
                      	<li><?php echo $enroller_totalGoal;?> <br/> sueños</li>
                        <li><?php echo $enroller_totalcompleteGoal;?> <br/> Logrados</li>
                        <li><?php echo count($total_followers);?> <br/>Seguidores</li>
                        <li><?php echo count($total_following);?> <br/>Siguiendo</li>
                      </ul>
                      <a class="Seguir my_target showdesktop" href="#landing-page-bottom_ankesh" >Seguir</a>
                    </div>
                  </div>
				  
                  <div class="cls"></div>
                </div>
                <!-- <div class="connect_but"><a href="http://www.facebook.com/share.php?u=<?php echo MODULE_URL; ?>/home/share_home_url.php?imp_data=<?=$imp_string; ?>" target="blank"><img src="<?php echo IMAGE_URL?>/fb-share-landing.jpg"></a> <a href="https://twitter.com/intent/tweet?&text=<?php echo 'Descubre como Vivir una Vida llena de Libertad, Diversión y Aventura. Únete al movimiento! '.$enroller_userdata['url'] ?>" target="blank" ><img src="<?php echo IMAGE_URL?>/twitter-share-landing.jpg"></a> <a class="Crear my_target" href="#landing-page-bottom">Crear mi Proyecto</a> <img class="landing-arrow" src="<?php echo IMAGE_URL?>/landing-arrow.jpg"></div>-->
				
	<?php if(isset($vid) && $vid!=''){ ?>			
<div class="video_detail_new">

<?php if($enroller_userdata['profile_pic'] != ''){ ?>
		
					<img style="width:33px; height:33px; display:inline-block;" src="<?php echo IMAGE_ADMIN_PROFILE_URL."/".$enroller_userdata['profile_pic']; ?>">
					<?php ?> 
			<?php }else{ ?>
				<img style="width:33px; height:33px;" class="profile-img " src="<?php echo IMAGE_URL; ?>/adminuser.png">
					
			<?php } ?>



					
					<img class="chat-icon" src="<?php echo IMAGE_URL; ?>/chat-icon.png">
					<p><?php  echo $video_detail['topic']?></p></p>
				</div>
                
                <?php } ?>
                
				
			</div>
		</div>
	</div>
</div>
<div class="blue-bar-landing" >
	<div class="container">
    	<div class="row-fluid">
        	<div class="span12" >
            	<h1>F<img class="h1-coin" src="<?php echo IMAGE_URL; ?>/h1-coin.png">NDEA &nbsp; TUS &nbsp; SUEÑOS</h1> <a class="Crear my_target" href="#landing-page-bottom_ankesh" >Hazlo Ya!</a> <img class="landing-arrow" src="<?php echo IMAGE_URL; ?>/blue-arrow.png"><br/>
<a href="$" id="landing-page-bottom_ankesh">&nbsp;</a>
            </div>

        </div>
    </div>
</div>

<?php } else{?>
<div class="border-top-green"></div>
<header class="landing-page leanding_registrer">
  <div class="container">
    <div class="row-fluid">
      <div class="span12"> <strong><!-- <a href="#landing-page-bottom">¿Cómo funciona?</a>--></strong> <span><a href="<?php echo MODULE_URL; ?>/home/index.php?to=login">Entrar</a></span></div>
    </div>
  </div>
</header>
<?php } ?>

<div class="landing-grey-bg" >
<div class="container" id="landing-page-bottom">
	<div class="row-fluid">
		<div class="span12">
			<div class="home_bg">
			<?php 
			$className = '';
			$className_arrow = '';
			if(isset($_GET['ref']) && $_GET['ref'] !=''){
				$className = 'hidehomeCoin';
				$className_arrow = 'arrow-responsive';
			}?>
				<h1><img  class="landing-coin-1 <?php echo $className?>" src="<?php echo IMAGE_URL; ?>/landing-coin-1.png"> <img class="thin-arrow <?php echo $className_arrow?>" src="<?php echo IMAGE_URL; ?>/thin-arrow.png"> <span class="lihgt-green">Fondea</span> Tus Sueños <span class="dark grey">en sólo 7 Pasos…</span> </h1>
                <div class="text-center ver-funciona"><img src="<?php echo IMAGE_URL?>/shadow-button.png"></div>
				<div class="row-fluid"><div class="span12">
                	
                    <div class="span6 right1">
                    	<h1>PASO 1 <img class="paso1-check" src="<?php echo IMAGE_URL; ?>/paso1-check.png"></h1>
						<h2>Pide una invitación</h2>
						<h4>Disponíbles: <span class="invitaciones_div">105 invitaciones</span><br> de 1,000</h4>
						<form style="margin:0" id="aweber" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="f1" enctype="multipart/form-data">
							<input name="username" type="hidden" placeholder="Tu Nombre" class="" value="" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tu Nombre'">
							
							<input name="email" type="text" placeholder="Ingrese su dirección de correo electrónico" class=" validate[required,custom[email]]" value="" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ingrese su dirección de correo electrónico'" data-prompt-position="bottomLeft">
							<?php
								if (($variables['error']) != '') { ?>
								<div class="" style="color: red; font-size: 12px; margin-bottom:7px;">
								<?php 
									echo $variables['error'];
								?></div> <?php } ?>
								
								<?php
								
								if (isset($_SESSION['DuplicatEmail']) && $_SESSION['DuplicatEmail'] != '') { ?>
									<div class="" style="color: red; font-size: 12px; margin-bottom:5px;">
									<?php
									echo $_SESSION['DuplicatEmail'];
									unset($_SESSION['DuplicatEmail']);
									?></div><?php
								}
							?>
							<input type="hidden" name="action"  value="aweberform"/>
							<input type="hidden" name="ref"  value="<?php echo $_GET['ref']?>"/>
                        	<input name="" type="submit" value="Enviar Invitación ->">
                        </form>
					</div>
					<div class="span6">
					    <div class="video-container" style=" border:none;">
                            <iframe src="//fast.wistia.net/embed/iframe/wuyclqi0wh" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="460" height="259"></iframe>
							<script src="//fast.wistia.net/assets/external/E-v1.js" async></script>
                        </div>
				    </div>
                </div></div>
                
                	<?php// if(isset($vid) && $vid!=''){ ?>
                    <div class="row-fluid"><div class="span12 text-center smart">
                    <p><span class="text-green">S</span><span class="text-red">M</span><span class="text-skyblue">A</span><span class="text-yellow">R</span><span class="text-dark-red">T</span></p>
                    <p>CROWDFUNDING</p>
                    </div>
                    </div>			

                
                <?php// } ?>
                
			</div>

		</div>
        <div class="span12 text-center ver-funciona">
        <div><img src="<?php echo IMAGE_URL?>/top-arrow.png"></div>
        <div><img src="<?php echo IMAGE_URL?>/ver-funciona.png"></div>
        </div>
	</div>
</div>
</div>
<div class="container" id="landing-page-bottom2">
	<div class="row-fluid">
		<div class="span12">
			<div class="home_bg">
                <div class="row-fluid">
                	<div class="span12 partners">
                		<img src="<?php echo IMAGE_URL?>/partners.jpg">
                	</div>
                </div>
                <div class="row-fluid">
                	<div class="span12 Notice">
                		<p>AVISO IMPORTANTE : Los ejemplos mencionados en esta página son extraordinarios . No sirven como garantía de un fondeo exitoso. Están diseñados para dar una idea de lo que puede ser posible . Un fondeo exitoso de cada sueño. como con cualquier cosa en la vida esta sujeto a todo tipo de variables. Nuestro objetivo es ayudarte lograr tus sueños comenzando por tomar una decisión informada de cual es ese sueño.</p>
                	</div>
                </div>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo JS_URL; ?>/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
		var timer_counter = 1;
           $( document ).ready(function() {
		<?php if($gid=='' && $vid==''){ ?>
		//$('body').css('height', '1550');
		<?php } else if($gid!='' && $vid=='') { ?>
		$('body').css('height', '1640');
		<?php } else if($gid=='' && $vid!=''){ ?>
		$('body').css('height', '1640');
		<?php } ?>
		<?php if($ref!=''){ ?>
		$('body').css('height', '1640');
		<?php } ?>

//$('body').css('height', '1640');
                $('.my_target').bind('click',function(event){
					
                    var $anchor = $(this);
                    
                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top
                    }, 1500,'easeInOutExpo');
                    /*
                    if you don't want to use the easing effects:
                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top
                    }, 1000);
                    */
                    event.preventDefault();
                });
				if(timer_counter<='4')
				{
				 	setInterval('show_data()', 4000);
				}
            });
		
	function show_data()
        {    

            
         var invitaciones_arr = ["105 invitaciones","102 invitaciones", "<font color='#FF0000'>97 invitaciones</font>", "<font color='#FF0000'>96 invitaciones</font>"];
          var lat_val = invitaciones_arr[timer_counter];
        
         $('.invitaciones_div').html(lat_val).fadeIn(4000);

			

        timer_counter = timer_counter+1;
		//if(timer_counter>='4')
			// {
				//timer_counter = 0;
			// }
    	}
        </script>	
			
