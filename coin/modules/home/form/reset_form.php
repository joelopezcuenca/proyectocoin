﻿<div id="forgot_password" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> × </button>
    <div class="form_logo"><img src="<?php echo IMAGE_URL; ?>/login-new-logo.png"></div>
    <h3 id="myModalLabel"><!--¿Olvidaste tu contraseña?-->RECUPERA TU CONTRASEÑA</h3>
  </div>
  <form id="forgot_password_form" action="<?php echo MODULE_URL;?>/home/code/reset_code.php" method="post" name="f1" enctype="multipart/form-data" >
    <div class="modal-body">
      <div class="popup">
        <div class=""> 
          <!--<span style="text-align: center;"><b>¿Olvidaste tu contraseña?</b></span>--> 
        </div>
        <p>Ingrese su correo electrónico para restablecer su contraseña</p>
        <div class="select_file">
          <label>correo electrónico</label>
          <input class="validate[required]" type="email" name="email_id" placeholder="Correo electrónico">
        </div>
      </div>
    </div>
    <div class="modal-footer">
    <div class="Restablecer">
      <button class="btn btn-success">RECUPERAR</button>
    </div>
    <hr />
    <div class="Cancelar"><a href="#" data-dismiss="modal">Cancelar</a></div>
    <input name="action" type="hidden" value="resetpassword" />
  </form>
</div>
</div>
