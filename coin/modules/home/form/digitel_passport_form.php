﻿<style>
.popup .name2 {
    float: left;
    font-size: 12px;
    font-weight: bold;
    text-align: left;
    width: 25%;
    margin-bottom: 15px;
}
	
.popup .discription {
    width: 75%;
    margin-bottom: 15px;
}
.popup.detail {
    width: 100%;
}
.popup .images {
    width: 100%;
    text-align: left;
}
.popup .images img{
    width: 120px;
}
.popup .images img {
    height: 110px;
    margin: 10px 0 0 10px;
    width: 118px;
}

.irf-new-popup{padding:10px; background:#fff; float: left; width: 95%;}
/*.size_adjust {
	height:200px !important;
	width:500px !important;
}*/

	
</style>

<div class="left_part">
	<?php
	include_once (INCLUDE_PATH . "/home_left_navigation.php");
	$user_id=$_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part">
	<div class="boxes round pasport">
    	<div class="cover-page4">
    		<img alt="image" src="<?php echo IMAGE_URL; ?>/passport.png" class="cover-image">
			<!-- <img alt="image" src="<?php echo IMAGE_URL; ?>/Passport-Stamps.jpg" class="cover-image"> -->
    		<img alt="image" src="<?php echo IMAGE_URL; ?>/pasport.png" class="profile-image">
    		<h1 class="name2">Pasaporte Digital</h1>
	 	</div>
	 	<?php 
	 
	 	$achieved_goal_count = count($goal_array) ; ?>
        <h1>Sueños logrados: <?php echo $achieved_goal_count ; ?></h1>
        <div class="row-fluid">
        	<div class="span12">
        		<!-- <div class="flexslider"> -->
        		<ul class="slides">
        		<?php for($i=0;$i<$achieved_goal_count;$i++)
				{
					$bucket_id =  $goal_array[$i]['id'] ;
				// to select single image to be dispalyed on digital passport section
				$achievement_img	=  $goalObj->select_visible_user_goal_image($user_id,$bucket_id);
				// to select all images for particular achievement for detail section 
				$all_goal_img_array	=  $goalObj->select_all_user_goal_images($user_id,$bucket_id);
					?>
					<li id="<?php echo "achieve_div_".$i ?>">
            	<div class="" >
            		<?php 
            		 @$img_detail = explode(".",$achievement_img['images']) ;
            		 $extension=end($img_detail) ;
					
            		if($extension=="" || empty($img_detail))
					$achievement_img['images'] = "1_1406227238.jpg" ;
            		?>
                	<img src="<?php echo IMAGE_ADMIN_GOAL_URL; ?>/<?php echo $achievement_img['images'] ?>" alt="tour">
                    <h3><?php echo substr($goal_array[$i]['bucket_name'],0,40) ?></h3>
                    <p><?php echo substr($goal_array[$i]['bucket_mdesc_l'],0,38) ?></p>
                    <a class="inline" href="#view_detail_<?php echo $i ; ?>" style="text-align: center">Ver más..</a>
        
           	    </div>
           	    </li>
<div style='display:none'>
<div id='view_detail_<?php echo $i ; ?>' class='irf-new-popup'>
<div class="popup detail">
					<div class="name2">
						Logro Nombre :
					</div>
					<div class="discription">
					<?php	echo $goal_array[$i]['bucket_name'] ; ?>
					</div>
					<div class="name2">
						al respecto:
					</div>
					<div class="discription">
						<?php	echo $goal_array[$i]['bucket_mdesc_l'] ; ?>
					</div>
					<div class="name2">
						Imágenes :
					</div>
					<div class="images">
						<?php for($j=0;$jend=count($all_goal_img_array),$j<$jend;$j++)
						{
							?>
						<img src="<?php echo IMAGE_ADMIN_GOAL_URL; ?>/<?php echo $all_goal_img_array[$j]['images'] ?>" alt="goal Image">&nbsp;
						<?php } ?>
					</div>
				</div>
</div>
</div>
      
           	<input type="hidden" id="achievement_count" value="<?php echo count($goal_array) ; ?>">
           	    <?php } ?>
            </ul>
				<!-- </div> -->
            </div>
             <?php if($achieved_goal_count>5) { ?>
               
		<div class="loadmore-but">
        <a id="loadmore_digitalpassport" class="hidethis" href="javascript:void(0);">Ver más</a>
       </div>
		
		<?php } ?>
        </div>
    <div class="cls"></div>
	</div>
</div>
