﻿<style>
.modal-backdrop {
    background-color: #fff;
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 1040;
}
</style>
<div class="row-fluid">
	<div class="span12">
		<div class="foot-links">
			<a href="#getTerms" data-toggle="modal">Términos y Condiciones</a> | <a href="#getPolicy" data-toggle="modal">Politicas de privacidad</a> | <a href="#getRefund" data-toggle="modal">Politicas de reembolso</a> <br/> &nbsp; Todos los derechos reservados <a href="<?php echo DEFAULT_URL?>">www.proyectocoin.com</a> 2016
		</div>
	</div>
</div>

<div id="getTerms" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<img src="<?php echo IMAGE_URL?>/popup_cross.png" />
		</button>
		<h3 id="myModalLabel"><?php echo ucfirst($termValue['page_title']); ?></h3>
	</div>
	<? /*
		 * Modal for Profile Photo
		 */
	?>

	<div class="modal-body">
		<div class="boxes round faq" style="text-align: left;padding:5px 25px;">
			<?php echo $termValue['page_content']; ?>
		</div>
	</div>
	<div class="modal-footer">

		<input name="action" type="hidden" value="add_goal" />
		<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/>

	</div>
</div>

<div id="getPolicy" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<img src="<?php echo IMAGE_URL?>/popup_cross.png" />
		</button>
		<h3 id="myModalLabel"><?php echo ucfirst($policyValue['page_title']); ?></h3>
	</div>
	<div class="modal-body">
		<div class="boxes round faq" style="text-align: left;padding:5px 25px;">
			<?php echo $policyValue['page_content']; ?>
		</div>
	</div>
	<div class="modal-footer">
	</div>
</div>

<div id="getRefund" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<img src="<?php echo IMAGE_URL?>/popup_cross.png" />
		</button>
		<h3 id="myModalLabel"><?php echo ucfirst($refundValue['page_title']); ?></h3>
	</div>
	<div class="modal-body">
		<div class="boxes round faq" style="text-align: left;padding:5px 25px;">
			<?php echo $refundValue['page_content']; ?>
		</div>
	</div>
	<div class="modal-footer">
	</div>
</div>
<div id="getAffiliate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<img src="<?php echo IMAGE_URL?>/popup_cross.png" />
		</button>
		<h3 id="myModalLabel"><?php echo ucfirst($affiliateValue['page_title']); ?></h3>
	</div>
	<div class="modal-body">
		<div class="boxes round faq" style="text-align: left;padding:5px 25px;">
			<?php echo $affiliateValue['page_content']; ?>
		</div>
	</div>
	<div class="modal-footer">
	</div>
</div>