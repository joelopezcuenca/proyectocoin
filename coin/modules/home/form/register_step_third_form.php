<header class="inicio-video">
  <div class="container">
    <div class="row-fluid">
      <div class="span12"> <img width="66" src="<?php echo IMAGE_URL?>/landing_logo_icon.png"> </div>
    </div>
  </div>
</header>
<p class="green_text"><img src="<?php echo IMAGE_URL?>/comprar-right-icon.png"> ¡TU FONDEO FUE EXITOSO!</p>

<div class="login-video new_design">
	<div class="container">
        <div class="row-fluid">
			<div class="span12 step posi_rela">
           	    <p><img src="<?php echo IMAGE_URL?>/login_step.png" alt="logo"></p>
				<p class="grey-text ft22">
					¡Bienvenido <span><?php echo $userdatal_funel['name'] ?></span>!<br />
						Ya solo faltan 2 pasos para comenzar<br />
						a Fondear tus sueños.
				</p>
			</div>
		</div>
		<div class="row-fluid">
			<form name="stepthird"  method="post" id="stepthird" action="<?php $_SERVER['PHP_SELF']; ?>">
				<div class="span12">
					<div style="color:red; text-align: center;">
					<?php
					if (($variables['userexisterror']) != '') {
						echo $variables['userexisterror'];
					}
					?>
				</div>
				<p class="grey-text ft18 width310">1.- Crea tu nombre de usuario:<br />Ejemplo: "<span>juan86</span>" </p>
					<div class="input_field staepthird">
						<input type="text" value="" placeholder="usuario"  name="username"  class="search-query" onfocus="this.placeholder = ''" onblur="this.placeholder ='usuario'">
                        <i>No utilizes espacios entre las letras</i>
						
					</div>
					<p class="grey-text ft18 width310">2.- Crea tu contraseña:</p>
					<div class="input_field staepthird">
					<input type="password" value="" placeholder="contraseña" name="password" class="search-query" onfocus="this.placeholder = ''" onblur="this.placeholder ='contraseña'">
                    <i>tu contraseña debe tener más de 8 dígitos</i>						
					</div>
					<div class=" PdTop20 comenzar staepthird">
						<input type="hidden" name="action" value="stepthird" />
						<input name="submit" type="submit" value="Comenzar">
					</div>
					
					
					
				<input type="hidden" name="meta_web_form_id" value="1812549614" />
				<input type="hidden" name="meta_split_id" value="" />
				<div style="display: none;">
					<input type="hidden" name="meta_web_form_id" value="1812549614" />
					<input type="hidden" name="meta_split_id" value="" />
					<input type="hidden" name="listname" value="lacomunidadcoin" />
					<input type="hidden" name="redirect" value="<?php echo $url;?>" id="redirect_c87288d8e1a93e3e2a6a4e7b8cf11171" />
					<input type="hidden" name="meta_adtracking" value="BienvenidoalaComunidad" />
					<input type="hidden" name="meta_message" value="1" />
					<input type="hidden" name="meta_required" value="name,email" />
					<input type="hidden" name="meta_tooltip" value="" />
				</div>
				<input type="hidden" name="listname" value="sisomo_salesfun" />
				<input type="hidden" name="redirect" value="<?php echo $url;?>" id="redirect_3c458152b39070e386b108fe92f50c84" />
				<input type="hidden" name="ref" value="<?php echo $ref; ?>">
				 <input type="hidden" name="uid" value="<?php echo $_POST['custom']; ?>">
				</div>
				
			</form>

		</div>
	</div>
</div>
