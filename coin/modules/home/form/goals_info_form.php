<div class="left_part">

	<?php
	include_once (INCLUDE_PATH . "/home_left_navigation.php");

	$user_id = $_SESSION["AD_user_id"];
	?>
</div>

<div class="right_part">

	<div class="boxes round pasport">

		<div class="cover-page3">

			<img class="cover-icon" src="<?php echo IMAGE_URL; ?>/world-icon.png" alt="image">

			<h1 class="name3">Metas de la Comunidad
			<div class="input-prepend irf">

				<form>

					<span class="add-on"><img src="<?php echo IMAGE_URL; ?>/1404990268_67.png" alt="image"></span>

					<input name="showbucketlist" type="text" placeholder="Ideas de sueño" onkeyup="Show_BucketList(this.value);"/>

				</form>

			</div><div id="display"></div></h1>

		</div>

		<div class="greybar">

			<ul class="goalsinfo-ir">

				<li class="le">
					<a style="color: black" href="javascript:void(0);" onclick="categorization_goal('newest')">Nuevas metas</a>
				</li>

				<li class="po">
					<a style="color: black" href="javascript:void(0);" onclick="categorization_goal('popular')">Populares</a>
				</li>

				<li class="">
					<a style="color: black" href="#add_profile" data-toggle="modal">+ Agregar nueva</a>
				</li>

			</ul>

		</div>

		<div class="row-fluid">

			<div id="goal_main_div" class="span12 goals-info">
<?php

for($i=0;$iend=count($goal_all_array),$i<$iend;$i++){

?>

<div id="<?php echo "bucket_div_".$i ?>" class="span4 masonryImage">
<?php // if(empty($goal_all_array[$i]['bg_img'])){ ?>
				<img class="bg" src="<?php echo IMAGE_URL; ?>/White-bg.jpg" alt="image">
				<?php // }else{ ?>
				<!-- <img class="bg" src="<?php echo IMAGE_ADMIN_GOAL_URL."/".$goal_all_array[$i]['bg_img'] ; ?>" alt="image"> -->
				<?php // } ?>
				<div class="goals-info">

				<span><img style="border-radius:27px; "  src="<?php echo IMAGE_URL; ?>/<?php echo $goal_all_array[$i]['bucket_icon']; ?>" alt="icon"></span>

				<h1><?php echo wordwrap($goal_all_array[$i]['bucket_name'], 15, "\n"); ?></h1>

				<p><?php echo substr(wordwrap($goal_all_array[$i]['bucket_desc'], 20, "\n"), 0, 80); ?></p>

				</div>
				<?php
$condition = "share_bucket_id=".$goal_all_array[$i]['id']." and user_id=".$user_id ;
$parent_array =  $goalObj->search_child_parent($condition) ;

if($goal_all_array[$i]['user_id']!==$user_id && $goal_all_array[$i]['user_id']!==$parent_array['parent_id']){
				?>
				<span><a href="<?php echo MODULE_URL."/home/code/goal_code.php?goalid=".$goal_all_array[$i]['id'] ?>&action=add_goal_in_my_list&parent_id=<?php echo $goal_all_array[$i]['user_id']; ?>">+ Agregar</a></span>
				<?php }else{ ?>
				<span><a class="blue-bg" href="#">+ Agregar</a></span>
				<?php } ?>
				</div>

				<?php } ?>

				<input type="hidden" id="bucketcount" value="<?php echo count($goal_all_array); ?>">

				<?php if(count($goal_all_array)>5) {
				?>

				<div class="loadmore-but">

				<a id="loadmore" class="hidethis" href="javascript:void(0);">Ver más</a>

				</div>

				<?php } ?>
			</div>

		</div>

		<div class="cls"></div>

	</div>

</div>

<!--this popup is for new sharing feature of bucket list -->

<div id="share_goal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header">

		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">

			×

		</button>

		<h3 id="myModalLabel">&nbsp;</h3>

	</div>

	<? /*

	 * Modal for Profile Photo

	 */
	?>

	<form id="unlock_goal_form" action="<?php echo MODULE_URL; ?>/home/bucket_list.php" method="post" name="f2" enctype="multipart/form-data" >

		<div class="modal-body">

			<div class="popup">

				<div class="share-goal-image"><img alt="image" src="<?php echo IMAGE_URL . "/" . $share_popup_goal[0]['bucket_icon']; ?>" class="">
				</div>

				<div class="share-goal-info">

					<div>
						<h1>[<?php echo $userdata1['name']; ?>]Comparté tu Logro!</h1>
					</div>

					<p>
						Al compartirlo con tus Amigos

						Ganarás más Coins, los cuáles

						Te permitirán seguir ganando Dinero

						Para lograr más Aventurás

					</p>

				</div>

			</div>

		</div>

		<div class="modal-footer">

			<div class="fl">
				<a href="<?php echo MODULE_URL; ?>/home/bucket_list.php">No quiero ganar Coins</a>
			</div>

			<div class="share-but-ir">

				<span> <a target="_blank" href="https://twitter.com/intent/tweet?&text=<?php echo "A new goal(". $share_popup_goal[0]['bucket_name'].") added by '".$_SESSION['User_url']."'" ?>"> <img src="<?php echo IMAGE_URL; ?>/twitter.png" alt="icon"> </a> &nbsp; <a target="_blank" href="http://www.facebook.com/share.php?u=http://www.comunidadcoin.com/coin/modules/home/share_bucket_fb.php?goal=<?php echo $share_popup_goal[0]['bucket_name']; ?>"> <img src="<?php echo IMAGE_URL; ?>/fb.png" alt="icon" id="share_button"> </a> </span>

			</div>

	</form>

	<? /*

	 * End of Modal

	 */
	?>

</div>

</div>

<!-- to share unlocked goal -->

<div id="add_profile" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header">

		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">

			×

		</button>

		<h3 id="myModalLabel">Agrega tu Nueva Meta</h3>

	</div>

	<? /*

	 * Modal for Profile Photo

	 */
	?>

	<form id="add_goal_form" action="<?php echo MODULE_URL; ?>/home/code/goal_code.php" method="post" name="f1" enctype="multipart/form-data" >
		<div class="modal-body">
			<div class="popup">
				<div class="name">
					<span style="color: red;">*</span>Título :
				</div>
				<div class="select_file">
					<input class="validate[required]" data-prompt-position="bottomRight:-50"  name="goal_name" type="text">
				</div>
				<div class="name">
					<span style="color: red;">*</span>Descripción:
				</div>
				<div class="inp">
					<textarea class="validate[required]" data-prompt-position="bottomRight:-50"  name="goal_description"></textarea>
				</div>
				<div class="name">
					<span style="color: red;">*</span>Categoría :
				</div>
				<div class="select_file">
					<select class="validate[required]" name="select_icon">
						<option value="air-traivel.png" selected="selected">Viajar</option>
						<option value="brief-bage.png">Desarrollo Profesional</option>
						<option value="dating-bage.png">Amor</option>
						<option value="dollar.png">Dinero</option>
						<option value="dumbl-bage.png">Salud</option>
						<option value="face-bage.png">Diversión</option>
						<option value="flag-bage.png">Libertad</option>
						<option value="foot-bage.png">Explorar</option>
						<option value="hand-bage.png">Amigos</option>
						<option value="thumb-bage.png">Aventura</option>
						<option value="unknown-bage.png" >Desarrollo Personal</option>
						<option value="otra.png">Otra</option>
					</select>
				</div>
				<!-- <div class="name">
				<span style="color: red;">*</span>Sube la imagen de fondo:
				</div>
				<div class="select_file">
				<input class="validate[required]" type="file" name="background_image" id="background_image">
				</div> -->
			</div>
		</div>
		<div class="modal-footer">

			<div class="fr">
				<button class="btn btn-success">
					Guardar en mi Lista
				</button>
			</div>
			<div class="fr">
				<div class="onoffswitch">
					<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
					<label class="onoffswitch-label" for="myonoffswitch"> <span class="onoffswitch-inner"></span> <span class="onoffswitch-switch"></span> </label>
				</div>
			</div>
			<input name="action" type="hidden" value="add_goal" />
			<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/>
	</form>

	<? /*

	 * End of Modal

	 */
	?>

</div>

</div>

<!-- here the sharing unlocked goal ends -- >

<!-- Modal Modified By Irfaan sir 18th June 2014-->

<!-- Modal -->

<div id="myModal" class="modal ir hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="login-video">

		<div class="container">

			<div class="row-fluid">

				<div class="span12 step">

					<div class="login-logo">

						<img alt="logo" src="<?php echo IMAGE_URL; ?>/logo.png">

					</div>

					<div class=" PdTop40 ft16">

						Creando tu Perfil...

					</div>

					<div class="progress progress-striped">

						<div class="bar" style="width: 75%;"></div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<!-- Modal -->

<div id="myModal2" class="modal ir hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="login-video">

		<div class="container">

			<div class="row-fluid">

				<div class="span12 step">

					<div class="login-logo">

						<img alt="logo" src="<?php echo IMAGE_URL; ?>/logo.png">

					</div>

					<div class=" PdTop40 ft16">

						Creando tu Perfil...

					</div>

					<div class="PdTop20">

						<a href="succes.html"> <img alt="icon" src="<?php echo IMAGE_URL; ?>/checkmark.png"></a>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php

if($_SESSION['OPEN_POPUP']==1)

{

?>

<script type="text/javascript">
	function submitForm() {

		$("#updatephoto").submit();

	}

	function submitForm1() {

		$("#add_goal_form").submit();

	}

	function submitForm2() {

		$("#unlock_goal_form").submit();

	}

</script>

<script type="text/javascript">
	$(document).ready(function() {

		$('#myModal').modal('show');

		setTimeout(function() {

			$('#myModal').modal('hide');

			$('#myModal2').modal('show');

			setTimeout(function() {

				$('#myModal2').modal('hide');

			}, 12000);

		}, 20000);

	});

</script>

<?php }

$_SESSION['OPEN_POPUP']=0;
?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#add_goal_form").validationEngine({
			promptPosition : "centerRight",
			scroll : true
		});
		$bucket_count = $('#bucketcount').val();

		for ( $i = 6; $i < $bucket_count; $i++) {

			$('#bucket_div_' + $i).hide();

		}

		$i = 6;

		$('#loadmore').click(function() {

			$bucket_count = $('#bucketcount').val();

			for ( $j = $i; $j < ($i + 6); $j++) {

				$('#bucket_div_' + $j).show("75000");

			}

			$i = $i + 6;

			if ($i >= $bucket_count) {

				$('.hidethis').hide();

			}

		});

	});

</script>

