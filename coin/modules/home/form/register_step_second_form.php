<script>
    var ticksCount = '0.75';
</script>
<script src="//fast.wistia.net/assets/external/iframe-api-v1.js"></script>
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>



<header class="inicio-video">
    <div class="container">
        <div class="row-fluid">
            <div class="span12"> <a href="<?php echo DEFAULT_URL ?>"><img width="66" src="<?php echo IMAGE_URL ?>/landing_logo_icon.png"></a> </div>
        </div>
    </div>
</header>

<section id="top_video">
    <div class="container">
        <div class="row-fluid">
            <div class="span12">
<input type="hidden" name="current_user_id" id="current_user_id" value="<?php echo base64_encode($_SESSION['AD_user_id'])?>" />
                <div class="embed-responsive embed-responsive-16by9" id="ifrm">
                    <iframe  class="embed-responsive-item" src="http://www.socioscoin.com.usrfiles.com/html/525b74_4b519b06dfbbf8605f81439db4cee12c.html?rel=0" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" ></iframe>
                </div>
				
                <div class="timer_sec">
                    <span id="button_span">Comienza a Fondear tus sueños en …</span>
                    <div id="centered_div">
                        <div id="counter">
                            <div id="counter_item1" class="counter_item">
                                <div class="front"></div>
                                <div class="digit digit0"></div>
                            </div>
                            <div id="counter_item2" class="counter_item">
                                <div class="front"></div>
                                <div class="digit digit0"></div>
                            </div>

                            <div id="counter_item4" class="counter_item">
                                <div class="front"></div>
                                <div class="digit digit0"></div>
                            </div>
                            <div id="counter_item5" class="counter_item">
                                <div class="front"></div>
                                <div class="digit digit0"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="comprar_page" class="inicio-video-page">
    <div class="container">
        <div class="row-fluid comprar_top_part">
            <div class="span12">
                <div class="logos-part">
                    <span class="comprar-right-icon"><img src="<?php echo IMAGE_URL ?>/comprar-right-icon-1.png"></span>
                    <p>Como lo viste en:</p>
                    <span class="comprar-logos-image"><img src="<?php echo IMAGE_URL ?>/comprar-logos-image-1.png"> <img src="<?php echo IMAGE_URL ?>/comprar-logos-image-2.png"></span>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="white_wrapper">
                <div class="span3">
                    <h1>Proyecto COIN</h1>
                    <h5>PLATAFORMA: COIN.OS</h5>
                    <img class="comprar-coin" src="<?php echo IMAGE_URL ?>/comprar-coin.png">
                   <!-- <a class="Acceso_Coin" href="#producto_part">Acceso a Coin</a> -->
                    <p class="dicen">Lo que dicen los miembros <br>de nuestra Comunidad.</p>


                    <div class="thumbnail adjust1">
                        <div class="span3">
                            <img class="media-object img-rounded img-responsive" src="<?php echo IMAGE_URL ?>/member_1.png">
                        </div>
                        <div class="span9">
                            <div class="caption">
                                <p class="text-info lead adjust2">Ricardo Martinez</p>
                                <img class="comprar-star" src="<?php echo IMAGE_URL ?>/star-rating.png">
                                <p>La Plataforma esta buenisima! No
                                    tuve problemas en entender como
                                    usarla y todo es super claro y 
                                    desde la primer samana ya habia
                                    cumplido 1 de mis metas.</p>
                            </div>
                        </div>
                    </div>

                    <div class="thumbnail adjust1">
                        <div class="span3">
                            <img class="media-object img-rounded img-responsive" src="<?php echo IMAGE_URL ?>/member_2.png">
                        </div>
                        <div class="span9">
                            <div class="caption">
                                <p class="text-info lead adjust2">Sergio Andres</p>
                                <img class="comprar-star" src="<?php echo IMAGE_URL ?>/comprar-star.png">
                                <p>Tengo 19 anos y no tenia idea de
                                    que queria estudiar. ni a que
                                    dedicarme. la plataforma solita.
                                    me ayudo aclarar mis metas...lo
                                    mejor fue que mientras aprendia 
                                    comenze a generar ingresos.</p>
                            </div>
                        </div>
                    </div>

                    <div class="thumbnail adjust1">
                        <div class="span3">
                            <img class="media-object img-rounded img-responsive" src="<?php echo IMAGE_URL ?>/member_3.png">
                        </div>
                        <div class="span9">
                            <div class="caption">
                                <p class="text-info lead adjust2">Jorge Herrera</p>
                                <img class="comprar-star" src="<?php echo IMAGE_URL ?>/star-rating.png">
                                <p>Este programa es lo MAXIMO!!! mi
                                    hobby y mis suenos era ser
                                    paracaidista profesional...esto me
                                    a ayudado a liberar tanto de mi
                                    tiempo y a generar los ingresos
                                    suficientes. que ya voy por mi
                                    salto numero 30!!!</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="span9">
                    <div class="co_part">
                        <h4>Descripción del Programa y de la Plataforma:</h4>
                        <p>¿Qué pasaría si con solo 7 pasos pudieras lograr ese sueño que siempre has deseado? Hoy en día ya es posible Fondear tus sueños gracias al Crowfunding (Fondeo Colectivo). No solo gracias a nuestra plataforma y al programa de 7 pasos para Fondear tus sueños. Si no que también te volverás parte de una Comunidad de Soñadores que piensan como tú y que ya están Fondeando y Logrando sus sueños. <!-- <a href="javascript:void(0)">Salvando al planeta un Árbol a la vez</a>--></p>
                        <div class="grayish">
                            <h4>Ejemplos del Producto</h4>
                            <div class="screenshotz">
                                <div id="content-6" class="content horizontal-images">
                                    <ul>
                                        <li><img src="<?php echo IMAGE_URL ?>/screenshot1.png" /></li>
                                        <li><img src="<?php echo IMAGE_URL ?>/screenshot2.png" /></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="guarantee_part">
                            <p><img src="<?php echo IMAGE_URL ?>/compar-guarantee.png">Completa tu orden Hoy y si por cualquier motivo, no te enamoras de este producto en los próximos 14 días, te reembolsaremos tu dinero, SIN PREGUNTAS.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3Z0fAUOl7Bx07I6X7o3P8aJWSQX2MfRN";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
