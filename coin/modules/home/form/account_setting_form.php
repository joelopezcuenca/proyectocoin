<style>
    .bdr-ir {
        text-align: left !important;
        float: left;
    }
</style>
<script>
    $(document).ready(function () {
        jQuery("#profile").validationEngine();
        jQuery("#pass_change").validationEngine({promptPosition: "bottomRight"});
    });</script>
<div class="left_part">
    <?php
    include_once (INCLUDE_PATH . "/home_left_navigation.php");
    ?>
</div>

<div class="right_part edit_profile">
    <div class="row-fluid">
        <div class="span11 offset1">
            <h3>Edita tu Perfil</h3>
        </div>
    </div>
    <div class="row-fluid grayborder PaddT30">
        <div class="span8 offset2">
    
			
            <form class="form-horizontal" id="profile" method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
                <div class="control-group">
                    <label class="control-label" for="inputEmail">Foto de Perfil</label>
                    <div class="controls setpicture"> 
                        <?php if ($allUserDetails['profile_pic'] != '') { ?>
                            <img  src="<?php echo IMAGE_ADMIN_PROFILE_URL . "/" . $allUserDetails['profile_pic']; ?>" style="width: 47px;">
                        <?php } else { ?>
                            <img src="<?php echo IMAGE_URL; ?>/edit-profile-img.jpg">
                        <?php } ?>
                        <input type="file" name="profile_pic">				<?php if ($allUserDetails['profile_pic'] != '') { ?>			<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png" /> 			<?php } ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Foto de portada</label>
                    <div class="controls setpicture">
                        <?php if ($allUserDetails['background_pic'] != '') { ?>
                            <img  src="<?php echo DEFAULT_URL . "/background_pic/" . $allUserDetails['background_pic']; ?>" style="width: 47px; height:50px;">
                        <?php } ?>
                        <input type="file" name="background_pic" id="bg_pic">		   <?php if ($allUserDetails['background_pic'] != '') { ?>		   <img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png"/>		   <?php } ?>		   
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Nombre</label>
                    <div class="controls">
                        <input class="span12" name="name" value="<?php echo $allUserDetails['name']; ?>" type="text" placeholder="Name" />			<?php if ($allUserDetails['name'] != '') { ?>				<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png" />			<?php } ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Email</label>
                    <div class="controls">
                        <input class="span12" name="email" readonly value="<?php echo $allUserDetails['email']; ?>" type="text" placeholder="Email">			<?php if ($allUserDetails['email'] != '') { ?>			<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png" />			<?php } ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Sexo</label>
                    <div class="controls">
                        <select name="gender" class="span12">			  <option value="">-Otro-</option>
                            <option value="1" <?php
                            if ($allUserDetails['gender'] == '1') {
                                echo "Selected";
                            };
                            ?> >Hombre</option>
                            <option value="0" <?php
                            if ($allUserDetails['gender'] == '0') {
                                echo "Selected";
                            };
                            ?> >Mujer</option>
                        </select>			<?php if ($allUserDetails['gender'] != '') { ?>			<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png" /> 			<?php } ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">¿A qué te dedicas?</label>
                    <div class="controls">
                        <textarea name="bio" id="bio" maxlength="100" class="span12" rows="5"><?php echo $allUserDetails['bio']; ?></textarea>						<?php if ($allUserDetails['bio'] != '') { ?>				<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png"  /> 			<?php } ?>			<br/><span id="chars">100</span> Caracteres restantes
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Facebook</label>
                    <div class="controls">
                        <input class="span12" name="website" value="<?php echo $allUserDetails['website']; ?>" type="text" id="inputEmail" placeholder="http://www.com">			<?php if ($allUserDetails['website'] != '') { ?>				<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png" />			<?php } ?>			
                    </div>
                </div>
                <input name="action" type="hidden" value="update">
                <div class="control-group">
                    <div class="controls text-right">
                        <button type="submit" class="btn btn-warning">Guardar cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span11 offset1">
            <h3>Cambiar o Actualizar contraseña</h3>
        </div>
    </div>
    <div class="row-fluid">  
        <?php
        if (!empty($_SESSION['message_password'])) {
            echo "<span style='color:Green;margin-left:130px;margin-bottom:30px;'>" . $_SESSION['message_password'] . "</span>";
            $_SESSION['message_password'] = "";
        }
        ?>
    </div>	
    <div class="row-fluid grayborder PaddT30">
        <div class="span8 offset2">
            <?php
            if (!empty($_GET['error'])) {
                echo "<span style='color:red;margin-left:130px;margin-bottom:30px;'>" . $_GET['error'] . "</span>";
            }
            ?>
            <form class="form-horizontal" id="pass_change" method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Contraseña actual</label>
                    <div class="controls">
                        <input class="validate[required]  span12" type="password" name="current_pass" type="text" id="inputEmail" placeholder="Contraseña actual">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Nueva contraseña</label>		
                    <div class="controls">
                        <input class="validate[required]  span12" type="password" name="new_pass" type="text" id="new_pass" placeholder="Nueva contraseña">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Confirma tu contraseña</label>		
                    <div class="controls">
                        <input class="validate[required,equals[new_pass]] span12" type="password" name="con_pass" type="text" id="inputEmail" placeholder="Confirma tu contraseña">
                    </div>
                </div>
                <input name="action" type="hidden" value="update_password">
                <div class="control-group">
                    <div class="controls text-right">
                        <button type="submit" class="btn btn-warning">Guardar cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="cls"></div>
</div><script>var maxLength = 100;
    var ini_length = $('#bio').val().length;
    var ini_length = maxLength - ini_length;
    $('#chars').text(ini_length);
    $('#bio').keyup(function () {
        var length = $(this).val().length;
        var length = maxLength - length;
        $('#chars').text(length);
    });</script>