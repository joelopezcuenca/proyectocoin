﻿<span style="color: #fff;padding: 10px 0 15px;"><b>Al continuar confirmas que eres mayor de 18 años y estás de acuerdo con nuestros <a href="#terms" data-toggle="modal">términos y condiciones</a></b></span>
						<div id="terms" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									×
								</button>
								<h3 id="myModalLabel">términos y condiciones</h3>
							</div>
							<? /*
								 * Modal for Profile Photo
								 */
							?>

							<div class="modal-body">
								<div class="boxes round faq" style="text-align: left;padding:5px 25px;">
									<h1 style="color:#A7D155;">TÉRMINOS DE USO</h1>
									Este sitio web contiene material que puede no ser adecuado para los niños sin supervisión de un adulto. Aunque no hay información pornográfica u ofensiva en este sitio web, se recomienda que exista supervisión de un adulto cuando quien lee este material, es menor de 18 años debido al contenido adulto materia madura.
									<br />
									<br />
									Desde “comunidadcoin.com” (llamado desde ahora “nosotros”) no somos responsables por las acciones de los niños sin supervisión en un adulto. Cada visitante de nuestro sitio tiene la elección de poder verla, y continuar, y eso significa que usted entiende y acepta la responsabilidad, liberando así a comunidadcoin.com y nuestro proveedor de servicios, de toda responsabilidad. Si usted decide seguir nuestro consejo es su decisión y de ninguna manera somos responsables de sus acciones como resultado de ver este sitio web.
									<br />
									<br />
									Descargo de responsabilidad de la publicidad o refrendo
									<br />
									<br />
									Cualquier referencia hecha en el sitio para ningún producto comercial o servicio no indica o implica que nos avala ni recomienda el asunto mencionado.
									<br />
									<br />
									Dado que es posible acceder a comunidadcoin.com desde otros países en los que no hacen negocios, comunidadcoin.com no representa que se trata de hacer negocios en todas las áreas, o que sus servicios estarán disponibles en un país en particular o jurisdicción. Estos productos sólo estarán disponibles en el cumplimiento de las leyes, y sólo cuando nos decidimos a ofrecerlos.
									<h1 style="color:#A7D155;">NEGACIÓN DE RESPONSABILIDAD</h1>
									No somos ni terapeutas, médicos, asesores legales, asesores financieros, asesores informáticos y si lo necesita usted no puede bajo ningún concepto, prescindir de un profesional en esa área. Estamos ofreciendo material de entretenimiento, de referencia, y para obtener un conocimiento básico de las muchas facetas de las áreas mencionadas. Sin embargo, el contenido de comunidadcoin.com y todos los productos que vendemos, no se ofrece como consejo médico, legal, financiero o de un experto y se le anima a consultar a un profesional preguntas, a pedir la opinión de expertos y asesoramiento. Toda la información, de esta o cualquier otra fuente, debe ser revisada cuidadosamente con su un profesional de su confianza antes de actuar sobre ella.
									<br />
									<br />
									Nosotros (comunidadcoin.com) no hacemos ninguna representación o garantía en cuanto a la idoneidad de los contenidos de este sitio web.
									<br />
									<br />
									Con respecto a los productos de información, documentos, artículos, videos, audios y toda la información disponible de comunidadcoin.com, sus asesores, afiliados, empleados, directores, agentes y cualquier otra parte involucrada en la creación, producción o entrega del sitio no son responsables por las mismas, y no asumimos ninguna responsabilidad por cualquier daño directo, incidental, consecuente, indirecto o punitivo que surja de su acceso, uso, exhibición, copia o transmisión de la información contenida en este sitio.
									<br />
									<br />
									Estamos proporcionando este sitio y sus contenidos “tal cual” y no ofrecemos garantías ni representaciones de ningún tipo con respecto a este sitio o su contenido.
									<br />
									<br />
									carlosbarahona.com renuncia a todas las representaciones y garantías, incluyendo pero sin limitarse a garantías de comerciabilidad, título y no infracción.

									<br />
									<br />
									Además, comunidadcoin.com exime de cualquier responsabilidad legal o responsabilidad por la exactitud, integridad o utilidad de cualquier información, procedimiento, método, aparato, producto o proceso publicado en el sitio.

									<br />
									<br />
									Creemos que el contenido de comunidadcoin.com es exacto, y no ofrecemos ninguna garantía o representación en cuanto a la exactitud, integridad o actualidad de los contenidos de este sitio web, ya sea a partir de la fecha de publicación o en cualquier momento posterior. Los lectores de los contenidos de este sitio web son los únicos responsables de la verificación de cualquier contenido antes de su dependencia de los contenidos publicados en este documento.

									<h1 style="color:#A7D155;">POLÍTICAS DE USO ACEPTABLE</h1>

									En Bold Media Interactive S de RL potenciamos un uso responsable y respetuoso hacia los demás de nuestros servicios y por este motivo no están permitidas las siguientes prácticas:

									<br />
									<br />
									•  Uso abusivo del servicio ofertado, incluyendo pero no limitado a las siguientes actividades:
									<br />
									<br />
									<strong>-Spamming:</strong> envío de comunicación comercial no solicitada mediante correo electrónico, además dicha práctica esta prohibida por la LSSI (Ley de Servicios de la Sociedad de la Información) y la L.O. Protección de Datos. No se admitirán el spam a terceros para promocionar un sitio web alojado por nosotros ya que bloquearía nuestros servidores y seríamos penalizados.
									<br />
									<br />
									<strong>-Trolling:</strong> envío de mensajes ofensivos a una comunidad de usuarios para generar numerosas respuestas.
									<br />
									<br />
									<strong>-Prácticas abusivas para terceros,</strong> incluyendo pero no limitado a actividades tales como el <strong>mailbombing</strong>(envío de mensajes múltiples sin cambio significativo en el contenido al mismo destinatario), <strong>intrusión en sistemas informáticos ajenos sin autorización, falseo de la dirección de correo o de sus cabeceras, usurpación de identidad y actividades similares.</strong>

									<br />
									<br />
									•  Ilegalidades de cualquier tipo, incluido pero no limitado a actividades tales como la distribución de propiedad intelectual sobre la cual no se dispongan derechos ("warez",mp3 y similares), fraude, estafa, exportación ilegal y cualquier actividad o servicio que viole la legislación vigente.

									<br />
									<br />
									<strong> •  Distribución, alojamiento, fomento y cualquier tipo de actividad ilegal, tales como las relacionada con la pornografía infantil, el servicio será suspendido de inmediato y se archivarán las pruebas para su posterior remisión junto con la demanda a las autoridades pertinentes. </strong>

									<br />
									<br />
									•  Uso erróneo de los recursos del sistema, incluyendo pero no limitado al uso de programas que consuman un exceso de CPU o memoria y la reventa del acceso a nuestras aplicaciones de servidor (CGI's). No se admitirá en ningún caso la instalación de servidores de juegos, robots de IRC, proxys o programas con funcionalidad similar.

									<strong> Nos reservamos el derecho a determinar qué constituye un uso abusivo del servicio</strong>
									<br />
									<strong>para la suspensión inmediata del servicio. </strong>
									<h1 style="color:#A7D155;">AVISO DE PRIVACIDAD</h1>
									<strong> DE DATOS DE PAGO AL CLIENTE</strong>&nbsp;&nbsp;&nbsp;
									En Bold Media Interactive S de R.L. (COIN). utilizamos conexión SSL de nuestro sistema para procesar los pagos que se realizan a través de la plataforma de pago PayPal. Al trabajar con la empresa más importantes de pago online, garantizamos el procesamiento rápido y seguro de los pagos de los clientes . No sólo no hacemos un registro de datos de la tarjeta de crédito de los clientes en nuestro sistema, sino que también - para la máxima seguridad en línea - NO compartimos esta información con terceros
									&nbsp;&nbsp;&nbsp;
									<strong>PRIVACIDAD DE CUENTA DEL CLIENTE</strong>
									&nbsp;&nbsp;
									Consideramos que los datos de registro de los clientes y el contenido de todos los mensajes de los clientes privados y confidenciales . La cabecera de los mensajes que los clientes envían contendrá su nombre (dirección de correo electrónico) y Protocolo de Internet (IP). No supervisaremos ni divulgar cualquier contenido de correo electrónico del cliente o los datos de registro a menos que estemos obligados a hacerlo intencionalmente:  - para hacer cumplir estos Términos y Condiciones  - por la ley - para defendernos de cualquier acción legal - para proteger nuestra propiedad   Dado que las contraseñas de los clientes son esenciales para la protección de su privacidad, recomendamos a todos los clientes que no compartan sus contraseñas con nadie y cambiarlos con frecuencia. Además, los clientes se comprometen a respetar la privacidad y confidencialidad de los demás usuarios.
									<strong>DE LOS DATOS DE REGISTRO CLIENTES</strong>Bold Media Interactive S de R.L. (COIN) recoge cierta información de los clientes durante el proceso de registro de la cuenta. La información de identificación personal que Bold Media Interactive S de R.L. (COIN) recopila incluye el nombre y apellidos. Bold Media Interactive S de R.L. (COIN) también recoge información no personal como país, edad, género… Los clientes pueden actualizar su información de registro en cualquier momento desde su Área de Cliente – Datos de la Cuenta.
									&nbsp;&nbsp;&nbsp;

									Consideramos que los datos de registro de los clientes y el contenido de todos los mensajes de los clientes privados y confidenciales . La cabecera de los mensajes que los clientes envían contendrá su nombre (dirección de correo electrónico) y Protocolo de Internet (IP). No supervisaremos ni divulgar cualquier contenido de correo electrónico del cliente o los datos de registro a menos que estemos obligados a hacerlo
									&nbsp;&nbsp;&nbsp;
									intencionalmente: -
									&nbsp;&nbsp;&nbsp;
									:- para hacer cumplir estos Términos y Condiciones  - por la ley - para defendernos de cualquier acción legal - para proteger nuestra propiedad Dado que las contraseñas de los clientes son esenciales para la protección de su privacidad, recomendamos a todos los clientes que no compartan sus contraseñas con nadie y cambiarlos con frecuencia. Además, los clientes se comprometen a respetar la privacidad y confidencialidad de los demás usuarios
									&nbsp;&nbsp;&nbsp;
									<strong>PRIVACIDAD DE LOS DATOS DE REGISTRO CLIENTES</strong> Media Interactive S de R.L. (COIN) recoge cierta información de los clientes durante el proceso de registro de la cuenta. La información de identificación personal que Bold Media Interactive S de R.L. (COIN) recopila incluye el nombre y apellidos.Bold Media Interactive S de R.L. (COIN) también recoge información no personal como país, edad, género… Los clientes pueden actualizar su información de registro en cualquier momento desde su Área de Cliente – Datos de la Cuenta.
									<br />
									<br />
									<br />
									Bold Media Interactive S de R.L. (COIN). mantendrá la información de identificación personal de todos los clientes privados y confidenciales y no la compartirá con terceras partes no autorizadas. Podemos revelar estadísticas agregadas de registro en relación con nuestras actividades de marketing.
									&nbsp;&nbsp;&nbsp;<strong>SEGURIDAD DE LA INFORMACIÓN,</strong>Bold Media Interactive S de R.L. (COIN) se ha comprometido a proteger la seguridad de los clientes recogidos como información personal. Para ello, utilizamos las medidas técnicas de seguridad para evitar la pérdida, mal uso, alteración o divulgación no autorizada de información bajo nuestro control. Utilizamos medidas de seguridad incluyendo y no limitado a: los procedimientos físicos, electrónicos y administrativos para salvaguardar y asegurar la información que recopilamos en línea. Cuando le pedimos a los clientes o usuarios que proporcionen la información financiera, por ejemplo, el número de tarjeta de crédito, los datos están protegidos mediante Secure Sockets Layer (SSL) .
									&nbsp;&nbsp;&nbsp;<strong>ACCESO A LA INFORMACIÓN PERSONAL</strong>Los clientes de Bold Media Interactive S de R.L. (COIN) tienen acceso a su información personal, incluyendo nombre completo, nombre de la empresa, dirección de correo electrónico e información de tarjeta de crédito. Los clientes tienen la capacidad de eliminar los datos relacionados con su nombre completo, nombre de la empresa , y la información de facturación de identificación que ya no se almacenan en nuestra base de datos, con la excepción de las direcciones de correo electrónico, los registros de la red y los casos en que se haya llevado a cabo una actividad fraudulenta según lo determinado por las fuerzas del orden o la gestión Bold Media Interactive S de R.L. (COIN), en cuyo caso la información puede permanecer en nuestra base de datos de forma indefinida con el fin de ayudar a facilitar la prevención del abuso repetido en el futuro.
									&nbsp;&nbsp;&nbsp;.<strong>POR QUÉ NECESITAMOS ESTA INFORMACIÓN?</strong>  Se necesita información de registro con el fin de operar el sitio, para proporcionar apoyo a la cuenta del cliente, con fines estadísticos…
									<br />
									<br />
									<br />
									Bold Media Interactive S de R.L. (COIN) enviará un correo electrónico de bienvenida a cada nuevo cliente, que contiene información sobre las características proporcionadas por el Servicio, así como los datos del Área de Cliente y los datos de acceso al FTP . Bold Media Interactive S de R.L. (COIN) también puede enviar notificaciones del sistema a los clientes  en relación a cualquier problema de actualización, mantenimiento u otros servicios relacionados con las cuentas de alojamiento de los clientes
									&nbsp;&nbsp;&nbsp;
									<strong>ARCHIVOS DE REGISTRO</strong>&nbsp;&nbsp;&nbsp;Utilizamos direcciones IP para analizar tendencias, administrar nuestro sitio y los servidores y recopilar información demográfica.

									<br />
									<br />
									<br />
									Las direcciones IP no están vinculadas a información personal. Es posible que la información personal de un cliente puede ser incluida en los archivos de registro debido a las funciones normales de las direcciones IP y navegar por la Web.
									&nbsp;&nbsp;<strong>COOKIES</strong>&nbsp;&nbsp;Utilizamos diferentes cookies en nuestro sitio web con el objetivo de facilitar la navegación tanto como sea posible posible . Las cookies son pequeños archivos de texto que colocamos en el ordenador del cliente o dispositivo móvil para saber lo que quiere utilizar de nuestro sitio sin darnos su identidad.   A continuación puedes aprender más acerca de las cookies y cómo controlarlos o eliminarlos.  Aquí están las cookies que utilizamos en nuestro sitio :

									&nbsp;&nbsp;&nbsp;
									<strong>Cookies</strong>&nbsp;&nbsp;&nbsp;Utilizamos cookies para recordar tu nombre de usuario y lo que has puesto en el carrito de compra. Son imprescindibles para operar en nuestro sitio web.
									&nbsp;&nbsp;&nbsp;
									<strong>Acceso a Redes Sociales</strong>&nbsp;&nbsp;&nbsp; En nuestro sitio utilizamos los accesos a las redes sociales que permiten a los usuarios compartir páginas web en Twitter y Facebook . Debes revisar las políticas respectivas de cada uno de estos sitios para ver exactamente cómo utilizan su información y decidir si se quiere usar o eliminar dicha información.   <strong>Servicios web externos  </strong>&nbsp;&nbsp;&nbsp;Utilizamos un servicio web externo, canal YouTube en nuestro sitio para mostrar vídeos en nuestras páginas web.
									<br />
									<br />
									Además ,utilizamos un servicio de chat en vivo con el consiguiente uso de cookies para recordar tu nombre de usuario al inicio de sesión para ofrecer soporte en tiempo real para los clientes.&nbsp;&nbsp;
									<strong>  Google Analytics  </strong>&nbsp;&nbsp;Utilizamos este servicio para valorar el sitio web y mejorar en la medida de lo posible el posicionamiento del mismo. Los datos recogidos son todos anónimos.
									&nbsp;&nbsp;<strong>   Foro comentarios  </strong>&nbsp;&nbsp;Para dejar un comentario en nuestro foro, tienes que ser un miembro registrado. Para ello, es necesario un nombre de usuario y una dirección de correo electrónico.
									&nbsp;&nbsp;<strong>   Procesamiento de pagos  </strong>Para procesar los pagos en línea, se requiere la dirección de facturación de la tarjeta de crédito utilizada y nuestros proveedores de pago externo como PayPal u otras plataformas, requieren datos de tu tarjeta de crédito.  Suponemos que estás de acuerdo con el uso de cookies en este sitio web. Si no estás de acuerdo, puedes abandonar el sitio o eliminar las cookies.   Por favor recuerda que si decides eliminar las cookies o utilizar el navegador de forma anónima, es posible que ciertas secciones de este sitio Web no funcionen correctamente.
									<br />
									<br />
									<strong>   EXCLUSIÓN  </strong>&nbsp;&nbsp;Los clientes reciben las actualizaciones del sistema de forma automática así como los boletines, las facturas y el resto de notificaciones de correo electrónico de los servicios. Cualquier cliente que recibe voluntariamente estas actualizaciones puede cancelar su suscripción en cualquier momento pero no puede darse de baja de las notificaciones importantes del sistema.
									&nbsp;&nbsp;<strong>   CUMPLIMIENTO DE LAS LEYES Y APLICACIÓN DE LA LEY  </strong>&nbsp;&nbsp;&nbsp;Bold Media Interactive S de R.L. (COIN) colabora con el gobierno y las fuerzas del orden para hacer cumplir la ley y revelaremos cualquier información relevante sobre los usuarios que nos lo soliciten.

									<h1 style="color:#A7D155;">CAMBIOS EN LA POLÍTICA DE PRIVACIDAD</h1>
									Cualquier cambio en nuestra política de privacidad será transmitida a todos nuestros clientes y revocarán cualquier versión anterior de nuestras Políticas de Privacidad.

								</div>
							</div>
							<div class="modal-footer">

								<input name="action" type="hidden" value="add_goal" />
								<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="login-logo-bottom">
			<img width="25" src="<?php echo IMAGE_URL; ?>/white-logo.png" alt="logo">
		</div>

	</div>
</div>