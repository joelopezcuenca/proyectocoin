<style>
.bdr-ir {
	text-align: left !important;
	float: left;
}
</style>
<script>
$(document).ready(function() {
	jQuery("#profile").validationEngine();  
	jQuery("#pass_change").validationEngine({promptPosition : "bottomRight"});  
});
</script>
<div class="left_part">
  <?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		?>
</div>
<div class="right_part edit_profile">
  <div class="row-fluid">
    <div class="span11 offset1">
      <h3>Edit Profile</h3>
    </div>
  </div>
  <div class="row-fluid">  
	  <?php 
			if(!empty($_SESSION['message'])){ 
				echo "<span style='color:Green;margin-left:180px;'>".$_SESSION['message']."</span>";
				$_SESSION['message'] = "";
			}
			?>
	</div>	
  <div class="row-fluid grayborder PaddT30">
    <div class="span8 offset2">
      <form class="form-horizontal" id="profile" method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
        <div class="control-group">
          <label class="control-label" for="inputEmail">Profile photo</label>
          <div class="controls setpicture"> 
          	
          	
          		<?php if($allUserDetails['profile_pic'] != ''){ ?>
			<img  src="<?php echo IMAGE_ADMIN_PROFILE_URL."/".$allUserDetails['profile_pic']; ?>" style="width: 47px;">
			<?php ?> 
	<?php }else{ ?>
		<img src="<?php echo IMAGE_URL; ?>/edit-profile-img.jpg">
	<?php } ?>
	
	
			
				<!--<a href="#">Set Picture</a>-->
			<input type="file" name="profile_pic" id="inputPassword">	
			</div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputPassword">Background photo</label>
          <div class="controls">
            <input type="file" name="background_pic" id="inputPassword">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputPassword">Name</label>
          <div class="controls">
            <input class="span12" name="name" value="<?php echo $allUserDetails['name']; ?>" type="text" id="inputEmail" placeholder="Name">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputPassword">Email</label>
          <div class="controls">
            <input class="span12" name="email" readonly value="<?php echo $allUserDetails['email']; ?>" type="text" id="inputEmail" placeholder="Email">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputPassword">Sex</label>
          <div class="controls">
            <select name="gender" class="span12">
              <option value="1" <?php if($allUserDetails['gender']=='1'){ ?> selected="selected" <?php } ?> >Male</option>
              <option value="0" <?php if($allUserDetails['gender']=='0'){ ?> selected="selected" <?php } ?>>Female</option>
            </select>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputPassword">Bio</label>
          <div class="controls">
            <textarea name="bio" class="span12" rows="5"><?php echo $allUserDetails['bio']; ?></textarea>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputPassword">Website</label>
          <div class="controls">
            <input class="span12" name="website" value="<?php echo $allUserDetails['website']; ?>" type="text" id="inputEmail" placeholder="http://www.com">
          </div>
        </div>
		<input name="action" type="hidden" value="update">
        <div class="control-group">
          <div class="controls text-right">
            <button type="submit" class="btn btn-warning">Update Profile</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span11 offset1">
      <h3>Change Password</h3>
    </div>
  </div>
  <div class="row-fluid">  
	  <?php 
			if(!empty($_SESSION['message_password'])){ 
				echo "<span style='color:red;margin-left:180px;'>".$_SESSION['message_password']."</span>";
				$_SESSION['message_password'] = "";
			}
			?>
	</div>	
  <div class="row-fluid grayborder PaddT30">
    <div class="span8 offset2">
		<?php
			if(!empty($_GET['error'])){
				echo "<span style='color:red;margin-left:180px;'>".$_GET['error']."</span>";
			}
		?>
      <form class="form-horizontal" id="pass_change" method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
        <div class="control-group">
          <label class="control-label" for="inputPassword">Password</label>
          <div class="controls">
            <input class="validate[required]  span12" type="password" name="current_pass" type="text" id="inputEmail" placeholder="Current Password">
          </div>
        </div>
        <div class="control-group">
          <div class="controls">
            <input class="validate[required]  span12" type="password" name="new_pass" type="text" id="new_pass" placeholder="New Password">
          </div>
        </div>
        <div class="control-group">
          <div class="controls">
            <input class="validate[required,equals[new_pass]] span12" type="password" name="con_pass" type="text" id="inputEmail" placeholder="Conform Password">
          </div>
        </div>
		<input name="action" type="hidden" value="update_password">
        <div class="control-group">
          <div class="controls text-right">
            <button type="submit" class="btn btn-warning">Update Profile</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="cls"></div>
</div>