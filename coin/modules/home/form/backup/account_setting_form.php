<style>
	.bdr-ir {
		text-align: left !important;
		float: left;
	}
	.btn
	{
		color:#000 !important;
	}
</style>
<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		?>
	</div>

<div class="right_part">
	<div class="boxes round">
		<div class="account-setting">
			<h1>Configuración de tu cuenta</h1>
			<span style="color: green;">
			<?php
			if ($variable['updatemessage'] != '') {echo $variable['updatemessage'];
			}
			?>
			</span>
			<ul>
				<!-- <li>
					<div class="width25">
						Name
					</div>
					<div class="fl">
						<strong><?php echo $userdetails['name']; ?></strong>
					</div>
					<a class="" href="javascript:void(0);" id="addButton">
					<div class="fr">
						Edit
					</div>
					</a>
				</li> -->
				<form id="account_name" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="f1" enctype="multipart/form-data" >
				<div id="NameEditContainer">
					<div class="width25">
						<h4>Edit Name</h4>
					</div>
					<div class="width75" style="padding: 10px;">
						<div class="cls">
								<input name="name" type="text" value="<?php echo $userdetails['name']; ?>" style="float: left;">
							</div>
							<p>&nbsp;</p>
					</div>
					<input name="action" type="hidden" value="NameSave" />
					
					<div class="bdr-ir">
								<button class="btn btn-success">
									Guardar cambios
								</button>
								
					</div>
				</div>
				</form>
				
				<li>
					<div class="width25">
						Email
					</div>
					<div class="fl">
						Primary:&nbsp;<strong><?php echo $userdetails['email']; ?></strong>
					</div>
					<a class="" href="javascript:void(0);" id="addButtonemail">
					<div class="fr">
						Editar
					</div></a>
				</li>
				<form id="account_email" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="account_email" enctype="multipart/form-data" >
				
				<div id="EmailEditContainer">
					<div class="width25">
						<h4>Edit Email</h4>
					</div>
						<div class="width75" style="padding: 10px;">
						
							<div class="cls"> 
							 <input name="email" type="email" value="<?php echo $userdetails['email']; ?>" style="float: left;">
							</div>
							<p>&nbsp;</p>
					</div>
					<input name="action" type="hidden" value="EmailSave" />
					
					<div class="bdr-ir">
								<button class="btn btn-success">
									Guardar cambios
								</button>
								
					</div>
				</div>
				</form>
				<li>
					<div class="width25"> 
						Contraseña
					</div>
					<div class="fl">
						<strong>********************</strong>
					</div>
					<a class="" href="javascript:void(0);" id="addpassword">
					<div class="fr">
						Editar
					</div>
					</a>
				</li>
				<form id="account_password" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="account_password" enctype="multipart/form-data" >
				<div id="PasswordEditContainer">
					<div class="width25">
						<h4>Nueva contraseña</h4>
					</div>
						<div class="width75" style="padding: 10px;">
						
							<div class="cls">
								<input class="validate[required]" name="confirmpassword1" type="password" id="confirmpassword1" style="float: left;">
							</div>
							
							<input name="action" type="hidden" value="PasswordSave" />
							</div>
							<div class="width25">
						<h4>Confirmar contraseña</h4>
					</div>
						<div class="width75" style="padding: 10px;">
						
							<div class="cls">
								<input class="validate[required]" name="confirmpassword" type="password" id="confirmpassword" style="float: left;">
							</div>
							
							<input name="action" type="hidden" value="PasswordSave" />
							</div>
							
							<div class="bdr-ir">
								<a class="btn btn-success" onclick="submitForm3()" href="javascript:void(0);">
									Guardar Cambios
								</a>
								
							</div>
					</div>
				
				</form></div>
			</ul>
			<div class="cls"></div>
		</div>
		<div class="cls"></div>
		</div>
	
</div>

<script type="text/javascript">
	function submitForm1() {
		$("#account_name").submit();
	}

	function submitForm2() {
		$("#account_email").submit();
	}

	function submitForm3() {
		 var pass = $("#confirmpassword1").val() ;
		 var cpass = $("#confirmpassword").val() ;
		$.trim(pass);
		$.trim(cpass);
		if(pass=="" || cpass=="")
		{
			   $("#confirmpassword1").val("") ;
				$("#confirmpassword1").focus() ;
 				$("#confirmpassword").val("") ;
 				$("#confirmpassword").focus() ;
				alert("Password or confirm Password is Blank");
				return false ;
		}
		 if(cpass!=pass)
			{
				$("#confirmpassword1").val("") ;
				$("#confirmpassword1").focus() ;
 				$("#confirmpassword").val("") ;
 				$("#confirmpassword").focus() ;
				alert("Password mismatch");
				return false ;
			}
			if(pass.length<8)
			{
				$("#confirmpassword1").val("") ;
				$("#confirmpassword1").focus() ;
 				$("#confirmpassword").val("") ;
 				$("#confirmpassword").focus() ;
				alert("Password must be of atleast 8 characters");
				return false ;
			}
			else{
				$("#account_password").submit();
			}
		// $("#account_password").submit();
			// var pass = $("#confirmpassword1").val() ;
			// var cpass = $("#confirmpassword").val() ;
			// alert(pass) ;
// 		
			// if(cpass!=pass)
			// {
				// $("#confirmpassword1").val("") ;
				// $("#confirmpassword1").focus() ;
 				// $("#confirmpassword").val("") ;
 				// $("#confirmpassword").focus() ;
				// alert("Password mismatch");
				// return false ;
			// }
			// if(pass.length<8)
			// {
				// $("#confirmpassword1").val("") ;
				// $("#confirmpassword1").focus() ;
 				// $("#confirmpassword").val("") ;
 				// $("#confirmpassword").focus() ;
				// alert("Password must be of atleast 8 characters");
				// return false ;
			// }
		
	}

</script>
	<script>
		$(document).ready(function() {
			$("#account_password").validationEngine();
			$('#NameEditContainer').css('display', 'none');
			$('#EmailEditContainer').css('display', 'none');
			$('#PasswordEditContainer').css('display', 'none');
		
			$('#addButton').click(function() {
				$('#NameEditContainer').show();
			});

			$('#addpassword').click(function() {
				$('#PasswordEditContainer').show();
			});

			$('#addButtonemail').click(function() {
				$('#EmailEditContainer').show();

			});

		});
</script>
