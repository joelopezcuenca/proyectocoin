<div class="login-video">
  <div class="container">
    <div class="row-fluid">
        <div class="span12 meet">
          <div class="login-logo"><img src="<?php echo IMAGE_URL; ?>/logo.png" alt="logo"></div>
          <div class="form_head"><span class="blue"><?php echo $current_user ; ?></span></div>
          <div class="form_head mrbot">¿Qué Sueño quieres cumplir?</div>
          <div class="formpage_form">
          	<ul>
          		<?php for($i=0;$i<3;$i++){ ?>
            	<li>
                	<div class="icon"><img alt="icon" src="<?php echo IMAGE_URL; ?>/<?php echo $goal_array[$i]['bucket_icon'] ; ?>"></div>
                    <div class="info-follow">
                    	<h3><?php echo $goal_array[$i]['bucket_name'] ; ?></h3>
                        <p><?php //echo $goal_array[$i]['bucket_desc'] ; ?></p>
                    </div>
                    <div class="follow-but"> 
                    <a href="<?php echo MODULE_URL."/home/invitation.php?goalid=".$goal_array[$i]['id'] ?>&action=add_goal_in_my_list&parent_id=<?php echo $goal_array[$i]['user_id'] ; ?>">+ Agregar</a>
                    </div>
                    <div class="cls"></div>
                </li>
                <?php } ?>
                
                <li>
                	<div class="icon"><img alt="icon" src="<?php echo IMAGE_URL; ?>/<?php echo $goal_array[3]['bucket_icon'] ; ?>"></div>
                    <div class="info-follow">
                    	<h3><?php echo $goal_array[3]['bucket_name'] ; ?></h3>
                    </div>
                    <div class="follow-but"> <a href="#goal_categories" data-toggle="modal">+ Agregar</a></div>
					
   
                    <div class="cls"></div>
                </li>
            </ul>
            <div class="cls"></div>
          </div>
          <p class="grey-text ft16 width390"><span class="green">Sólo es capaz de realizar los sueños el que, cuando llega la hora, sabe estar despierto.</span>
                       </p>
                       <p class="width390 ft16" style="text-align:right; margin:auto">León Daudí</p>
	  </div>
    </div>
  </div>  
</div>
<script>
$(document).ready(function(){
	$("#add_goal_form").validationEngine({promptPosition : "centerRight", scroll: true});
});
function submitForm() {
		$("#add_goal_form").submit();
	}
</script>