<?php
ob_start();
include ("../../../conf/config.inc.php");
extract($_POST);
extract($_GET);
$utilityObj = new utility();
$objUser = new user();
$goalObj = new goal();
 /* $dbObj = new DB();  */
//Used for BucketList Autosuggestion details onselecting
if ($goalid != '') {
	$condition = "id=$goalid";
	$goaldata = $goalObj -> Select_Goal_Details($condition);
}
//**********************************
if ($action == "add_goal") {
	if ($onoffswitch == "on")
		$is_public = 1;
	else {
		$is_public = 0;
	}
	$category_Obj = new category;
	$condition="id = $category_id order by id desc";
	$allrecord = $category_Obj -> SelectAll_Category($condition);
	$select_icon = $allrecord[0]['image'];
	
	$goal_img = '';
	if(!empty($_FILES['incomplete_goel_image']['name'])){

		$goal_pic = $_FILES['incomplete_goel_image']['name'];
		$goal_img = time() . "_".$goal_pic;
		$storeDirectory = IMAGE_ADMIN_GOAL_DIR . "/$goal_img";
		$filename = $_FILES['incomplete_goel_image']['tmp_name'];
		move_uploaded_file($filename, $storeDirectory);
		
	}
	
	
	
	$goal_array = array('user_id' => $userid, 'bucket_name' => $goal_name, 'bucket_desc' => $goal_description, 'bucket_icon' => $select_icon,'bucket_image' => $goal_img, 'is_public' => $is_public, 'bg_img' => $bg_random ,'rating' => $ratting ,'category_id' => $category_id);
	/* echo "<pre>";
	print_r($_REQUEST); die; */ 
	//print_r($goal_array);die;
	
	$return_id = $goalObj -> add_goal($goal_array);
	
	
	
	
	
	header("location:" . MODULE_URL . "/home/bucket_list.php?bucket_send_id=" . $return_id . "&action2=share_bucket");
	exit ;
}
if ($action == "add_goal_in_my_list") {

	 $condition = "id=$goalid";
	$goal_detail = $goalObj -> select_particular_goal($condition);

	$goal_array = array('user_id' => $_SESSION['AD_user_id'], 'bucket_name' => $goal_detail[0]['bucket_name'], 'bucket_desc' => $goal_detail[0]['bucket_desc'], 'category_id' => $goal_detail[0]['category_id'],'rating' => $goal_detail[0]['rating'],'bucket_icon' => $goal_detail[0]['bucket_icon'], 'is_public' => 0);

	$return_id = $goalObj -> add_goal($goal_array);
	$goalImages = $goalObj->select_all_user_goal_images($goal_detail[0]['user_id'],$goalid);

	
	
	foreach($goalImages as $images_name) {
			$goal_img_array = array('bucket_id' => $return_id, 'user_id' => $_SESSION['AD_user_id'], 'images' => $images_name['images']);
			$goalObj -> insert_goal_images($goal_img_array);
		}
	
	
	$parent_child_array = array('user_id' => $_SESSION['AD_user_id'], 'parent_id' => $parent_id, 'share_bucket_id' => $goalid, 'new_bucket_id' => $return_id);
	
	$goalObj -> add_Bucket_parent_child($parent_child_array);
	
	
	
	
	header("location:" . MODULE_URL . "/home/bucket_list.php");
	exit ;
}
if ($action == "add_goal_autosuggestion") {
	$condition = "id=$goalid";
	$goaldata = $goalObj -> Select_Goal_Details($condition);
	$goal_array = array('user_id' => $userid, 'bucket_name' => $goaldata[0]['bucket_name'], 'bucket_icon' => $goaldata[0]['bucket_icon']);
	$return_id = $goalObj -> add_goal($goal_array);
	header("location:" . MODULE_URL . "/home/bucket_list.php");
	exit ;
}

if ($action == "unlock_goal") {

	//echo "<pre>"; print_r($_REQUEST); die;

/* $total_img_count = count($_FILES['unlock_file']['name']);
	if(!empty($_FILES['unlock_file']['name'][0])){
	for ($i = 0; $i < $total_img_count; $i++) {
		$goal_pic = $_FILES['unlock_file']['name'][$i];
		$r_explode = explode(".", $goal_pic);
		//$fileextension = end($r_explode);
		$fileextension = "jpg";
		$goal_img = ($i + 1) . "_" . time() . "." . $fileextension;
		$storeDirectory = IMAGE_ADMIN_GOAL_DIR . "/$goal_img";
		$AchievementstoreDirectory = IMAGE_ACHIEVEMENT_DIR . "/$goal_img";
		if ($i < 1) {
			$new = new SimpleImage;
			$new -> load($_FILES['unlock_file']['tmp_name'][$i]);
			$new -> resize(960, 960);
			$new -> save($AchievementstoreDirectory);
			$shown_image_name = $goal_img;
		}
		$filename = $_FILES['unlock_file']['tmp_name'][$i];
		move_uploaded_file($filename, $storeDirectory);
		$goal_img_array = array('bucket_id' => $bucket_id, 'user_id' => $userid, 'images' => $goal_img);
		$goalObj -> insert_goal_images($goal_img_array);
	}
}else{
	$goal_img = "Flat-right-icon11.png" ;
	$goal_img_array = array('bucket_id' => $bucket_id, 'user_id' => $userid, 'images' => $goal_img);
	$goalObj -> insert_goal_images($goal_img_array);
} */

	/* Add goal code start */
	//$is_public = 1;
	
	if ($onoffswitch == "on")
		$is_public = 1;
	else {
		$is_public = 0;
	}
	
		$category_Obj = new category;
		$condition="id = $complete_goal_category order by id desc";
		$allrecord = $category_Obj -> SelectAll_Category($condition);
		
	//echo '<br/> Bucket: '.$bucket_id.'<br/>';	
	if($bucket_id=='')
	{	
		$select_icon = $allrecord[0]['image'];
		
		$goal_img = '';
			//----Add goel image-----------
	if(!empty($_FILES['incomplete_goel_image']['name'])){

		$goal_pic = $_FILES['incomplete_goel_image']['name'];
		$goal_img = time()."_".$goal_pic;
		$storeDirectory = IMAGE_ADMIN_GOAL_DIR ."/$goal_img";
		$filename = $_FILES['incomplete_goel_image']['tmp_name'];
		move_uploaded_file($filename, $storeDirectory);
		
	}
		
		
		$goal_array = array('user_id' => $userid, 'bucket_name' => $complete_goal_title, 'bucket_desc' => $complete_goal_description, 'bucket_icon' => $select_icon,'bucket_image' => $goal_img, 'is_public' => $is_public, 'bg_img' => $bg_random ,'rating' => $complete_goal_periority ,'category_id' => $complete_goal_category);
		$bucket_id = $goalObj -> add_goal($goal_array);
		/* Add goal code end */
	}
	

	
	
	
	$images_val = explode(',',$unlock_files);
	if(!empty($images_val)){
		foreach($images_val as $images_name) {
			$goal_img_array = array('bucket_id' => $bucket_id, 'user_id' => $userid, 'images' => $images_name);
			$goalObj -> insert_goal_images($goal_img_array);
		}
	}else{
		$goal_img = "Flat-right-icon11.png" ;
		$goal_img_array = array('bucket_id' => $bucket_id, 'user_id' => $userid, 'images' => $goal_img);
		$goalObj -> insert_goal_images($goal_img_array);
	}
	$shown_image_name = $images_val[0];
	$make_date = strtotime($date);
	

	$goal_array = array('bucket_name' => $short_des, 'bucket_mdesc_l' => $goal_full_description,'short_des' => $short_des,'youtube' => $youtube , 'date'=>$make_date , 'share_type' => $share_type ,'bucket_image' => $shown_image_name, 'is_unlock' => 1);
	$condition = "id=$bucket_id";
	//echo $bucket_id;
	//echo "<pre>"; print_r($goal_array); die;
	$return_id = $goalObj -> unlock_goal($goal_array, $condition);
	
	$goal_notification_array = array("user_id" => $userid, "bucket_id" => $bucket_id, "message" => "You just unlocked one of your Goal..", "entry_date" => time(), "is_active" => 1);
	$goal_notofication_entry = $goalObj -> insert_goal_notification($goal_notification_array);
	header("location:" . MODULE_URL . "/home/mi_prefil.php?bucket_share_id=" . $bucket_id . "&actionm=share_unlocked_bucket");
	exit ;
}
?>