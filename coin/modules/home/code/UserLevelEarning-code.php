<?php 
########################################################
/*
 * Here all function which are being called
 * defined in functions.php as a name of leveluser.function.php
 * you will find every function as needed to modify but please
 * keep track that backup before modifying
 * 
 */ 

#########################################################
$objUser = new user();
$userDetails = $objUser -> getOnId($_SESSION['AD_user_id']);
$user_status=$userDetails['status'];
$node = binarytree($_SESSION['AD_user_id'], true, $level = 0);
unset($_SESSION['data']);
unset($_SESSION['status']);
$UserStatus = $node[1];
$uid = $userDetails['enroller'];
$k = 1;
$a = 0;
$b = 0;
$prouser = array();
$user = array();
$nodeUser = $node[0];
//for taking out status of user
$nodeUserstatus=$node[1];

for ($j = 1; $j < 11; $j++) {
	for ($i = 0; $i < count($nodeUser); $i++) {
		if ($nodeUser[$i][$j] != 0) {
			$a++;
		}
		if ($UserStatus[$i][$j] != 0) {
			$b++;
		}

	}
	//for basic user array
	array_push($user, $a);
	//for Pro user array
	array_push($prouser, $b);
	$a = 0;
	$b = 0;
}

##################################################################
/*
 *Here Each User Level calculation 
 *At this time at What level he is 
 *at Current
 */
##################################################################
for ($l = 0; $lend = count($nodeUser), $l < $lend; $l++) {
	for ($k = 1; $k <= 10; $k++) {
		if (array_key_exists($k, $nodeUser[$l])) {

			$level_array[$k][] = $nodeUser[$l][$k];
			if ($nodeUserStatus[$l][$k] == 1) {
				$level_current = $k;
				$level_statuswise_array[$k][] = $nodeUser[$l][$k];
			}
			break;
		}
	}

}
##################################################################
##################################################################
##################################################################
/*
 * Commision Calculation Code
 * we have calculated in such a way 4 % of 25 is $1 fixed
 * and then 4%,5% on each level * number of people get joined
 * under his level
 */
##################################################################

	$commision = 25;
	$level = 4;
	$levelper=4;
	$i = 1;
	$k = 0;
	$total_comm = 0;
	$comm = array();
	$count_user = count($prouser);
	while ($i <= 10 and $i <= $count_user) {
		
		if($i>1)
		{
			$commision=3.5;
		}
		if($i==1) //for first level user will get 1$ .it is fixed so i have calculated it with (25$*4/100) for its first level
		{
		$comm[] = (($commision * $level) / 100) * $prouser[$k];
		$comisionfirsttime=(($commision * $level) / 100) * $prouser[$k];
		}
		if($i>1)
		{
		$comm[] =(($commision * $levelper) / 100) * $prouser[$k];
		$commprevious = $comisionfirsttime+(($commision * $levelper) / 100) * $prouser[$k];
		$levelper++;
		}
		$total_comm = $total_comm + $comm[$k];
		$level++;
		$i++;
		$k++;
	}
	
##################################################################	
/*
 *This function calculate according to level
 * and return as well according to their defined class
 * by mistake it made here it should be in functions.php
 */
##################################################################
function levelcheckmre($level)
{
	switch($level)
	{
	case 1:$class='levelone-small';
	break;
	case 2:$class='leveltwo-small';
	break;
	case 3:$class='levelthree-small';
	break;
	case 4:$class='levelfour-small';
	break;
	case 5:$class='levelfive-small';
	break;
	case 6:$class='levelsix-small';
	break;
	case 7:$class='levelseven-small';
	break;
	case 8:$class='leveleight-small';
	break;
	case 9:$class='levelnine-small';
	break; 
	case 10:$class='levelten-small';
	break; 	
	}
	return $class;
	
} 
?>
