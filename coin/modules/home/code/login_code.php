<?php
//ob_start();
$loginObj = new LoginSystem();
if (isset($_SESSION['isLoggedIn'])) {
	header("location: " . MODULE_URL . "/home/index.php");
	exit ;
}

if (count($_POST))// will run only if some data is being posted to the login form.
{

	if ($action == 'login') {
		extract($_POST);
		$loginObj -> doLogin($username, $password, $remember);

	}

}
?>