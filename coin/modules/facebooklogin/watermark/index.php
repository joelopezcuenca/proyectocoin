<?php
    if (! extension_loaded('gd')) { // small check - are GD installed or not
        echo 'GD not installed, please install';
        exit;
    }

     $sOrigImg = "pic1.jpg";
     $sWmImg = "myprofile.jpg";

    $aImgInfo = getimagesize($sOrigImg);
    $aWmImgInfo = getimagesize($sWmImg);
    if (is_array($aImgInfo) && count($aImgInfo)) {
        header ("Content-type: image/jpeg");

        $iSrcWidth = $aImgInfo[0];
        $iSrcHeight = $aImgInfo[1];

        $iFrameSize = 15;

        $rImage = imagecreatetruecolor($iSrcWidth+$iFrameSize*2, $iSrcHeight+$iFrameSize*2); // creating new true color image
        $rSrcImage = imagecreatefromjpeg($sOrigImg); //  creating source image resource

        $aGrid[1] = imagecolorallocate($rImage, 130, 130, 130); // define colors for rectangular frame
        $aGrid[2] = imagecolorallocate($rImage, 150, 150, 150);  
        $aGrid[3] = imagecolorallocate($rImage, 170, 170, 170);  
        $aGrid[4] = imagecolorallocate($rImage, 190, 190, 190);  
        $aGrid[5] = imagecolorallocate($rImage, 210, 210, 210);  
        for ($i=1; $i<=5; $i++) { // our little frame will contain 5 rectangulars to emulate gradient
            imagefilledrectangle($rImage, $i*3, $i*3, ($iSrcWidth+$iFrameSize*2)-$i*3, ($iSrcHeight+$iFrameSize*2)-$i*3, $aGrid[$i]); // drawing filled rectangle
        } 

        imagecopy($rImage, $rSrcImage, $iFrameSize, $iFrameSize, 0, 0, $iSrcWidth, $iSrcHeight); // copy image to main resource image

        if (is_array($aWmImgInfo) && count($aWmImgInfo)) {
            $rWmImage = imagecreatefromjpeg($sWmImg); //  creating watermark image resource
            imagecopy($rImage, $rWmImage, $iSrcWidth-$aWmImgInfo[0], $iFrameSize, 0, 0, $aWmImgInfo[0], $aWmImgInfo[1]); // copy watermark image to main resource image
        }

        $iTextColor = imagecolorallocate($rImage, 255, 255, 255); // defining color for text
        $sIP = $_SERVER['REMOTE_ADDR']; // define guest ip
        imagestring($rImage, 10, $iFrameSize*2, $iFrameSize*2, "<h3>Name: Raghwendra Pathak</h3>", $iTextColor); // draw text

        imagestring($rImage,10, $iFrameSize*4, $iFrameSize*4, "Email:raghwendra@fpdemo.com", $iTextColor);
        
        imagestring($rImage, 10, $iFrameSize*6, $iFrameSize*6, "Developer:php,", $iTextColor);
        
        imagejpeg($rImage); // output as png image
    } else {
        echo 'wrong image';
        exit;
    }
?>