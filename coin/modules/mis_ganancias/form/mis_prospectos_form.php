﻿<?php /*
 * User Level Updation When User LogggedIn Automatic
 */
?>

<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/mis_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part tutorials">
<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
  <hr class="mar0" />
  <div class="round1">
		<div class="row-fluid">
			<?php //foreach($allrecordTutorials as $tutorials){ ?>
				<div class="span12">
					<div class="row-fluid">
                    <div class="span12">
                      <table class="table table-hover">
                      <thead>
                        <tr>
                          <!--<th>Tipo de Coin</th>-->
                          <!-- <th>Level</th>-->
                          <th>Fecha</th>
                          <th>Nombre</th>
                          <th>Correo</th>
						  <th>Borrar</th>
                        </tr>
                      </thead>
                      <tbody>
					  <?php
				
					  
					  if(!empty($userprospectosList)){
					  foreach($userprospectosList as $value) {
						  
						  if($value['enroller']==$value['real_enroller'])
						  {
							  $memberType = 'CO';
							  $level = $value['dl_level'];
						  }else{
							   $memberType = 'KC';
							    $level = $value['pl_level'];
						  }
						  
						  ?>
                        <tr id="record_tr_<?php echo $value['id'] ?>">
                         <!-- <td><?php echo $memberType?> </td> -->
                          <!--<td><?php echo $level;?> </td>-->
                          <td><?php echo date('m/d/y',$value['modified_date']);?> </td>
                          <td><?php echo $value['name'];?> </td>
                          <td><?php echo $value['email'];?> </td>
						  <td><img src="<?php echo IMAGE_URL; ?>/delete_icon.png" onclick="return delete_record('<?php echo $value['id'] ?>','<?php echo $value['email'] ?>');" style="cursor: pointer;" /></td>
                        </tr>
						
						<?php } } else { ?>
                       <tr><td align="center" colspan="5">Aún no tienes fondos.</td></tr> 
                    <?php } ?>  
                        
                      
                      </tbody>
                    </table>
                    </div>
                  </div>
				</div>
			<?php //} ?>
		</div>
  </div>
</div>



<div id="del_confirm_box" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="del_form_common" action="" method="post" name="goal form" enctype="multipart/form-data"   >
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
    </div>
    <div class="modal-body">
      <h1 id="myModalLabel">¿Seguro que quieres borrar a este contacto?</h1>
     
      <div>
        <div class="select_file">
		<input type="hidden" id="recordId" value="0" />
           <h4 id="myModal_email" class="text-center gray"></h4>
        </div>
       
        
      </div>
      <div class="PaddT20">
        <div class="row-fluid">
          <div class="span12">
            <button type="button" class="btn btn-default delete-but" data-dismiss="modal" onclick="return confirm_delete();">BORRAR</button>
			 <button type="button" class="btn btn-default cencel-but" data-dismiss="modal" aria-hidden="true">CANCELAR</button>
          </div>
        </div>
        
      </div>
    </div>
  </form>
</div>

		
<script>
	function delete_record(val,email)
	{
		jQuery('#myModal_email').html(email);
		jQuery('#recordId').val(val);
		 $('#del_confirm_box').modal('show');	
	}
	function confirm_delete()
	{
		var id = jQuery('#recordId').val();
		
		if(id==0)
		{
			return false;
		}else{
			  $.post("<?php echo MODULE_URL?>/mis_ganancias/ajax/mis_ajax.php", {action:'delete',recordId:id}, function(result){
					//alert(result);
					jQuery("#record_tr_"+id).hide();
			}); 
		}
		
	}
</script>
