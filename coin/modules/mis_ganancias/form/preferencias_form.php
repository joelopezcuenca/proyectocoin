﻿<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/mis_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part tutorials">
<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
  <hr class="mar0" />
  <div class="round1">
		<div class="row-fluid">
			<?php //foreach($allrecordTutorials as $tutorials){ ?>
				<div class="span12 preferencias">
                 
                  <div class="row-fluid">
                    <ul>
                      <li><h5>Método de pago</h5><!-- <i class="fa fa-plus text-right"></i>--></li>
                      <li><img src="<?php echo IMAGE_URL; ?>/PayPal-icon.png"> Personal PayPal <sub><?php echo $userDetailresult['paypal_email']?></sub> <a class="text-right" href="#">Borrar</a>
					  <a class="text-right" href="#paypal_email" data-toggle="modal"> Editar</a>
					  
					  </li>
					  <li>Cuando recibe un fondeo colectivo para cumplir tus sueños, Nosotros lo depositaremos a tu cuenta de manera mensual. Siempre y cuando pase de $50 USD. Por el momento nuestro sistema de pago seguro admite únicamente cobros y depósitos  a través de la plataforma PayPal, que puedes configurar y editado aquí . Si tienes más preguntas acerca de fechas de pago, facturación o cualquier otra duda nos puedes enviarnos un correo a: <a class="text-left" href="javascript:void(0)">admin@proyectocoin.com</a></li>
                      <!-- <li><img src="<?php echo IMAGE_URL; ?>/Master_Card_Icon.png"> Personal MasterCard <sub>Expires 08/2024</sub> <a class="text-right" href="#">Borrar</a> <a class="text-right" href="#master_card" data-toggle="modal"  >Editar</a></li>-->
                    </ul>
                  </div>
				</div>
			<?php //} ?>
		</div>
		<!-- <div class="row-fluid">
			<div class="span12 manage_form">
				<h4>EDITA TU INFORMACIÓN BANCARIA</h4>
				<p>ASEGÚRATE DE LLENAR TUS DATOS CORRECTAMENTE PARA PODER RECIBIR LOS DEPOSITOS DE TU FONDEO.</p>
				  <form id="patment_info_frm" name="patment_info_frm" action="<?php echo MODULE_URL ; ?>/mis_ganancias/preferencias.php?event=preferencias" method="post" name="goal form" enctype="multipart/form-data">
				  <input type="hidden" name="event" value="preferencias" />
				   <input type="hidden" name="action" value="save_pamentinformation" />
				   <input type="hidden" name="user_id" value="<?php echo $user_id ?>" /> -->
				  <!-- <div class="form-group">
					<label for="exampleInputEmail1">NOMBRE COMPLETO <a href="javascript:void(0)" data-toggle="tooltip" title="Name!"><img src="<?php echo IMAGE_URL; ?>/questionmark.png" /></a></label>
					<input type="text" class="form-control validate[required]"  data-prompt-position="bottomLeft"  name="name" id="name" placeholder="Juan Perez" value="<?php echo $payment_personl_detail['name'] ?>" />
				  </div> -->
				  
				 <!-- <div class="form-group">
					<label for="exampleInputPassword1">CITY <a href="javascript:void(0)" data-toggle="tooltip" title="City!"><img src="<?php echo IMAGE_URL; ?>/questionmark.png" /></a></label>
					<input type="text" class="form-control validate[required]"  data-prompt-position="bottomLeft" name="city" id="city" value="<?php echo $payment_personl_detail['city'] ?>" placeholder="City" />
				  </div> -->
				  
				 <!-- <div class="form-group">
					<label for="exampleInputFile">ADDRESS <a href="javascript:void(0)" data-toggle="tooltip" title="Address!" ><img src="<?php echo IMAGE_URL; ?>/questionmark.png" /></a></label>
					<input type="text" class="form-control validate[required]"  data-prompt-position="bottomLeft" name="address" id="address" value="<?php echo $payment_personl_detail['address'] ?>" placeholder="Address" />  
				  </div>-->
				  
				  <!--<div class="form-group">
					<label for="exampleInputFile">POSTSAL CODE <a href="javascript:void(0)" data-toggle="tooltip" title="Postal Code!"><img src="<?php echo IMAGE_URL; ?>/questionmark.png" /></a></label>
					<input type="text" class="form-control validate[required]"  data-prompt-position="bottomLeft" name="postal_code" value="<?php echo $payment_personl_detail['postal_code'] ?>" id="postal_code" placeholder="Postal code" />   
				  </div> -->
				  
				 <!-- <div class="form-group">
					<label for="exampleInputFile">NOMBRE DE TU BANCO <a href="javascript:void(0)" data-toggle="tooltip" title="Bank Name!"><img src="<?php echo IMAGE_URL; ?>/questionmark.png" /></a></label>
					<input type="text" class="form-control validate[required]"  data-prompt-position="bottomLeft" name="bank_name" id="bank_name" value="<?php echo $payment_personl_detail['bank_name'] ?>" placeholder="EJ: Santander" />
				  </div>
				   
				  <div class="form-group">
					<label for="exampleInputFile">CLABE BANCARÍA <a href="javascript:void(0)" data-toggle="tooltip" title="CLABE NUMBER!" ><img src="<?php echo IMAGE_URL; ?>/questionmark.png" /></a></label>
					<input type="text" class="form-control validate[required]"  data-prompt-position="bottomLeft" name="clabe_number" id="clabe_number" value="<?php echo $payment_personl_detail['clabe_number'] ?>" placeholder="" />
				  </div>
				  
				  <button type="submit" class="btn btn-default">Guardar cambios</button>
				</form>
				</div>
		</div>-->
  </div>
</div> 
<div id="paypal_email" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="paypal_email_frm" name="paypal_email_frm" action="<?php echo MODULE_URL ; ?>/mis_ganancias/preferencias.php?event=preferencias" method="post" name="goal form" enctype="multipart/form-data">
  <input type="hidden" name="event" value="preferencias" />
   <input type="hidden" name="action" value="set_paypalemail" />
   <input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
    </div>
    <div class="modal-body">
		<div class="inputmain">
			<input type="text" name="paypal_email" id="paypal_email" value="<?php echo $userDetailresult['paypal_email']?>" class="validate[required,custom[email]]"  data-prompt-position="bottomLeft" placeholder="Enter Your Email" />
			
		</div>
		<div class="inputmain tr">
			<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</butto>
			<button class="btn btn-warning">Submit</button>
		</div>
    </div>
  </form>
</div>

<div id="master_card" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="master_card_frm" name="master_card_frm" action="<?php echo MODULE_URL ; ?>/mis_ganancias/preferencias.php?event=preferencias" method="post" name="goal form" enctype="multipart/form-data">
  <input type="hidden" name="event" value="preferencias" />
   <input type="hidden" name="action" value="set_master_card_val" />
   <input type="hidden" name="user_id" value="<?php echo $user_id ?>" />
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
    </div>
    <div class="modal-body">
		<div class="inputmain">
		<select name="card_type" id="card_type" class="validate[required,]"  >
			<option value="">Choose your card type</option>
			<option <?php 	 if('Visa' == $userDetailresult['card_type']){	echo $sl='selected="selected"';	 }	 ?> value="Visa">Visa</option>
		   <option <?php 	 if('MasterCard' == $userDetailresult['card_type']){	echo $sl='selected="selected"';	 }	 ?> value="MasterCard">Master Card</option>
		   <option <?php 	 if('Discover' == $userDetailresult['card_type']){	echo $sl='selected="selected"';	 }	 ?> value="Discover">Discover</option>
		   <option <?php 	 if('American Express' == $userDetailresult['card_type']){	echo $sl='selected="selected"';	 }	 ?> value="American Express">American Express</option>
		</select>
		
		
			<input type="text" name="card_number" id="card_number" value="<?php echo $userDetailresult['card_number']?>" class="validate[required,]"  data-prompt-position="bottomLeft" placeholder="Card Number" />
			
			<select name="expiry_month" id="expiry_month"  >
					<option value=""> Expiry Month</option>
					<?php
					for ($i = 1; $i <= 12; $i++){
						$sl ='';
						 if($i == $userDetailresult['expiry_month']){
							$sl='selected="selected"';
						 }
						echo "<option $sl value=" . $i . ">" . $i . "</option>";
					}
					?>
			</select>
			<select name="expiry_year" id="expiry_year" >
				<option value="">Expiry Year</option>
				<?php $start = date('Y');
				$end = $start + 50;
				for ($i = $start; $i <= $end; $i++){
					$sl ='';
					 if($i == $userDetailresult['expiry_year']){
						$sl='selected="selected"';
					 }
					echo "<option $sl value=" . $i . ">" . $i . "</option>";}
				?>
			</select>
			<input type="password" name="cvv_code" id="cvv_code" value="<?php echo $userDetailresult['cvv_code']?>" class="validate[required,]"  data-prompt-position="bottomLeft" placeholder="CVV Code" />
			
		</div>
		<div class="inputmain tr">
			<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</butto>
			<button class="btn btn-warning">Submit</button>
		</div>
    </div>
  </form>
</div>




<script>
jQuery(document).ready(function(){
	jQuery("#paypal_email_frm").validationEngine();
	jQuery("#patment_info_frm").validationEngine();
});
</script>
