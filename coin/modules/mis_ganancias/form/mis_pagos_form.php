﻿<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/mis_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part tutorials">
<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
  <hr class="mar0" />
  <div class="round1">
		<div class="row-fluid graph-page">
			<div class="span12">
				<h4 class="text-center">ESTADÍSTICAS DE MI FONDEO</h4>
				<form name="search_frm" action="" method="get" id="search_frm" >
				<input type="hidden" name="event" value="mis_pagos" />
			<div class="text-center"> 
                <label>Mes </label> 
			    <select name="s" id="s" onchange="jQuery('#search_frm').submit()">
				<?php foreach($month_arr_spanish as $val=>$value){?>
				<option <?php if($searchMonth==$val) {?> selected="selected" <?php } ?> value="<?php echo $val;?>" > <?php echo $value;?></option>
				<?php } ?>
                </select>
              </div>
			  </form>
            </div>
		</div>
  </div>
  <hr class="mar0" />
  <div class="round1" id="ankesh">
	  <div class="row-fluid graph-page">
         <!--<div class="span3">
         <div class="clicks">
            <h3><?php echo number_format($totalProspectos,0);?></h3>
            <p>Prospectos</p>
         </div>
         </div> -->
         <div class="span3">
         <div class="clicks">
            <h3><?php echo number_format($totalgoldCoin_count,0);?></h3>
            <p>Fondeadores</p>
         </div>
         </div>
		 <div class="span3">
         <div class="clicks">
            <h3>$<?php echo number_format($totalgoldCoin,0);?></h3>
            <p>Mis Fondos</p>
         </div>
         </div>
		 
		 
         <div class="span3">
         <div class="clicks">
            <h3>$<?php echo $totalDscoinIncome;?></h3>
            <p>Coins de Oro</p>
         </div>
         </div>
		  <div class="span3">
		 <div class="clicks">
            <h3>$<?php echo $totalSilvercoinIncome;?></h3>
            <p>Karma Coins</p>
         </div> </div>
       
         <div class="clearfix"></div>
	</div>
    
    <div class="row-fluid graph-page MarT20">
         <div class="span9">
         	<div class="graph-box">
            	<h4>REGISTRO DE ACTIVIDAD</h4>
                <div class="graphs">
				<?php if($totalgoldCoin>0) {?>
                  <div id="curve_chart" style="width: 100%; height: 300px;"></div>
                <?php } else{
					echo ' <div style="width: 100%; height: 300px; text-align: center; color: red; font-size: 13px">Hey! No tenemos resultados para ti en este mes, al parecer aún no eras miembro, por favor selecciona otro mes.</div>';
				}?>  
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="<?php echo JS_URL; ?>/graph/jspdf.min.js"></script>
<script type="text/javascript" src="<?php echo JS_URL; ?>/graph/canvg.js"></script>
<script type="text/javascript" src="<?php echo JS_URL; ?>/graph/rgbcolor.js"></script>

<script>
var doc = new jsPDF();
function getImgData(chartContainer) {
        var chartArea = chartContainer.getElementsByTagName('svg')[0].parentNode;
        var svg = chartArea.innerHTML;
        var chartDoc = chartContainer.ownerDocument;
        var canvas = chartDoc.createElement('canvas');
        canvas.setAttribute('width', (chartArea.offsetWidth * 5));
        canvas.setAttribute('height', (chartArea.offsetHeight * 5));
        
        
        canvas.setAttribute(
            'style',
            'position: absolute; ' +
            'top: ' + (-chartArea.offsetHeight * 2) + 'px;' +
            'left: ' + (-chartArea.offsetWidth * 2) + 'px;');
        chartDoc.body.appendChild(canvas);
        canvg(canvas, svg);
        var imgData = canvas.toDataURL('image/JPEG');
        doc.addImage(imgData, "JPEG", 10,10, 200,110); 
        canvas.parentNode.removeChild(canvas);
        return imgData;
      }

$( document ).ready(function() {

    $('#pdfBtn').click(function () {   
        var chartContainer = document.getElementById('ankesh');
        var chartDoc = chartContainer.ownerDocument;
        var img = chartDoc.createElement('img');
        img.src = getImgData(chartContainer);
        doc.save('mis_estadisticas.pdf');
    });
        
});

</script>
			  
<script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Months', 'Prospectos', 'Fondeo'],
		  <?php 
	
	for($i=1; $i<=count($chartDataarr['prospectos_arr']); $i++)
	{ ?>
		['<?php echo $i?>',<?php echo $chartDataarr['prospectos_arr'][$i]?>, <?php echo $chartDataarr['sale'][$i]?>],
		
	<?php } ?>		      
        ]);

        var options = {
          title: '',
		  width:725,
		 isStacked: true,
          curveType: 'function',
          legend: { position: 'top',x:10, y:20 },
		   colors: ['#3366cc', '#82cac7'],
		  min:0,
		  /* vAxis:{ textPosition: 'none'  } */
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
	
	<style>
		.graph-box .graphs {
    overflow: hidden;
	position:relative;
}
.graphs > div#curve_chart {
    margin-left: -85px;
}
	</style>
                </div>
            </div>
         </div>
         
         <div class="span3 graph-right">
		 
         <!-- <div class="clicks">
            <h4>$<?php echo $totalSilvercoinIncome;?></h4>
            <p>Total of PL Coins</p>
         </div>-->
		 
		 
		 
         
         <div class="clicks" style="text-align: left; padding: 10px 21px;">
            <h4><img style="display: inline-block; vertical-align: text-bottom; margin: 0px 3px 0px 0px;" src="<?php echo IMAGE_URL; ?>/ventas_icon.png"  />Fondeos</h4>
         </div>
		 
		 <div class="clicks" style="text-align: left; padding: 10px 25px;">
            <h4><img style="display: inline-block; vertical-align: text-bottom; margin: 0px 3px 0px 0px;" src="<?php echo IMAGE_URL; ?>/prospectos_icon.png"  />Invitados</h4>
         </div> 
         
         <div class="clicks">
            <h3><?php echo number_format($totalSeles,0); ?>%</h3>
            <p>Conversión</p>
         </div>
         </div>
         <div class="clearfix"></div>
	</div>
    <div class="row-fluid graph-page">
    	<div class="span12">
			<div class="text-right"> 
			<!-- <input type="button" name="pdfBtn" id="pdfBtn"  class="btn " value="Export" /> -->
               <!-- <select name="" id="">
                    <option> Export</option>
                   
                </select> -->
              </div>
            </div>
    </div>
 </div> 
</div>
