<?php
include ("../../conf/config.inc.php");
include ("code/mis_ganancias_code.php");
include (INCLUDE_PATH . "/header.php");
//to get count of notifications for user
$user_id = $_SESSION['AD_user_id'];
$var_page = 'Mis Ganancias';
include_once (INCLUDE_PATH . "/info_header.php");
include_once (INCLUDE_PATH . "/navigation.php");
include_once (INCLUDE_PATH . "/user_level_define.php");
?>

<div class="inner">
	<?php
	include_once (MODULE_PATH."/mis_ganancias/form/mis_ganancias_form.php");
	include_once (SITE_ROOT."/js_php/home.js.php");
	?>
	<div class="cls"></div>
</div>
<?php include_once (INCLUDE_PATH . "/footer.php"); ?>