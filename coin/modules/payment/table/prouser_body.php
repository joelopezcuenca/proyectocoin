<?php
#########################################################################
/* Middle Body Part...
 * Of Commision Pro User
 * Mis-Gancias Page
 */
##############################################################################
?>

<div class="boxes round">
            	<h2>Ganancias de <?php echo $month; ?></h2>
                <input class="klc-input" type="text" name="" value="<?php echo '$' . $month_commission_total; ?>" readonly="readonly"><span class="usd">USD</span>
                <div style="border-bottom: 2px solid #7ca627;margin-bottom: 14px;">&nbsp;</div>
                <h2>Total de Ganancias </h2>
                <input class="klc-input" type="text" name="" value="<?php echo '$' . $total_money; ?>" readonly="readonly"><span class="usd">USD</span>
             	<div class="table_head">Historial de Transacciones</div>
             	<table width="100%" class="history">
                <tbody>
                <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '01') {echo $class;
						}
				?>">Enero <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[0]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '01') { echo '$' . $month_commission_total;

						} else {
							echo '$' . $montharray[1];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '02') {echo $class;
						}
					?>">Febrero <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[1]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history">
                        	<input type="text" name="" value="<?php
							if (date('m') == '02') { echo '$' . $month_commission_total;
							} else {
								echo '$' . $montharray[2];
							}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '03') {echo $class;
						}
					?>">Marzo <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[2]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history "><input type="text" name="" value="<?php
						if (date('m') == '03') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[3];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '04') {echo $class;
						}
					?>">Abril <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[3]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '04') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[4];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '05') {
							echo $class;
						}
					?>">Mayo <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[4]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '05') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[5];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '06') {echo $class;
						}
					?>">Junio <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[5]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '06') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[6];
						}
 ?>"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '07') {echo $class;
						}
					?>">Julio <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[6]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '07') { echo '$' . $month_commission_total;
						} else {

							echo '$' . $montharray[7];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history  <?php
						if (date('m') == '08') {echo $class;
						}
					?>">Agosto <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history">
                        	<input type="text" name="" value="<?php
							if ($report_commision[7]['paid_by_admin'] == 'yes') { echo "pagado";
							} else { echo "N/A";
							}
 ?>" readonly="readonly"></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '08') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[8];
						}
 ?>" readonly="readonly">
                        </td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history  <?php
						if (date('m') == '09') {echo $class;
						}
					?>">Septiembre <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[8]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"> </td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '09') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[9];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history  <?php
						if (date('m') == '10') {echo $class;
						}
					?>">Octubre <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[9]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history">
                        	<input type="text" name="" value="<?php
							if (date('m') == '10') { echo '$' . $month_commission_total;
							} else {
								echo '$' . $montharray[10];
							}
 ?>" readonly="readonly"></td> 
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history <?php
						if (date('m') == '11') {echo $class;
						}
					?>">Noviembre <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[10]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history "><input type="text" name="" value="<?php
						if (date('m') == '11') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[11];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                    <tr class="history">
                    	<td width="33.33%" class="history<?php
						if (date('m') == '12') {echo $class;
						}
					?>">Diciembre <?php echo date('Y'); ?></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if ($report_commision[11]['paid_by_admin'] == 'yes') { echo "pagado";
						} else { echo "N/A";
						}
						?>" readonly="readonly"></td>
                        <td width="33.33%" class="history"><input type="text" name="" value="<?php
						if (date('m') == '12') { echo '$' . $month_commission_total;
						} else {
							echo '$' . $montharray[12];
						}
 ?>" readonly="readonly"></td>
                    </tr>
                </tbody></table>
             <div class="cls"></div>
            </div>
<?php
##################################################################################################################
			?>   