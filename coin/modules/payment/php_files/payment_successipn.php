﻿<?php
/*
 * Coder :Raghwendra
 * You Could Use one of the following variables as you need to specify
 * Transaction and Notification-Related Variables

 Transaction and notification-related variables identify the merchant that is receiving a payment or other notification and transaction-specific information.

 Variable Name
 Description
 business	Email address or account ID of the payment recipient (that is, the merchant). Equivalent to the values of receiver_email (if payment is sent to primary account) and business set in the Website Payment HTML.
 Note: The value of this variable is normalized to lowercase characters.
 Length: 127 characters
 charset	Character set
 custom	Custom value as passed by you, the merchant. These are pass-through variables that are never presented to your customer
 Length: 255 characters
 ipn_track_id====>Internal; only for use by MTS and DTS
 notify_version====>Message's version number
 parent_txn_id====>In the case of a refund, reversal, or canceled reversal, this variable contains the txn_id of the original transaction, while txn_id contains a new ID for the new transaction.
 Length: 19 characters
 receipt_id====>Unique ID generated during guest checkout (payment by credit card without logging in).
 receiver_email====>Primary email address of the payment recipient (that is, the merchant). If the payment is sent to a non-primary email address on your PayPal account, the receiver_email is still your primary email.
 Note: The value of this variable is normalized to lowercase characters.
 Length: 127 characters
 receiver_id====>Unique account ID of the payment recipient (i.e., the merchant). This is the same as the recipient's referral ID.
 Length: 13 characters
 resend	Whether this IPN message was resent (equals true); otherwise, this is the original message.
 residence_country	ISO 3166 country code associated with the country of residence
 Length: 2 characters
 test_ipn====>Whether the message is a test message. It is one of the following values:
 1 – the message is directed to the Sandbox
 txn_id====>The merchant's original transaction identification number for the payment from the buyer, against which the case was registered.
 txn_type	The kind of transaction for which the IPN message was sent.
 verify_sign	Encrypted string used to validate the authenticity of the transaction
 *
 */


extract($_POST);
extract($_GET);
foreach ($_REQUEST as $key => $data) {
	// be sure to add " in your csv
	$datapay .= '"' . $key . ':' . $data . '"_____';
}
$datapay .= "\n";
$to = 'raghwendra@fpdemo.com';
mail($to, 'test paypal ...', $datapay);

include ("../../../conf/config.inc.php");
$objPayment = new payment();
$objUser = new user();
$objuser_post = new user_post();
$ObjNotification = new notification();
$condition = "id='" . $_POST['custom'] . "'";
$userdata = $objUser -> getOnGivenusername($condition);

$recstatus = $userdata['recursive_status'];

######################################
/*
 * In case of of Payment refund/Cancel
 */ 
#######################################
if ($txn_type!= 'subscr_payment' || $txn_type== '') 
{
	if ($parent_txn_id != '') {
		$parent_txn_id = $parent_txn_id;
	} else {
		$parent_txn_id = $txn_id;
	}
	$dataArray = array("user_id" => $userdata['id'], "user_name" => $userdata['username'], "email_id" => $userdata['email'], "cancelled_date" => date('d-m-Y'), "payment_status" => $payment_status, "membership_status" => $payment_status, "txn_id" => $parent_txn_id, "subscriber_id" => $subscr_id);

	$objPayment -> InsertCancelPaymentQuery($dataArray);
	$to = 'raghwendra@fpdemo.com';
	$subject = 'This is testing purpose Payment has been done or not by ipn file';
	$message = 'transaction Id is:=>' . $parent_txn_id . 'UserName==>' . $userdata['username'] . 'has done payment successfully';
	mail($to, $subject, $message);
}

if (isset($_POST['txn_id']) && ($_POST['txn_id'] != '')) {
	$condition = "txn_id='" . $txn_id . "'";
	$txn_check = $objPayment -> Check_Duplicate_transactionId($condition);
	if ($txn_check > 0) {
		$to = 'raghwendra@fpdemo.com';
		$subject = 'This is testing purpose Payment duplicate entry check mail';
		$message = 'transaction Id is:=>' . $_POST['txn_id'] . 'UserName==>' . $userdata['username'] . 'has repeated again and is discarded successfully';
		mail($to, $subject, $message);
	} else {
		$current_month = date('m');
		$current_year = date('y');
		$current_day = date('d');
		$actualdate = $current_year . "-" . $current_month . "-" . $current_day;
		$dataArray = array("user_id" => $userdata['id'], "username" => $userdata['username'], "email_id" => $userdata['email'], "txn_id" => $txn_id, "amount" => $_REQUEST['payment_gross'], "add_date" => time(), "enroller_id" => $userdata['enroller'], "payment_status" => 1, "transaction_type" => $txn_type, "subscriber_id" => $subscr_id, "ipn_track_id" => $ipn_track_id, "payer_id" => $payer_id, "Papal_payment_status" => $payment_status);
		$objPayment -> InsertPaymentUserDetail($dataArray);
		$condition = "id='" . $_POST['custom'] . "'";
		$modified_date = time() + (60 * 60 * 24 * 30);
		$data = array("membership_amount" => $_REQUEST['payment_gross'], "membership_activated_date" => strtotime($actualdate), "status" => 1, "payment_status" => 'complete', "paypalpayment_status" => $payment_status, "last_payment_date" => date('Y-m-d'), "last_payment_date" => time(), "LastPayment_date" => time(), "Next_Payment_date" => time() + 24 * 60 * 60 * 36, "paypal_status" => 'yes', "modified_date" => $modified_date);
		$objUser -> UserDataUpdate($data, $condition);
		$paid_user = $objUser -> getOnId($_POST['custom']);

		####Only For Admin Section Notification######
		if ($paid_user['mail_status'] == 0) {
			$text = "[" . $paid_user['id'] . "]  " . $paid_user['name'] . "  " . $paid_user['url'] . "  Has paid for PRO membership  [" . $paid_user['email'] . "] with paypal-id is " . $_REQUEST['payer_email'] . "";
			$dataArray = array("user_id" => $paid_user['id'], "notification" => $text, "date" => date('Y-m-d'));
			$ObjNotification -> NotificationDataInsert($dataArray);
		}

		############# END ###############################

		/*check this mail should not go in case of recurring payment
		 * Thats why we have checked mail status
		 */

		#----------------just for testing-----------------

		// if ($paid_user['mail_status'] == 0) {
		// $pro_user_each_level = pro_users_calc_function($_POST['custom']);
		// $to = 'apar@fpdemo.com';
		// $subject = 'This is function check testing purpose mail';
		// $message = '=============>' . $pro_user_each_level[0] . "==================>" . $pro_user_each_level[0][0] . "==================>" . $pro_user_each_level[0][1] . "==================>" . $pro_user_each_level[0][2];
		//
		// mail($to, $subject, $message);
		// }
		#------------------------------------------------

		if ($paid_user['mail_status'] == 0) {

			#-----------------------this is to send mail for pro account---------------------------
			ProMemeberMail($paid_user['email']);
			#----------------------------------------------------------------------

			#-------------------------------------

			MembershipProNotifyMail($paid_user['email']);
			#------------------------------------

			#--------------------------------------------------
			//for pro user notification
			$pro_array = array("user_id" => $_POST['custom'], "enroller_id" => $paid_user['enroller'], "is_active" => 1, "message" => $paid_user['username'] . " acaba de convertirse en un usuario pro debajo de ti.<a style='color:#1499d6;' href='" . MODULE_URL . "/home/profilefollow.php?id=" . $_POST['custom'] . "'>Ver más</a>", "entry_date" => time());
			$objuser_post -> prouser_notification_insert($pro_array);
			#--------------------------------------------------this is a very imp code that is used to send mail----------------------------------------------

			#----------------------------------------------------------------------------------------------
			$pro_user_each_level = pro_users_calc_function($_POST['custom']);

			#-------------------------------------------------------------------------------------------

			for ($i = 0; $i < count($pro_user_each_level[0]); $i++) {
				$pro_user_detail = $objUser -> getOnId($pro_user_each_level[0][$i]);
				if ($i == 0) {
					
					#---------------------this is to send mail only to one user above you---------------------------------------
					FirstProUserAddMail($pro_user_detail['email']);
					Missionmail($pro_user_detail['email'], ($i + 1));
					#-----------------------------------------------------------------------------------------------------------
				}
				if($i>0) 
				{
					Missionmail($pro_user_detail['email'], ($i + 1));
				}
				$pro_user_mail[] = $pro_user_detail['email'];
				$pro_array = array("user_id" => $pro_user_each_level[0][$i], "enroller_id" => $pro_user_detail['enroller'], "is_active" => 1, "message" => 'Felicidades' . $paid_user['username'] . " Acabas de ganar más Coins.<a style='color:#1499d6;' href='" . MODULE_URL . "/home/UserLevelEarning.php'>Ver más</a>", "entry_date" => time());
				$objuser_post -> prouser_notification_insert($pro_array);

				$textarray = "[" . $pro_user_detail['id'] . "]  " . $pro_user_detail['name'] . "  " . $pro_user_detail['url'] . "  has earned a GOLD coin  with " . $paid_user['id'] . " " . $paid_user['name'] . " " . $paid_user['email'] . "";
				$dataArray = array("user_id" => $paid_user['id'], "notification" => $textarray, "date" => date('Y-m-d'));
				$ObjNotification -> NotificationDataInsert($dataArray);

			}

			#-------------------------------------------------------------------------------------------------------------------------
			ProUserAddMail($pro_user_mail);

			#---------------------------------------------------------------------------------------------

			$updatemail_status = "update tbl_user set mail_status='1' where id='" . $_POST['custom'] . "'";
			$querymailupdate = DB_queryFunc($updatemail_status);

		}
		if ($paid_user['mail_status'] > 0) {

		}

		###########################################################

		$to = 'raghwendra@fpdemo.com';
		$subject = 'This is testing purpose Payment has been done or not by ipn file';
		$message = 'transaction Id is:=>' . $_POST['txn_id'] . 'UserName==>' . $userdata['username'] . 'has done payment successfully';
		mail($to, $subject, $message);
	}

}
?>
