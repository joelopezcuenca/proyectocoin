﻿<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part  tutorials" id="print_area">
	<?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
		<span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
	<?php unset($_SESSION['image_ext_error']);
			}
	?>

<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>

  <div class="round1 Font14 calculator" >
		<div class="row-fluid MarT20">
				<div class="span8 offset2">
				<form name="calculator_frm" id="calculator_frm" action="" method="POST" />
					<ul class="calculatorul">
					<input type="hidden" id="total_item" value="<?php echo count($expenses_array);?>" />
					<?php 
					$count = 1;
															
					foreach($expenses_array as $value) {
					if(number_format($userInput_arr[urldecode($value)],0)=='0')
						$cal_value = '';
					else
						$cal_value = number_format($userInput_arr[urldecode($value)],0);
					
					?>
					<li id="calculator_li_<?php echo $count?>">
						  <div class="row-fluid">
								<div class="span9 marginTop8px"><?php echo $value;?></div>
								<div class="span3 "><a href="javascript:void(0)" onclick="delete_option(<?php echo $count?>)"><img src="<?php echo IMAGE_URL; ?>/bin.png"></a> <input class="calculate_input" type="text" id="expense_<?php echo $count;?>" name="<?php echo urlencode($value); ?>" value="<?php echo $cal_value ?>" placeholder="0" onchange="get_total_expences();" /></div>
						 </div>
					</li>
					<?php $count++; } ?>
					
					
					</ul>
					<input type="hidden" id="counter" value="<?php echo $count; ?>" />
                    <h2 class="font_2"><span class="color_33"><a href="javascript:void(0)" onclick="add_new_option()">ADD ANOTHER ONE &nbsp;<img width="15" src="<?php echo IMAGE_URL; ?>/Add-icon.png"></a></span></h2>
                    
                    <hr />
					
					
					<?php if(number_format($calculationResult['increment_presentage'],0)=='0'){
						$predefineDate = '';
					}else{
						$predefineDate = number_format($calculationResult['increment_presentage'],0);
					}?>
					
                    <div class="row-fluid MarT20N1 text-center">
                    	<div class="span12 marT5New"><span class="font_12">Añade un porcentaje extra por cualquier imprevisto</span> &nbsp;<input type="text" name="increment_presentage" id="increment_presentage" value="<?php echo $predefineDate;?>" placeholder="0" class="validate[required]" onchange="get_total_expences();" data-prompt-position="bottomLeft" > <span class="color_33">%</span></div>
                    </div>
                    
					<div class="row-fluid MarT20N1 text-center">
					<div class="span12 marT5New"><h4><strong>Total <!-- por Mes--></strong></h4>
					<input type="text" name="total_only_for_show" id="expense_total_new" value="<?php echo '$'.number_format($calculationResult['total'],0)?>" readonly="readonly" class="validate[required]" data-prompt-position="bottomLeft" >
					
					<input type="hidden" name="total" id="expense_total" value="<?php echo number_format($calculationResult['total'],0)?>" readonly="readonly" class="validate[required]" data-prompt-position="bottomLeft" ></div>
					</div>
                    
					
					
					<div class="MarT20N1 text-center hide_metter">
						<!--<button class="btn btn-success" type="button"><em>Next Step</em></button>-->
						<a class="btn btn-success" href="javascript:void(0)" onclick="jQuery('#calculator_frm').submit()"><em>Siguiente</em></a>
					</div>
						<input type="hidden" name="action" value="add_calculation" />
						<input type="hidden" name="user_id" value="<?php echo $user_id?>" />
						<input type="hidden" name="submit_from" id="submit_from" value="" />
                        
                        <div class="MarT20 tc"><a href="javascript:void(0)" onclick="jQuery('#submit_from').val('save');jQuery('#calculator_frm').submit();" ><img src="<?php echo IMAGE_URL; ?>/save.png"></a></div>
					</form>
				</div>

		</div>
  </div>	
</div>
<script>
function get_total_expences()
{
	var expenseTotal = 0;
		jQuery( ".calculate_input" ).each(function() {
		var perticularExp = jQuery(this).val();	
			if(perticularExp=='')
				perticularExp = 0;
		
		 if (!isNaN(perticularExp))
			{
				expenseTotal +=parseInt(perticularExp);
			}
	});
	
	jQuery('#expense_total').val(expenseTotal);
	
	jQuery('#expense_total_new').val(expenseTotal); 
	
	
	var percentage = jQuery('#increment_presentage').val();
	//alert(percentage);
	var new_width = (percentage / 100) * expenseTotal;
	
	var dreamline_total = expenseTotal+ new_width; 
	jQuery('#expense_total').val(dreamline_total);
	var dreamline_total_new = '$'+addCommas(dreamline_total);
	jQuery('#expense_total_new').val(dreamline_total_new);
	
}

function reset_total_expences()
{
	var expenseTotal = 0;
		jQuery( ".calculate_input" ).each(function() {
		jQuery(this).val('0');	
	});
	
	
	jQuery('#expense_total').val(expenseTotal); 
	var expenseTotal_new = '$'+addCommas(expenseTotal);
	jQuery('#expense_total_new').val(addCommas(expenseTotal_new)); 
}

$( document ).ready(function() {
    $("#calculator_frm").validationEngine();
});

function add_new_option()
{
	var counter = jQuery('#counter').val();
	
	counter = parseInt(counter)+1;
	jQuery('#counter').val(counter);

	jQuery('.calculatorul').append('<li id="calculator_li_'+counter+'"> <div class="row-fluid"> <div class="span9 marginTop8px"><input class="calculate_input" id="input_calculator_li_'+counter+'" type="text" onkeyup="set_input_name('+counter+');" id="input_'+counter+'" name="" value="" placeholder="" style="width: 361px; border: 0px;" /></div> <div class="span3 "><a href="javascript:void(0)" onclick="delete_option('+counter+')"><img src="<?php echo IMAGE_URL; ?>/bin.png"></a><input class="calculate_input" type="text" id="expense_'+counter+'" name="expense_'+counter+'" value="" placeholder="0" onchange="get_total_expences();" /></div> </div> </li>');
	
	jQuery('#input_calculator_li_'+counter).focus();
}

function delete_option(val)
{
	jQuery('#calculator_li_'+val).remove();
	get_total_expences();
}

function set_input_name(val)
{
	var setName =  $('#input_calculator_li_'+val).val();
	setName = encodeURI(setName);
	//alert(setName);
	$('#expense_'+val).attr('name',setName);
	//alert($(this).attr('name'));
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
</script>
