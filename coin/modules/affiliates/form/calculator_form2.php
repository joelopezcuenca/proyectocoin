﻿<div class="left_part">
  <?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part  tutorials">
  <?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
  <span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
  <?php unset($_SESSION['image_ext_error']);
			}
	?>
  <?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
  <div class="round1 Font14 calculator">
    <div class="row-fluid MarT20">
      <form name="calculator_frm2" id="calculator_frm2" action="" method="POST" />
      <div id="Calcular_stap2" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
    </div>
    <div class="modal-body cal_height">
      <div class="row-fluid">
        <div class="span12 heading">
          <h6>INSTRUCCIONES:</h6>
          <p style="font-size:13.5px" >El paso más importante es saber exactamente cuanto cuesta cumplir tu sueño.
        Si tienes dudas de cómo utilizar esta calculadora,Mira el Tutorial</p>
			<div class="video-a"> <a href="javascript:void(0)" data-toggle="modal" >Ver tutorial</a> <!--<img src="<?php echo IMAGE_URL; ?>/device_camera_recorder_video_.png"> --></div> 
        </div>
      </div>
      <ul class="calculatorul MarT20N1">
        <input type="hidden" id="total_item" value="10">
		
		<li style="color:#DEC328; text-align:right; padding-right:76px;"># de pagos</li>
		
		<?php 
		$count = 1;
		$monthCounter = 0;

			if(!empty($userInput_arr_print_new)) {									
		foreach($userInput_arr_print_new as $value) { 
		
		
		?>
		<li id="calculator_li_<?php echo $count?>">
			  <div class="row-fluid">
					<div class="span8 marginTop8px"><?php echo $value;?></div>
					<div class="span4 "><a href="javascript:void(0)" onclick="delete_option(<?php echo $count?>)"><img src="<?php echo IMAGE_URL; ?>/bin.png"></a> <input class="calculate_input" type="text" id="expense_<?php echo $count;?>" name="<?php echo urlencode($value); ?>" value="<?php echo number_format($userInput_arr_new[urldecode($value)],0); ?>" placeholder="" onchange="get_total_expences('<?php echo $count ?>');" />
					<select name="multiply_by[]" id="multiply_by_expense_<?php echo $count ?>" class="multiply_by" onchange="get_total_expences('<?php echo $count ?>');" >
				<?php for($i=1; $i<=24;$i++) { ?>
					<option <?php if($my_criteria_arr[$monthCounter]==$i) { ?> selected="selected" <?php } ?> value="<?php echo $i; ?>" ><?php echo $i;?></option>
			<?php } ?>
			</select>
					</div>
			 </div>
		</li>
		<?php $count++; $monthCounter++; } }else { ?>
		<li id="calculator_li_<?php echo $count ?>">
		<div class="row-fluid"> 
			<div class="span8 marginTop8px"><input  type="text" onkeyup="set_input_name('<?php echo $count ?>');" id="input_<?php echo $count ?>" name="" value="" placeholder="" style="width: 100%; border: 0px;" /></div> <div class="span4 "><a href="javascript:void(0)" onclick="delete_option('<?php echo $count ?>')"><img src="<?php echo IMAGE_URL; ?>/bin.png"></a><input class="calculate_input" type="text" id="expense_<?php echo $count ?>" name="expense_<?php echo $count ?>" value="" placeholder="" onchange="get_total_expences('<?php echo $count ?>');" />
			<select name="multiply_by[]" id="multiply_by_expense_<?php echo $count ?>" class="multiply_by"   onchange="get_total_expences('<?php echo $count ?>');">
				<?php for($i=1; $i<=24;$i++) { ?>
					<option value="<?php echo $i; ?>" ><?php echo $i;?></option>
			<?php } ?>
			</select>
			</div>
		</div>
		</li>
		
		<?php } ?>
		
      </ul>
	  <input type="hidden" id="counter" value="<?php echo $count; ?>" />
      <h2 class="font_2"><span><a class="color_33" href="javascript:void(0)" onclick="add_new_option()">AGREGAR OTRO &nbsp;<img width="15" src="<?php echo IMAGE_URL; ?>/Add-icon.png"></a></span></h2>
      <hr />
	  <?php if(number_format($calculationResult['increment_presentage_2'],0)=='0') { 
		$safty_percentage = '';
	  } else {
		$safty_percentage = number_format($calculationResult['increment_presentage_2'],0);
	  } ?>
	  
      <div class="row-fluid MarT20N1 text-center">
        <div class="span12 marT5New"><span class="font_12">Añade un porcentaje extra por cualquier imprevisto</span> &nbsp;<input type="text" name="increment_presentage_2" id="increment_presentage" value="<?php echo $safty_percentage; ?>" class="validate[required]" onchange="get_total_expences('<?php echo $count ?>');" data-prompt-position="bottomLeft" vk_15f64="subscribed"> <span style="color:#DEC328;">%</span></div>
    </div>
    
		<?php if(number_format($calculationResult['total_monthly_expences'],0)=='0'){ 
			$total_spend_month_popup_total = '';
		} else{
			$total_spend_month_popup_total = number_format($calculationResult['total_monthly_expences'],0);
		}?>
	    <div class="row-fluid text-center">
        <div class="span12 marT5New"><h4><strong>Total</strong></h4><input type="text" name="popup_total_only_show" id="popup_total_new" value="<?php echo '$'.$total_spend_month_popup_total;?>" readonly="readonly" class="validate[required]" data-prompt-position="bottomLeft">
		<input type="hidden" name="popup_total" id="popup_total" value="<?php echo $total_spend_month_popup_total;?>" readonly="readonly" class="validate[required]" data-prompt-position="bottomLeft">
		</div>
        </div>
        
        <div class="MarT20N1 text-center hide_metter">
            <a class="btn btn-success" href="javascript:void(0)" onclick="set_step2_total()"  data-dismiss="modal" aria-hidden="true"><em>Guardar</em></a>
        </div>
                    
    </div>
</div>
      <div class="span12 mission-brief">
        <div class="top-part">
         <!--  <label>Yo <?php //echo $_SESSION['AD_user_name'];?><br /> -->
            En
            <input class="x-small validate[required]" data-prompt-position="bottomLeft" name="time_period" id="time_period" type="text" value="<?php echo $calculationResult['time_period']?>" />
           meses quiero lograr mi sueño de
            <select name="user_type" id="user_type">
            	<option value="vivir" <?php if($calculationResult['user_type']=='vivir'){?> selected="selected" <?php } ?> >vivir</option>
				<option value="viajar" <?php if($calculationResult['user_type']=='viajar'){?> selected="selected" <?php } ?> >viajar</option>
              <option value="tener" <?php if($calculationResult['user_type']=='tener'){?> selected="selected" <?php } ?> >tener</option>
              <option value="hacer" <?php if($calculationResult['user_type']=='hacer'){?> selected="selected" <?php } ?> >hacer</option>
			  <option value="ser" <?php if($calculationResult['user_type']=='ser'){?> selected="selected" <?php } ?> >ser</option>
            </select>
            <br />
            <input class="bigwala" name="description" id="description" value="<?php echo $calculationResult['description']?>" type="text" maxlength="140" />
            <span class="color_33 color_33_new  font_10 limit_text"><em>Describe en menos de <span id="chars">140</span> caracteres tu meta:</em></span> </label>
        </div>
		
		<?php if(number_format($calculationResult['total_calculation2'],0)=='0'){ 
			$total_spend_month = '';
		} else{
			$total_spend_month = number_format($calculationResult['total_calculation2'],0);
		}
		
	//	echo $total_spend = $total_spend_month;
		//echo '<br/> Time Pre: '.$calculationResult['time_period'];
		?>
		
		<?php
			$input_text = "+ Calcular";
		?>
		
        <div class="result-part">
          <label>Para lograr este sueño debo juntar cada mes alrededor de…<br />
            <br />
            $
                       <a href="#Calcular_stap2" data-toggle="modal"> <input style="color:#30BDFF!important; text-align:center;" class="chotawala" name="total_calculation2_only_show" id="total_new" type="text" value="<?php echo $input_text?>" readonly="readonly" /></a>
			<input class="chotawala" name="total_calculation2" id="total" type="hidden" value="<?php echo $total_spend_month;?>" />
            <!-- USD   -
            <select name="payment_type" id="payment_type" >
              <option>Select Payment method</option>
              <option <?php if($calculationResult['payment_type']=='Un pago'){?> selected="selected" <?php } ?> >Un pago</option>
              <option <?php if($calculationResult['payment_type']=='Pago mensual'){?> selected="selected" <?php } ?> >Pago mensual</option>
            </select> -->
            <br />
            <!-- <a href="#Calcular_stap2" data-toggle="modal"><span class="color_18 font_10">+ Calcular</span></a> </label> -->
        </div>
        <a class="btn btn-success" href="javascript:void(0)" onclick="jQuery('#calculator_frm2').submit()" ><em>Siguiente</em></a> </div>
      <input type="hidden" name="action" value="add_calculation2" />
	  <input type="hidden" name="submit_from" id="submit_from" value="" />
      <input type="hidden" name="user_id" value="<?php echo $user_id?>" />
      <div class="MarT20 tc fl width_full"><a href="javascript:void(0)" onclick="jQuery('#submit_from').val('save');jQuery('#calculator_frm2').submit();" ><img src="<?php echo IMAGE_URL; ?>/save.png"></a></div>
    
    </div>
  </div>
</div>


  </form>
  <?php 
  
  //-----------get ver tutorial  ------------
$condition = " page_name='calculator2_popup' ";
$var_tutorial_result = $additional_Obj->get_var_tutorial_video($condition);
  ?>
 
<div id="video_tutorial_pop" class="modal hide fade inspried" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <form id="add_goal_form_common" action="<?php echo MODULE_URL ; ?>/home/code/goal_code.php" method="post" name="goal form" enctype="multipart/form-data"   >
    <div class="modal-header">
     <a href="javascript:void(0)" class="close video_close" data-dismiss="modal" aria-hidden="true" > <img class="" src="<?php echo IMAGE_URL?>/close_video.png" /> </a>
    </div>
    <div class="modal-body">
    <?php echo $var_tutorial_result['video'];?>
    </div>
  </form>
</div> 
  
<script>
$( document ).ready(function() {
    $("#calculator_frm2").validationEngine();
	
	<?php if($count>3) { ?> jQuery('.calculatorul').height('175px'); <?php } ?>
	
});
</script>
<script>
var maxLength = 140;

var ini_length = $('#description').val().length;
 var ini_length = maxLength-ini_length;
  $('#chars').text(ini_length);
  
$('#description').keyup(function() {
  var length = $(this).val().length;
  var length = maxLength-length;
  $('#chars').text(length);
  
  if(length=='0')
  {
	//  alert('d');
	 jQuery('.limit_text').removeClass('color_33_new');
	  jQuery('.limit_text').css('color', 'red');
  }
  
});
</script>
<script>
function get_total_expences(val)
{
	var expenseTotal = 0;
	var perticularExp2 = 1;
		jQuery( ".calculate_input" ).each(function() {
		var perticularExp = jQuery(this).val();
		perticularExp = perticularExp.replace(',', '');
		
		var inputId = jQuery(this).attr('id');
		
		//alert(inputId);
		 perticularExp2 = jQuery('#multiply_by_'+inputId).val();
		
			if(perticularExp=='')
				perticularExp = '1';
			
			if(perticularExp2=='undefined')
					perticularExp2 = '1';
		//alert(' OUT: '+perticularExp2);
		 if (!isNaN(perticularExp) && perticularExp2!='undefined')
			{
				//alert('perticularExp: '+perticularExp);
				//alert('perticularExp2: '+perticularExp2);
				
				expenseTotal +=(parseInt(perticularExp)*parseInt(perticularExp2));
				//alert('In: '+expenseTotal);
			}
	});
	
	jQuery('#popup_total').val(expenseTotal); 
	
	var expenseTotal_new = '$'+addCommas(expenseTotal);
	jQuery('#popup_total_new').val(expenseTotal_new);
	
	
	var percentage = jQuery('#increment_presentage').val();
	//alert(percentage);
	var new_width = (percentage / 100) * expenseTotal;
	
	var dreamline_total = expenseTotal+ new_width;
	dreamline_total = dreamline_total.toFixed(0);
	jQuery('#popup_total').val(dreamline_total);
	var dreamline_total_new = '$'+addCommas(dreamline_total);
	jQuery('#popup_total_new').val(dreamline_total_new);
	
}

function reset_total_expences()
{
	var expenseTotal = 0;
		jQuery( ".calculate_input" ).each(function() {
		jQuery(this).val('0');	
	});
	
	expenseTotal = dreamline_total.toFixed( 0 );
	jQuery('#popup_total').val(expenseTotal);
	var expenseTotal_new = '$'+addCommas(expenseTotal);
	jQuery('#popup_total_new').val(expenseTotal_new); 
}

$( document ).ready(function() {
    $("#calculator_frm").validationEngine();
});

function add_new_option()
{	
	var counter = jQuery('#counter').val();
	
	if(counter>3)
	{
		jQuery('.calculatorul').height('175px');
	}
	
	counter = parseInt(counter)+1;
	jQuery('#counter').val(counter);

	jQuery('.calculatorul').append('<li id="calculator_li_'+counter+'"> <div class="row-fluid"> <div class="span8 marginTop8px"><input  type="text" onkeyup="set_input_name('+counter+');" id="input_'+counter+'" name="" value="" placeholder="" style="width: 100%; border: 0px;" /></div> <div class="span4 "><a href="javascript:void(0)" onclick="delete_option('+counter+')"><img src="<?php echo IMAGE_URL; ?>/bin.png"></a><input class="calculate_input" type="text" id="expense_'+counter+'" name="expense_'+counter+'" value="" placeholder="" onchange="get_total_expences('+counter+');" /><select name="multiply_by[]" class="multiply_by" id="multiply_by_expense_'+counter+'" onchange="get_total_expences('+counter+');"><?php for($i=1; $i<=24;$i++) { ?> <option value="<?php echo $i;?>"><?php echo $i;?></option><?php } ?></select></div> </div> </li>');
	
	jQuery('#input_'+counter).focus();
}

function delete_option(val)
{
	jQuery('#calculator_li_'+val).remove();
	get_total_expences(val);
}

function set_input_name(val)
{
	var setName =  $('#input_'+val).val();
	setName = encodeURI(setName);
	$('#expense_'+val).attr('name',setName);
	//alert($(this).attr('name'));
}

function set_step2_total() {
	var popup_total = 0;
	var time_period = 0;
	var total = 0 ;
	popup_total = jQuery('#popup_total').val();
	time_period = jQuery('#time_period').val();
	popup_total = popup_total.replace(',', '');
	time_period = time_period.replace(',', '');
	
	//alert(popup_total);
	//alert(time_period);
	
	total = popup_total/time_period;
	total = total.toFixed( 0 );
	jQuery('#total').val(total);
	var total_new_data = addCommas(total);
	jQuery('#total_new').val(total_new_data);
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
</script>
