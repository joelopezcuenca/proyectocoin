﻿<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part  tutorials">
	<?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
		<span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
	<?php unset($_SESSION['image_ext_error']);
			}
	?>
<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
  <!--<hr class="mar0" />
  <ul class="klc-nav">
    <li><span><a href="<?php // echo MODULE_URL?>/affiliates/calculator.php?event=calculator" >1. Mission Calculator</a></span></li>
    <li class="select"><span>2. Mission Brief</span></li>
    <li><span><a href="javascript:void(0)">3. How to Do it</span></a></li>
  </ul>-->
  
  <?php $target_monthly_income = $calculationResult['total']+$calculationResult['total_calculation2']; ?>
  
  <div class="round1 Font14 calculator">
		<div class="row-fluid MarT20">
		<form name="calculator_frm2" id="calculator_frm2" action="" method="POST" />
			<div class="span12 mission-brief">
              <div class="top-part step_5">
                <label>Necesito Fondear al mes <br /><input name="target_monthly_income" id="target_monthly_income" class="form-control" readonly="" value="$<?php echo number_format($target_monthly_income,0); ?> USD"></label>
                <div class="row-fluid">
				
				<?php if($calculationResult['product_to_sell']!=''){
					$product_to_sell = $calculationResult['product_to_sell'];
				}else{
					$product_to_sell = 'COIN-ATLAS';
				}

				if($product_to_sell == 'COIN-ATLAS')
				{

					if($calculationResult['product_price']=='0.00')
						$product_price = '50';
					else
						$product_price = $calculationResult['product_price'];
				
					
					if($calculationResult['comission_given']=='0.00')
						$comission_given = '80';
					else
						$comission_given = $calculationResult['comission_given'];
					
					if($calculationResult['conversion']=='0.00')
						$conversion = '10';
					else
						$conversion = $calculationResult['conversion'];
					
					
					
				}else{
					$product_price = $calculationResult['product_price'];
					$comission_given = $calculationResult['comission_given'];
					$conversion = $calculationResult['conversion'];
				}
				
				$profit = ($product_price*$comission_given)/100;
				$sales_needed = $target_monthly_income/$product_price;
				
				
				
				
				?>
				
                  <div class="span4"><label>2. VELA<span class="red">*</span> <br />
				  <select name="product_to_sell" id="product_to_sell" onchange="set_default(this.value)" >
                  	<option value="COIN-ATLAS" <?php if($product_to_sell=='VELA – COIN'){ ?> selected="selected" <?php } ?> >VELA – COIN</option>
                    <option value="OTHER" <?php if($product_to_sell=='VELA – Otro Producto'){ ?> selected="selected" <?php } ?>>VELA – Otro Producto</option>
                    <option value="MY OWN PRODUCT" <?php if($product_to_sell=='VELA – Mi producto o servicio '){ ?> selected="selected" <?php } ?>>VELA – Mi producto o servicio </option>
                  </select>
                  </label></div>
                  <div class="span4"><label>3. Precio<span class="red">*</span> <br />
				  $ <input name="product_price" id="product_price" value="<?php echo number_format($product_price,0);?>" type="text" class="validate[required]" data-prompt-position="bottomLeft" onchange="get_profie_sales(this.value)"  /></label></div>
                  <div class="span4"><label>4. Comisión<span class="red">*</span> <br />
				  <input name="comission_given" id="comission_given" value="<?php echo number_format($comission_given,0);?>" type="text" class="validate[required]" data-prompt-position="bottomLeft" onchange="get_profie_sales(this.value)" /> %</label></div>
                </div>
                <div class="row-fluid">
                  <div class="span4"><label>5. Ganancia<br />
				  $ <input name="profit" id="profit" type="text" value="<?php echo number_format($profit,1);?>" class=" validate[required]" data-prompt-position="bottomLeft" readonly="readonly" /></label></div>
                  <div class="span4"><label>6. Necesarias <br />
				  <input name="sales_needed" id="sales_needed" value="<?php echo number_format($sales_needed,0);?>" type="text" class=" validate[required]" data-prompt-position="bottomLeft" readonly="readonly" /></label></div>
                  <div class="span4"><label>7. Conversion<span class="red">*</span> <br />
				  <input name="conversion" id="conversion" value="<?php echo number_format($conversion,0);?>" type="text" class=" validate[required]" data-prompt-position="bottomLeft"  onchange="get_profie_sales(this.value)" /> %</label></div>
                </div>
              </div>
              <div class="result-part">
                <label><img src="<?php echo IMAGE_URL; ?>/result_tag.png"> <span class="font_weight_300">RESULTADOS</span></label>
				
				<?php 

			//echo '<br/> sales_needed: '.str_replace(',','',number_format($sales_needed,0));
			//echo '<br/> conversion: '.(100/number_format($conversion,0));
				$salesneed_cal = str_replace(',','',number_format($sales_needed,0));
				$conversion_cal = (100/str_replace(',','',number_format($conversion,0)));

				$monthly_click = $salesneed_cal*$conversion_cal;
				$daily_click = $monthly_click/30;
				?>
				
				
                <div class="row-fluid MarT20">
                  <div class="span3">
                    <div class="clicks" id="monthly_click"><?php echo $monthly_click;?></div>
                    <div>Cliks x mes</div>
                  </div>
                    
                  <div class="span3">
				  <div class="clicks" id="daily_click"><?php echo number_format($daily_click,0);?></div>
                    <div>Clicks x dia</div>
                  </div>
                  <div class="span3">
                    <div class="clicks" id="monthly_income">$<?php echo number_format($target_monthly_income);?></div>
                    <div>Ingreso mensual</div>
                  </div>
                  <div class="span3">
                    <div class="clicks" id="daily_income">$<?php echo number_format($target_monthly_income/30,0);?></div>
                    <div>Ingreso Diario</div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
			  <div class="MarT20 tc fl width_full"><a href="javascript:void(0)" onclick="jQuery('#submit_from').val('save'); jQuery('#calculator_frm2').submit();"><img src="<?php echo IMAGE_URL; ?>/save.png"></a>&nbsp;<a class="btn btn-success" href="javascript:void(0)" onclick="jQuery('#calculator_frm2').submit()"><em>Guardar Proyecto</em></a></div>
              
			</div>
			<input type="hidden" name="action" value="add_calculation5" />
			<input type="hidden" name="submit_from" value="" />
			<input type="hidden" name="user_id" value="<?php echo $user_id?>" />
			</form>
		</div>
  </div>	
</div>
<script>
$( document ).ready(function() {
    $("#calculator_frm2").validationEngine();
});


function set_default(val)
{
	var product_price = '';
	var comission_given = '';
	var profit = '';
	var sales_needed = '';
	var conversion = '';
	var monthly_click = '';
	var daily_click = '';
	var monthly_income = '';
	var daily_income = '';
	 var target_monthly_income = jQuery('#target_monthly_income').val();
	 	 
		
	target_monthly_income = target_monthly_income.replace("$", "");
	target_monthly_income = target_monthly_income.replace("USD", "");target_monthly_income = target_monthly_income.replace(",", "");
	
	if(val=='VLA – COIN')
	{
		 product_price = '47';
		 comission_given = '80';
		 conversion = '10';
		 
	
		sales_needed = parseInt(target_monthly_income)/ parseInt(product_price);
		sales_needed = Math.round(sales_needed);
		
		monthly_click = parseInt(sales_needed)*(100/parseInt(conversion));
		daily_click = Math.round(monthly_click/30);
		
		
		jQuery('#sales_needed').val(sales_needed);
		profit = (parseInt(product_price) * parseInt(comission_given))/100;
		profit = Math.round(profit);
		jQuery('#profit').val(profit);
	
		 
	}
		
	monthly_income = target_monthly_income;
	daily_income = Math.round(monthly_income/30);
		//alert(daily_income);
	jQuery('#product_price').val(product_price);
	jQuery('#comission_given').val(comission_given);
	jQuery('#profit').val(profit);
	jQuery('#sales_needed').val(sales_needed);
	jQuery('#conversion').val(conversion);
	jQuery('#monthly_click').html(monthly_click);
	jQuery('#daily_click').html(daily_click);
	jQuery('#monthly_income').html('$'+monthly_income);
	jQuery('#daily_income').html('$'+daily_income);
		
}

function get_profie_sales(val)
{
	var target_monthly_income = jQuery('#target_monthly_income').val();
	target_monthly_income = target_monthly_income.replace("$", "");
	target_monthly_income = target_monthly_income.replace("USD", "");
	target_monthly_income = target_monthly_income.replace(",", "");
	var product_price = jQuery('#product_price').val();
	var comission_given = jQuery('#comission_given').val();
	if(comission_given=='')
		 comission_given = 0;
	
	var sales_needed = 0;
	var profit = '';
	var monthly_click = '';
	var daily_click = '';
	var conversion = jQuery('#conversion').val();
	if(conversion=='')
		 conversion = 1;
	
	
	sales_needed = parseInt(target_monthly_income)/ parseInt(product_price);
	sales_needed = Math.round(sales_needed);
	jQuery('#sales_needed').val(sales_needed);
	profit = (parseInt(product_price) * parseInt(comission_given))/100;
	profit = Math.round(profit);
	jQuery('#profit').val(profit);	
	
	 monthly_click = parseInt(sales_needed)*(100/parseInt(conversion));
	 daily_click = Math.round(monthly_click/30);
	
	jQuery('#monthly_click').html(monthly_click);
	jQuery('#daily_click').html(daily_click);
	
	
}

</script>
