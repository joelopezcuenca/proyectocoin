﻿<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/home_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part  tutorials" id="print_area">
	<?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
		<span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
	<?php unset($_SESSION['image_ext_error']);
			}
	?>
<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>

  <!--<hr class="mar0" />
  <ul class="klc-nav hide_metter">
    <li><span><a href="<?php // echo MODULE_URL?>/affiliates/calculator.php?event=calculator" >1. Mission Calculator</span></a></li>
    <li><span><a href="<?php // echo MODULE_URL?>/affiliates/calculator2.php?event=calculator" >2. Mission Brief</span></a></li>
    <li class="select"><span><a href="javascript:void(0)" >3. How to Do it</span></a></li>
  </ul>-->
  <div class="round1 Font14 calculator" style="padding-top:0px;" >
		<div class="row-fluid MarT20">
		<form name="calculator_frm3" id="calculator_frm3" action="" method="POST" enctype="multipart/form-data" />
		    <div class="span12 mission-brief">
            	<!--<h3 class="h3">TODOS NECESITAMOS UN POCO DE INSPIRACIÓN</h3> -->
                <h4 class="h4">SUBE UNA FOTO O VIDEO  QUE TE INSPIRE<br />A LOGRAR TU SUEÑO.</h4>
                <div class="input_radio"><input name="video_image_type" type="radio" value="1" <?php echo ($userdata1['video_image_type'] =='1' || $userdata1['video_image_type']=='')?"checked='checked'":""; ?> /> 
                	<label>Video:</label> <input name="video" type="text" id="video" placeholder="Video Url" <?php echo ($userdata1['video_image_type'] =='0')?"disabled='disabled'":""; ?> />
					<?php if($userdata1['user_wallpaper']!='' && $userdata1['video_image_type']=='1'){  ?>
					<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png" style="width: 30px;">
					<?php } ?>
					</div>
                <div class="input_radio"><input name="video_image_type" type="radio" value="0" <?php echo ($userdata1['video_image_type'] =='0')?"checked='checked'":""; ?>/> <label>Image:</label> 
                	<input name="image" type="file"  onchange="readURL(this);" <?php echo ($userdata1['video_image_type'] =='1' || $userdata1['video_image_type']=='')?"disabled='disabled'":""; ?> />
					 <?php if($userdata1['user_wallpaper']!='' && $userdata1['video_image_type']=='0'){  ?>
					<img class="check-img" src="<?php echo IMAGE_URL; ?>/check_img.png" style="width: 30px;">
					 <?php } ?>
					</div>
                <div class="MarT40" id="predefine_div">
                <?php if($userdata1['user_wallpaper']==''){ 
				echo '<img src="'.IMAGE_URL.'/user_wallpaper_thum.png" />';
				
				} else{
        	
        	if($userdata1['video_image_type']=='0')
			{
				echo '<img src="'.USER_WALLPAPER.'/'.$userdata1['user_wallpaper'].'" />';
			}else{
				
				$video_url =  parseVideos($userdata1['user_wallpaper']);
				echo '<iframe  src="'.$video_url[0]['url'].'?controls=0" frameborder="0" width="384" height="260" allowfullscreen></iframe>';
			}
   }?></div>
                <div class="MarT40" style="display: none" id="target_div" ><img src="<?php echo IMAGE_URL; ?>/step-3-img.jpg" width="384" height="260" id="preview" /></div>                
                <input type="hidden" name="submit_from" id="submit_from" />
                <div class="MarT20 tc fl width_full">
                	<a href="javascript:void(0)" onclick="jQuery('#submit_from').val('save'); jQuery('#calculator_frm3').submit();" >
                		<img src="<?php echo IMAGE_URL; ?>/save.png"></a>&nbsp;
                	<a class="btn btn-success" href="javascript:void(0)" onclick="jQuery('#calculator_frm3').submit()" ><em>Siguiente</em></a></div>
		    </div>
			<input type="hidden" name="action" value="upload_userinput" />
			<input type="hidden" name="user_id" value="<?php echo $user_id?>" />
		</form>
		</div>
  </div>	
</div>
<script>
$( document ).ready(function() {
    $("#calculator_frm3").validationEngine();
    //$("input[name=video_image_type]:radio").attr( "checked","true" );
    //$("#video").attr( "disabled","true" );
    $("input[name=video_image_type]:radio").change(function() {
    	var radio = $(this).val();
        if(radio =='1'){
        	$(":file").attr( "disabled","true" );
        	$("#video").removeAttr("disabled");
        } else{
        	 $(":file").removeAttr("disabled");
        	 $("#video").attr( "disabled","true" );
        }
    });
});

function get_profie_sales(val)
{
	var product_sell_cost_val = jQuery('#product_sell_cost').val();
	 jQuery("#comission").attr("readonly", false); 
	var comission_val = jQuery('#comission').val();
	var target_val = 	jQuery('#total').val();
		target_val = target_val.replace("$", "");
		target_val = target_val.replace("USD", "");
		target_val = target_val.replace(",", "");
	var profit_val = 0;
	var sales_needed_val = 0;
	var daily_income_req = 0;
	profit_val = parseInt(product_sell_cost_val) - (parseInt((product_sell_cost_val, 10) * 100)/ parseInt(comission_val, 10));
	profit_val = Math.round(profit_val);

	if(profit_val>0)
	{
		jQuery('#profit').val(profit_val);
		sales_needed_val = Math.round(parseInt(target_val,10)/parseInt(profit_val,10));
		jQuery('#sales_needed').val(sales_needed_val);
		jQuery('#monthly_income').html(sales_needed_val+' Clicks');
		daily_income_req = Math.round(parseInt(sales_needed_val,10)/30);		
		jQuery('#daily_income').html(daily_income_req+' Clicks');
	
	}
	
	
	
}
</script>
<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                	$('#predefine_div').hide();
                	$('#target_div').show();
                    $('#preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
