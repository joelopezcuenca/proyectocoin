﻿<div class="left_part">
  <?php
		include_once (INCLUDE_PATH . "/affiliates_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part tutorials">
  <?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
  <span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
  <?php unset($_SESSION['image_ext_error']);
			}
	?>
	<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
  <hr class="mar0" />
  <div class="boxes round round1">
    <div class="row-fluid">
      <?php foreach($allrecordTutorials as $tutorials){ ?>
      <div class="span4 boxbor text-left"> <img  style="height:181px;width: 260px;" src="<?php echo DEFAULT_URL.'/tutorial/'.$tutorials['image']; ?>">
        <div class="paddTB101 min-height110">
          <p style="margin-top:7px;">Blog : <a href="<?php echo $tutorials['blog_link']; ?>" target="_blank"><?php echo $tutorials['blog_link']; ?></a></p>
          <h4><strong><?php echo substr($tutorials['title'],0,20); ?></strong></h4>
          <p><?php echo substr($tutorials['description'],0,100); ?></p>
        </div>
      </div>
      <?php } ?>
    </div>
    <div class="cls"></div>
  </div>
</div>

	<script>
		jQuery(document).ready(function(){
			jQuery("#tutorialSearch").validationEngine();
		});
		function formSubmit(){
			$('#tutorialSearch').submit();
		}
	</script>