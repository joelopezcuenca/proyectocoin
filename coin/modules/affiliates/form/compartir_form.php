﻿<div class="left_part">
	<?php
		include_once (INCLUDE_PATH . "/affiliates_left_navigation.php");
		$user_id = $_SESSION["AD_user_id"];
	?>
</div>
<div class="right_part tutorials">
	<?php 
	if(isset($_SESSION['image_ext_error']))
	{ ?>
		<span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
	<?php unset($_SESSION['image_ext_error']);
			}
	?>
<?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
	
  <hr class="mar0" />
  <div class="round1">
		<div class="row-fluid MarT20">
			 <div class="redios-div">
 				<div class="main-h pd-lr">1.- Selecciona el tipo Link a publicar * </div>
			
			<div class="pd-lr">
			
			<input type="radio" name="share_id" id="share_by_self" value="1" checked="checked"  /> <label>Mi COIN ID</label>
               <input class="form-control" readonly value="<?php echo DEFAULT_URL.'/'.$user_detail['username'];?>"      style="width:295px;" />
					<input type="hidden" id="shareUsername" value="<?php echo $user_detail['username']; ?>"  />  
			</div>	
            <div class="bdr-dotted mr-lr"></div>
			
			<?php if(!empty($top_down_line_detail)){?>
            <div class="pd-lr">
			<input type="radio" name="share_id" id="share_by_powerline" value="0" onclick="  jQuery('#first_power_line').attr('disabled', false); jQuery('.share_line_tag').hide(); jQuery('.power_line_tag').show() " /> <label>KARMALINK  ID</label>
			<div>
               <select class="form-control" name="first_power_line" id="first_power_line" disabled="disabled" onchange="jQuery('.share_line_tag').show();  jQuery('.power_line_tag').hide()">
					  <option value="">-Select user-</option>
					  <?php foreach($top_down_line_detail as $value){
						  if($value['username']=='')
							  continue;
						  ?>
					  <option value="<?php echo $value['username'];?>"><?php echo str_replace('http://www.','',DEFAULT_URL).'/'.$value['username'];?></option>
					  <?php } ?>
					  
				</select>
				<img src="<?php echo IMAGE_URL?>/bx_loader.gif" id="power_line" style="display:none;" />
			</div>
			
			<!-- <div id="second">
               <select class="form-control" name="second_power_line" id="second_power_line" disabled="disabled" onchange=" get_downline('third',this.value)">
				  <option value="">-Select user-</option>
					  <?php foreach($second_down_line_detail as $value){?>
					  <option value="<?php echo $value['username'];?>"><?php echo DEFAULT_URL.'/'.$value['username'];?></option>
					  <?php } ?>
				</select>
			</div>
			
			<div id="third">
               <select class="form-control" name="third_power_line" id="third_power_line" disabled="disabled">
				  <option value="">-Select user-</option>
					  <?php foreach($third_down_line_detail as $value){?>
					  <option value="<?php echo $value['username'];?>"><?php echo DEFAULT_URL.'/'.$value['username'];?></option>
					  <?php } ?>
				</select>
			</div> -->
            </div>
			<?php } ?>
                </div>
		</div>
		<hr class="mar0">
		<div class="row-fluid MarT20">
			 <div class="pd-lr">
 				<div class="main-h f-left">2.- Selecciona el Video que quieras compartir*</div>               
                <div class="edit-ir-but"><a class="edit ir" href="#share_idea" data-toggle="modal">Subir Video</a></div>
             </div>
		</div>
		<hr class="mar0">
		<!-- Share video popup-->
		<div id="share_idea" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<img src="<?php echo IMAGE_URL?>/viemo.png" />
            <img src="<?php echo IMAGE_URL?>/youtube.png" />
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>
		<form id="add_idea" action="<?php echo MODULE_URL; ?>/affiliates/compartir.php?event=compartir" method="post" name="f1" enctype="multipart/form-data" >
			<div class="modal-body">
				<div class="popup">
					<div class="name">
						<span style="color: red">*</span>Titulo :
					</div>
					<div class="select_file">
						<input class="validate[required]" data-prompt-position="bottomLeft:-7" name="idea_name" type="text" placeholder="Titulo ">
					</div>
					<div class="name">
						<span style="color: red">*</span>vídeo Url :
					</div>
					<div class="select_file">
						<textarea placeholder="Copia y pega el link de Youtube..." class="validate[required]" name="idea_video_url"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success">Subir Video</button>
				<input name="action" type="hidden" value="add_idea" />
				<input type="hidden" name="userid" value="<?php echo $_SESSION['AD_user_id']; ?>"/>
				<input type="hidden" name="missionlevel" value="3"/> 
		</form>
		<? /*
			 * End of Modal
			 */
		?>
	</div>
</div>
		
		<ul class="share-idea">
            	
            	<?php
			
				
            	 for($i=0;$iend=count($share_idea_total),$i<$iend;$i++)
				{					
					unset($exp);
					unset($Video_id);	
					$image_video = '';
					//$image_video = get_video_thumnail($share_idea_total[$i]['id'],$share_idea_total[$i]['video']);
					
					$image_video = parseVideos($share_idea_total[$i]['video']);
					
					
					?>
					<li id="<?php echo "share_div_" . $i; ?>">						
                	<div class="video-image"><a href="<?php echo "#video_div_" . $i; ?>" data-toggle="modal">	
                		<img src='<?php echo $image_video[0]['thumbnail']; ?>' alt='' title='<?php
						if (!empty($match[1])) { echo $match[1];
						} else { echo "video ";
						}
 ?>' width='105' height='90' />
                		</a></div>
              <div id="<?php echo "video_div_" . $i; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<img src="<?php echo IMAGE_URL?>/logo.png" />
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="myModalLabel"></h3>
		</div>
		<? /*
			 * Modal for Profile Photo
			 */
		?>
		<form id="add_idea_1" action="<?php echo MODULE_URL; ?>/home/share_the_idea.php" method="post" name="f1" enctype="multipart/form-data" >
			<div class="modal-body">
				<span><iframe width="100%" height="315" src="<?php echo $image_video[0]['url']; ?>" frameborder="0" allowfullscreen></iframe>
				
				<?php  //play_youtube_video($share_idea_total[$i]['video'])?></span>
			</div>
			<div class="modal-footer">
              <img src="<?php echo IMAGE_URL?>/facbookbutton.png" />
              <img src="<?php echo IMAGE_URL?>/twitter-but.png" />
              <div class="font-14 pdtop5 cls"><em>Step 2 of 2</em></div>
				<input type="hidden" name="missionlevel" value="<?php echo $user_detail['mission_level']; ?>"/> 
		</form>
		<? /*
			 * End of Modal
			 */
		?>
	</div>
</div>
                    <div class="share-discription">
                    	<div class="share-head" id="topic_<?php echo $share_idea_total[$i]['id'];?>"><?php echo $share_idea_total[$i]['topic']; ?></div>
                        <div class="share-info"><?php echo $share_idea_total[$i]['discussion']; ?></div>
                        <div class="social-share">
                        	<input type="hidden" name="image_video<?php echo $image_video; ?>" id="image_video<?php echo $image_video; ?>">
                        	<input type="hidden" name="vid<?php echo $image_video; ?>" id="vid<?php echo $image_video; ?>">
                        	<?php 

							
							$imp_array = array(
							"image" => $image_video,
							"user_url"=>DEFAULT_URL.'/'.$_SESSION['AD_user_id'],
							"vid"=>$share_idea_total[$i]['id']
							);
							$temp_imp = serialize($imp_array) ;
							$imp_string = str_replace('"', "--", $temp_imp) ;
							
							
							
					 ?>
					
					 <input type="hidden" id="image_id_<?php echo $share_idea_total[$i]['id']; ?>" value="<?php echo $image_video?>" />
					 <input type="hidden" id="metch_id_<?php echo $share_idea_total[$i]['id']; ?>" value="<?php echo $match[1]?>" />
					
					 
					 
					 <a class="power_line_tag" href="javascript:void(0)" style="display:none" ><img src="<?php echo IMAGE_URL; ?>/twitter.png" class="tweet"></a>
					
                    <a class="power_line_tag" href="javascript:void(0)" style="display:none"><img src="<?php echo IMAGE_URL; ?>/fb.png" class="fb"></a>
					 
					 
						<a class="share_line_tag" href="javascript:void(0)" onclick="share_social('tweet','<?php echo $share_idea_total[$i]['id']; ?>')" ><img src="<?php echo IMAGE_URL; ?>/twitter.png" class="tweet"></a>
					
                    <a class="share_line_tag" href="javascript:void(0)" onclick="share_social('fa','<?php echo $share_idea_total[$i]['id']; ?>')">
                    	<img src="<?php echo IMAGE_URL; ?>/fb.png" class="fb"></a>
						
						
						
						
						
						
						
						
						</div>
                   	<?php } ?>
                    </div>
           </li>  
				 <input type="hidden" value="<?php echo count($share_idea_total); ?>" id="share_count" name="share_count">
            	</ul>					
  </div>	
</div>
<script>
function get_downline(stage,val)
{

	if(val!=''){
		
		jQuery("#power_line").show();
		$.post("<?php echo DEFAULT_URL?>/modules/affiliates/code/ajax_amigos_code.php", {stage:stage, username: val}, 
		function(result){
			jQuery("#power_line").hide();
		   jQuery("#"+stage).html(result);
		});
	}
}
function share_social(socialType,val)
{
	var video_title = '';
	video_title =  jQuery('#topic_'+val).html();
	var videoimage = jQuery('#image_id_'+val).val();
	var metch = jQuery('#metch_id_'+val).val();
	
	var userName = '';	
	
	if (jQuery("#share_by_self").is(":checked")) 
	{
		userName = '<?php echo DEFAULT_URL?>/'+jQuery('#shareUsername').val();
		
		//alert(userName);
	}else{
		var third_power_line_val = '';
		var second_power_line_val = '';
		var first_power_line_val = '';
		 third_power_line_val = jQuery('#third_power_line').val();
		 second_power_line_val = jQuery('#second_power_line').val();
		 first_power_line_val = jQuery('#first_power_line').val();
		//var third_power_line_val = jQuery("#third_power_line :selected").text();
		//if(third_power_line_val!='undefined')
		//{	
			//userName = '<?php echo DEFAULT_URL?>/'+third_power_line_val;
		//}else if(second_power_line_val!="undefined"){					
			//userName = '<?php echo DEFAULT_URL?>/'+second_power_line_val;
		//}else{		
			//userName = '<?php echo DEFAULT_URL?>/'+first_power_line_val;
		//}	
		userName = '<?php echo DEFAULT_URL?>/'+first_power_line_val;
	}
	
		if(socialType=='tweet')
		{
			var url = 'https://twitter.com/intent/tweet?text='+video_title+' proyectocoin '+metch+' '+userName;
			window.open(url,'_blank');
		}else{
				
		jQuery("#power_line").show();
		$.post("<?php echo DEFAULT_URL?>/modules/affiliates/code/ajax_share_facebook.php", {image:videoimage, user_url: userName,vid:val}, 
		function(result){
			
			jQuery("#power_line").hide();
			window.open(result,'_blank');
		});
		}
	
	
}
</script>