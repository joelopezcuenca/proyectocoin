﻿<div class="left_part">
  <?php

		include_once (INCLUDE_PATH . "/affiliates_left_navigation.php");

		$user_id = $_SESSION["AD_user_id"];

	?>
</div>
<div class="right_part  tutorials">
  <?php 

	if(isset($_SESSION['image_ext_error']))

	{ ?>
  <span style="color: red;"><b><?php echo $_SESSION['image_ext_error']; ?></b></span>
  <?php unset($_SESSION['image_ext_error']);

			}

	?>
  <?php include_once (INCLUDE_PATH . "/var_tutorial_section.php");  ?>
  <hr class="mar0" />
  <div class="round1 Font14 banner-page">
    <div class="row-fluid">
      <div class="span10 offset1">
        <label>1.- Selecciona el Link a publicar <span class="red">*</span></label>
        <div class="round row-fluid">
          <div class="span4"><input checked="checked" type = "radio" name="banner_type"><span class="lable-name">Landing Page 1</span></div>
          <div class="span8">
            <input type="text" name="promotion url" value="<?php echo DEFAULT_URL.'/'.$_SESSION['AD_user_name']; ?>">
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="round row-fluid">
          <div class="span4">
            <input type = "radio" name="banner_type"><span class="lable-name">Landing Page 2</span> </div>
          <div class="span8">
            <input type="text" name="promotion url" value="<?php echo DEFAULT_URL.'/webinar/'.$_SESSION['AD_user_name']; ?>">
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="round row-fluid">
          <div class="span4">
            <input type = "radio" name="banner_type"><span class="lable-name">Landing Page 3</span></div>
          <div class="span8">
            <input type="text" name="promotion url" value="<?php echo DEFAULT_URL.'/new/'.$_SESSION['AD_user_name']; ?>">
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <div class="cls"></div>
    <div class="row-fluid MarT20">
      <div class="span10 offset1">
        <label>2.- Copia el codigo HTML  para colocarlo en el medio que desees <span class="red">*</span></label>
      </div>
      <div class="span10 offset1 text-center bannersap"></div>
    </div>
    <?php foreach($allrecordBanner as $banner){ ?>
    <div class="bannerpage-bannerpart">
      <div class="row-fluid">
        <div class="span10 offset1">
          <div class="row-fluid">
            <div class="span4">
              <textarea><a target="_blank" href="<?php echo DEFAULT_URL.'/'.$_SESSION['AD_user_name']; ?>"><img src="<?php echo DEFAULT_URL.'/banners/'.$banner['banner']; ?>"/></a></textarea>
            </div>
            <div class="span8 text-right">
              <h3>Skycraper &nbsp; &nbsp;700x800</h3>
              <img style="height:100px;" alt="logo" src="<?php echo DEFAULT_URL.'/banners/'.$banner['banner']; ?>"> </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="span10 offset1 text-center bannersap"></div>
    <?php } ?>
    <h3 class="center-title">All of the othe banner sizes</h3>
    <div class="cls"></div>
  </div>
</div>
