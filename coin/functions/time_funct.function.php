<?php
function getCurrentTimestamp(){
	$timestamp = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
	return $timestamp;
}

function getTodayDate(){
	$date_take 			= date(DEFAULT_DATE_FORMAT);
	$dateObj 			= new Date_Time_Calc($date_take, "m-d-Y");
	$currentDateStamp	= $dateObj->date_time_stamp;
	return $currentDateStamp;
}
?>