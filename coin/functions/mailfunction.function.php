<?php

//include ("../../conf/config.inc.php");

$mail = new PHPMailer();
function UserFirstSubscriberMail($email) {

	global $mail;
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);

	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	$send_id = $_SESSION["AD_user_id"];
	$mail -> CharSet = 'UTF-8';
	$mail -> From = CLIENT_FROM;
	$mail->FromName = 'Coin';
	$mail -> AddAddress($email);
	//$mail -> AddAddress('apar@fpdemo.com','raghwendra@fpdemo.com');
	$mail -> Subject = "Bienvenido a la Comunidad Coin.";
	$message = file_get_contents(DEFAULT_URL.'/email_template/userfirstsubscribermail.html');
	$message = str_replace('[SITE_URL]',DEFAULT_URL,$message);
	$message = str_replace('[USERID]',base64_encode($user_data['id']),$message);
	
	$mail -> Body = $message;
	$mail -> IsHTML(true);
	$mail -> WordWrap = 50;
	if (!$mail -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail -> ErrorInfo;

	} else {
		unset($mail);
		return;

	}

}

function UserSecondSubscriberMail($email, $username, $name='', $password) {

	global $mail;

	$send_id = $_SESSION["AD_user_id"];
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);

	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	$mail -> From = CLIENT_FROM;
	$mail->FromName = 'Coin';
	$mail -> AddAddress($email);
	$mail -> CharSet = 'UTF-8';
	$mail -> Subject = "IMPORTANTE - la Contraseña para lograr tus sueños!";
	$message = file_get_contents(DEFAULT_URL.'/email_template/usersecondsubscribermail.html');
	$message = str_replace('[SITE_URL]',DEFAULT_URL,$message);
	$message = str_replace('[USERNAME]',$username,$message);
	$message = str_replace('[PASSWORD]',$password,$message);
	$message = str_replace('[NAME]',$name,$message);
	$message = str_replace('[USERID]',base64_encode($user_data['id']),$message);

	
	$mail -> Body = $message;
	$mail -> IsHTML(true);
	$mail -> WordWrap = 50;
	if (!$mail -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail -> ErrorInfo;

	} else {
		//echo "message sent form here successfully";
		unset($mail);
		//echo $message ;

	}

}

function Complete3MissionsMail($email) {

	global $mail;
	//$from = "hola@comunidadcoin.org" ;
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	//$mail->isSMTP();
	$mail -> From = CLIENT_FROM;
	for ($i = 0; $i < count($email); $i++) {
		$mail -> addBCC($email[$i]);
	}
	$mail -> IsHTML(true);
	$mail -> CharSet = 'UTF-8';
	$mail -> Subject = "3 Misiónes = Libertad, Diversión y Aventura";
	$message = ' <body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116"><img src="'.IMAGE_URL.'/nesweletter-logo.png" width="600" height="116" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px 0 20px;line-height:normal">Completa estas 3 misiones<br/>
para comenzar a vivir una<br/>
vida llena de Libertad,<br/>
Diversión y Aventura</h1></td>
              </tr>
              <tr>
                <td width="600" bgcolor="ffffff" style=" padding:40px 0 50px; text-align:center;"><a href="http://www.proyectocoin.com/coin/modules/home/missions.php" target="_blank"><img width="364" height="203" src="'.IMAGE_URL.'/3_Misiónes_Libertad_Diversión y_Aventura.jpg"></a></td>
              </tr>
              <tr>
                <td class="over" width="600" bgcolor="ffffff" style=" padding-bottom:50px;color:#4a4a4a; text-align:center; color:#4a4a4a; line-height:22px;"><a href="http://www.proyectocoin.com/coin/modules/home/missions.php" style="text-align: center; background:rgba(100, 183, 67, 0.82); color:#fff; cursor:pointer; padding:21px 36px;"><span style="line-height: 60px; font-size:20px;">Comenzar AHORA!</span></a></td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                          
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
	$mail -> Body = $message;

	$mail -> WordWrap = 50;
	if (!$mail -> send()) {
	//	echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail -> ErrorInfo;

	} else {

		//echo "Mail Sent <br/>";
		unset($mail);
	}
}

function SeeVideoMail($email) {
	global $mail;
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	//$from = "hola@comunidadcoin.org" ;
	$mail -> From = CLIENT_FROM;
	for ($i = 0; $i < count($email); $i++) {
		$c = $mail -> validateAddress($email[$i]);
		if ($c == 1) {
			$mail -> addBCC($email[$i]);
		}
	}
	$mail -> CharSet = 'UTF-8';
	$mail -> Subject = "Un poco de inspiración...(VIDEO)";
	$message = ' <body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116"><img src="'.IMAGE_URL.'/nesweletter-logo.png" width="694" height="145" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px;line-height:normal">Queremos que te inspires!</h1></td>
              </tr>
              <tr>
                <td bgcolor="ffffff" align="center" ><p style="padding:0px 70px;font-size:20px;color:rgb(67,67,67);line-height:30px;font-family:Arial, Helvetica, sans-serif;">Queremos compartir contigo este video que<br/>
                 ha inspirado a cientos de miembros de la<br/>
                  Comunidad Coin.<br/>

Esperamos te sirva de inspiración :)
</p></td>
              </tr>
              <tr>
                <td width="600" bgcolor="ffffff" style="padding-left:40px;padding-right:40px; text-align:center;padding-bottom: 40px;"><a target="_blank" href="http://www.comunidadcoin.org/#!manifesto-coin/c1jjb"><img src="'.IMAGE_URL.'/Un_poco_de_inspiración(VIDEO).jpg" width="364" height="203" /></a></td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                          
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
	$mail -> Body = $message;
	$mail -> IsHTML(true);
	$mail -> WordWrap = 50;
	if (!$mail -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail -> ErrorInfo;

	} else {

		//echo "Mail Sent <br/>";
		unset($mail);
	}

}


function NewUserAddMail($email) {
	
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	$mail1 = new PHPMailer();
    $send_id = $_SESSION["AD_user_id"];
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> From = CLIENT_FROM;
	$mail1 -> AddAddress($email);
	$mail1 -> Subject ="Acabas de recibir tu Primer COIN, Activalo!";
	
	$message = file_get_contents(DEFAULT_URL.'/email_template/get_first_coin.html');
	$message = str_replace('[SITE_URL]',DEFAULT_URL,$message);
	$message = str_replace('[USERID]',base64_encode($user_data['id']),$message);

	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
	//	echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
		
		//echo "message sent form here";
		
		//echo $message ;

	}


}



function enroller_mail($email,$type) {
	
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	$mail1 = new PHPMailer();
    $send_id = $_SESSION["AD_user_id"];
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> From = CLIENT_FROM;
	$mail1 -> AddAddress($email);
	
	
	
	if($type=='afterfirst'){
		$mail1 -> Subject ="Ganaste más Coins! revisa tus Ganancias";
		$message = file_get_contents(DEFAULT_URL.'/email_template/afterfirst_email.html');
		$message = str_replace('[SITE_URL]',DEFAULT_URL,$message);
}else if('powerline_notification'){
	$mail1 -> Subject ="Ganaste un Karma Coin! revisa tus Ganancias";
	$message = file_get_contents(DEFAULT_URL.'/email_template/powerline_notification.html');
	$message = str_replace('[SITE_URL]',DEFAULT_URL,$message);
	
}else if($type=='prospects_notification')
{
	$mail1 -> Subject ="Tu Primer Soñador";
	$message = file_get_contents(DEFAULT_URL.'/email_template/prospects_notification.html');
	$message = str_replace('[SITE_URL]',DEFAULT_URL,$message);
}
	$message = str_replace('[USERID]',base64_encode($user_data['id']),$message);
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
	//	echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
		
		//echo "message sent form here";
		
		//echo $message ;

	}


}




function SeeVideoMail_4thday($email) {
	global $mail;

	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	//$from = "hola@comunidadcoin.org" ;
	$mail -> From = CLIENT_FROM;
	for ($i = 0; $i < count($email); $i++) {
		$c = $mail -> validateAddress($email[$i]);
		if ($c == 1) {
			$mail -> addBCC($email[$i]);
		}
	}
	$mail -> CharSet = 'UTF-8';
	$mail -> Subject = "Ricardo antes trabajaba de 9 a 5 ...mira a lo que se dedica Hoy! (VIDEO)";
	$message = ' <body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116"><img src="'.IMAGE_URL.'/nesweletter-logo.png" width="600" height="116" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px 0 20px;line-height:normal">Un caso de éxito más!<br/>
Mira como Ricardo esta viviendo<br/>
una Vida llena de Libertad,<br/>
Diversión y Aventura<br/></h1></td>
              </tr>
              <tr>
                <td width="600" bgcolor="ffffff" style=" padding:40px 0 50px; text-align:center;"><a href="http://www.comunidadcoin.org/#!bienvenido/c24u9" target="_blank"><img width="364" height="203" src="'.IMAGE_URL.'/coin_video_img.png"></a></td>
              </tr>
              
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                         
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
	$mail -> Body = $message;
	$mail -> IsHTML(true);
	$mail -> WordWrap = 50;
	if (!$mail -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail -> ErrorInfo;

	} else {

		//echo "Mail Sent <br/>";
		unset($mail);
	}

}

function ProAccountMail($email) {
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	//global $mail;
$mail1 = new PHPMailer();
	//$from = "hola@comunidadcoin.org" ;

	$mail1 -> From = CLIENT_FROM;
	for ($i = 0; $i < count($email); $i++) {
		$c = $mail1 -> validateAddress($email[$i]);
		if ($c == 1) {
			$mail1 -> addBCC($email[$i]);
		}
	}
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> Subject = "Cambia tus Coins por dinero para cumplir tus sueños";
	$message = ' <body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116"><img src="'.IMAGE_URL.'http://www.proyectocoin.com/coin/images/nesweletter-logo.png" width="600" height="116" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px 0;line-height:normal">Cambia tus COINS por<br />Dinero y cumple cada uno de<br />Tus Sueños
</h1></td>
              </tr>
              <tr><td bgcolor="ffffff" align="center">&nbsp;</td></tr>
              <tr>
                <td align="center" width="600" bgcolor="ffffff" style="padding:25px 0 0; color:#4a4a4a; text-align:center;#4a4a4a; line-height:22px;"><img src="'.IMAGE_URL.'/coins.png" width="224" height="126" /></td>
              </tr>
              <tr>
                <td bgcolor="ffffff" align="center" ><p style="padding:0px 70px;font-size:20px;color:#4a4a4a;line-height:26px;font-family:Arial, Helvetica, sans-serif;">Sabías que puedes cambiar los coins que<br/>recibas de otros miembros de la Comunidad<br/> por Dinero Real?<br/>
Es posible y con ese Dinero queremos <br/>motivarte a que cumplas todos los sueños de<br/> tu Lista</p></td>
              </tr>
              <tr>
                <td class="over" width="600" bgcolor="ffffff" style=" padding-bottom:50px;color:#4a4a4a; text-align:center; color:#4a4a4a; line-height:22px;"><a href="http://comunidadcoin.com/coin/modules/home/dopayment.php" style="text-align: center; background:rgba(100, 183, 67, 0.82); color:#fff; cursor:pointer; padding:21px 36px;"><span style="line-height: 60px; font-size:20px;">Cambiar mis Coins por Dinero</span></a></td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                         
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
		//echo "mail sent successfully";
		

	}

}



function Missionmail($email, $level) {
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	
	//global $mail;
   $mail1 = new PHPMailer();
	//$from = "hola@comunidadcoin.org" ;

	$mail1 -> From = CLIENT_FROM;
	$mail1 -> AddAddress($email);
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> Subject = "Misión " . $level . " = lograda!";
	$message = '<body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116">
                <img src="'.IMAGE_URL.'http://www.proyectocoin.com/coin/images/nesweletter-logo.png" width="695" height="139" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px;line-height:normal">MISION ' . $level . ':<br /><span style="color: #64b743;">COMPLETADA</span></h1></td>
              </tr>
              <tr>
                <td bgcolor="ffffff" align="center" ><p style="padding:0px 70px;font-size:20px;color:rgb(67,67,67);line-height:30px;font-family:Arial, Helvetica, sans-serif;">Estas listo para la<br /><span style="color: #0099ff;">MISION ' . ($level + 1) . ':</span><br />Ayuda a los de tu comunidad a<br />lograr sus primeros COINS</p></td>
              </tr>
              <tr>
                <td width="600" bgcolor="ffffff" style="padding-left:40px;padding-right:40px; text-align:center;padding-bottom: 40px;"><a target="_blank" href="http://www.proyectocoin.com/coin/modules/home/missions.php?mission_check=2">
                <img src="'.IMAGE_URL.'/lussy.png" width="364" height="203" /></a>
                </td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                          
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
die;
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
	//	echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
unset($mail);
	}

}

function ProUserAddMail($email) {
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	//global $mail;
 $mail1 = new PHPMailer();
	//$from = "hola@comunidadcoin.org" ;

	$mail1 -> From = CLIENT_FROM;
	for ($i = 0; $i < count($email); $i++) {
		$c = $mail1 -> validateAddress($email[$i]);
		if ($c == 1) {
			$mail1 -> addBCC($email[$i]);
		}
	}
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> Subject = "WOW!... Sigues ganando más COINS!";
	$message = '<body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116"><img src="'.IMAGE_URL.'/nesweletter-logo.png" width="600" height="116" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px 0;line-height:normal">Hey! Felicidades! <br />
Has ganado más <br /> COINS!!!<br />

</h1></td>
              </tr>
              <tr><td bgcolor="ffffff" align="center">&nbsp;</td></tr>
              <tr>
                <td align="center" width="600" bgcolor="ffffff" style="padding:25px 0 0; color:#4a4a4a; text-align:center;#4a4a4a; line-height:22px;"><img src="'.IMAGE_URL.'/coins3.png" width="264" height="128" /></td>
              </tr>
              <tr><td bgcolor="ffffff" align="center">&nbsp;</td></tr>
              <tr>
                <td bgcolor="ffffff" align="center" ><p style="padding:0px 70px;font-size:20px;color:#4a4a4a;line-height:26px;font-family:Arial, Helvetica, sans-serif;">Al parecer todo va viento en Popa.<br/>Muchas felicidades! <br/>No olvides que no solo se trata de ganar<br/>
COINS, si no que los utilices<br/>para lograr tus sueños!</p></td>
              </tr>
              <tr>
                <td class="over" width="600" bgcolor="ffffff" style=" padding-bottom:50px;color:#4a4a4a; text-align:center; color:#4a4a4a; line-height:22px;"><a href="http://www.proyectocoin.com/coin/modules/home/UserLevelEarning.php" style="text-align: center; background:rgba(100, 183, 67, 0.82); color:#fff; cursor:pointer; padding:21px 36px;"><span style="line-height: 60px; font-size:20px;">Cuántos COINS Gané?
</span></a></td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                       
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail -> ErrorInfo;

	} else {

	}

}

function FirstProUserAddMail($email) {
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	//global $mail;
	$mail1 = new PHPMailer();
	//$from = "hola@comunidadcoin.org" ;

	$mail1 -> From = CLIENT_FROM;
	$mail1 -> AddAddress($email);
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> Subject = "(IMPORTANTE) Tu primer Dólar! acabad de ganar tu primer...";
	$message = '<body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116"><img src="'.IMAGE_URL.'/nesweletter-logo.png" width="600" height="116" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px 0;line-height:normal">WOW!!<br/>Oficialmente a partir de<br/><span style="color: #0099ff;">HOY</span><br/>comenzarás a<br/>cumplir tus sueños.</h1></td>
              </tr>
              <tr>
                <td width="600" bgcolor="ffffff" style="padding:25px 0 0; color:#4a4a4a; text-align:center;line-height:22px;"><img src="'.IMAGE_URL.'/coins.png" width="224" height="126" /></td>
              </tr>
              <tr>
                <td bgcolor="ffffff" align="center" ><p style="padding:0px 70px 20px 70px;font-size:18px;color:#717070;line-height:26px;font-family: "open_sansregular";">Hey! Ya lograste tu primer dólar!<br/>es cuestión de tiempo para que recibas<br/>más noticias de este tipo!</p></td>
              </tr>
              <tr>
                <td class="over" width="600" bgcolor="ffffff" style=" padding-bottom:50px;color:#4a4a4a; text-align:center; color:#4a4a4a; line-height:22px;"><a href="http://www.proyectocoin.com/coin/modules/payment/php_files/prouser.php" style="text-align: center; background:rgba(100, 183, 67, 0.82); color:#fff; cursor:pointer; padding:21px 36px;"><span style="line-height: 60px; font-size:20px;">Ver mis Ganancias</span></a></td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                       
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		///echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
unset($mail);
	}

}

function ProMemeberMail($email) {
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	//global $mail;
$mail1 = new PHPMailer();
	//$from = "hola@comunidadcoin.org" ;

	$mail1 -> From = CLIENT_FROM;
	$mail1 -> AddAddress($email);
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> Subject = "Congratulations Pro Member";
	$message = '<body style="padding:0; margin:0;">
<div style="margin:0px;padding:0px;background:rgb(247,246,244)">
  <table cellspacing="0" cellpadding="0" border="0" style="width:100%">
    <tbody>
      <tr>
        <td bgcolor="#f7f6f4" align="center" style="margin:0px">
        	<table cellspacing="0" cellpadding="0" border="0" align="center" style="font-family:Arial, Helvetica, sans-serif;color:#4a4a4a);width:600px; margin-top:30px;">
            <tbody>
              <tr>
                <td height="116"><img src="'.IMAGE_URL.'/nesweletter-logo.png" width="600" height="116" alt="logo" /></td>
              </tr>
              <tr>
                <td height="75" bgcolor="#ffffff" align="center" style=" border-bottom:1px dotted #b2b2b2; margin:0px;color:rgb(111,111,111);background-color:rgb(255,255,255)"><h1 style="color:#4a4a4a; font-weight:normal;font-size:27px;font-family: "Conv_Georgia";margin:0px;padding:20px 0;line-height:normal">Felicidades y Bienvenido<br />Oficialmente a la <br /><span style="color: rgba(100, 183, 67, 0.82);">Comunidad Coin!!!<span>
</h1></td>
              </tr>
              <tr><td bgcolor="ffffff" align="center">&nbsp;</td></tr>
              <tr>
                <td align="center" width="600" bgcolor="ffffff" style="padding:25px 0 0; color:#4a4a4a; text-align:center;#4a4a4a; line-height:22px;"><img src="'.IMAGE_URL.'/round-image.png" width="121" height="151" /></td>
              </tr>
              <tr><td bgcolor="ffffff" align="center">&nbsp;</td></tr>
              <tr><td width="600" bgcolor="ffffff" style="padding:25px 0 0; color:#4a4a4a; text-align:center;#4a4a4a; line-height:22px;">&nbsp;</td></tr>
              <tr>
                <td bgcolor="ffffff" align="center" ><p style="padding:0px 70px;font-size:20px;color:#4a4a4a;line-height:26px;font-family:Arial, Helvetica, sans-serif;">
               Es momento de la verdad!!<br/>Es momento de <br/>cumplir tus sueños<br/>
Es cuestión de tiempo para <br/>que generes la suficiente cantidad de.<br/>Coins e ingresos que te permitan <br/>
lograr cualquier sueño</p></td>
             
			</tr>
              <tr><td width="600" bgcolor="ffffff" style="padding:25px 0 0; color:#4a4a4a; text-align:center;#4a4a4a; line-height:22px;">&nbsp;</td></tr>
              <tr>
                <td class="over" width="600" bgcolor="ffffff" style=" padding-bottom:50px;color:#4a4a4a; text-align:center; color:#4a4a4a; line-height:22px;"><a href="http://www.proyectocoin.com/coin/modules/home/bucket_list.php" style="text-align: center; background:rgba(100, 183, 67, 0.82); color:#fff; cursor:pointer; padding:21px 36px;"><span style="line-height: 60px; font-size:20px;">Agregar más sueños!</span></a></td>
              </tr>
              <tr>
                <td bgcolor="#ffffff" style="padding:15px 0px;border-top:1px dotted #b2b2b2;background:rgb(255,255,255)"><table cellpadding="0" border="0" align="center" style="width:100%">
                    <tbody>
                      <tr>
                      	<td>
                        	<table width="100%" cellpadding="0" border="0">
                            	<tr>
                                	<td align="left" style="padding-left:25px;"><img src="'.IMAGE_URL.'/news-foot-logo.png" width="82" height="26" /></td>
                                    <td align="right" style="padding-right:25px;"><a target="_blank" href="https://www.facebook.com/"><img src="'.IMAGE_URL.'/fb1.png" width="40" height="40" alt="facebook" /></a>
                        <a target="_blank" href="https://twitter.com/"><img src="'.IMAGE_URL.'/tw.png" width="40" height="40" alt="twitter" /></a></td>
                                </tr>
                            </table>
                        </td>
                        
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
              <tr>
                <td height="20"><img width="600" height="20" border="0" alt="" src="'.IMAGE_URL.'/sheddow.png"></td>
              </tr>
              <tr>
                <td><table cellspacing="0" cellpadding="0" border="0" style="font-size:11px;width:600px; padding:0 0 50px;">
                    <tbody>
                      <tr>
                        <td align="left">
                        
                          <p style="font-size:11px;margin:0px;line-height:16px"><a target="_blank" href="'.MODULE_URL.'/modules/home/mail_unsubscribe.php?action=unsubscribe&data='.base64_encode($user_data['id']).'" style="text-decoration:none;color:#919191">Unsubscribe</a></p>
                         </td>
                        <td align="right"></td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
  
  <div>
  </div>
  </div>
</body>';
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {

		$to = 'apar@fpdemo.com';
		$subject = 'This is testing purpose mail to show that mail for your pro account is send from here';
		$message = 'This is testing purpose mail to show that mail for your pro account is send from here where email id=' . $email;
		mail($to, $subject, $message);
		unset($mail);
	}


}

function InviteFriendMAil($email, $msg) {
	//global $mail;
	//echo $email ;
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	$mail1 = new PHPMailer();
	$email_array = explode(",", $email);
	$mail1 -> From = CLIENT_FROM;
	for ($i = 0; $i < count($email_array); $i++) {
		$mail1 -> addBCC($email_array[$i]);
	}
	$mail1 -> CharSet = 'UTF-8';
	$mail1 -> Subject = "Te invito a unirte a mi Comunidad";
	$message = $msg;
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
		unset($mail1);
		echo "<script>window.location.href='" . MODULE_URL . "/home/invitation_thankyou.php'</script>";
	}

}

function UserInfoNotifyMail($email, $username, $password, $url) {

	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	//global $mail;
	$mail1 = new PHPMailer();
	$mail1 -> From = "comunidadcoin@gmail.com";
	$mail1 -> AddAddress($email, '');
	$mail1 -> Subject = "Bienvenido a la Comunidad Coin.";
	$message = "<div style='width:620px;'>
						  <table width='100%' cellspacing='1'  cellpadding='0' style='background:#f1ebe5; border:1px solid #c8beb2;'>
						   <tr style='float:left' bgcolor='#565655'>
						      <td style='padding:10px 0px 10px 20px; width:600px;'>
						      <img src='http://phpdemo.internetbusinesssolutionsindia.com/coin/images/logo.png'>
						        </td>
						    </tr>
						    <tr>
						      <td><div>
						          <table  border='0' cellpadding='10'width='100%'>
						           <tr>
						              <td style='color:#545454; font-family:'Arial'; font-size:12px;'><strong>Hola " . $name . "</strong></td>
						            </tr>
						            <tr>
						              <td style='color:#545454; font-family:'Arial'; font-size:12px;'><strong>Bienvenido a la Comunidad Coin!<br />
Listo para tomar el control de tu Libertad Financiera??
 
</strong></td>
						            </tr>
						           
						            
						            <tr>
						              <td style='color:red; font-family:'Arial'; font-size:12px;'>Este es tu Link personalizado." . $url . ", la llave a tu libertad financiera ;)
<br/>
						                </td>
						            </tr>
						            
									
									<tr>
						              <td style='color:red; font-family:'Arial'; font-size:12px;'>Estas son tus claves para accesar a tu perfil y comenzar a generar ganancias.
						                </td>
						            </tr>
						            
									<tr>
						              <td style='color:red; font-family:'Arial'; font-size:12px;'>Usuario: " . $username . "
						                </td>
						            </tr>
						            <tr>
						              <td style='color:red; font-family:'Arial'; font-size:12px;'>Contraseña: " . $password . "
						                </td>
						            </tr>
									<tr>
						              <td style='color:#2d2d2c; font-family:'Arial'; font-size:12px;'>Saludos! <br/>La Comunidad Coin
						                </td>
						            </tr>
						            
						            
						          </table>
						        </div>
						        </td>
						    </tr>
						  </table>
						</div>";

}

function BucketlistNotifyMail($from, $email, $bucket, $currentusername, $bucket_name) {

	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	//global $mail;
$mail1 = new PHPMailer();
	$send_id = $_SESSION["AD_user_id"];

	$mail1 -> From = CLIENT_FROM;
	for ($i = 0; $i < count($email); $i++) {

		if ($email[$i] != "") {

			$mail1 -> addBCC($email[$i]);
		}
	}
	$mail1 -> CharSet = 'UTF-8';
	//$mail -> AddAddress('apar@fpdemo.com','raghwendra@fpdemo.com');
	$mail1 -> Subject = "Hacemos esto?.";
	$message = "<div style='width:620px;'>
						  <table width='100%' cellspacing='1'  cellpadding='0' style='background:#f1ebe5; border:1px solid #c8beb2;'>
						   <tr style='float:left' bgcolor='#565655'>
						      <td style='padding:10px 0px 10px 20px; width:600px;'>
						      <img src='http://www.proyectocoin.com/coin/images/logo.png'>
						        </td>
						    </tr>
						    <tr>
						      <td><div>
						          <table  border='0' cellpadding='10'width='100%'>
						           <tr>
						              <td style='color:#545454; font-family:'Arial'; font-size:12px;'><strong>Hola</strong></td>
						            </tr>
						            <tr>
						              <td style='color:#545454; font-family:'Arial'; font-size:12px;'><strong>¿Qué te parece si logramos esto juntos?<br />
'" . $bucket_name . "'
 
</strong></td>
						            </tr>
						           
						            <tr>
						              <td style='color:red; font-family:'Arial'; font-size:12px;'>Agrégalo a tu lista de sueños.
Si aún no tienes cuenta, aquí te mando
Una invitación:

						                </td>
						            </tr>
						            <tr>
						              <td style='color:red; font-family:'Arial'; font-size:12px;'>
						              <a target='_blank' href='" . MODULE_URL . "/home/bucketinvite_second.php?bucket=" . $bucket . "&send_id=" . $send_id . "'>" . MODULE_URL . "/home/bucketinvite_second.php</a>
						                </td>
						            </tr>
									<tr>
						              <td style='color:#2d2d2c; font-family:'Arial'; font-size:12px;'>Saludos! <br/>La Comunidad Coin
						                </td>
						            </tr>
						            
						            
						          </table>
						        </div>
						        </td>
						    </tr>
						  </table>
						</div>";
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
unset($mail);
	}

}

function MembershipProNotifyMail($email) {
		
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}	
	//global $mail;
    $mail1 = new PHPMailer();
	$mail1 -> From = CLIENT_FROM;
	$mail1 -> AddAddress($email);

	$mail1 -> CharSet = 'UTF-8';
	//$mail -> AddAddress('apar@fpdemo.com','raghwendra@fpdemo.com');
	$mail1 -> Subject = "Felicidades! Te has convertido en Usuario Pro.";
	$message = "<div style='width:620px;'>
						  <table width='100%' cellspacing='1'  cellpadding='0' style='background:#f1ebe5; border:1px solid #c8beb2;'>
						   <tr style='float:left' bgcolor='#565655'>
						      <td style='padding:10px 0px 10px 20px; width:600px;'>
						      <img src='".IMAGE_URL."/logo.png'>
						        </td>
						    </tr>
						    <tr>
						      <td><div>
						          <table  border='0' cellpadding='10'width='100%'>
						           <tr>
						              <td style='color:red; font-family:'Arial'; font-size:12px;'>
						            A partir de ahora tendrás derecho a convertir en dinero todos tus coins de oro. <br/>
Dirígete a la sección de Misiones para averiguar cómo.
  </td>
						            </tr>
						            <tr>
						             <td style='color:red; font-family:'Arial'; font-size:12px;'>
						              <a target='_blank' href='" . MODULE_URL . "/home/home.php</a>
						                </td>
						                </tr>
						            
									<tr>
						              <td style='color:#2d2d2c; font-family:'Arial'; font-size:12px;'>Saludos! <br/>La Comunidad Coin
						                </td>
						            </tr>
						            
						            
						          </table>
						        </div>
						        </td>
						    </tr>
						  </table>
						</div>";
	$mail1 -> Body = $message;
	$mail1 -> IsHTML(true);
	$mail1 -> WordWrap = 50;
	if (!$mail1 -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail1 -> ErrorInfo;

	} else {
	}

}

function UserVerification($email,$msg) {
	
	$objUser = new user();
	$condition_fun = "email='" . $email . "'";
	$user_data = $objUser->getOnGivenusername($condition_fun);
	if($user_data['mail_status'] == '1')
	{
		return;
	}
	
	$mail-> From = CLIENT_FROM;
	$mail -> AddAddress($email);
	$mail-> CharSet = 'UTF-8';
	$mail-> Subject = "Email Verification";
	$message = $msg;
	$mail-> Body = $message;
	$mail-> IsHTML(true);
	$mail-> WordWrap = 50;
	if (!$mail -> send()) {
		//echo 'Message could not be sent.';
		//echo 'Mailer Error: ' . $mail -> ErrorInfo;

	} 
}



?>