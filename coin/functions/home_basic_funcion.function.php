<?php
/*
 * Developer :Raghwendra
 * Here Function designed
 * for usefull task such as search a url
 * in a texts,sentences and in paragraph
 * 
*/
function autolink($str, $attributes=array()) {
	    $attrs = '';
	    foreach ($attributes as $attribute => $value) {
	        $attrs .= " {$attribute}=\"{$value}\"";
	    }
	 	$str = ' ' . $str;
	    $str = preg_replace(
	        '`([^"=\'>])((http|https|ftp)://[^\s<]+[^\s<\.)])`i',
	        '$1<a  target="_blank" href="$2"'.$attrs.'>$2</a>',
	        $str
	    );
	    $str = substr($str, 1);	     
	    return $str;
	}

	
?>