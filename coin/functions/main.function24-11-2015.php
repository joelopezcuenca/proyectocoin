<?php
	/* Basics Functions
	 * Here All Basic Functions Declaration
	 * if Needed Directly call it No Isuues at all
	 * It will return same results to avoid big-code
	 */
	 
	//Get Page Name..
	function getMyPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 
		//getting a position and use with substr next to 1 adding after /
	}
    
    // get random password
    function genRandomPass($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = "";
        
        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters))];
        }
        return $string;
    }
    
    // is email valid
    function is_valid_email($email)
    {
        $result = TRUE;
        if(filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $result = FALSE;
        }
        return $result;
    }
    
    // is valid URL
    function isValidURL($url)
    {
        if($url != "")
        {
            return !filter_var($url, FILTER_VALIDATE_URL);
        }
    }
    
    // Allow only numbers
    function onlyNumValidation($number)
    {
        if($number != "")
        {
            return is_int($number);
        }
    }
    
    // Allow only chars
    function onlyCharValidation($string)
    {
        if($string != "")
        {
            return preg_match('/^[A-Z][a-zA-Z -]+$/',$user_name);
        }
    }
    
    // Create hash from string (SHA512)
    function createHash($string)
    {
        if($string != "")
        {
            return hash(DEFAULT_HASH_ALGO, $string);
        }
    }
    
    
    // count total words
    function am_countWords($string)
    {
        $word_count = 0;
        $string = eregi_replace(" +", " ", $string);
        $string = eregi_replace("\n+", " ", $string);
        $string = eregi_replace(" +", " ", $string);
        $string = explode(" ", $string);
    
        while (list(, $word) = each ($string)) {
            if (eregi("[0-9A-Za-z�-��-��-�]", $word)) {
                $word_count++;
            }
        }
        return $word_count;
    }
    
    function searchMultiArrays($needle,$haystack,$nextIndex)    // Function useful in finding the key from multidimentional arrays.
    {
        if($needle != "" && $haystack != "")
        {
            for($i=0;$i<count($haystack);$i++)
            {
                if($haystack[$i][$nextIndex] == $needle)
                    return $i;
            }
            return -1;
        }
    }
	
	// This function use for video url or embed code both change in embed code.
	function youtubeVideo($video){
	
		$filter = explode("/",$video);
		
		if($filter[3]=='embed'){
			$videoUrl = $video;
		}else{
			
			$allfilter = explode("&",$filter[3]);
		
			$againFilter = explode("=",$allfilter[0]);
			$videoUrl = "https://www.youtube.com/embed/$againFilter[1]";
		}
		return $videoUrl;
	}
	
	// This function use for calculate ago days.
	function agoFun($timeVal){
		$postdate = $timeVal;
		$currentdate = time();
		$interval = $currentdate - $postdate;
		$interval = floor($interval / (60 * 60 * 24));
			if ($interval == 0){
				$interval = 'posted&nbsp' . $interval . ' days ago';
			}else{
				$interval = 'posted&nbsp' . $interval . ' days ago';
			}
		return $interval;
	}	
	//-----------send email------
	
	function sendEmail($to,$subject,$content)
	{
		mail($to,$subject,$content);
	}
	//---------get video thumnail------------------
	function get_video_thumnail($id,$video)
	{		
	
		$MissionObj = new Mission() ;
		$user_id = $_SESSION['AD_user_id'];
		
		if (preg_match('/<iframe.*src=\"(.*)\".*><\/iframe>/isU', $video, $match)){			
			$exp = explode('/', $match[1]);
			$Video_id = explode('?', $exp[4]);
		}		
		if (empty($Video_id[0]) || $Video_id[0] == "video") {
			
				$tmp_vid_array = explode("//", $video);
				

				$tmp_vids_array = explode("/", $tmp_vid_array[1]);

				if ($tmp_vids_array[0] == "youtu.be") {

					if ($tmp_vids_array[1] == 'bydefault') {
						// die('yes i am here');
						$image_video = IMAGE_URL . '/homeurl.png';
					} else {
						$image_video = "http://img.youtube.com/vi/" . $tmp_vids_array[1] . "/0.jpg";
					}

				}
				if ($tmp_vids_array[0] == "www.youtube.com") {
					
					$tmp_v = str_replace("watch?v=", "", $tmp_vids_array[1]);
				
					$image_video = "http://img.youtube.com/vi/" . $tmp_v . "/0.jpg";
					
					$update_v_url = "http://youtu.be/" . $tmp_v;
					$condition = "user_id=$user_id and mission_level='3' and id=" . $id;
					$data_array = array("video" => $update_v_url);
					$MissionObj -> Updatesharevideolink($data_array, $condition);
				}
				if ($tmp_vids_array[0] == "dai.ly") {

					$image_video = "http://www.dailymotion.com/thumbnail/video/" . $tmp_vids_array[1];
				}
				if ($exp[2] == "www.dailymotion.com") {
					$image_video = "http://www.dailymotion.com/thumbnail/video/" . $exp[5];
				}
				

			} else {
			
				$image_video = "http://img.youtube.com/vi/" . $Video_id[0] . "/0.jpg";
			}
				//echo $image_video;
			return $image_video;
	}
	
	function play_youtube_video($video)
	{
		
			if (!empty($match[1])) { echo $match[1];
				} else {
					$tmp_video = explode("/", $video);
					switch($tmp_video[2]) {
						case "youtu.be" :
							$center_val = "youtube";
							break;
						case "dai.ly" :
							$center_val = "dailymotion";
							$mid_val = "video/";
							break;
					}
					$video_url = end($tmp_video);

					if ($video_url == 'bydefault') {
						$video_url =  '//fast.wistia.net/embed/iframe/ijqy2uuxzv';
					} else {

						$video_url =  "//www." . $center_val . ".com/embed/" . $mid_val . $video_url;
					}

				}
		
		echo '<iframe width="100%" height="315" src="'.$video_url.'" frameborder="0" allowfullscreen></iframe>';
	}
	
	function get_presnatage($percentage,$totalWidth)
	{
		echo $new_width = ($percentage / 100) * $totalWidth;
	}
	
	//--------------------------------
function parseVideos($videoString = null)
{
    // return data
    $videos = array();

    if (!empty($videoString)) {

        // split on line breaks
        $videoString = stripslashes(trim($videoString));
        $videoString = explode("\n", $videoString);
        $videoString = array_filter($videoString, 'trim');

        // check each video for proper formatting
        foreach ($videoString as $video) {

            // check for iframe to get the video url
            if (strpos($video, 'iframe') !== FALSE) {
                // retrieve the video url
                $anchorRegex = '/src="(.*)?"/isU';
                $results = array();
                if (preg_match($anchorRegex, $video, $results)) {
                    $link = trim($results[1]);
                }
            } else {
                // we already have a url
                $link = $video;
            }

            // if we have a URL, parse it down
            if (!empty($link)) {

                // initial values
                $video_id = NULL;
                $videoIdRegex = NULL;
                $results = array();

                // check for type of youtube link
                if (strpos($link, 'youtu') !== FALSE) {
					
					
                    if (strpos($link, 'youtube.com') !== FALSE) {
						
						if(strpos($link, 'watch') !== FALSE){	
							
							$filter = explode("/",$link);

							if($filter[3]=='embed'){
								$videoUrl = $video;
							}else{
								
								$allfilter = explode("&",$filter[3]);
								$againFilter = explode("=",$allfilter[0]);
								//$videoUrl = "https://www.youtube.com/embed/$againFilter[1]";
								$videoIdRegex = '/youtube.com\/(?:embed|v){1}\/([a-zA-Z0-9_]+)\??/i';
								$link = 'https://www.youtube.com/embed/'.$againFilter[1];
							}
							
						}else{
							
						// works on:
                        // http://www.youtube.com/embed/VIDEOID
                        // http://www.youtube.com/embed/VIDEOID?modestbranding=1&amp;rel=0
                        // http://www.youtube.com/v/VIDEO-ID?fs=1&amp;hl=en_US
                        $videoIdRegex = '/youtube.com\/(?:embed|v){1}\/([a-zA-Z0-9_]+)\??/i';
						}
						
                        
						
						//echo $videoIdRegex;
						
						
                    } else if (strpos($link, 'youtu.be') !== FALSE) {
                        // works on:
                        // http://youtu.be/daro6K6mym8
                        $videoIdRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
                    }
				
                    if ($videoIdRegex !== NULL) {
                        if (preg_match($videoIdRegex, $link, $results)) {
                            $video_str = 'http://www.youtube.com/embed/%s?autohide=1&showinfo=0&controls=0';
                            $thumbnail_str = 'http://img.youtube.com/vi/%s/2.jpg';
                            $fullsize_str = 'http://img.youtube.com/vi/%s/0.jpg';
                            $video_id = $results[1];
                        }
                    }
                }

                // handle vimeo videos
                else if (strpos($video, 'vimeo') !== FALSE) {
                    if (strpos($video, 'player.vimeo.com') !== FALSE) {
                        // works on:
                        // http://player.vimeo.com/video/37985580?title=0&amp;byline=0&amp;portrait=0
                        $videoIdRegex = '/player.vimeo.com\/video\/([0-9]+)\??/i';
                    } else if (strpos($video, 'channels') !== FALSE) {
						
						$videoDetail = explode('/',$video);
						$videoIdRegex = '/vimeo.com\/([0-9]+)\??/i';
						$link = 'https://vimeo.com/'.end($videoDetail);
						 
					} else {
                        // works on:
                        // http://vimeo.com/37985580
                        $videoIdRegex = '/vimeo.com\/([0-9]+)\??/i';
                    }

					//echo '<br/>Last : '.$videoIdRegex; 
					
                    if ($videoIdRegex !== NULL) {
                        if (preg_match($videoIdRegex, $link, $results)) {
                            $video_id = $results[1];

                            // get the thumbnail
                            try {
                                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_id.php"));
                                if (!empty($hash) && is_array($hash)) {
                                    $video_str = 'https://player.vimeo.com/video/%s';
                                    $thumbnail_str = $hash[0]['thumbnail_small'];
                                    $fullsize_str = $hash[0]['thumbnail_large'];
                                } else {
                                    // don't use, couldn't find what we need
                                    unset($video_id);
                                }
                            } catch (Exception $e) {
                                unset($video_id);
                            }
                        }
                    }
                }

			//	echo '<br/> Video: '.$video_id;
                // check if we have a video id, if so, add the video metadata
                if (!empty($video_id)) {
                    // add to return
                    $videos[] = array(
                        'url' => sprintf($video_str, $video_id),
                        'thumbnail' => sprintf($thumbnail_str, $video_id),
                        'fullsize' => sprintf($fullsize_str, $video_id)
                    );
                }
            }

        }

    }

	//echo "<pre>";
	//print_r($videos);
    // return array of parsed videos
   return $videos;
}
	
//---------------------------

function display_tree($local_root,$totalcount)
{
	$objUser2 = new user();
	if($local_root=='0') 
	{
	  return $totalcount;
	}
		
	$condition = " id = '".$local_root."' ";
	$result = $objUser2->SelectAll_User($condition); 
	//echo '<br/>'.$totalcount;

	$totalcount++;
	return display_tree($result['real_enroller'],$totalcount);
} 	
	
function hyphenize($string) {
    $dict = array(
        "I'm"      => "I am",
        "thier"    => "their",
    );
    return strtolower(
        preg_replace(
          array( '#[\\s-]+#', '#[^A-Za-z0-9\. -]+#' ),
          array( '-', '' ),
          // the full cleanString() can be download from http://www.unexpectedit.com/php/php-clean-string-of-utf8-chars-convert-to-similar-ascii-char
          cleanString(
              str_replace( // preg_replace to support more complicated replacements
                  array_keys($dict),
                  array_values($dict),
                  urldecode($string)
              )
          )
        )
    );
}

function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
        '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}
	
?>