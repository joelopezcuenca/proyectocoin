<?php

	/*==================================================================*\
	#   Coder: Manish Sonwal (m_sonwal@yahoo.co.in)
		Date : 7 May 2012                                              
	\*==================================================================*/
	# ----------------------------------------------------------------------------------------------------
	# SITE TITLE
	# ----------------------------------------------------------------------------------------------------
	define('MAIN_TITLE', "Coin"); 
	# ----------------------------------------------------------------------------------------------------
	# DATE FORMAT - SET JUST ONE FORMAT
	# Y - numeric representation of a year with 4 digits (xxxx)
	# m - numeric representation of a month with 2 digits (01 - 12)
	# d - numeric representation of a day of the month with 2 digits (01 - 31)
	# ----------------------------------------------------------------------------------------------------
	define('DEFAULT_DATE_FORMAT', "d/m/Y");
	# ----------------------------------------------------------------------------------------------------
	# FRIENDLY URL CONSTANTS
	# IMPORTANT - PAY ATTENTION
	# Any changes here need to be done in all .htaccess (modrewrite)
	# ----------------------------------------------------------------------------------------------------
	//define(FRIENDLYURL_SEPARATOR, "_");
	//define(FRIENDLYURL_VALIDCHARS, "a-zA-Z0-9");
	//define(FRIENDLYURL_REGULAREXPRESSION, "^[".FRIENDLYURL_VALIDCHARS.FRIENDLYURL_SEPARATOR."]{1,}$");
	# ----------------------------------------------------------------------------------------------------
    # DEFAULT HASH ALGORITHM
    # ----------------------------------------------------------------------------------------------------
    define('DEFAULT_HASH_ALGO', "sha512");
	# ----------------------------------------------------------------------------------------------------
	# IMAGE FOLDER CONSTANTS
	# ----------------------------------------------------------------------------------------------------
	define('IMAGE_RELATIVE_PATH', "/images");
	define('IMAGE_ADMIN_PATH', "/img");
	define('IMAGE_ADMIN_PROFILE_PATH', "/profile_pic");
	define('IMAGE_ADMIN_BACKGROUND_PATH', "/background_pic");
	define('IMAGE_ADMIN_GOAL_IMG_PATH', "/goal_img");
	define('IMAGE_ACHIEVEMENT_IMG_PATH', "/achievement_img");
	define('IMAGE_DIR', SITE_ROOT.IMAGE_RELATIVE_PATH);
	define('IMAGE_URL', DEFAULT_URL.IMAGE_RELATIVE_PATH);
    define('ADMIN_IMAGE_URL', DEFAULT_URL.ADMIN_FOLDER.IMAGE_ADMIN_PATH);
    define('IMAGE_ADMIN_PROFILE_DIR', SITE_ROOT.IMAGE_ADMIN_PROFILE_PATH);
	 define('IMAGE_ADMIN_BACKGROUND_DIR', SITE_ROOT.IMAGE_ADMIN_BACKGROUND_PATH);
	define('IMAGE_ADMIN_PROFILE_URL', DEFAULT_URL.IMAGE_ADMIN_PROFILE_PATH);
	 define('IMAGE_ADMIN_BACKGROUND_URL', DEFAULT_URL.IMAGE_ADMIN_BACKGROUND_PATH);
	define('BROWSE_IMAGE_DIR', SITE_ROOT.BROWSE_IMAGE_RELATIVE_PATH);
	define('IMAGE_ADMIN_GOAL_DIR', SITE_ROOT.IMAGE_ADMIN_GOAL_IMG_PATH); 
	define('IMAGE_ADMIN_GOAL_URL', DEFAULT_URL.IMAGE_ADMIN_GOAL_IMG_PATH);
	define('IMAGE_ACHIEVEMENT_DIR', SITE_ROOT.IMAGE_ACHIEVEMENT_IMG_PATH); 
	define('IMAGE_ACHIEVEMENT_URL', DEFAULT_URL.IMAGE_ACHIEVEMENT_IMG_PATH);
	define('BROWSE_IMAGE_URL', DEFAULT_URL.BROWSE_IMAGE_RELATIVE_PATH); 
	# ----------------------------------------------------------------------------------------------------
	# CONSTANTS
	# ----------------------------------------------------------------------------------------------------
	define('INCLUDES_DIR',  SITE_ROOT."/includes");
	define('CLASSES_DIR',   SITE_ROOT."/classes");
	define('FUNCTIONS_DIR', SITE_ROOT."/functions");
    define('JS_PATH',       SITE_ROOT."/js");
	define('MODULES_PATH',  SITE_ROOT."/modules");
	define('ADMIN_PATH',       SITE_ROOT."/admin");
	define('CSS_PATH',         SITE_ROOT."/css");
	# ----------------------------------------------------------------------------------------------------
    # Global Seperator
    # ----------------------------------------------------------------------------------------------------
	define('GLOBAL_SEP','_$$||$$_');
    # ----------------------------------------------------------------------------------------------------
    # Global Variables
    # ----------------------------------------------------------------------------------------------------
    global $db_link;