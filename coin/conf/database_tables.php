<?php
/*#############################################################
 *#######Here we define constants for all Database tables.#####
 *#############################################################
 */
 
//Not Using Now but All Admin-User Credential Table information
define('TABLE_ADMIN', 'tbladmin');
//All User Table information
define('TABLE_USER', 'tbl_user');
//Commission table distribution
define('TABLE_COMMISSION', 'tbl_commision_report');
//Payment Record data
define('TABLE_PAYMENT', 'tbl_payment');
//pagemanager table info
define('TABLE_PAGE', 'tbl_pages');
define('TABLE_TEMP', 'tbl_temp');
//Faq related queries handling table
define('TABLE_LINKS', 'links_table');
//Payment status month wise record
define('MONTH_USER_STATUS', 'payment_record_monthstatus');
//Admin user Permission Security purposes
define('ADMIN_PERMISSION', 'admin_user_permission');
//For Mission managing table on level wise
define('MISSION', 'ourmission');
//For Payment setting from admin
define('AMOUNT_SET', 'payment_setting');
//For User general notification as a messages
define('USER_NOTIFICATION', 'tbl_user_notification');
//Creating a Notification table all admin notification as a message
define('ADMIN_NOTIFICATION', 'tbl_notification_admin');
//Creating a  post table all post will be stored here
define('POST_TABLE', 'tbl_user_posts');
//Commenting on particullar post table
define('POST_COMMENT_TABLE', 'tbl_post_comment');
//for following to someone table
define('TABLE_FOLLOW', 'tbl_follow');
//FUTURE USE:Table for storing all user password for future use
define('CONFIDENTIAL_INFORMATION', 'tbl_user_conf_information');
//FUTURE USE:Table for managing bucket list values for each user
define('BUCKETLIST_TABLE', 'tbl_bucketlist_entry');
//FUTURE USE:Table for managing bucket list values for each user
define('GOAL_IMG_TABLE', 'tbl_goal_images');
//FUTURE USE:Table for managing notifications for followers
define('FOLLOW_NOTIFICATION_TABLE', 'tbl_follow_notification');
//FUTURE USE:Table for managing notifications for achievements
define('BUCKET_NOTIFICATION_TABLE', 'tbl_bucketlist_notification');
//FUTURE USE:Table for managing notifications for pro users
define('PROUSER_NOTIFICATION_TABLE', 'tbl_prouser_notification');
define('BASICUSER_NOTIFICATION_TABLE', 'tbl_basicuser_notification');
define('BASICUSER_GETNOTIFICATION_TABLE', 'tbl_basicuser_getnotification');
//FUTURE USE:Table for managing discussions
define('DISCUSSION_TABLE', 'tbl_discussion');
//FUTURE USE:Table for managing notifications for achievements
define('COMMENTS_TABLE', 'tbl_disc_topic_comments');
//FUTURE USE:Table for managing notifications for achievements
define('DISCUSSION_TOPIC_TABLE', 'tbl_d_topic');
//FUTURE USE:Table for managing mission level for users
define('MISSION_FRONT', 'tbl_mission');
//FUTURE USE:Table for managing share idea module for missions unlocking
define('SHARE_IDEA', 'tbl_shareidea');
//FUTURE USE:Table for managing general discussions
define('G_DISCUSSION_TABLE', 'tbl_general_discussion');
//FUTURE USE:Table for managing general discussion topic comments
define('G_COMMENTS_TABLE', 'tbl_gen_disc_topic_comments');
//FUTURE USE:Table for managing notifications for achievements
define('G_DISCUSSION_TOPIC_TABLE', 'tbl_gen_d_topic');
define('BUCKETLIST_PARENT_CHILD', 'tbl_bucketlist_parent_child');
define('LEVEL_MAIL_STATUS', 'tbl_user_mail_status');
define('TBL_DEACTIVATION', 'tbl_deactivation_user');
define('CANCELELLED_PAYMENT_RECORDS', 'tbl_paymentCancelRecords');
define('ALL_NOTIFICATION', 'tbl_all_notification');
define('USERLEVEL_NOTIFICATION_TABLE', 'tbl_userlevel_notification');
define('GOLDEN_COIN_NOTIFICATION', 'tbl_goldencoin_notification');
define('LOST_COIN_NOTIFICATION', 'tbl_lostuser_notification');
define('USER_VERIFY_ACCOUNT', 'tbl_user_verification');
define('SITE_ADDITIONAL_SETTING', 'tbl_site_additional_setting');
define('LESSIONS_TABLE', 'tbl_lessions');
define('LESSIONS_VIDEO_TABLE', 'tbl_lession_video');
define('GOAL_CATEGORY_TABLE', 'tbl_goal_categories');
define('GOAL_LIKE_TABLE', 'tbl_goal_likes');
define('GOAL_COMMENT_TABLE', 'tbl_goal_comments');
define('TUTORIAL_TABLE', 'tbl_tutorials');
define('PROMOTION_BANNER_TABLE', 'tbl_promotion_banner');
define('TBL_CALCULATION', 'tbl_calculation');
define('TBL_USER_DETAIL', 'tbl_user_detail');
define('TBL_MONTHLY_PAID_STATUS', 'tbl_monthly_paid_status');
define('TBL_PROGRAM_ACCESS', 'tbl_program_access');
define('TBL_VAR_TUTORIAL_VIDEO', 'tbl_ver_tutorial_video');
define('TBL_CALCULATOR_PASSWORD', 'tbl_calculator_password');
define('TBL_USER_CALCULATOR_PASSWORD', 'tbl_user_calculator_password');
?>