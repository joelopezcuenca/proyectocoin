<?php
	@session_start();
	@define(SITE_ADMIN_EMAIL,"ankesh@internetbusinesssolutionsindia.com");
	//ini_set('date.timezone', 'Asia/Calcutta');
	ini_set('date.timezone', 'US/Eastern');
	ini_set('session.gc_maxlifetime', 3600);

	
	//date_default_timezone_set ('America/Los_Angeles');
	//date_default_timezone_set ("Asia/Calcutta");
	
	# ----------------------------------------------------------------------------------------------------
	# DATABASE CONNECTION PARAMETERS
	# ----------------------------------------------------------------------------------------------------
	define('PROGRAM_PASSWORD',  "123456");
	define('USER_COMSSION',  "80");
	define('COMPANY_COMSSION',  "20");
	define('ENROLLER_COMSSION',  "40");
	define('MAINDB_HOST',  "localhost");
	define('MAINDB_USER',  "videogar_devroot"); 
	define('MAINDB_PASS',  "pass@123");
	define('MAINDB_NAME',  "videogar_coindev");
	define('MAINDB_EMAIL', SITE_ADMIN_EMAIL);
	define('TRIAL_SUBSRIBER_AMOUNT', 5);
	define('SUBSRIBER_AMOUNT', 50);
	
	
	# ----------------------------------------------------------------------------------------------------
	# Enabling error reporting.
	# ----------------------------------------------------------------------------------------------------
	
	ini_set('display_errors',1);
 	error_reporting(E_ALL & ~E_NOTICE);
    set_time_limit(0);
    ini_set('memory_limit',-1);
	
	# ----------------------------------------------------------------------------------------------------
	# DEFINE GREEN FOLDER
	# ----------------------------------------------------------------------------------------------------
	define('ROOT_FOLDER', "/coin"); 
	
	// $paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL         
	//$paypal_url='https://www.paypal.com/cgi-bin/webscr';//Live 
	define('PAYMENT_URL', "https://www.paypal.com/cgi-bin/webscr");
	# ----------------------------------------------------------------------------------------------------
	# TMP FOLDER PATH DEFINITION
	# ----------------------------------------------------------------------------------------------------
	//define(TMP_FOLDER, "/tmp");
	# ----------------------------------------------------------------------------------------------------
	# DEFINE MODULE FOLDER
	# ----------------------------------------------------------------------------------------------------
	
	
	define('MODULE_FOLDER', "/modules");
	define('ADMIN_FOLDER', "/admin");
	
	# ----------------------------------------------------------------------------------------------------
	# DEFINE GREEN ROOT
	# ----------------------------------------------------------------------------------------------------
	
	$lastchar	= substr($_SERVER["DOCUMENT_ROOT"], -1, 1);
	/* 	if($lastchar=='/'){
			$rest = substr($_SERVER["DOCUMENT_ROOT"], 0, -1);  
			define('SITE_ROOT', $rest.ROOT_FOLDER);
		}else{
			define('SITE_ROOT', $_SERVER["DOCUMENT_ROOT"].ROOT_FOLDER);
		} */
	define('SITE_ROOT', "/home2/videogar/public_html/proyectocoin/coin");
	#----------------------------------------------------------------
	# admin root
	#----------------------------------------------------------------
	define('ADMIN_ROOT', SITE_ROOT."/admin");
	# ----------------------------------------------------------------------------------------------------
	# DEFAULT URL
	# ----------------------------------------------------------------------------------------------------
	if ((!$_SERVER["HTTPS"]) || ($_SERVER["HTTPS"] == "off")) {

		define('HTTPS_MODE', "off");
		define('DEFAULT_URL', "http://".$_SERVER["HTTP_HOST"].ROOT_FOLDER);
	} else {
		define('HTTPS_MODE', "on");
		define('DEFAULT_URL', "https://".$_SERVER["HTTP_HOST"].ROOT_FOLDER);
	}
	
	# ----------------------------------------------------------------------------------------------------
	# DEFINE HOST NAME
	# ----------------------------------------------------------------------------------------------------
	define('HOST_NAME', $_SERVER["HTTP_HOST"]);
	# ----------------------------------------------------------------------------------------------------
	# DEFINE MODULE URL
	# ----------------------------------------------------------------------------------------------------
	define('MODULE_URL', "http://".$_SERVER["HTTP_HOST"].ROOT_FOLDER.MODULE_FOLDER);
	# ----------------------------------------------------------------------------------------------------
	# DEFINE ADMIN URLs
	# ----------------------------------------------------------------------------------------------------
	define('ADMIN_URL', DEFAULT_URL."/admin");
	define('ADMIN_MODULE_URL', ADMIN_URL.MODULE_FOLDER);
	# ----------------------------------------------------------------------------------------------------
	define('MODULE_PATH',SITE_ROOT."/modules");
	define('INCLUDE_PATH', SITE_ROOT."/includes");
	
	define('ADMIN_MODULE_PATH',SITE_ROOT.ADMIN_FOLDER."/modules");
	define('ADMIN_INCLUDE_PATH', SITE_ROOT.ADMIN_FOLDER."/includes");
	# ----------------------------------------------------------------------------------------------------
	define('SECURE_URL', "https://".$_SERVER["HTTP_HOST"].GREEN_FOLDER);
	# ----------------------------------------------------------------------------------------------------
	# NON_SECURE_URL
	# ----------------------------------------------------------------------------------------------------
	define('NON_SECURE_URL', "http://".$_SERVER["HTTP_HOST"].ROOT_FOLDER);
	define('CSS_URL', DEFAULT_URL."/css");
	define('JS_URL', DEFAULT_URL."/js");
	define('IMAGE_URL', DEFAULT_URL."/images");
	define('IMAGE_GOAL_DIR', DEFAULT_URL."/goal_img");
	define('ADMIN_CSS_URL', DEFAULT_URL.ADMIN_FOLDER."/css");
	define('ADMIN_JS_URL', DEFAULT_URL.ADMIN_FOLDER."/js");
	define('ADMIN_IMAGE_URL', DEFAULT_URL.ADMIN_FOLDER."/images");
	define('FB_PATH',SITE_ROOT."/fb");
	define('FB_URL', DEFAULT_URL."/fb");
	define('USER_WALLPAPER', DEFAULT_URL."/user_upload/wallpaper");
	# ----------------------------------------------------------------------------------------------------
	# FACEBOOK_USAGE_CONSTANT_DEFINE
	# APP ID AND FACEBOOK SECRET
	# ----------------------------------------------------------------------------------------------------
	define('FB_APP_ID','864769350224382');
	define('FB_SECRET_ID','626767c75a6fba67771c9d27e8d9cb8a');
	#----------------------------------------------------Mail From Defination-------------------------------------------------
	define('CLIENT_FROM', "hola@proyectocoin.com") ;
	# ----------------------------------------------------------------------------------------------------
	# INCLUDE GLOBAL INCLUDES
	# ----------------------------------------------------------------------------------------------------
	include(SITE_ROOT."/conf/includes.inc.php");
