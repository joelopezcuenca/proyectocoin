<?php
/*
 * CODER:RAGHWENDRA
 * THIS CLASS CONTAINS ADMIN USER SECTION/ADMIN CONTROLLER
 */

class AdminUserController {
	public function AdminUserController() {//CONSTRUCTOR DEFINED HERE
	}

	/*
	 * FUNCTION FOR INSERTING ALL USER ACCOUNTS
	 */
	public function AdminUserInsert($dataArray) {
		global $db;
		$utilityObj = new utility();

		if (empty($dataArray))
			return;

		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_USER, $dataArray));
		return DB_insertIdFunc();

	}

	/*
	 * FUNCTION FOR SELECT ALL ADMIN USER ACCOUNTS
	 */
	public function selectAllAdminUser() {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$username = '';

		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "Admin_Role='Superadmin' or Admin_Role='admin'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	/*
	 * FUNCTION SELECT ALL ADMIN USER ACCOUNTS BY THEIR ID
	 */

	public function selectAllAdminUserById($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "id='$id'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	/*
	 * FUNCTION UPDATE  ADMIN USER ACCOUNTS BY THEIR ID
	 *
	 */

	public function UpdateAdminUserData($data, $condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		return $sql;
	}

	/*
	 * FUNCTION DELETE ADMIN USER ACCOUNTS BY THEIR ID
	 *
	 */
	public function DeleteAdminUserData($condition) {
		
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_USER, $condition));
		return $sql;

	}
	
	
	/*
	 * User Permission Granting for admin Users
	 * 
	 */ 
	 
	 public function ExistUserAdmin($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(ADMIN_PERMISSION, "$condition", ""));
		$result = DB_fetchAssocFunc($sql);
			//$mainArry[] = $result;
			return $result;

	}
	 
	 
	 public function AdminUserpermissionInsert($dataArray) {
		global $db;
		$utilityObj = new utility();

		if (empty($dataArray))
			return;

		$sql = $db -> query($utilityObj -> am_createInsertQuery(ADMIN_PERMISSION, $dataArray));
		return DB_insertIdFunc();

	}
	 
	 public function AdminUserUpdate($dataArray,$condition) {
		
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(ADMIN_PERMISSION, $dataArray, $condition));
		return $sql;

	}
	
}

/*
 * END OF CLASSS
 *
 */
?>