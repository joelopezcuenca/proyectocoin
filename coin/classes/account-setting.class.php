<?php
/*
 * Account General Setting Class
 * All Setting will be handled by here this class
 */
class accountsetting {
	public function accountsetting() {
	}

	/*
	 * Function for Get all Details of user after
	 * getting details i means using id
	 */
	public function GetDetailOnGivenCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;

	}

	/*
	 * Function Updates all details Account general Settings
	 * using their id
	 */

	function UpdateAccountSetting($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		return $sql;

	}

}
?>