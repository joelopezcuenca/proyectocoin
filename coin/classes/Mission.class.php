<?php
class Mission {
	public function Mission() {
	}

	/*
	 * Function For Inserting Mission All Data
	 */

	public function AdminMissionInsert($dataArray) {
		global $db;
		$utilityObj = new utility();

		if (empty($dataArray))
			return;

		$sql = $db -> query($utilityObj -> am_createInsertQuery(MISSION, $dataArray));
		return DB_insertIdFunc();

	}

	public function MissionLevelInsert($dataArray) {
		global $db;
		$utilityObj = new utility();

		if (empty($dataArray))
			return;

		$sql = $db -> query($utilityObj -> am_createInsertQuery(MISSION_FRONT, $dataArray));
		return DB_insertIdFunc();

	}

	/*
	 * Function For Seleceting All Data
	 */

	public function selectAllMission() {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$username = '';
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(MISSION, "", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	/*
	 * Function For Selecting Mission Data by their id
	 */

	public function selectAllMissionById($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(MISSION, "id=$id", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	/*
	 * Function For Delete Mission Module In Given Condition
	 */

	public function DeleteMissionById($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(MISSION, $condition));
		return $sql;

	}

	/*
	 * Function For Update Mission Module In Given Condition
	 */

	public function UpdatemissionById($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(MISSION, $data, $condition));
		return $sql;

	}

	/*
	 * For Front End Data Accessing I mean This Functions
	 * will be Used for Front ENd
	 */

	public function selectAllMissionType($type) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$username = '';
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(MISSION, "type='$type'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	public function getMissionDetail($condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(MISSION_FRONT, $condition, ""));
		$result = DB_fetchArrayFunc($sql);
		return $result;
	}

	public function selectMissionVideo($condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(MISSION, $condition, ""));
		$result = DB_fetchArrayFunc($sql);
		return $result;
	}

	public function selectParticularShareVideo($condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(SHARE_IDEA, $condition, ""));
		$result = DB_fetchArrayFunc($sql);
		return $result;
	}

	public function shareidea_for_mission($data) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(SHARE_IDEA, $data, ""));
		return DB_insertIdFunc();
	}

	public function Updatesharevideolink($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(SHARE_IDEA, $data, $condition));
		return $sql;

	}

	public function all_share_idea($condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(SHARE_IDEA, $condition, ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}
		return $mainArry;
	}

	public function getSharedIdeacondition($condition) {
		global $db;
		$utilityObj = new utility();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(SHARE_IDEA, $condition, ""));
		$result = DB_fetchArrayFunc($sql);
		return $result;
	}

}
?>