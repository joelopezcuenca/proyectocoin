<?php
class Additional {

	public function Additional() {

	}
	
	public function VideoDataInsert($dataArray) {

				global $db;
				$utilityObj = new utility();
				if (empty($dataArray))
				return;
				$sql = $db -> query($utilityObj -> am_createInsertQuery(SITE_ADDITIONAL_SETTING, $dataArray));
				return DB_insertIdFunc();
	}
		//Function to update an information of page update
	
	public function VideoDataUpdateById($data, $condition) 
	{
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj ->am_createUpdateQuery(SITE_ADDITIONAL_SETTING, $data, $condition));
		return $sql;
	}
	
	
	//function to select all records from table customer
	public function SelectAll_Video($condition) 
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(SITE_ADDITIONAL_SETTING, $condition, ""));
		while($result = DB_fetchAssocFunc($sql))
		{
			$mainArry []=$result;
		}
		return $mainArry;
		
	}
	
	//function to select all records from table additional setting
	public function get_aditional_record($condition) 
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(SITE_ADDITIONAL_SETTING, $condition, ""));
		while($result = DB_fetchAssocFunc($sql))
		{
			$mainArry []=$result;
		}
		return $mainArry;
		
	}
	
	//function to select all records from table additional setting
	public function get_aditional_detail($condition) 
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(SITE_ADDITIONAL_SETTING, $condition, ""));
		$mainArry = DB_fetchAssocFunc($sql);
		return $mainArry;
		
	}
	
	
	//function to select all records from get ver tutorial ---------
	
	public function get_var_tutorial_video($condition) 
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TBL_VAR_TUTORIAL_VIDEO, $condition, ""));
		$mainArry = DB_fetchAssocFunc($sql);
		return $mainArry;
		
	}
	
	public function DeleteCustomerById($condition) {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(SITE_ADDITIONAL_SETTING, $condition));
		return $sql;
	}
}