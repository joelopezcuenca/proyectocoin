<?php
class settings
{
    public function settings(){}
    
    /* For admin settings */
    public function getSettings($admin_id)
    { 
        global $db;
        $utilityObj = new utility();
        
        if($admin_id != "")
        {
            $adminDetails = $db->fetchNextAssoc($db->query($utilityObj->am_createSelectAllQuery(TABLE_USER,"id=".$admin_id)));
            return $adminDetails;
        }
    }
    
    public function updateSettings($admin_id,$dataArr)
    {
        global $db;
        $utilityObj = new utility();
        
        if(count($dataArr) && $admin_id != "")
        $db->query($utilityObj->am_createUpdateQuery(TABLE_USER,$dataArr,"id=".$admin_id));
    }

}

?>