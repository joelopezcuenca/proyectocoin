<?php
class tutoriale {

	public function tutoriale() {

	}
	public function TutorialeDataInsert($dataArray) {

				global $db;
				$utilityObj = new utility();
				if (empty($dataArray))
				return;
				$sql = $db -> query($utilityObj -> am_createInsertQuery(TUTORIAL_TABLE, $dataArray));
				return DB_insertIdFunc();
	}
		//Function to update an information of page update
	public function TutorialeDataUpdateById($data, $condition){
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj ->am_createUpdateQuery(TUTORIAL_TABLE, $data, $condition));
		return $sql;
	}
	
	
	//function to select all records from table customer
	public function SelectAll_Tutoriale($condition){
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TUTORIAL_TABLE, $condition, ""));
		while($result = DB_fetchAssocFunc($sql))
		{
			$mainArry []=$result;
		}
		return $mainArry;
		
	}

	public function DeleteCustomerById($condition) {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TUTORIAL_TABLE, $condition));
		return $sql;
	}	
	
}
?>