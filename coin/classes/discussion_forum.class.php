<?php 
 /*	
 * apar sharma(developer)+Raghwendra
 * this class is created to maintain Forum-Discussions Board	
 * All function as required      
 */
class discussion {
	function discussion() {
	}

	function select_discussion($condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(DISCUSSION_TABLE, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;
	}
	
	function select_G_discussion($condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(G_DISCUSSION_TABLE, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;
	}
	
// select topics of discussion
	function select_discussion_topic($condition) {
		global $db;
		$utilityObj = new utility();
		$orderby ="id asc" ;
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(DISCUSSION_TOPIC_TABLE, $condition ,$orderby));
		while ($rec = DB_fetchArrayFunc($sql)) {
			 $result[] = $rec;
		}
		return $result;
	}
	
	function select_G_discussion_topic($condition) {
		global $db;
		$utilityObj = new utility();
		$orderby ="id asc" ;
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(G_DISCUSSION_TOPIC_TABLE, $condition ,$orderby));
		while ($rec = DB_fetchArrayFunc($sql)) {
			 $result[] = $rec;
		}
		return $result;
	}
	
	
	// insert topics fir discussion
	function insertdiscussion_topic($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(DISCUSSION_TOPIC_TABLE, $dataArray));
		//print_r($sql);
		//die;
		return DB_insertIdFunc();
	}
	
	function insert_G_discussion_topic($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(G_DISCUSSION_TOPIC_TABLE, $dataArray));
		//print_r($sql);
		//die;
		return DB_insertIdFunc();
	}
	
	
	/*
	 * Function For Update discussion  Module In Given Condition
	 */
	
	 public function UpdatetopicById($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(DISCUSSION_TOPIC_TABLE, $data, $condition));
		return $sql;

	 }
	 // to delete particualr topic
	 public function DeleteDiscussionById($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(DISCUSSION_TOPIC_TABLE, $condition));
		return $sql;

	}

	function insertdiscussion_comment($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(COMMENTS_TABLE, $dataArray));
		//print_r($sql);
		//die;
		return DB_insertIdFunc();
	}
	
	function insert_G_discussion_comment($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(G_COMMENTS_TABLE, $dataArray));
		
		return DB_insertIdFunc();
	}
	

  /*	 * Select comment with user info by topic id  	 */
	function select_commentById($topic_id) {
		global $db;
		$result = array();
		$query = "select a.*,b.name from " . COMMENTS_TABLE . " as a join " . TABLE_USER . " as b on(a.user_id=b.id)   where a.topic_id=$topic_id order by a.entry_date DESC limit 0,10";
		$sql = DB_queryFunc($query);
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}
	
	function select_G_commentById($topic_id) {
		global $db;
		$result = array();
		$query = "select a.*,b.name from " . G_COMMENTS_TABLE . " as a join " . TABLE_USER . " as b on(a.user_id=b.id)   where a.topic_id=$topic_id order by a.entry_date DESC limit 0,10";
		$sql = DB_queryFunc($query);
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}
	
	
  
  //for deleting comments throgh ajax files functions
  public function delete_commentbyCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(COMMENTS_TABLE, $condition));
		return $sql;
	}
  
   public function delete__G_commentbyCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(G_COMMENTS_TABLE, $condition));
		return $sql;
	}
  
  //Categorized Data for Top Most header scetion on Disussion Forum
    public function search_most_recentData($orderby)
	{
		global $db;
		$utilityObj = new utility();
		//$orderby ="id desc" ;
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(DISCUSSION_TOPIC_TABLE, "" ,$orderby));
		while ($rec = DB_fetchArrayFunc($sql)) {
			 $result[] = $rec;
		}
		return $result;
		
		
	}
	
	 public function search_most__G_recentData($orderby)
	{
		global $db;
		$utilityObj = new utility();
		//$orderby ="id desc" ;
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(G_DISCUSSION_TOPIC_TABLE, "" ,$orderby));
		while ($rec = DB_fetchArrayFunc($sql)) {
			 $result[] = $rec;
		}
		return $result;
		
		
	}
	
	 public function select_most_commentedtopicid()
	 {
	 	global $db;
		$utilityObj = new utility();
		//SELECT count(id), topic_id FROM `tbl_disc_topic_comments` group by topic_id
		 $query = "select * from ".DISCUSSION_TOPIC_TABLE." as a INNER JOIN (SELECT count(id),topic_id FROM ".COMMENTS_TABLE." group by topic_id order by count(id) DESC) as b ON(a.id=b.topic_id)";
		//die;
		$sql = DB_queryFunc($query);
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	 }
	 
	  public function select_most__G_commentedtopicid()
	 {
	 	global $db;
		$utilityObj = new utility(); 
		//SELECT count(id), topic_id FROM `tbl_disc_topic_comments` group by topic_id
		 //$query = "select * from ".G_DISCUSSION_TOPIC_TABLE." as a INNER JOIN (SELECT count(id),topic_id FROM ".G_COMMENTS_TABLE." group by topic_id order by count(id) DESC) as b ON(a.id=b.topic_id)";
		 $query = "select a.*,count(b.id),b.topic_id from ".G_DISCUSSION_TOPIC_TABLE." as a JOIN ".G_COMMENTS_TABLE." as b ON a.id=b.topic_id group by b.topic_id order by count(b.id) DESC";
		//die;
		$sql = DB_queryFunc($query);
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	 }
	 
	 
	 function select_discussionagain($condition) {
	 	
		die;
		
		
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(DISCUSSION_TOPIC_TABLE, $condition, ""));
		//die;
		$result = DB_fetchAssocFunc($sql);
		return $result;
	}
}
?>