<?php
class tutorial{
	public function tutorial(){}
	
     
	public function AdminTutorialInsert($dataArray)
	{
		global $db;
		$utilityObj = new utility();
		
		if(empty($dataArray))
			return;
		
		$sql=$db->query($utilityObj->am_createInsertQuery(tutorial_data, $dataArray));
		return DB_insertIdFunc();

	}
	 
  //Admin Tutorial Module Sleceting Data

	public function selectAllTutorialData()
	{
		
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$username = '';
		//am_createSelectAllQuery($tblName,$whereCondition,$orderBy="")
		$sql=$db->query($utilityObj->am_createSelectAllQuery('tutorial_data',"",""));
		while($result=DB_fetchArrayFunc($sql))
		{
		$mainArry[] = $result;
			
	    }
		
		return $mainArry;
		
	}
	
	 //Admin Tutorial Module Sleceting Data For Editing

	public function selectAllTutorialDataById($id)
	{
		
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		//$username = '';
		//am_createSelectAllQuery($tblName,$whereCondition,$orderBy="")
		 $sql=$db->query($utilityObj->am_createSelectAllQuery('tutorial_data',"id=$id",""));
		
		while($result=DB_fetchArrayFunc($sql))
		{
		$mainArry[] = 	$result;
			
	    }
		
		return $mainArry;
		
	}
	
	
    //Admin Tutorial Module Deleting  Data
    
	public function deletetutorialOnGivenCondition($condition)
	{
		
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql=$db->query($utilityObj->am_createDeleteAllQuery(tutorial_data,$condition));
		return $sql;
		//echo "<pre>"; print_r($mainArry); echo "</pre>"; die;
	}


 //Admin Tutorial Module Updating  Data
		
	public function updateTutorialById($data,$condition)
	{
		
		global $db;
		$utilityObj = new utility();
		$sql=$db->query($utilityObj->am_createUpdateQuery(tutorial_data,$data,$condition));
		
		//$result=DB_queryFunc($sql);
		return $sql;
		
	}
	//Front Tutorial Module Selecting  Data
	
	public function selectAllFrontTutorialData($type)
	{
		
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$username = '';
		//am_createSelectAllQuery($tblName,"$whereCondition",$orderBy="")
		$sql=$db->query($utilityObj->am_createSelectAllQuery('tutorial_data',"type='$type'",""));
		while($result=DB_fetchArrayFunc($sql))
		{
		$mainArry[] = $result;
			
	    }
		
		return $mainArry;
		
	}

	 
}	