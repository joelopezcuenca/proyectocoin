<?php
class register {
	public function register() {
	}

	public function InsertRegisterQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_USER, $dataArray));
		//$result=DB_queryFunc($sql);

		return DB_insertIdFunc();

	}

	public function updatePasswordAweber($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		//$result=DB_queryFunc($sql);
		return $sql;

	}

	public function updatePasswordRegistration($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(tutorial_data, $data, $condition));
		//$result=DB_queryFunc($sql);
		return $sql;

	}

	public function updatePhoto($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		//$result=DB_queryFunc($sql);
		return $sql;

	}

	public function InsertAweberInsertQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_USER, $dataArray));
		//$result=DB_queryFunc($sql);

		return DB_insertIdFunc();

	}

	public function selectAllData() {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$email = '';
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "email='$email'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;
		//echo "<pre>"; print_r($mainArry); echo "</pre>"; die;
	}

	public function getOnId($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "id = '$id' and username!=''", ""));
		$result = DB_fetchAssocFunc($sql);

		return $result;
		
	}

	public function getOnstatus() {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "status = '1' and payment_status= 'complete'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;
		
	}

	public function getOnGivenCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);

		//return $result;
		return count($result);
		//echo "<pre>"; print_r($result); echo "</pre>"; die;
	}

	public function getOnGivenusername($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);

		return $result;
		//echo "<pre>"; print_r($result); echo "</pre>"; die;
	}

	public function deleteOnGivenCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_USER, $condition));

		return $sql;
		//echo "<pre>"; print_r($mainArry); echo "</pre>"; die;
	}

	public function updateRegisterById($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));

		//$result=DB_queryFunc($sql);
		return $sql;

	}

	public function getOnEnroller($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "id = '$id'", ""));
		$result = DB_fetchAssocFunc($sql);

		return $result['name'];
		//echo "<pre>"; print_r($result); echo "</pre>"; die;
	}

	public function checkEmail($uid, $email) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "email = '$email' and id != $uid", ""));
		$result = DB_fetchAssocFunc($sql);

		//return $result;
		echo "<pre>";
		print_r($result);
		echo "</pre>";
		die ;
	}

	public function InsertPaymentQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_PAYMENT, $dataArray));
		//$result=DB_queryFunc($sql);

		return DB_insertIdFunc();

	}

	public function InsertCommissionQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_COMMISSION, $dataArray));
		//$result=DB_queryFunc($sql);

		return DB_insertIdFunc();

	}

	public function selectAllCommissionData() {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_COMMISSION, "", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;
		//echo "<pre>"; print_r($mainArry); echo "</pre>"; die;
	}

	public function getCommissionOnId($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_COMMISSION, "id = '$id'", ""));
		$result = DB_fetchAssocFunc($sql);

		return $result;
		//echo "<pre>"; print_r($mainArry); echo "</pre>"; die;
	}

	public function InsertForumQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery('phpbb_users', $dataArray));
		//$result=DB_queryFunc($sql);

		return DB_insertIdFunc();

	}

	public function InsertTempQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_TEMP, $dataArray));
		//$result=DB_queryFunc($sql);

		return DB_insertIdFunc();

	}

	//RESET PASSWORD IF FORGOT IN LOGIN PAGE
	function updatePasswordByEmail($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		return $sql;

	}

	function updateLinksByUserId($data, $condition) {

		//echo $condition;die;
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_LINKS, $data, $condition));
		// print_r($sql);
		// die;

		return $sql;

	}

	public function Insertlinks($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_LINKS, $dataArray));
		//$result=DB_queryFunc($sql);

		return DB_insertIdFunc();

	}

	public function selectAlllinks($userid) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		//$username = '';
		//am_createSelectAllQuery($tblName,$whereCondition,$orderBy="")
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_LINKS, "user_id=$userid", ""));

		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	public function getId($userid) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_LINKS, "user_id= '$userid'", ""));
		$result = DB_fetchAssocFunc($sql);

		return $result;
		//echo "<pre>"; print_r($mainArry); echo "</pre>"; die;
	}

	public function deleteLinksOnGivenCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_LINKS, $condition));

		return $sql;
		//echo "<pre>"; print_r($mainArry); echo "</pre>"; die;
	}

	//Select all according to selection of Pro/Basic

	public function selectAllEmailData() {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$email = '';
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	public function AllUserType($userstatus) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "status='$userstatus'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}
		return $mainArry;
	}

}
?>