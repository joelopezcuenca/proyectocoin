<?php
/***********************************************************
Developed By : Manish Sonwal
Purpose: General utility for generating the Sql Query(insert,update & select).
Creation Date : August 01 2012
/***********************************************************/

class utility
{
	/*********** DATABASE QUERY RELATED FUNCTIONS *******************
	****************************************************************/
	/**
     * Returns the INSERT query string. 
     * NOTE: This function creates insert query from tablename and data array 
     * using mysql_real_escape_string(), get_magic_quotes_gpc(), implode(),
     * array_values() and array_keys().
     *
     * @param tblname string and dataArray array
     *
     * @access public
     *
     * @return insert query string
     */
     
     
    function am_createInsertQuery($tblName, $dataArray)
    {
		$sqlQuery = "";
		if(!empty($tblName) && !empty($dataArray) && is_array($dataArray)) {
	    	if (get_magic_quotes_gpc()) {
		        foreach ($dataArray as $key => $val) {
		    		if (!is_numeric($val)) {
				        $val = stripslashes(trim($val));
		    			$val = DB_realExcapeString($val);
		    			$dataArray[$key] = $val;
				    }
		        }
		    } else {
		    	foreach ($dataArray as $key => $val) {
		    		if (!is_numeric($val)) {
				        $val = DB_realExcapeString(trim($val));
		    			$dataArray[$key] = $val;
				    }
		        }
		    }
			
			$sqlQuery = "INSERT INTO $tblName ";
			$sqlQuery .= "(`". implode("`, `",array_keys($dataArray)) . '`)';
			$sqlQuery .= " VALUES ('". implode("', '",array_values($dataArray)) . "')";
		}
		return $sqlQuery;
		
	} // end func am_createInsertQuery
	
	/**
     * Returns the UPDATE query string. 
     * NOTE: This function creates update query from tablename and data array 
     * using mysql_real_escape_string(), get_magic_quotes_gpc()
     *
     * @param tblname string, whereCondition string and dataArray array
     *
     * @access public
     *
     * @return update query string
     */
    
    function am_createUpdateQuery($tblName, $dataArray, $whereCondition)
    {
        $sqlQuery = "";
        if(!empty($tblName) && !empty($dataArray) && is_array($dataArray) && !empty($whereCondition)) {
        	 $sqlQuery = "UPDATE $tblName SET"; 
        
        if (get_magic_quotes_gpc()) {
            foreach ($dataArray as $key => $val) {
        		if (!is_numeric($val)) {
        	        $val = stripslashes($val);
        			$val = DB_realExcapeString($val);
        		}
        	     $sqlQuery .= " `" . $key . "` = '". $val . "',";    	
            }
        } else {
        	foreach ($dataArray as $key => $val) {
        		if (!is_numeric($val)) {
        	        $val = DB_realExcapeString($val);
        		}
        	    $sqlQuery .= " `" . $key . "` = '". $val . "',";    	
            }
        }
        $sqlQuery = substr($sqlQuery,0,-1); // to remove last comma
        $sqlQuery .= " WHERE " . $whereCondition; 
        }
        return $sqlQuery;
	} // end func am_createUpdateQuery
	
	/**
     * Returns the SELECT ALL query string. 
     * NOTE: This function creates Select query from tablename 
     *
     * @access public
     *
     * @return select All query string
     */
     
     
    function am_createSelectAllQuery($tblName,$whereCondition,$orderBy="")
    {
        $sqlQuery = "";
        if(!empty($tblName)) {
            $sqlQuery = "SELECT * FROM  $tblName";
            if(!empty($whereCondition))
                $sqlQuery .= " WHERE " . $whereCondition; 
            if(!empty($orderBy))
                $sqlQuery .= " ORDER BY " . $orderBy;	
        }
        return $sqlQuery;
	} // end func am_createSelectAllQuery
	
	
    function am_createDeleteAllQuery($tblName,$whereCondition)
    { 
		$sqlQuery = "";
		if(!empty($tblName)) {
	    	$sqlQuery = "DELETE FROM  $tblName";
	    if(!empty($whereCondition))
		    $sqlQuery .= " WHERE " . $whereCondition;
		}
		return $sqlQuery;
	}
	
	/* Create hash (algo: sha512) from string. */

	/* Added By: Manish Sonwal. Date: 15-3-2012 */

	function createHash($string)

	{

		if($string != "")

		{

			return hash("sha512", $string);

		}

	}
	
	//------this function related to uplaod on site. it's accepect two perameter on file array and second uplaod folder path
	function upload_file($path,$file,$file_temp_name,$user_id)
	{
		
		$target_dir = SITE_ROOT."/".$path;
		 $target_file = $target_dir."/".$file;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		$filename_arr = explode('.'.$imageFileType, $file);
		$new_file_name = strtolower(str_replace(" ", '_', $filename_arr[0])).'_'.$user_id.'.'.$imageFileType;
		$target_file = $target_dir."/".$new_file_name;
		

		// Check if image file is a actual image or fake image
		    $check = getimagesize($file_temp_name);
		    if($check !== false) {
		        $error =  "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        $error =  "File is not an image.";
		        $uploadOk = 0;
		    }
		
		// Check file size
		if ($_FILES["image"]["size"] > 5000000) {
		    $error =  "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if(strtolower($imageFileType) != "jpg" && strtolower($imageFileType) != "png" && strtolower($imageFileType) != "jpeg"
		&& strtolower($imageFileType) != "gif" ) {
		    $error =  "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    $error =  $error;
		// if everything is ok, try to upload file
		} else {

		    if (move_uploaded_file($file_temp_name, $target_file)) {
		        $error =  "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
		    } else {
		        $error =  "Sorry, there was an error uploading your file.";
		    }
		}
		
		$return_arr['image_name'] = $new_file_name;
		$return_arr['upload_status'] = $uploadOk;
		$return_arr['upload_error'] = $error;
		
		return $return_arr;
	}

//---------get default user wallpaper------

	function get_default_admin_seetings($condition)
	{
		global $db;
		$utilityObj = new utility();
	
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(SITE_ADDITIONAL_SETTING, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		if(count($result) > 0) {
			return $result['option_value'] ; 
		}
	} 
	
	
	function get_program_password($condition)
	{
		global $db;
		$utilityObj = new utility();
	
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TBL_CALCULATOR_PASSWORD, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		
		return $result;
	} 
	
	function programPassword_Update($data, $condition) {
		global $db;
		$utilityObj = new utility();		
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TBL_CALCULATOR_PASSWORD, $data, $condition));
		return $sql;
	}
	

	
	
	 // end func am_createSelectAllQuery
}   // end class utility

?>