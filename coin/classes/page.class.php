<?php

class page {

	public function page() {

	}
	public function PageDataInsert($dataArray) {

				global $db;
				$utilityObj = new utility();
				if (empty($dataArray))
				return;
				$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_PAGE, $dataArray));
				return DB_insertIdFunc();
	}
		//Function to update an information of page update
	public function PageDataUpdateById($data, $condition) 
	{
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj ->am_createUpdateQuery(TABLE_PAGE, $data, $condition));
		return $sql;
	}
	
	
	//function to select all records from table customer
	public function SelectAll_Page($condition) 
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_PAGE, $condition, ""));
		while($result = DB_fetchAssocFunc($sql))
		{
			$mainArry []=$result;
		}
		return $mainArry;
		
	}
	
	public function Select_selected_Page($condition) 
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_PAGE, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		
		return $result;
		
	}
	
	
	
	public function DeleteCustomerById($condition) {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_PAGE, $condition));
		return $sql;
	}
}
?>