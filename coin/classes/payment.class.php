<?php
class payment {
	public function payment() {
	}

	public function InsertPaymentQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(MONTH_USER_STATUS, $dataArray));
		return DB_insertIdFunc();

	}
	
	public function InsertCommissionQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_COMMISSION, $dataArray));
		return DB_insertIdFunc();

	}
	
	public function InsertDeactivationDetails($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TBL_DEACTIVATION, $dataArray));
		return DB_insertIdFunc();

	}

	public function updatePayment($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_PAYMENT, $data, $condition));
		return $sql;

	}

	

	public function getstatusOnGivenCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(MONTH_USER_STATUS, $condition, ""));

		$result = DB_fetchAssocFunc($sql);
		return count($result);
		
	}
	
	public function getDeactivationstatus($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TBL_DEACTIVATION, $condition, ""));

		$result = DB_fetchAssocFunc($sql);
		return $result ;
		
	}

	public function getOnGivendetailname($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(MONTH_USER_STATUS, $condition, ""));

		$result = DB_fetchAssocFunc($sql);
		return $result;
		
	}

	public function getemailOnGivenconditon($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;

	}

	/* AMOUNT SETTING FROM ADMIN MODULE
	 * Function Contain for Amount Setting By Admin From Payment Edit Module
	 * this class also will be used as amount updation from there
	 * 
	 */
	 
	 public function SelectPaymentSettingAmt() {
	 	
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(AMOUNT_SET, "", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}
	  public function SelectPaymentSettingAmtonCondition($condition) {
	 	
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(AMOUNT_SET, $condition, ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}
	  public function Delete_deactivate_details($condition)
	 {
	 	global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TBL_DEACTIVATION, $condition));
		return $sql;
		
	
	 }
	 
	 /* 
	  * Update Amount setting being posted From Admin Module
	 */
	 
	 public function UpdateAmountSetting($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(AMOUNT_SET, $data, $condition));
		return $sql;

	 }
	 
	  public function Get_Payment_Details($condition) {
	 	
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_COMMISSION, "$condition", ""));
		
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}
		
		//print_r($mainArry);

		return $mainArry;

	}
	  
	   public function Get_Payment_Details_monthwise($condition) {
	 	
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_PAYMENT, $condition, ""));
		
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;
		}
		
		

		return $mainArry;

	}
	
	  public function Get_Payment_Details_monthwise_total($condition) {
	 	
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query("SELECT sum(amount) as total_income,month(payment_date) as month FROM ".TABLE_PAYMENT." p, ".TABLE_USER." u where p.user_id=u.id and ".$condition);
		
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;
		}
		
		

		return $mainArry;

	}
	  
	   public function Check_Duplicate_transactionId($condition) {
	 	
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_PAYMENT, "$condition", ""));
		$result = DB_numRowsFunc($sql);
		return $result;
		

	}
	  
	  
	  public function InsertPaymentUserDetail($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_PAYMENT, $dataArray));
		return DB_insertIdFunc();

	}
	  
		public function getOnId($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_PAYMENT, "user_id =$id",""));
		$result = DB_fetchAssocFunc($sql);

		return $result;
		
	}  
	  
	  public function InsertCancelPaymentQuery($dataArray) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createInsertQuery(CANCELELLED_PAYMENT_RECORDS, $dataArray));
		return DB_insertIdFunc();

	}
	  
	  
	  public function GetNextPaymentStatus($condition) {
			global $db;
		    $utilityObj = new utility();
		    $sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_PAYMENT, "$condition",""));
		    $result = DB_fetchAssocFunc($sql);
		    return $result;
		
	}  
	  
	  //the User Who is Pro active User From table main tbl_user
	  public function GetAllActiveProMember($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}
		
		return $mainArry;

	}
	
	//------------Caclulation --------------------------
	public function get_total_system_fix_income(){
		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
	}
	
	public function get_diect_admin_income()
	{		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.enroller='0' and u.real_enroller='0'  ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	
	public function get_admin_enroller_income()
	{		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.enroller='0' and u.real_enroller!='0'  ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	
	public function get_user_income()
	{		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.enroller!='0' ");
		
		
		
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	
	
	public function get_user_enroller_income()
	{		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.real_enroller!=0 and u.enroller!=u.real_enroller and u.enroller=0 ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		
		
		return $mainArry;
		
	}
	
	
	
	
	
	
	
	
	//----------Get total comssion-------------
	
	public function get_total_system_comssion_enroller()
	{
		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.enroller!='0' and u.real_enroller!='0'  ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
			
	}
	
	//------------
	//----------Get total comssion-------------
	
	public function get_total_system_comssion_real_enroller()
	{
		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.real_enroller!='0' and u.real_enroller!=u.enroller group by u.enroller");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
			
	}
	
	public function get_total_company_profit()
	{
		
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.real_enroller='0' and u.enroller='0' ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
			
	}
	
	
	//--------------------------------------------------------------
	public function get_total_system_income_alluser()
	{	
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id  ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	
	public function get_total_coin_income_alluser()
	{	
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id  and (u.enroller='0' || u.real_enroller='0' )");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	
	public function get_total_system_income()
	{	
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.enroller!='0' ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	
	public function get_total_admin_powor_line_income()
	{	
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as total_power_income from tbl_payment p, tbl_user u where p.user_id=u.id and u.enroller='0' and u.real_enroller!='0' ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	
	public function get_total_admin_income()
	{	
		global $db;
		$utilityObj = new utility();
		$userObj = new user();
		$mainArry = array();
		$sql = $db -> query("select sum(amount) as admin_totalincome from tbl_payment p, tbl_user u where p.user_id=u.id and u.enroller='0' and enroller=real_enroller ");
		$mainArry = DB_fetchArrayFunc($sql); 
		
		return $mainArry;
		
	}
	 

}
?>