<?php
class notification {
	function notification() {
	}

	//function to create an entry for user notification in database
	function NotificationDataInsert($dataArray) {
		global $db;
		$utilityObj = new utility();
		$sql = $db->query($utilityObj->am_createInsertQuery(ALL_NOTIFICATION,$dataArray));
		return DB_insertIdFunc();
	}
	
	//to get all notification
	public function AllNotification($condition) {
		
         global $db;
		 $utilityObj = new utility();
		 $mainArry = array();
		 $sql = $db -> query($utilityObj -> am_createSelectAllQuery(ALL_NOTIFICATION, "$condition", ""));
		 while ($result = DB_fetchArrayFunc($sql)) {
		 $mainArry[] = $result;

		}
		return $mainArry;

	}

}
?>