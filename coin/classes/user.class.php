<?php  ##### Coder:Raghwendra ####
class user {
		
	public function user() {
		
	}

	var $compulsoryFieldsError = false;
	var $validEmailError = false;
	var $duplicateEmail = false;
	var $duplicateUserName = false;
	var $CompulsoryAmountAchieve = false;
	var $CompulsoryMonthField = false;
	var $CompulsoryUserNameField = false;
	var $CompulsoryUserPasswordField = false;
	var $AmountAchieveInt = false;
	var $TargetMonthInt = false;
	var $imageTypeCheck = false;
	
	/*
	 * Valudation for checking Authentication
	 * for user registering Email or other format
	 */
	function validateRegisterForm() {

		global $dbObj;

		if (trim($_POST['email']) == "") {

			$this -> compulsoryFieldsError = true;

		} else {

			// Assign post values to class varibales
			$this -> email = addslashes(nl2br(trim($_POST['emailId'])));
			//	$this -> isValidEmail();

		}

	}

	//check whether Targeted Amount is Entered or left blank
	function ValidateTargetAmount() {

		if (trim($_POST['targetamount']) == "") {

			$this -> CompulsoryAmountAchieve = true;
		}
		$amount = trim($_POST['targetamount']);
		if (!is_numeric($amount)) {

			$this -> AmountAchieveInt = true;
		}

		if (trim($_POST['targetmonth']) == "") {
			$this -> CompulsoryMonthField = true;
		}
		$targetmonth = trim($_POST['targetmonth']);
		if (!is_numeric($targetmonth)) {
			$this -> TargetMonthInt = true;
		}

		if (trim($_POST['username']) == "") {
			$this -> CompulsoryUserNameField = true;
		}
	}

	// function to check email syntax in correct Format or not
	function isValidEmail() {

		if (!eregi("^([a-z0-9_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,4}$", $this -> email)) {
			$this -> validEmailError = true;
		}

	}

	// function to check whether username already exist
	function isDuplicateEmail() 
	{

		global $db;
		$utilityObj = new utility();
		$condition = "email='$this->email'";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		if(count($result) > 0) {
			$this -> duplicateEmail = true;
		}
	}

	public function CheckDuplicateEmail($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);
		return $result;

	}
	//Validate all info as well
	function validateUserInfo() {

		global $dbObj;

		if (trim($_POST['username']) == "") {
			$this -> CompulsoryUserNameField = true;

		}

		if (trim($_POST['password']) == "") {
			$this -> CompulsoryUserPasswordField = true;

		}

	}

	//To check UserName Exists with same name

	public function CheckDuplicateUser($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);
		return $result;

	}
	
	public function Delete_user($condition)
	 {
	 	global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$condition2 = ' user_id='.$condition;
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TBL_USER_DETAIL, $condition2));
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_PAYMENT, $condition2));
		
		$condition = ' id='.$condition;
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_USER, $condition));
		return $sql;
		
	
	 }

	// function to insert All Data As a Array User Information
	function UserDataInsert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_USER, $dataArray));
		return DB_insertIdFunc();
	}
	
	// function to insert monthly comssion status--------
	function UsercomssionDataInsert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TBL_MONTHLY_PAID_STATUS, $dataArray));
		return DB_insertIdFunc();
	}

	//function to get an information of user or display all user by their conditions

	public function getOnGivenusername($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);

		return $result;

	}
	
	//function to get an information of user or display all user by their conditions

	public function get_monthly_comssion_status($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TBL_MONTHLY_PAID_STATUS, $condition, ""));

		$result = DB_fetchAssocFunc($sql);

		return $result;

	}
	//-------------
	public function getLevelwisemailstatus($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(LEVEL_MAIL_STATUS, $condition, ""));

		

		return $sql;

	}
	
	function LevelwisemailstatusInsert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(LEVEL_MAIL_STATUS, $dataArray));
		return DB_insertIdFunc();
	}
	

	//function to update an information of user

	function UserDataUpdate($data, $condition) {
		global $db;
		$utilityObj = new utility();
		
		//print_r($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		
		//mail('ankesh@internetbusinesssolutionsindia.com','Query',$utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		return $sql;
	}

	function to_save_user_confidential_information($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(CONFIDENTIAL_INFORMATION, $dataArray));
		return DB_insertIdFunc();
	}

	public function getusername_like($condition) {
		
         global $db;
		 $utilityObj = new utility();
		 $mainArry = array();
		 $sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		 while ($result = DB_fetchArrayFunc($sql)) {
		 $mainArry[] = $result;

		}
		return $mainArry;

	}
	
	public function getOnId($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "id = '$id' and username!=''", ""));
		$result = DB_fetchAssocFunc($sql);

		return $result;
		
	}
	
	//---------------get user detail--------
	public function get_user_detail($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition , ""));
		$result = DB_fetchAssocFunc($sql);

		return $result;
		
	}
	
	
	/*
	 * Total User Counting from table User
	 * Function
	 */
		public function gettingtotalUser() {
		
         global $db;
		 $utilityObj = new utility();
		 $mainArry = array();
		 $sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "", ""));
		 while ($result = DB_fetchArrayFunc($sql)) {
		 $mainArry[] = $result;

		}
		return $mainArry;

	}
		
		//Select all according to selection of Pro/Basic

	public function selectAllEmailData() {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$email = '';
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}
   
    //filtering data according to type status of user
	public function AllUserType($userstatus) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "status='$userstatus'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}
		return $mainArry;
	}
	
	// function to insert Activation Code in Tbl_user_verification
	public function UserVerifyInsertCode($data_verify_array) {
		
		global $db;
		$utilityObj = new utility();
		if (empty($data_verify_array))
			return;
		$sql = $db ->query($utilityObj ->am_createInsertQuery(USER_VERIFY_ACCOUNT,$data_verify_array));
		
		return DB_insertIdFunc();
	}
	
	public function Check_Verification_CodeUser($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj ->am_createSelectAllQuery(USER_VERIFY_ACCOUNT, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;

	}
	
	//RESET PASSWORD IF FORGOT IN LOGIN PAGE
	function updatePasswordByEmail($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		return $sql;

	}

	
	// function to use get user information .
	public function SelectAll_User($condition){
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		$mainArry = DB_fetchAssocFunc($sql);
		return $mainArry;
		
	}
	
	//----------get user lower line------------
	
	public function get_user_down_line_one_level($condition)
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		if($condition)
		{						
			$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
			while ($result = DB_fetchArrayFunc($sql)) {
				$mainArry[] = $result;
			}
		  return $mainArry;
						
		}else{
			return false;
		}
	}
	
	public function get_user_down_line_one_level_withpayment($condition)
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		if($condition)
		{						
		   
		//  echo "select u.*,p.amount,p.payment_date,p.add_date,p.payment_status,DATEDIFF('".date('Y-m-d h:i:s')."',payment_date) as week from ".TABLE_USER." u, ".TABLE_PAYMENT." p where u.id=p.user_id and  ".$condition."";
		//echo "select u.*,p.amount,p.payment_date,p.add_date,p.payment_status,DATEDIFF('".date('Y-m-d h:i:s')."',payment_date) as week from ".TABLE_USER." u, ".TABLE_PAYMENT." p where u.id=p.user_id and  ".$condition."";
		
		//echo "select u.*,p.amount,sum(p.amount) as total_amount,p.payment_date,p.add_date,p.payment_status,DATEDIFF('".date('Y-m-d h:i:s')."',payment_date) as week from ".TABLE_USER." u, ".TABLE_PAYMENT." p where u.id=p.user_id and  ".$condition." group by user_id";
		
		  $sql = $db -> query("select u.*,p.amount,sum(p.amount) as total_amount,p.payment_date,p.add_date,p.payment_status,DATEDIFF('".date('Y-m-d h:i:s')."',payment_date) as week from ".TABLE_USER." u, ".TABLE_PAYMENT." p where u.id=p.user_id and  ".$condition." group by user_id");
			while ($result = DB_fetchArrayFunc($sql)) {
				$mainArry[] = $result;
			}
		  return $mainArry;
						
		}else{
			return false;
		}
	}
	
	//-----------month wise user---------
	public function get_user_mothwise_with_payment($condition)
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		if($condition)
		{						
		   
		//  echo "select u.username,u.email,u.password,p.amount,ud.payment_information,ud.paypal_email,u.payment_status from ".TABLE_USER." u, ".TABLE_PAYMENT." p,tbl_user_detail ud where u.id=p.user_id and u.id=ud.user_id and  ".$condition."";
		
		// "select u.username,u.email,u.password,p.amount,ud.payment_information,ud.paypal_email,u.payment_status from ".TABLE_USER." u, ".TABLE_PAYMENT." p,tbl_user_detail ud where u.id=p.user_id and u.id=ud.user_id and  ".$condition."";
		
		
		  $sql = $db -> query("select u.id,u.username,u.email,u.password,p.amount,u.payment_status from ".TABLE_USER." u, ".TABLE_PAYMENT." p where u.id=p.user_id and  ".$condition." group by user_id");
			while ($result = DB_fetchArrayFunc($sql)) {
				$mainArry[] = $result;
			}
		  return $mainArry;
						
		}else{
			return false;
		}
	}

	
	// function to insert user card/paypal detail
	function User_detail_DataInsert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TBL_USER_DETAIL, $dataArray));
		return DB_insertIdFunc();
	}
	// function to update card/paypal user detail
	function User_detail_DataUpdate($data, $condition) {
		global $db;
		$utilityObj = new utility();
		
		//print_r($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TBL_USER_DETAIL, $data, $condition));
		return $sql;
	}
	
	// function to use get user information .
	public function user_payment_gateway_detail($condition){
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TBL_USER_DETAIL, $condition, ""));
		$mainArry = DB_fetchAssocFunc($sql);
		return $mainArry;
		
	}
	
	public function get_all_user_system_detail()
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$currentMonth = date('m');
		$currentYear = date('Y');
	    $lastDate = date("Y-m-d", time() - 86400);
		$currentDate = date("Y-m-d", time() );

	  "SELECT sum(IF(`payment_status`=0 && (`id`!=16 && `id`!=259),1, 0)) as total_prospects,sum(IF(`payment_status`=1 && (`id`!=16 && `id`!=259),1, 0)) as total_user,sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%m')=$currentMonth && DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y')=$currentYear  && (`id`!=16 && `id`!=259),1, 0)) as total_registred_user,sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d') >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY AND DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d') < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY && (`id`!=16 && `id`!=259),1, 0)) as lastweek_registred_user, sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d')='$lastDate' && (`id`!=16 && `id`!=259),1, 0)) as total_yesterday_user,sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d')='$currentDate' && (`id`!=16 && `id`!=259),1, 0)) as total_today_user FROM `tbl_user`";
		
		  $sql = $db -> query("SELECT sum(IF(`payment_status`=0 && (`id`!=16 && `id`!=259),1, 0)) as total_prospects,sum(IF(`payment_status`=1 && (`id`!=16 && `id`!=259),1, 0)) as total_user,sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%m')=$currentMonth && DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y')=$currentYear  && (`id`!=16 && `id`!=259),1, 0)) as total_registred_user,sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d') >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY AND DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d') < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY && (`id`!=16 && `id`!=259),1, 0)) as lastweek_registred_user, sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d')='$lastDate' && (`id`!=16 && `id`!=259),1, 0)) as total_yesterday_user,sum(IF(DATE_FORMAT(FROM_UNIXTIME(`modified_date`), '%Y-%m-%d')='$currentDate' && (`id`!=16 && `id`!=259),1, 0)) as total_today_user FROM `tbl_user` where Admin_Role=''");
			$mainArry = DB_fetchAssocFunc($sql);
		  return $mainArry;
		
	}
	
	public function current_user_total_income($user_id)
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		
		
		//-------------------Get total income----------------
			$selectedMonth = date('F');
			$current_year =  date('Y');

			$condition = " monthname(p.payment_date) = '".$selectedMonth."' and year(p.payment_date) = '".$current_year."' and (u.enroller= '".$user_id."' || u.real_enroller= '".$user_id."') ";
			$downLine_list = $this->get_user_down_line_one_level_withpayment($condition);

			$totalEarning = 0;
			foreach($downLine_list as $value)
			{
				  if($value['enroller']==$value['real_enroller'])
				  {
					   $totalSilvercoin_result =get_presnatage(80,number_format($value['total_amount'],0));
					   $totalEarning+=$totalSilvercoin_result;
					   
				  }else{
					   $totalSilvercoin_result =get_presnatage(80,number_format($value['total_amount'],0));
					   $totalSilvercoin_result =get_presnatage(50,number_format($totalSilvercoin_result,0));
					   $totalEarning+=$totalSilvercoin_result;
				  }
				
				//$totalEarning +=$value['total_amount'];
			}		
		return $totalEarning;
	}
	
	
	public function user_registraction_process($email,$first_name,$last_name,$ref)
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		//Check Duplicate Email or Not
		$condition = "email='$email'";
		$EmailDuplicate = $this -> CheckDuplicateEmail($condition);
		if (!empty($EmailDuplicate) && count($EmailDuplicate) > 0) {
			$_SESSION['DuplicatEmail'] = "Ya existe una cuenta con este correo electrónico o nombre de usuario ";
			header("location: ".$_SERVER['HTTP_REFERER']);
			exit ;
		} else {
		//------------set sponcer----------	
		if($ref!='')
		{
			$condition = "username='".$ref."'";
			$sponcerData = $this -> getOnGivenusername($condition);
			$enroller_id = $sponcerData['id'];
		}else{
			 $enroller_id = 0; 
		}

		$_SESSION['email'] = $email;
		$regdate = strtotime(date('Y-m-d'));
		//Validate User Information if javascript validation not working at all
		$this -> validateRegisterForm();
		//Check  Email Field not Empty
		if ($this -> compulsoryFieldsError) {
			$variables['error'] = "Email Fields are compulsory.";
			//Check  InvalidEmail FilledUp What or in correct format?
			header("location: ".$_SERVER['HTTP_REFERER']);
			exit ;
		} else if ($this -> validEmailError) {
			$variables['error'] = "Invalid email address! Please Enter valid email address.";
			header("location: ".$_SERVER['HTTP_REFERER']);
			exit ;

		} else {
		
			if(isset($first_name) && $first_name!='')
			{
				$username = $first_name.' '.$last_name;
			}else{
				$username = $username;
			}
			
			$dataArray = array("name" => $username, "email" => $email, "enroller" => $enroller_id, "real_enroller" => $enroller_id, "modified_date" => time(), "registration_date" => $regdate);
			$this -> UserDataInsert($dataArray);
			$condition = "email='$email'";
			$userdata = $this -> getOnGivenusername($condition);
			$_SESSION['OPEN_POPUP'] = 1;
		//	$_SESSION['AD_LoggedIn'] = true;
			$_SESSION['AD_user_id'] = $userdata['id'];
			$_SESSION['current_user_name'] = $userdata['name'];
			//Sending mail this function is comming from mail.function.php created file in functions.php
		
			UserFirstSubscriberMail($_SESSION['email']);
				
 			#############################################
			/*
			 * Here We are inserting pretextvideo to be
			 * shared on facebook/or twitter
			 */
			############################################
			$MissionObj = new Mission();
			$data_array = array("user_id" => $_SESSION['AD_user_id'], "mission_level" => 2, "video" => 'http://youtu.be/bydefault', "topic" => '10 Cosas que tienes que hacer antes de morir', "discussion" => '', "entry_date" => time(), "hits" => 1);
			$mission_id = $MissionObj -> shareidea_for_mission($data_array);
			$data_array = array("user_id" => $_SESSION['AD_user_id'], "mission_level" => 3, "video" => 'http://youtu.be/bydefault', "topic" => '10 Cosas que tienes que hacer antes de morir', "discussion" => '', "entry_date" => time(), "hits" => 1);
			$mission_id = $MissionObj -> shareidea_for_mission($data_array);
			return true;
		}
	}
	
  }
	
}