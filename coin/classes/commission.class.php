<?php
//Comision table operation perform

class commission {
	public function commission() {
	}

	  /*
	  * Insert data With array in tbl_commision
	  * table with crone this function is being
	  * used with crone updation...
	  */
	  
	  function CommisionInsertData($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_COMMISSION, $dataArray));
		return DB_insertIdFunc();
	}
	
 	  /*
	   * Find the Comision of particullar user
	   * With the condition based we will
	   * only pass the condition and get records
	   * from comision tables
	   */
	  
	  public function getComisiononGivenId($condition) {
	  	
		//echo $condition;
		//die;

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_COMMISSION, $condition, ""));
		while($result = DB_fetchAssocFunc($sql))
		{
		$mainArry[]= $result;
		}
		return $mainArry;
	}
	  
	  /*
	   * Upade for comision Function
	   * in tbl_user table
	   */
	  
	  function ComisionUpdate($data, $condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_COMMISSION, $data, $condition));
		return $sql;
	}
	  
}
?>