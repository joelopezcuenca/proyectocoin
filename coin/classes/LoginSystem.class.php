<?php
//error_reporting(0);
class LoginSystem {
	var $db_host = MAINDB_HOST;
	var $db_name = MAINDB_NAME;
	var $db_user = MAINDB_USER;
	var $db_password = MAINDB_PASS;
	var $connection, $username, $password;

	/**	 * Constructor	 */
	function LoginSystem() {
	}

	/** * Check if the user is logged in, redirect if not. */

	function isLoggedIn() {
		//echo "asdasdasd";
		@session_start();
		if (!isset($_SESSION['AD_LoggedIn'])) {
			header("location: " . MODULE_URL . "/home/home.php");
			exit ;
		}
	}

	/** * Check username and password against DB *

	 * @return true/false */

	function doLogin($username, $password, $remember) {
		$this -> connect();
		$this -> username = $username;
		$this -> password = $password;
		$this -> remember = $remember;
		 $sql = "SELECT * FROM " . TABLE_USER . " WHERE (email='" . $this -> clean($this -> username) . "' or username='" . $this -> clean($this -> username) . "') and password= '" . createHash($this -> password) . "' and Admin_Role=''";
		//die;
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		// If no user/password combo exists return false
		if (mysql_num_rows($result) != 1) {
			ob_start();
			$_SESSION['Msg'] = "Correo o contraseña incorrectos!!";
			header("location:" . MODULE_URL . "/home/index.php?to=login");
			exit ;
		} else// matching login ok

		{

			if (($row['Block_Status'] == 'blocked')||($row['Account_status'] == 'suspended')) {
				$_SESSION['AdminMsg'] = " Su cuenta ha sido bloqueada por favor contacto con administrador";
				header("location:" . MODULE_URL . "/home/index.php?to=login");
				exit ;
			}

			if (isset($remember)) {
				/* Set cookie to last 1 year */
				setcookie('username', $this -> username, time() + 60 * 60 * 24 * 365, '', '');
				setcookie('password', $this -> password, time() + 60 * 60 * 24 * 365, '', '');
			}
			/* Delete Cookies when Unchecked Box */
			if (!isset($remember)) {
				setcookie('username', $this -> username, time() - 60 * 60 * 24 * 365, '', '');
				setcookie('password', $this -> password, time() - 60 * 60 * 24 * 365, '', '');
			}

			@session_start();
			$_SESSION['OPEN_POPUP'] = 0;
			$_SESSION['AD_LoggedIn'] = true;
			$_SESSION['AD_user_id'] = $row['id'];
			$_SESSION['AD_user_name'] = $row['username'];
			$_SESSION['AD_email'] = $row['email'];
			$_SESSION['User_url'] = $row['url'];
			//$_SESSION['show_goal_noti'] = '1';
			//$_SESSION['show_welcome_noti'] = '1';
			
			$userobj = new user();
			$condition = "id=" . $_SESSION['AD_user_id'];
			$dataArray = array("last_login" => time());
			$userobj -> UserDataUpdate($dataArray, $condition);
			header("location:" . MODULE_URL . "/home/home.php");
			exit ;
		}
	}
	
	//-------------new  login function for direct login-----------
	
	function doLogin_new($username, $password, $remember) {
		$this -> connect();
		$this -> username = $username;
		$this -> password = $password;
		$this -> remember = $remember;
		 $sql = "SELECT * FROM " . TABLE_USER . " WHERE (email='" . $this -> clean($this -> username) . "' or username='" . $this -> clean($this -> username) . "') and password= '" . $this -> password . "' and Admin_Role=''";
		//die;
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		// If no user/password combo exists return false
		if (mysql_num_rows($result) != 1) {
			ob_start();
			$_SESSION['Msg'] = "Correo o contraseña incorrectos!!";
			header("location:" . MODULE_URL . "/home/index.php?to=login");
			exit ;
		} else// matching login ok

		{

			if (($row['Block_Status'] == 'blocked')||($row['Account_status'] == 'suspended')) {
				$_SESSION['AdminMsg'] = " Su cuenta ha sido bloqueada por favor contacto con administrador";
				header("location:" . MODULE_URL . "/home/index.php?to=login");
				exit ;
			}

			if (isset($remember)) {
				/* Set cookie to last 1 year */
				setcookie('username', $this -> username, time() + 60 * 60 * 24 * 365, '', '');
				setcookie('password', $this -> password, time() + 60 * 60 * 24 * 365, '', '');
			}
			/* Delete Cookies when Unchecked Box */
			if (!isset($remember)) {
				setcookie('username', $this -> username, time() - 60 * 60 * 24 * 365, '', '');
				setcookie('password', $this -> password, time() - 60 * 60 * 24 * 365, '', '');
			}

			@session_start();
			$_SESSION['OPEN_POPUP'] = 0;
			$_SESSION['AD_LoggedIn'] = true;
			$_SESSION['AD_user_id'] = $row['id'];
			$_SESSION['AD_user_name'] = $row['username'];
			$_SESSION['AD_email'] = $row['email'];
			$_SESSION['User_url'] = $row['url'];
			//$_SESSION['show_goal_noti'] = '1';
			//$_SESSION['show_welcome_noti'] = '1';
			$userobj = new user();
			$condition = "id=" . $_SESSION['AD_user_id'];
			$dataArray = array("last_login" => time());
			$userobj -> UserDataUpdate($dataArray, $condition);
			header("location:" . MODULE_URL . "/home/home.php");
			exit ;
		}
	}

	/**	 * Destroy session data/Logout.php */
	function logout() {

		$_SESSION['AD_LoggedIn'] = false;
		unset($_SESSION['AD_LoggedIn']);
		unset($_SESSION['AD_user_id']);
		unset($_SESSION['AD_user_name']);
		unset($_SESSION['AD_email']);
		unset($_SESSION['email']);
		unset($_SESSION['Msg']);
		unset($_SESSION['ref']);
		//@session_unset();
		//@session_destroy();
		return true;
	}

	/**	 * Connect to the Database *
	 * @return true/false	 */
	function connect() { $this -> connection = mysql_connect($this -> db_host, $this -> db_user, $this -> db_password) or die("Unable to connect to MySQL");
		mysql_select_db($this -> db_name, $this -> connection) or die("Unable to select DB!");
		// Valid connection object? everything ok?
		if ($this -> connection) {
			return true;
		} else
			return false;
	}

	/** * Disconnect from the db */

	function disconnect() {

		mysql_close($this ->connection);

	}

	/**	 * Cleans a string for input into a MySQL Database.

	 * Gets rid of unwanted characters/SQL injection etc. *

	 * @return string */

	function clean($str) {

		// Only remove slashes if it's already been slashed by PHP

		if (get_magic_quotes_gpc()) {

			$str = stripslashes($str);

		}

		// Let MySQL remove nasty characters.

		$str = mysql_real_escape_string($str);

		return $str;

	}

	/** * create a random password

	 * @param	int $length - length of the returned password

	 * @return	string - password * */

	function randomPassword($length = 8) {

		$pass = "";

		// possible password chars.

		$chars = array("a", "A", "b", "B", "c", "C", "d", "D", "e", "E", "f", "F", "g", "G", "h", "H", "i", "I", "j", "J", "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "q", "Q", "r", "R", "s", "S", "t", "T", "u", "U", "v", "V", "w", "W", "x", "X", "y", "Y", "z", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9");

		for ($i = 0; $i < $length; $i++) {

			$pass .= $chars[mt_rand(0, count($chars) - 1)];

		}

		return $pass;

	}

}
?>