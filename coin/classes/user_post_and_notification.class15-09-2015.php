<?php
/*
 * Developer:XX
 * Here Most all Functions Regarding
 * Posts,Comments,etc Every functions regarding
 * this one
 */

class user_post {
	function user_post() {
	}

	//function to create an entry for user notification in database
	function UserNotificationDataInsert($dataarray, $message) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataarray))
			return;
		$email_arr = $this -> email_array($dataarray['email']);
		for ($i = 0; $iend = count($email_arr), $i < $iend; $i++) {

			$email = $email_arr[$i];
			$user_id = $this -> EmailFromId($email);
			$referral_id = $_SESSION['MAIN_admin_user_id'];
			$table_arr = array(user_id => $user_id, referral_id => $referral_id, description => $message, is_active => "1", entry_date => time());
			$sql = $db -> query($utilityObj -> am_createInsertQuery(ADMIN_NOTIFICATION, $table_arr));
		}
		//	$sql = $db->query($utilityObj->am_createInsertQuery(USER_NOTIFICATION,$dataarray));
		return DB_insertIdFunc();
	}

	function email_array($string) {
		$string_arr = explode(",", $string);
		return $string_arr;
	}

	function EmailFromId($email) {

		global $db;
		$utilityObj = new utility();
		$condition = "email='$email'";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		$user_id = $result['id'];
		return $user_id;

	}

	function referral_user_post_insert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(USER_NOTIFICATION, $dataArray));
		//print_r($sql);
		return DB_insertIdFunc();

	}

	function user_notofication_insert_for_new_user($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(ADMIN_NOTIFICATION, $dataArray));
		//print_r($sql);
		return DB_insertIdFunc();

	}

	function prouser_notification_insert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(PROUSER_NOTIFICATION_TABLE, $dataArray));
		//print_r($sql);
		return DB_insertIdFunc();

	}

	##################### This function is to insert values in database according to every table################################
	function Basic_insert($dataArray, $table) {
		/// echo " i m here in sql file" ;
		//echo "<pre>" ;
		//print_r($dataArray);
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery($table, $dataArray));
		//print_r($sql);
		return DB_insertIdFunc();

	}

	###########################################################################################################################

	/*
	 * For Notification for Users
	 */
	function basic_notification_insert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(BASICUSER_NOTIFICATION_TABLE, $dataArray));
		//print_r($sql);
		return DB_insertIdFunc();

	}

	/*
	 * Only for Basic User Notification when he is free
	 *
	 */

	function basic_getnotification_insert($dataArray) {

		//print_r($dataArray);
		//die;

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(BASICUSER_GETNOTIFICATION_TABLE, $dataArray));
		return DB_insertIdFunc();

	}

	//post information posted by particullar user

	function finduserpostinformation($id) {

		$all_post_notification_array = array();
		$admin_condition = "user_id='$id' and is_active=1 order by id desc";
		$admin_table = ADMIN_NOTIFICATION;
		// $all_post_notification_array[] = $this->notifications_from_table($admin_condition,$admin_table) ;
		$user_condition = "referral_id='$id' and is_active=1 order by id desc";
		$user_table = USER_NOTIFICATION;
		$all_post_notification_array[] = $this -> notifications_from_table($user_condition, $user_table);
		$follow_condition = "follower_id='$id' and is_active=1 order by id desc";
		$follow_table = FOLLOW_NOTIFICATION_TABLE;
		$all_post_notification_array[] = $this -> notifications_from_table($follow_condition, $follow_table);
		$bucket_condition = "user_id='$id' and is_active=1 order by id desc";
		$bucket_table = BUCKET_NOTIFICATION_TABLE;

		//for basic user notification for all user
		$basic_condition = "enroller_id='$id' and is_active=1 order by id desc";
		$basic_table = BASICUSER_NOTIFICATION_TABLE;
		$all_post_notification_array[] = $this -> notifications_from_table($basic_condition, $basic_table);

		//show level while user cross
		$levelget_condition = "user_id='$id' and is_active=1 order by id desc";
		$levelgetnotify_table = USERLEVEL_NOTIFICATION_TABLE;
		$all_post_notification_array[] = $this -> notifications_from_table($levelget_condition, $levelgetnotify_table);

		//end
		//golden coin notification data fetch
		$goldencoin_condition = "enroller_id='$id' and is_active=1 order by id desc";
		$goldencoinnotify_table = GOLDEN_COIN_NOTIFICATION;
		$all_post_notification_array[] = $this -> notifications_from_table($goldencoin_condition, $goldencoinnotify_table);

		//end

		//losting a user when some one get before you
		$lost_condition = "user_id='$id' and is_active=1 order by id desc";
		$lostnotify_table = LOST_COIN_NOTIFICATION;
		$all_post_notification_array[] = $this -> notifications_from_table($lost_condition, $lostnotify_table);

		//if basic user has not paid then he will get notification

		$basicget_condition = "user_id='$id' and is_active=1 order by id desc";
		$basicgetnotify_table = BASICUSER_GETNOTIFICATION_TABLE;
		$all_post_notification_array[] = $this -> notifications_from_table($basicget_condition, $basicgetnotify_table);

		// $all_post_notification_array[] = $this->notifications_from_table($bucket_condition,$bucket_table) ;
		$prouser_condition = "enroller_id='$id' and is_active=1 order by id desc";
		$pro_table = PROUSER_NOTIFICATION_TABLE;
		$all_post_notification_array[] = $this -> notifications_from_table($prouser_condition, $pro_table);
		$return_array = $this -> to_redefine_notification_array($all_post_notification_array);
		return $return_array;

	}

	function notifications_from_table($condition, $table) {
		global $db;
		$utilityObj = new utility();

		$sql = $db -> query($utilityObj -> am_createSelectAllQuery($table, $condition, ""));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}

		return $result;
	}

	function to_redefine_notification_array($array) {
		$notification_r_array = array();
		for ($i = 0; $iend = count($array), $i < $iend; $i++) {
			for ($j = 0; $jend = count($array[$i]), $j < $jend; $j++) {
				$notification_r_array[] = $array[$i][$j];
			}
		}
		return $notification_r_array;
	}

	function finduserpostcount($id) {

		$all_post_notification_array = array();
		$admin_condition = "user_id='$id' and is_active=1 order by id desc";
		$admin_table = ADMIN_NOTIFICATION;
		$all_post_notification_array[] = $this -> notifications_from_table($admin_condition, $admin_table);
		$user_condition = "referral_id='$id' and is_active=1 order by id desc";
		$user_table = USER_NOTIFICATION;
		$all_post_notification_array[] = $this -> notifications_from_table($user_condition, $user_table);
		$follow_condition = "user_id='$id' and is_active=1 order by id desc";
		$follow_table = FOLLOW_NOTIFICATION_TABLE;
		$all_post_notification_array[] = $this -> notifications_from_table($follow_condition, $follow_table);
		$bucket_condition = "user_id='$id' and is_active=1 order by id desc";
		$bucket_table = BUCKET_NOTIFICATION_TABLE;
		$all_post_notification_array[] = $this -> notifications_from_table($bucket_condition, $bucket_table);

		$return_array = $this -> to_redefine_notification_array($all_post_notification_array);
		return count($return_array);

	}

	function in_active($id, $referral, $user, $identifier) {
				
			
		//echo "$id, $referral, $user, $identifier";
		
		//die('i am here in-active messagesss');

		if ($identifier == '0') {
			$query = "select * from " . USERLEVEL_NOTIFICATION_TABLE . " where id=$id ";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
			 $query1 = "update " . USERLEVEL_NOTIFICATION_TABLE . " set
			is_active = 0
			where id=$id  ";
				$sql = DB_queryFunc($query1);
				// 1 is for user notifications
				$post_action = 1;
				return $post_action;
			}
		}
			if ($identifier == 1) {
			$query = "select * from " . USER_NOTIFICATION . " where id=$id and user_id=$user and referral_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . USER_NOTIFICATION . " set
			is_active = 0
			where id=$id and user_id=$user and referral_id=$referral
			";
				$sql = DB_queryFunc($query1);
				// 1 is for user notifications
				$post_action = 1;
				return $post_action;
			}
		}
		if ($identifier == 2) {
			$query = "select * from " . ADMIN_NOTIFICATION . " where id=$id and user_id=$user and referral_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . ADMIN_NOTIFICATION . " set
			is_active = 0
			where id=$id and user_id=$user and referral_id=$referral
			";
				$sql = DB_queryFunc($query1);
				// 2 is for admin notifications
				$post_action = 2;
				return $post_action;
			}
		}
		if ($identifier == 3) {
			$query = "select * from " . FOLLOW_NOTIFICATION_TABLE . " where id=$id and user_id=$user and follower_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . FOLLOW_NOTIFICATION_TABLE . " set
			is_active = 0
			where id=$id and user_id=$user and follower_id=$referral
			";
				$sql = DB_queryFunc($query1);
				// 3 is for follow notifications
				$post_action = 3;
				return $post_action;
			}
		}
		if ($identifier == 4) {
			$query = "select * from " . BUCKET_NOTIFICATION_TABLE . " where id=$id and user_id=$user and bucket_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . BUCKET_NOTIFICATION_TABLE . " set
			is_active = 0
			where id=$id and user_id=$user and bucket_id=$referral
			";
				$sql = DB_queryFunc($query1);
				// 4 is for achievement notifications
				$post_action = 4;
				return $post_action;
			}
		}
		if ($identifier == 5) {
			$query = "select * from " . PROUSER_NOTIFICATION_TABLE . " where id=$id and user_id=$user and enroller_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . PROUSER_NOTIFICATION_TABLE . " set
			is_active = 0
			where id=$id and user_id=$user and enroller_id=$referral
			";
				$sql = DB_queryFunc($query1);
				// 4 is for prouser notifications
				$post_action = 5;
				return $post_action;
			}
		}

		if ($identifier == 6) {
			$query = "select * from tbl_basicuser_getnotification where id=$id and user_id=$user and enroller_id=$referral";

			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update tbl_basicuser_getnotification set
			is_active = 0
			where id=$id and user_id=$user and enroller_id=$referral
			";
				$sql = DB_queryFunc($query1);
				// 4 is for prouser notifications
				$post_action = 6;
				return $post_action;
			}
		}

		if ($identifier == 7) {
			echo $query = "select * from " . BASICUSER_GETNOTIFICATION_TABLE . " where id=$id and user_id=$user and enroller_id=$referral";

			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				echo $query1 = "update " . BASICUSER_GETNOTIFICATION_TABLE . " set
			is_active = 0
			where id=$id and user_id=$user and enroller_id=$referral
			";
				$sql = DB_queryFunc($query1);
				// 4 is for prouser notifications
				$post_action = 7;
				return $post_action;
			}
		}

		if ($identifier == 8) {
			$query = "select * from " . USERLEVEL_NOTIFICATION_TABLE . " where id=$id and user_id=$user and enroller_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . USERLEVEL_NOTIFICATION_TABLE . " set
			is_active = 0
			where id=$id and user_id=$user and enroller_id=$referral
			";
				$sql = DB_queryFunc($query1);
				$post_action = 8;
				return $post_action;
			}
		}
		if ($identifier == 9) {
			$query = "select * from " . GOLDEN_COIN_NOTIFICATION . " where id=$id and user_id=$user and enroller_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . GOLDEN_COIN_NOTIFICATION . " set
			is_active = 0
			where id=$id and user_id=$user and enroller_id=$referral
			";
				$sql = DB_queryFunc($query1);
				$post_action = 9;
				return $post_action;
			}
		}

		if ($identifier == 10) {
			$query = "select * from " . LOST_COIN_NOTIFICATION . " where id=$id and user_id=$user and enroller_id=$referral";
			$sql = DB_queryFunc($query);
			$result = DB_numRowsFunc($sql);
			if ($result > 0) {
				$query1 = "update " . LOST_COIN_NOTIFICATION . " set
			is_active = 0
			where id=$id and user_id=$user and enroller_id=$referral
			";
				$sql = DB_queryFunc($query1);
				$post_action = 10;
				return $post_action;
			}
		}

	}

	//to fetch single message data  form notification table on basis of identifier - apar sharma
	function selected_notification($id, $referral, $user, $identifier) {
		if ($identifier == 1) {
			$query = "select * from " . USER_NOTIFICATION . " as a join " . TABLE_USER . " as b on(a.user_id=b.id) where a.id=$id and a.user_id=$user and a.referral_id=$referral";
			$sql = DB_queryFunc($query);
			$rec = DB_fetchAssocFunc($sql);
			return $rec;

		}
		if ($identifier == 2) {
			$query = "select * from " . ADMIN_NOTIFICATION . " as a join " . TABLE_USER . " as b on(a.referral_id=b.id) where a.id=$id and a.user_id=$user and a.referral_id=$referral";
			$sql = DB_queryFunc($query);
			$rec = DB_fetchAssocFunc($sql);
			return $rec;
		}
		if ($identifier == 3) {
			$query = "select * from " . FOLLOW_NOTIFICATION_TABLE . " as a join " . TABLE_USER . " as b on(a.user_id=b.id) where a.id=$id and a.user_id=$user and a.follower_id=$referral";
			$sql = DB_queryFunc($query);
			$rec = DB_fetchAssocFunc($sql);
			return $rec;
		}
		if ($identifier == 4) {
			$query = "select * from " . BUCKET_NOTIFICATION_TABLE . " as a join " . BUCKETLIST_TABLE . " as b on(a.bucket_id=b.id) where a.id=$id and a.user_id=$user and a.bucket_id=$referral";
			$sql = DB_queryFunc($query);
			$rec = DB_fetchAssocFunc($sql);
			return $rec;
		}

	}

	//

	function get_data_from_user($id) {

		$result = array();
		$query = "select * from " . USER_NOTIFICATION . " as a join " . TABLE_USER . " as b on(a.user_id=b.id)     where a.referral_id=$id order by a.id desc";

		//	$query = "select * from " . USER_NOTIFICATION . " where referral_id=$id order by id desc limit 6";
		$sql = DB_queryFunc($query);
		//	echo DB_numRowsFunc($sql);
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}

		return $result;
	}

	function get_data_from_admin($id) {
		$result = array();
		$query = "select * from " . ADMIN_NOTIFICATION . " as a join " . TABLE_USER . " as b on(a.referral_id=b.id)     where a.user_id=$id order by a.id desc";

		//$query = "select * from " . ADMIN_NOTIFICATION . " where user_id=$id order by id desc limit 6";
		$sql = DB_queryFunc($query);

		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}

		return $result;
	}

	function get_data_from_follow($id) {
		$result = array();
		$query = "select * from " . FOLLOW_NOTIFICATION_TABLE . " as a join " . TABLE_USER . " as b on(a.user_id=b.id) where a.follower_id=$id order by a.id desc";

		//$query = "select * from " . FOLLOW_NOTIFICATION_TABLE . " where user_id=$id order by id desc limit 6";
		$sql = DB_queryFunc($query);

		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}

		return $result;
	}

	function get_data_from_achievement($id) {
		$result = array();
		$query = "select * from " . BUCKET_NOTIFICATION_TABLE . " as a join " . BUCKETLIST_TABLE . " as b on(a.bucket_id=b.id) where a.user_id=$id order by a.id desc";

		//$query = "select * from " . BUCKET_NOTIFICATION_TABLE . " where user_id=$id order by id desc limit 6";
		$sql = DB_queryFunc($query);

		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}

		return $result;
	}

	function insert_post($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(POST_TABLE, $dataArray));
		return DB_insertIdFunc();

	}

	function select_allpost($user_id) {
		global $db;
		$result = array();
		$post_id = array();
		$utilityObj = new utility();
		$post_id[] = $user_id;
		$all_posts = array();
		$follow_post_id = $this -> select_follow_p_id($user_id);
		for ($i = 0; $iend = count($follow_post_id), $i < $iend; $i++) {
			$post_id[] = $follow_post_id[$i];
		}
		for ($j = 0; $jend = count($post_id), $j < $jend; $j++) {
			//$all_posts[$post_id[$j]][] = $post_id[$j] ;
			$temp_post_array = $this -> get_all_posts_by_id($post_id[$j]);
			for ($g = 0; $gend = count($temp_post_array), $g < $gend; $g++) {

				$all_posts[] = $temp_post_array[$g];
			}

		}

		return $all_posts;

	}

	// to calculate all posts according to id pass
	function get_all_posts_by_id($user_id) {

		//	print_r($_SESSION);
		global $db;
		$result = array();
		$utilityObj = new utility();

		if ($_SESSION['AD_user_id'] == $user_id) {
			$condition = "user_id='$user_id'";
		} else {
			$condition = "user_id='$user_id' and post_status=0";
		}

		$orderBy = "date_upload desc";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(POST_TABLE, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$rec['p_id'] = $user_id;
			$result[] = $rec;
		}
		return $result;

	}

	//

	// to select all followed people id
	function select_follow_p_id($user_id) {

		global $db;
		$result = array();
		$utilityObj = new utility();
		$condition = "Followers_Id='$user_id'";
		//$orderBy = "date_upload desc";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_FOLLOW, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec['UsertoFollow_Id'];
		}
		return $result;

	}

	//
	function select_post_by_id($post_id) {
		global $db;
		$result = array();
		$utilityObj = new utility();
		$condition = "id='$post_id'";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(POST_TABLE, $condition, ''));
		$rec = DB_fetchAssocFunc($sql);
		return $rec;
	}

	function update_post($dataArray, $condition) {
		global $db;
		$result = array();
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(POST_TABLE, $dataArray, $condition));
		return $sql;
	}/*	 *  Insert comment	 */

	function insert_comment($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(POST_COMMENT_TABLE, $dataArray));
		return DB_insertIdFunc();
	}/*	 * Select comment by id 	 */

	function select_comment_by_id($post_id) {
		global $db;
		$result = array();
		$utilityObj = new utility();
		$condition = "post_id='$post_id'";
		$orderBy = "add_date desc limit 0,5";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(POST_COMMENT_TABLE, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}

	/*	 * Select comment with user info by post id  	 */
	function select_commentById($post_id) {
		global $db;
		$result = array();
		$query = "select a.*,b.name from " . POST_COMMENT_TABLE . " as a join " . TABLE_USER . " as b on(a.user_id=b.id)   where a.post_id=$post_id order by a.add_date DESC limit 0,5";
		$sql = DB_queryFunc($query);
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}

	public function delete_commentbyCondition($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(POST_COMMENT_TABLE, $condition));
		return $sql;
	}

	function Post_select($post_id) {

		$query = "select * from " . POST_TABLE . " where id='" . $post_id . "'";
		$sql = DB_queryFunc($query);
		$rec = DB_fetchAssocFunc($sql);
		return $rec;

	}

	function all_messages($userid) {
		$all_msg_array = array();
		$all_msg_array[] = $this -> get_data_from_user($userid);
		$all_msg_array[] = $this -> get_data_from_admin($userid);
		$all_msg_array[] = $this -> get_data_from_follow($userid);
		$all_msg_array[] = $this -> get_data_from_achievement($userid);
		return $all_msg_array;
	}

	function makeClickableLinks($text) {

		$text = html_entity_decode($text);
		$text = " " . $text;
		$text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)', '<a href="\\1" target=_blank>\\1</a>', $text);
		$text = eregi_replace('(((f|ht){1}tps://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)', '<a href="\\1" target=_blank>\\1</a>', $text);
		$text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)', '\\1<a href="http://\\2" target=_blank>\\2</a>', $text);
		$text = eregi_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})', '<a href="mailto:\\1" target=_blank>\\1</a>', $text);

		return $text;
	}

	##  this function is used to update user level notification in database tableon info header page.
	###################
	function user_level_updation($id, $level) {

		$condition = "level=$level and user_id=$id";
		$data = $this -> select_dataByleveluserid($condition);

		if (count($data) > 0) {
			$dataarray = array("level" => $level);
			$this -> UserLevelDataUpdate($dataarray, $condition);
		}

		if ((count($data) == 0) && ($level != 0)) {
			$dataarray = array("level" => $level, "user_id" => $id, "is_active" => 1, "entry_date" => time(), "identifier" => 8);

			$this -> insert_leveluser($dataarray);
		}

	}

	###################################################

	function insert_leveluser($dataArray) {

		//die('i am in insert function the dsdfsfsdfsdf');
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(USERLEVEL_NOTIFICATION_TABLE, $dataArray));
		return DB_insertIdFunc();
	}

	function select_dataByleveluserid($condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(USERLEVEL_NOTIFICATION_TABLE, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;
	}

	function UserLevelDataUpdate($data, $condition) {
		global $db;
		$utilityObj = new utility();

		//print_r($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(USERLEVEL_NOTIFICATION_TABLE, $data, $condition));
		return $sql;
	}

	function insert_lostusernotification($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(LOST_COIN_NOTIFICATION, $dataArray));
		return DB_insertIdFunc();
	}

}
?>