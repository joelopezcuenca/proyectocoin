<?php
/*
 * CODER:RAGHWENDRA
 * THIS CLASS CONTAINS ADMIN USER SECTION/ADMIN CONTROLLER
 */

class AdminUserPermission {
	public function AdminUserPermission() {//CONSTRUCTOR DEFINED HERE
	}

	/*
	 * FUNCTION FOR INSERTING ALL USER ACCOUNTS PERMISSION !!
	 */
	public function AdminUserPermissionInsert($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
		return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(ADMIN_PERMISSION, $dataArray));
		return DB_insertIdFunc();

	}

	/*
	 * FUNCTION FOR SELECT ALL ADMIN USER ACCOUNTS
	 */
	public function AdminselectAllAdminUser($userid) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$username = '';

		 $sql = $db -> query($utilityObj -> am_createSelectAllQuery(ADMIN_PERMISSION, "userid='".$userid."'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}

	

	/*
	 * FUNCTION UPDATE  ADMIN USER ACCOUNTS BY THEIR ID
	 *
	 */

	public function AdminUserUpdate($data, $condition) {
		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(ADMIN_PERMISSION, $data, $condition));
		return $sql;
	}

	/*
	 * FUNCTION DELETE ADMIN USER ACCOUNTS BY THEIR ID
	 *
	 */
	public function DeleteAdminUserData($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_USER, $condition));
		return $sql;

	}

}

/*
 * END OF CLASSS
 *
 */
?>