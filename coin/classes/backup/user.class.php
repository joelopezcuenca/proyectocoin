<?php  ##### Coder:Raghwendra ####
class user {
		
	public function user() {
		
	}

	var $compulsoryFieldsError = false;
	var $validEmailError = false;
	var $duplicateEmail = false;
	var $duplicateUserName = false;
	var $CompulsoryAmountAchieve = false;
	var $CompulsoryMonthField = false;
	var $CompulsoryUserNameField = false;
	var $CompulsoryUserPasswordField = false;
	var $AmountAchieveInt = false;
	var $TargetMonthInt = false;
	var $imageTypeCheck = false;
	
	/*
	 * Valudation for checking Authentication
	 * for user registering Email or other format
	 */
	function validateRegisterForm() {

		global $dbObj;

		if (trim($_POST['email']) == "") {

			$this -> compulsoryFieldsError = true;

		} else {

			// Assign post values to class varibales
			$this -> email = addslashes(nl2br(trim($_POST['emailId'])));
			//	$this -> isValidEmail();

		}

	}

	//check whether Targeted Amount is Entered or left blank
	function ValidateTargetAmount() {

		if (trim($_POST['targetamount']) == "") {

			$this -> CompulsoryAmountAchieve = true;
		}
		$amount = trim($_POST['targetamount']);
		if (!is_numeric($amount)) {

			$this -> AmountAchieveInt = true;
		}

		if (trim($_POST['targetmonth']) == "") {
			$this -> CompulsoryMonthField = true;
		}
		$targetmonth = trim($_POST['targetmonth']);
		if (!is_numeric($targetmonth)) {
			$this -> TargetMonthInt = true;
		}

		if (trim($_POST['username']) == "") {
			$this -> CompulsoryUserNameField = true;
		}
	}

	// function to check email syntax in correct Format or not
	function isValidEmail() {

		if (!eregi("^([a-z0-9_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,4}$", $this -> email)) {
			$this -> validEmailError = true;
		}

	}

	// function to check whether username already exist
	function isDuplicateEmail() 
	{

		global $db;
		$utilityObj = new utility();
		$condition = "email='$this->email'";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		if(count($result) > 0) {
			$this -> duplicateEmail = true;
		}
	}

	public function CheckDuplicateEmail($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);
		return $result;

	}
	//Validate all info as well
	function validateUserInfo() {

		global $dbObj;

		if (trim($_POST['username']) == "") {
			$this -> CompulsoryUserNameField = true;

		}

		if (trim($_POST['password']) == "") {
			$this -> CompulsoryUserPasswordField = true;

		}

	}

	//To check UserName Exists with same name

	public function CheckDuplicateUser($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);
		return $result;

	}
	
	public function Delete_user($condition)
	 {
	 	global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(TABLE_USER, $condition));
		return $sql;
		
	
	 }

	// function to insert All Data As a Array User Information
	function UserDataInsert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(TABLE_USER, $dataArray));
		return DB_insertIdFunc();
	}

	//function to get an information of user or display all user by their conditions

	public function getOnGivenusername($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));

		$result = DB_fetchAssocFunc($sql);

		return $result;

	}
	
	public function getLevelwisemailstatus($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(LEVEL_MAIL_STATUS, $condition, ""));

		

		return $sql;

	}
	
	function LevelwisemailstatusInsert($dataArray) {

		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(LEVEL_MAIL_STATUS, $dataArray));
		return DB_insertIdFunc();
	}
	

	//function to update an information of user

	function UserDataUpdate($data, $condition) {
		global $db;
		$utilityObj = new utility();
		
		//print_r($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		return $sql;
	}

	function to_save_user_confidential_information($dataArray) {
		global $db;
		$utilityObj = new utility();
		if (empty($dataArray))
			return;
		$sql = $db -> query($utilityObj -> am_createInsertQuery(CONFIDENTIAL_INFORMATION, $dataArray));
		return DB_insertIdFunc();
	}

	public function getusername_like($condition) {
		
         global $db;
		 $utilityObj = new utility();
		 $mainArry = array();
		 $sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		 while ($result = DB_fetchArrayFunc($sql)) {
		 $mainArry[] = $result;

		}
		return $mainArry;

	}
	
	public function getOnId($id) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "id = '$id' and username!=''", ""));
		$result = DB_fetchAssocFunc($sql);

		return $result;
		
	}
	/*
	 * Total User Counting from table User
	 * Function
	 */
		public function gettingtotalUser() {
		
         global $db;
		 $utilityObj = new utility();
		 $mainArry = array();
		 $sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "", ""));
		 while ($result = DB_fetchArrayFunc($sql)) {
		 $mainArry[] = $result;

		}
		return $mainArry;

	}
		
		//Select all according to selection of Pro/Basic

	public function selectAllEmailData() {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$email = '';
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;

	}
   
    //filtering data according to type status of user
	public function AllUserType($userstatus) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, "status='$userstatus'", ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}
		return $mainArry;
	}
	
	// function to insert Activation Code in Tbl_user_verification
	public function UserVerifyInsertCode($data_verify_array) {
		
		global $db;
		$utilityObj = new utility();
		if (empty($data_verify_array))
			return;
		$sql = $db ->query($utilityObj ->am_createInsertQuery(USER_VERIFY_ACCOUNT,$data_verify_array));
		
		return DB_insertIdFunc();
	}
	
	public function Check_Verification_CodeUser($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj ->am_createSelectAllQuery(USER_VERIFY_ACCOUNT, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;

	}
	
	//RESET PASSWORD IF FORGOT IN LOGIN PAGE
	function updatePasswordByEmail($data, $condition) {

		global $db;
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(TABLE_USER, $data, $condition));
		return $sql;

	}

}