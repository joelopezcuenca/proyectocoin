<?php
/*
 * ###### Developer: Apar Sharma ##########
 *  * Classes for Achievenments/goal
 *  * by which he could define their goal
 *  * and could see whether he has achieved or not
 *  */
class goal {
	function goal() {
	}
	function add_goal($dataarray) {
		global $db;
		$utiltyobj = new utility();
		if (empty($dataarray))
			return false;
		$sql = $db -> query($utiltyobj -> am_createInsertQuery(BUCKETLIST_TABLE, $dataarray));
		return DB_insertIdFunc();
	}
	
	function add_Bucket_parent_child($dataarray) {
		global $db;
		$utiltyobj = new utility();
		if (empty($dataarray))
			return false;
		$sql = $db -> query($utiltyobj -> am_createInsertQuery(BUCKETLIST_PARENT_CHILD, $dataarray));
		return DB_insertIdFunc();
	}
	
	function select_goal($user_id) {
		global $db;
		$utilityObj = new utility();
		$result = array();
		$condition = "user_id='$user_id' and is_unlock=0";
		$orderBy = "id desc";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}
	
	public function DeleteBucket($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(BUCKETLIST_TABLE, $condition));
		return $sql;

	}
	public function DeleteBucketNotification($condition) {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createDeleteAllQuery(BUCKET_NOTIFICATION_TABLE, $condition));
		return $sql;

	}
	
	function search_child_parent($condition) {
		global $db;
		$utilityObj = new utility();
		$result = array();
		$orderBy = "";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_PARENT_CHILD, $condition, $orderBy));
		$rec = mysqli_fetch_assoc($sql) ;
		return $rec;
	}
	
	function select_all_community_goal() {
		global $db;
		$utilityObj = new utility();
		$result = array();
		
		
		$sql = $db -> query("select * from ".BUCKETLIST_TABLE." where id NOT IN(select new_bucket_id from ".BUCKETLIST_PARENT_CHILD.") and is_public=0");
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}
	
	function select_particular_goal($condition) {
		global $db;
		$utilityObj = new utility();
		$result = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}
	
	function unlock_goal($dataArray, $condition) {
		global $db;
		$result = array();
		$utilityObj = new utility();
		$sql = $db -> query($utilityObj -> am_createUpdateQuery(BUCKETLIST_TABLE, $dataArray, $condition));
		return $sql;
	}
	
	//to insert notification for unlocking any goal
	function insert_goal_notification($dataArray) {
		global $db;
		$utiltyobj = new utility();
		if (empty($dataArray))
			return false;
		$sql = $db -> query($utiltyobj -> am_createInsertQuery(BUCKET_NOTIFICATION_TABLE, $dataArray));
		return DB_insertIdFunc();
	}
	
	function insert_goal_images($dataArray) {
		global $db;
		$utiltyobj = new utility();
		if (empty($dataArray))
			return false;
		$sql = $db -> query($utiltyobj -> am_createInsertQuery(GOAL_IMG_TABLE, $dataArray));
		return DB_insertIdFunc();
	}
	
	
	function select_all_achieved_goal($user_id,$orderBy) {
		global $db;
		$utilityObj = new utility();
		$result = array();
	 $condition = "user_id='$user_id' and is_unlock=1";
		//$orderBy = "id desc limit 0,4";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		
		return $result;
	}
	
	//to Count achievemnets so far Users on Leadership board
	function User_Count_Achievements($condition) {
		
		
		//echo $condition;die;
		global $db;
		$utilityObj = new utility();
		$result = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, ""));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		
		return $result;
	}
	
	
	function User_Count_Achievementsmain($condition)
	{
		//echo $condition;die;
		global $db;
		$utilityObj = new utility();
		$result = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, ""));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		
		return array_unique($result);
	}
	
	
	function User_Count_Achievements_Second($condition) {
		
		
		global $db;
		$utilityObj = new utility();
		$result = array();
		
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, ""));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
		
	}
	
	//to show single image on digital passport section
	function select_visible_user_goal_image($user_id, $id) {
		global $db;
		$utilityObj = new utility();
		$result = array();
		$condition = "user_id='$user_id' and bucket_id=$id limit 1";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(GOAL_IMG_TABLE, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;
	}
	
	//to show images on digital passport section in detail popup
	function select_all_user_goal_images($user_id, $id) {
		global $db;
		$utilityObj = new utility();
		$result = array();
		$condition = "user_id='$user_id' and bucket_id=$id";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(GOAL_IMG_TABLE, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}
	
	//function to check that is user is following atleast 1
	function is_having_any_goal($user_id,$condition) {
		global $db;
		$utilityObj = new utility();
		$result = array();
		//$condition = "user_id='$user_id' and bucket_id=$id";
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;
	}
	
	//when selecting a goals from Autosuggestion Box
	function Select_Goal_Details($condition) {
		global $db;
		$utilityObj = new utility();
		$result = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(BUCKETLIST_TABLE, $condition, $orderBy));
		while ($rec = mysqli_fetch_array($sql)) {
			$result[] = $rec;
		}
		return $result;
	}

}
?>