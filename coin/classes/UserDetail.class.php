<?php
class UserDetail {
	public function UserDetail() {
	}

	//All User Who has been subscribed free or paid user Pro user

	public function SelectAllProUser() {

		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$condition ="status=1 and paypalpayment_status='Completed'" ;
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}
		return $mainArry;

	}
	
	
	public function SelectAllBasicUser()
	{
		
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$condition ="status=0 and Admin_Role='' and url!=''"   ;
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;
		
		
	}
	
	public function SelectDetailUserById($condition) {
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		$result = DB_fetchAssocFunc($sql);
		return $result;
	}
	
	public function SelectAllSuspendedUser()
	
	{
		global $db;
		$utilityObj = new utility();
		$mainArry = array();
		$condition ="status='-1' and Account_status='suspended'"   ;
		$sql = $db -> query($utilityObj -> am_createSelectAllQuery(TABLE_USER, $condition, ""));
		while ($result = DB_fetchArrayFunc($sql)) {
			$mainArry[] = $result;

		}

		return $mainArry;
		
	}
	
}
?>