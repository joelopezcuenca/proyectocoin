<?php
require_once('Connections/Midash.php'); 
require_once('common_data.php');
$QueryObj = new Query;

#Courses List
$selectQuery = $QueryObj->select(REGISTERED_L, " *, count(`cref`) " , "cnum ='".$row_LoggedInUserDetails['centre_number']."' and cref != ''", "cref",  "count(cref) desc");
$runQuery = $QueryObj->run_query($selectQuery);
$fetchQuery  = $QueryObj->fetch($runQuery);

#Withdrawn Learners List
	$withdrawnQuery = $QueryObj->select(REGISTERED_L, " *, count(cname) " , "skip = '1' and cnum = '".$row_LoggedInUserDetails['centre_number']."' and cname != '' " , "cname" );
	$certiQuery_run = $QueryObj->run_query($withdrawnQuery);
	$certiQuery_fetch = $QueryObj->fetch($certiQuery_run);
	if(!empty($certiQuery_fetch)){
		$withdrawn_L_Arr[] = $certiQuery_fetch;
	}
//}
#Registered Learners List
$learnerQuery = $QueryObj->select(REGISTERED_L, " *, count(cref) " , "cnum ='".$row_LoggedInUserDetails['centre_number']."'" ,"cref" );
$learnerQuery_run = $QueryObj->run_query($learnerQuery);
$learnerQuery_fetch = $QueryObj->fetch($learnerQuery_run);


#Certificates Requested List
$certiQuery = $QueryObj->select(REQUESTED_C, " * ,count(cname)" , "cnum ='".$row_LoggedInUserDetails['centre_number']."'" , "cname" );
$certiQuery_run = $QueryObj->run_query($certiQuery);
$certiQuery_fetch = $QueryObj->fetch($certiQuery_run);

$k = $fetchQuery/10 ;


for($i=0; $i<10; $i++)
{
?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="js/jspdf.min.js"></script>
<script type="text/javascript" src="js/canvg.js"></script>
<script type="text/javascript" src="js/rgbcolor.js"></script>
<script type="text/javascript">
var doc = new jsPDF();
function getImgData(chartContainer) {
        var chartArea = chartContainer.getElementsByTagName('svg')[0].parentNode;
        var svg = chartArea.innerHTML;
        var chartDoc = chartContainer.ownerDocument;
        var canvas = chartDoc.createElement('canvas');
        canvas.setAttribute('width', (chartArea.offsetWidth * 5));
        canvas.setAttribute('height', (chartArea.offsetHeight * 5));
        
        
        canvas.setAttribute(
            'style',
            'position: absolute; ' +
            'top: ' + (-chartArea.offsetHeight * 2) + 'px;' +
            'left: ' + (-chartArea.offsetWidth * 2) + 'px;');
        chartDoc.body.appendChild(canvas);
        canvg(canvas, svg);
        var imgData = canvas.toDataURL('image/JPEG');
        doc.addImage(imgData, "JPEG", 10,10, 200,110); 
        canvas.parentNode.removeChild(canvas);
        return imgData;
      }

$( document ).ready(function() {

    $('#pdfBtn').click(function () {   
        var chartContainer = document.getElementById('chart_div');
        var chartDoc = chartContainer.ownerDocument;
        var img = chartDoc.createElement('img');
        img.src = getImgData(chartContainer);
        doc.save('all_course_vs_learners_stats.pdf');
    });
        
});



google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
  	
    ['Courses', 'Total Learners Registered',{ role: 'annotation' }, 'Certificates Requested', { role: 'annotation' }, 'Learners Withdrawn', { role: 'annotation' }],
   
  <?php  if(empty($_REQUEST['token'])) { for($i=0; $i<$ikcount; $i++) { ?>
  	
['<?php echo $course_popular[$i]['cname']; ?>',<?php echo $reg_l_f_a[$i]['count(cref)']; ?>,<?php echo $reg_l_f_a[$i]['count(cref)'];  ?>, <?php echo $course_cert_req[$i]['count(`cname`)']; ?>,<?php echo $course_cert_req[$i]['count(`cname`)']; ?>, <?php if($withdraw_l_a[$i]['count(cname)'] != 0){ echo $withdraw_l_a[$i]['count(cname)']; } else { echo "0"; }?>,<?php if($withdraw_l_a[$i]['count(cname)'] != 0) { echo $withdraw_l_a[$i]['count(cname)']; } else { echo "0"; }?>],
<?php  } }
else { 

 ?>
	['<?php echo $bar_query_f['qualification_name']; ?>', <?php if($reg_l_q_f['count(cref)'] != 0) { echo $reg_l_q_f['count(cref)']; } else { echo "0"; } ?>,<?php if($reg_l_q_f['count(cref)'] != 0) { echo $reg_l_q_f['count(cref)']; } else { echo "0"; } ?>,<?php if($cert_req_q_f_p['count(`cname`)'] != 0) { echo $cert_req_q_f_p['count(`cname`)']; } else { echo "0"; } ?>,<?php if($cert_req_q_f_p['count(`cname`)'] != 0) { echo $cert_req_q_f_p['count(`cname`)']; } else { echo "0"; } ?>,<?php if($withdraw_l_q_f_p['count(cname)'] != 0) { echo $withdraw_l_q_f_p['count(cname)']; } else { echo "0"; } ?>,<?php if($withdraw_l_q_f_p['count(cname)'] != 0) {  echo $withdraw_l_q_f_p['count(cname)']; } else { echo "0"; } ?> ],
	 

<?php } ?>
  ]);


  var options = {
    title: 'Course vs Total Learners Registered',
    hAxis: {title: 'Courses', titleTextStyle: {color: 'red'}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));

  chart.draw(data, options);
}


    </script>
    
<div style="width:900px;">
<div style="float:left;"><button id="pdfBtn" class="green-btn">Download All Charts in PDF</button></div>
</div>
<div id="chart_div" style="width: 900px; height: 600px; "></div>

<?php } ?>