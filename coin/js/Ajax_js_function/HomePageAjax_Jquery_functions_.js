/*Coder :Raghwendra Pathak
*All Ajax Js Customs Functions
*
*/
//Home Pagination Data
function displayRecords(numRecords, pageNum) {
	$.ajax({
		type : "GET",
		url : "ajax/pagination/home_pagination_Ajax.php",
		data : "show=" + numRecords + "&pagenum=" + pageNum,
		cache : false,
		beforeSend : function() {
			$('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
		},
		success : function(html) {
			$("#results").html(html);
		}
	});
}

/*######Functions for  a posts#################*/

function post_entry(user_id) {
	var current_post = $("#current_post").val();
	$.post("ajax/user_post_ajax.php", {
		user_id : user_id,
		current_post : current_post
	}).done(function(data) {
		$('#current_post').attr({
			value : ''
		});

		$("#all_news").html(data);
	});
}




/** function to call inactive function to inactive current seen message    **/
function inactive(notify, referral, user, identifier) {

	$.post("ajax/change_notification.php", {
		notify : notify,
		referral : referral,
		user : user,
		identifier : identifier
	}, function(data) {

	});
}

/* *  Method for adding likes */

/*###### LIKE POSTS #################*/

function post_like(user_id, post_id, action, spanId) {
	$.post("ajax/user_post_like_comment_ajax.php", {
		user_id : user_id,
		post_id : post_id,
		action : action,
		spanId : spanId
	}).done(function(data) {
		$("#" + spanId).html(data);
	});
}/**Method for showing comment box */

function show_box(id) {

	$("#comment" + id).css("display", "block");
	$("#new_comment" + id).removeAttr("disabled");
}/** Method for saving comment */

/*########### SAVING COMMENTS #################*/

function save_comment(e, comment, user_id, post_id, id) {

	if (e.keyCode == 13) {

		var save_comment = 'save_comment';
		var spanId = 'user_comment' + id;
		var textId = 'comment' + id;
		$("#new_comment" + id).attr("disabled", "disabled");
		$.post("ajax/user_post_like_comment_ajax.php", {
			user_id : user_id,
			spanId : spanId,
			post_id : post_id,
			action : save_comment,
			comment : comment
		}).done(function(data) {
			$("#" + textId).css("display", "none");
			$("#" + spanId).html(data);
			$("#new_comment" + id).val("");

		});

	} else {
	}
}

/*######### DELETING A COMMENTS #################*/

function delete_comment(commentId, spanId, post_id, user_id) {
	
	var r = confirm("Are you sure want to delete this comment");
	if (r == true) {
	hide_comment(commentId);
	var delete_comment = 'delete_comment';
	var spanId = spanId;
	$.post("ajax/user_post_like_comment_ajax.php", {
		commentId : commentId,
		post_id : post_id,
		spanId : spanId,
		action : delete_comment,
		user_id : user_id

	}).done(function(data) {
		$("#" + spanId).html(data);
	});
	}
	else{
		return false;
	}

}

function hide_comment(id) {
	$("#comment" + id).css("display", "none");
}/* * Method for saving comment */




