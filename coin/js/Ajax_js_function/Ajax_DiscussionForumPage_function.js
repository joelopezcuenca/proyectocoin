/*########### DISCUSSION TOPIC COMMENTS ON FORUM SECTION #################*/

function savediscussion_comment(e, commenttext, userid, discussionid, spanid) {

	if (e.keyCode == 13) {

		var save_discussion = 'savediscussion_comment';
		var spanId = 'user_comment' + spanid;
		var spantextId = 'comment' + spanid;
		var userid = userid;
		var comment = commenttext;
		var discussionid = discussionid;

		$("#new_comment" + spanid).attr("disabled", "disabled");
		$.post("ajax/Discussion_comment_ajax.php", {
			user_id : userid,
			commenttext : commenttext,
			discussionid : discussionid,
			action : save_discussion

		}).done(function(data) {

			//alert(data);
			$("#" + spantextId).css("display", "none");
			$("#" + spanId).html(data);
			$("#new_comment" + spanid).val("");

		});

	}

}

//delete forum section comments

function deleteForum_comment(commentId, spanId, topic_id, user_id) {

	var r = confirm("Are you sure want to delete this comment");
	if (r == true) {

		hide_comment(commentId);
		var delete_comment = 'delete_comment';
		var spanId = spanId;
		$.post("ajax/Discussion_comment_ajax.php", {
			commentId : commentId,
			topic_id : topic_id,
			spanId : spanId,
			action : delete_comment,
			user_id : user_id

		}).done(function(data) {
			
			$("#" + spanId).html(data);
		});

	} else {
		return false;
	}
}

function hide_comment(id) {
	$("#comment" + id).css("display", "none");
}/* * Method for saving comment */

function categorize(type_search)
{
//alert('me this is in category search');
	//alert(type_search);
	$.post("ajax/ajax_DiscussionForum_category.php",{action:type_search}).done(function(data){
		
		
		$("#ajax_data_replacing").html(data);
	});
}
