/*Coder :Raghwendra Pathak
*All Ajax Js Customs Functions
*
*/
//Home Pagination Data
function displayRecords(numRecords, pageNum) {
	$.ajax({
		type : "GET",
		url : "http://www.proyectocoin.com/coin/modules/home/ajax/pagination/home_pagination_Ajax.php",
		data : "show=" + numRecords + "&pagenum=" + pageNum,
		cache : false,
		beforeSend : function() {
			$('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
		},
		success : function(html) {
			$("#results").html(html);
		}
	});
}

/*######Functions for  a posts#################*/

function post_entry(user_id) {
	var current_post = $("#current_post").val();
	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/user_post_ajax.php", {
		user_id : user_id,
		current_post : current_post
	}).done(function(data) {
		$('#current_post').attr({
			value : ''
		});

		$("#all_news").html(data);
		
		$valuetotalcount = $('#newstotal').val();
		
		for ( $i = 6; $i < $valuetotalcount; $i++) {
			$('#news' + $i).hide();
		}
		$i = 7;
		$j = 0;
		$k = 0;
		$l = 0;
		$('#loadmore').click(function() {

			$valuetotalcount = $('#newstotal').val();
			$('#news' + $i).show("85000");
			$j = $i + 1;
			$('#news' + $j).show("85000");
			$k = $j + 1;
			$('#news' + $k).show("85000");
			$l = $k + 1;
			$('.hidethis').hide()
			$('#news' + $l).show("85000");

			$i = $i + 4;
			$j++;
			$k++;
			$l++;
			$('.hidethis').show();
			if (($i == $valuetotalcount) || ($j == $valuetotalcount) || ($k == $valuetotalcount) || ($l == $valuetotalcount)) {
				$('.hidethis').hide();
			}
			return false;
		});
	});
}




/** function to call inactive function to inactive current seen message    **/
function inactive(notify, referral, user, identifier) {

	$.post("ajax/change_notification.php", {
		notify : notify,
		referral : referral,
		user : user,
		identifier : identifier
	}, function(data) {

	});
}

/* *  Method for adding likes */

/*###### LIKE POSTS #################*/

function post_like(user_id, post_id, action, spanId) {
	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/user_post_like_comment_ajax.php", {
		user_id : user_id,
		post_id : post_id,
		action : action,
		spanId : spanId
	}).done(function(data) {
		$("#" + spanId).html(data);
	});
}/**Method for showing comment box */

function show_box(id) {

	$("#comment" + id).css("display", "block");
	$("#new_comment" + id).removeAttr("disabled");
}/** Method for saving comment */

/*########### SAVING COMMENTS #################*/

function save_comment(e, comment, user_id, post_id, id) {

	if (e.keyCode == 13) {

		var save_comment = 'save_comment';
		var spanId = 'user_comment' + id;
		var textId = 'comment' + id;
		$("#new_comment" + id).attr("disabled", "disabled");
		$.post("http://www.proyectocoin.com/coin/modules/home/ajax/user_post_like_comment_ajax.php", {
			user_id : user_id,
			spanId : spanId,
			post_id : post_id,
			action : save_comment,
			comment : comment
		}).done(function(data) {
			$("#" + textId).css("display", "none");
			$("#" + spanId).html(data);
			$("#new_comment" + id).val("");

		});

	} else {
	}
}

/*######### DELETING A COMMENTS #################*/

function delete_comment(commentId, spanId, post_id, user_id) {
	
	var r = confirm("Are you sure want to delete this comment");
	if (r == true) {
	hide_comment(commentId);
	var delete_comment = 'delete_comment';
	var spanId = spanId;
	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/user_post_like_comment_ajax.php", {
		commentId : commentId,
		post_id : post_id,
		spanId : spanId,
		action : delete_comment,
		user_id : user_id

	}).done(function(data) {
		$("#" + spanId).html(data);
	});
	}
	else{
		return false;
	}

}

function inactive_msg(identifier, id, notify, user,referral,ptag_id) {
	
	
	//alert("i am  heree");
	
	var identifier0count = $("#identifier0").val() ;
	var identifier1count = $("#identifier1").val() ;
	var identifier2count = $("#identifier2").val() ;
	var identifier3count = $("#identifier3").val() ;
	var identifier4count = $("#identifier4").val() ;
	var identifier5count = $("#identifier5").val() ;
	var identifier6count = $("#identifier6").val() ;
	var identifier7count = $("#identifier7").val() ;
	var identifier8count = $("#identifier8").val() ;
	var identifier9count = $("#identifier9").val() ;
	var identifier10count = $("#identifier10").val() ;
	var total_count = $("#totalcount_hidden").val() ;
	if(identifier)
		{
			var ptag = "#ptag"+identifier+"_"+ptag_id ;
			
			$(ptag).hide() ;
		switch(identifier)	
		{
			
			case "0":
			identifier0count = identifier0count - 1 ;
			if(identifier0count<=0)
			{
				$("#identifier0_div").hide();
			}
			$("#identifier1").val(identifier0count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			
			case "1":
			identifier1count = identifier1count - 1 ;
			if(identifier1count<=0)
			{
				$("#identifier1_div").hide();
			}
			$("#identifier1").val(identifier1count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			case "2":
			identifier2count = identifier2count - 1 ;
			if(identifier2count<=0)
			{
				$("#identifier2_div").hide();
			}
			$("#identifier2").val(identifier2count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			case "3":
			identifier3count = identifier3count - 1 ;
			if(identifier3count<=0)
			{
				
				$("#identifier3_div").hide();
			}
			$("#identifier3").val(identifier3count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			case "4":
			identifier4count = identifier4count - 1 ;
			if(identifier4count<=0)
			{
				$("#identifier4_div").hide();
			}
			$("#identifier4").val(identifier4count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			case "5":
			identifier5count = identifier5count - 1 ;
			if(identifier5count<=0)
			{
				$("#identifier5_div").hide();
			}
			$("#identifier5").val(identifier5count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			
			case "6":
			
			
			identifier6count = identifier6count - 1 ;
			if(identifier6count<=0)
			{
				$("#identifier6_div").hide();
			}
			$("#identifier6").val(identifier6count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			case "7":
			
			
			identifier7count = identifier7count - 1 ;
			if(identifier7count<=0)
			{
				$("#identifier7_div").hide();
			}
			$("#identifier7").val(identifier7count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			
			
			case "8":
			
			identifier8count = identifier8count - 1 ;
			if(identifier8count<=0)
			{
				$("#identifier8_div").hide();
			}
			$("#identifier8").val(identifier8count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			
			case "9":
			
			identifier9count = identifier9count - 1 ;
			if(identifier9count<=0)
			{
				$("#identifier9_div").hide();
			}
			$("#identifier9").val(identifier9count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			
			case "10":
			
			identifier10count = identifier10count - 1 ;
			if(identifier10count<=0)
			{
				$("#identifier10_div").hide();
			}
			$("#identifier10").val(identifier10count) ;
			total_count = total_count - 1 ;
			$("#totalcount_hidden").val(total_count) ;
			$("#total_count").html(total_count) ;
			break ;
			
			
		}
		}
	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/change_notification_ajax.php", {
		notify : notify,
		referral_c : referral,
		user : user,
		identifier : identifier

	}).done(function(data) {
		
		//window.location.href="http://www.proyectocoin.com/coin/modules/home/home.php";
});


}

function hide_comment(id) {
	$("#comment" + id).css("display", "none");
}/* * Method for saving comment */


// function getsecretcode(id,email)
// {
	// alert('thankyou Please Check your Email again');
	// $.ajax({
		// type : "GET",
		// url : "ajax/user/getverificationcode_again.php",
		// data : "userid=" + id + "&email=" + email,
		// cache : false,
		// beforeSend : function() {
			// $('#content').html('<img src="loader.gif" alt="" width="24" height="24" style=" padding-left:469px;">');
		// },
		// success : function(html) {
			// //$("#results").html(html);
// 			
			// alert('thankyou Please Check your Email again');
		// }
	// });
// }

