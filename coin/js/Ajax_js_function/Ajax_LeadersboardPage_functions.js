/*
 * LEADERSHIP BOARD ALL CUSTOM JS AND AJAX FUNCTIONS
 * CUSTOM FUNCTION DEFINED HERE
 */

/* SEARCHING A FRIENDS VIA AUTOSUGGESTIONS BOX */

function suggest(inputString) {
	if ((inputString.length == 0) || (inputString == '')) {
		$('#display_box').fadeOut();
		//for fading whole div if nothing pressed on key!!
		$('#display').hide();
		//or hide whole div

	} else {
		$.ajax({
			url : "http://www.proyectocoin.com/coin/modules/home/ajax/autosuggestion_ajax.php",
			data : 'act=autoSuggestUser&queryString=' + inputString,
			success : function(msg) {
				if (msg.length > 0) {
					$("#display").html(msg).show();

				}
			}
		});
	}
}

/*######### FOLLOW A FRIENDS #####*/

function follow_someone(userid, followers_id) {

	var followfriend = 'follow';
	var userId = userid;
	var followersId = followers_id;
	var replacementid = replacementid;

	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/user_follow_ajax.php", {
		followersid : followersId,
		action : followfriend,
		user_id : userId,
		replacementid : replacementid
	}).done(function(data) {
		//$('#follow' + replacementid).replaceWith(data);
		
		$('#followmee').html(data);
	});

}

function Hide_Follow() {
	$('#follow').hide();
	$('#unfollow').show();

}

function Show_Follow() {

	$('#unfollow').hide();
	$('#follow').show();
}

function unfollow_someone(userid, followers_id) {

	var unfollowfriend = 'unfollowfriend';
	var userId = userid;
	var replacementid = replacementid;
	var followersId = followers_id;

	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/user_follow_ajax.php", {
		followersid : followersId,
		action : unfollowfriend,
		user_id : userId,
		replacementid : replacementid
	}).done(function(data) {
	//alert(data);
		$('#followmee').html(data);
	});

}


function follow_someone_leaderboard(userid, followers_id,replacementid) {

	var followfriend = 'followfriend_leaderboard';
	var userId = userid;
	var followersId = followers_id;
	var replacementid = replacementid;

	$.post("ajax/user_follow_ajax.php", {
		followersid : followersId,
		action : followfriend,
		user_id : userId,
		replacementid : replacementid
	}).done(function(data) {
		
	  $('#follow' +replacementid).replaceWith(data);
	});

}



function unfollow_someone_leaderboard(userid, followers_id,replacementid) {

	var unfollowfriend = 'unfollowfriend_leaderboard';
	var userId = userid;
	var replacementid = replacementid;
	var followersId = followers_id;

	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/user_follow_ajax.php", {
		followersid : followersId,
		action : unfollowfriend,
		user_id : userId,
		replacementid : replacementid
	}).done(function(data) {
		
	$('#unfollow' +replacementid).replaceWith(data);

	});

}

//
function categorize_leaderboard(type_search) {

	$.post("http://www.proyectocoin.com/coin/modules/home/ajax/ajax_leaderboard_category.php", {
		action : type_search
	}).done(function(data) {
		// alert(data);

		$("#leaderboard_data").html(data);
		$leaderboard_count = $('#leaderboard_count').val();
		for ( $i = 5; $i < $leaderboard_count; $i++) {
			$('#leaderboard_div' + $i).hide();
		}
		$i = 5;
		$('#loadmore').click(function() {
			$leaderboard_count = $('#leaderboard_count').val();
			for ( $j = $i; $j < ($i + 4); $j++) {
				$('#leaderboard_div' + $j).show("75000");
			}
			$i = $i + 4;
			if ($i >=$leaderboard_count) {
				$('.hidethis').hide();
			}
		});
	});
}

/*END SECTIONS OF FOLLOW OR UNFOLLOW TO SOMEONE*/