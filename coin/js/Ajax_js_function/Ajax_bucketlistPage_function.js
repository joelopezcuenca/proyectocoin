/*
 *All Ajax Js Customs Functions
 *for Bucket List Page
 */
function Show_BucketList(InputString) {
	
	//alert(InputString);
	
	if (InputString.length == 0) {
		$('#display_box').fadeOut();
		//for fading whole div if nothing pressed on key!!
		$('#display').hide();
		//or hide whole div
	} else {
		$.ajax({
			url : "ajax/autosuggestion_ajax.php",
			data : 'action=showbucketlist&String=' + InputString,

			success :function(msg) {
				
				//alert(msg);

				if (msg.length > 0) {
					$("#display").html(msg).show();

				}
			}
		});
	}
}
function categorization_goal(inputstring)
{
	
	$.post("ajax/bucketlist_ajax_category.php",{
		action:inputstring
	}).done(function(data){
		
		$("#goal_main_div").html(data);
		$bucket_count = $('#bucketcount').val() ;

		for($i=6;$i<$bucket_count;$i++)

		{

			$('#bucket_div_'+$i).hide() ;

		}

		$i =6 ;

		$('#loadmore').click(function(){

			$bucket_count = $('#bucketcount').val() ;

			for($j=$i;$j<($i+6);$j++)

			{

				$('#bucket_div_'+$j).show("75000") ;

			}

			$i = $i + 6 ;

			if($i>=$bucket_count)

			{

				$('.hidethis').hide() ;

			}

		});
	});
}
