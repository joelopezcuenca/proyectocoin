/*########### DISCUSSION TOPIC COMMENTS ON FORUM SECTION #################*/
function save_G_discussion_comment(e, commenttext, userid, discussionid, spanid) {
	if (e.keyCode == 13) {
		var save_discussion = 'savediscussion_comment';
		var spanId = 'user_comment' + spanid;
		var spantextId = 'comment' + spanid;
		var userid = userid;
		var comment = commenttext;
		var discussionid = discussionid;
		$("#new_comment" + spanid).attr("disabled", "disabled");
		$.post("ajax/G_Discussion_comment_ajax.php", {
			user_id : userid,
			commenttext : commenttext,
			discussionid : discussionid,
			r_id : spanid,
			action : save_discussion
		}).done(function(data) {
			//alert(data);
			$("#" + spantextId).css("display", "none");
			$("#" + spanId).html(data);
			$("#new_comment" + spanid).val("");
		});
	}
}//delete forum section comments
function delete_G_Forum_comment(commentId, spanId, topic_id, r_id, user_id) {
	//alert(commentId+"/"+spanId+"/"+topic_id+"/"+r_id+"/"+user_id) ;
	//alert ("bgih") ;
	var r = confirm("Are you sure want to delete this comment");
	if (r == true) {
		hide_comment(commentId);
		var delete_comment = 'delete_comment';
		var spanId = spanId;
		$.post("ajax/G_Discussion_comment_ajax.php", {
			commentId : commentId,
			topic_id : topic_id,
			spanId : spanId,
			action : delete_comment,
			user_id : user_id,
			r_id : r_id
		}).done(function(data) {
			//alert(data) ;
			$("#" + spanId).html(data);
		});
	} else {
		return false;
	}
}function hide_comment(id) {
	$("#comment" + id).css("display", "none");
}/* * Method for saving comment */function G_categorize(type_search) {
	$.post("ajax/ajax_generaldiscussion_category.php", {		action : type_search	}).done(function(data) {
		$("#ajax_data_replacing").html(data);
		$topic_count = $('#topic_count').val();
		for ( $i = 4; $i < $topic_count; $i++) {
			$('#topic_div_' + $i).hide();
		}
		$i = 4;
		$('#loadmore').click(function() {
			$topic_count = $('#topic_count').val();
			for ( $j = $i; $j < ($i + 3); $j++) {
				$('#topic_div_' + $j).show("85000");
			}
			$i = $i + 3;
			if ($i >= $topic_count) {
				$('.hidethis').hide();
			}
		});
	});
}
