/*
 Google Analytical Code
 ###########################################
 */
(function(i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] ||
	function() {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o), m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
ga('create', 'UA-48279747-1', 'comunidadcoin.com');
ga('send', 'pageview');

/*#########################################
 * BUCKET LIST FUNCTIONS FOR CONTROLLING
 *#########################################
 */

$(document).ready(function(){
	
	$("#login").validationEngine('attach');
	$("#forgot_password_form").validationEngine('attach');
	
	$(".form-search").removeClass("index2");
	$("img.tweet").removeClass( "index3");
	$("img.fb").removeClass("index4");
	
	$("#add_goal_form").validationEngine({promptPosition : "centerRight", scroll: true});
	$("#unlock_goal_form").validationEngine();

	$bucket_count = $('#bucketcount').val() ;
		for($i=6;$i<$bucket_count;$i++)
		{
			$('#bucket_div_'+$i).hide() ;
		}
		$i =6 ;
		$('#loadmorebucket').click(function(){
			$bucket_count = $('#bucketcount').val() ;
			for($j=$i;$j<($i+4);$j++)
			{
				$('#bucket_div_'+$j).show("75000") ;
			}
			$i = $i + 4 ;
			if($i>=$bucket_count)
			{
				$('.hidethis').hide() ;
			}
		});
		
	$(function() {
    $('#fbclick').click(function(){
        console.log(1);
        $('#share_unocked_goal').modal('hide');
        
       
        
       window.location = "mi_prefil.php";
       // window.location = "http://localhost/coin/modules/home/bucket_list.php";
        
        
    });
});

 $(".inline").colorbox({inline:true, width:"50%"});

//for aweber form
$("#aweber").validationEngine('attach');

$bucket_count = $('#achievement_count').val() ;
		for($i=8;$i<$bucket_count;$i++)
		{
			$('#achieve_div_'+$i).hide() ;
		}
		$i =8 ;
		$('#loadmore_digitalpassport').click(function(){
			$bucket_count = $('#achievement_count').val() ;
			for($j=$i;$j<($i+8);$j++)
			{
				$('#achieve_div_'+$j).show("75000") ;
			}
			$i = $i + 8 ;
			if($i>=$bucket_count)
			{
				$('.hidethis').hide() ;
			}
		});
		
		//home page news feed
		$valuetotalcount = $('#newstotal').val();
		for ( $i = 5; $i < $valuetotalcount; $i++) {
			$('#news' +$i).hide();
		}
		$i = 5;
		$('#loadmoreany').click(function() {
			$valuetotalcount = $('#newstotal').val();
			for ( $j = $i; $j < ($i + 4); $j++) {
				$('#news' + $j).show("75000");
			}
			$i = $i + 4;
			if ($i >= $valuetotalcount) {
				$('.hidethis').hide();
			}
		});
		
		
		//leadreboard page next user showing
		$leaderboard_count = $('#leaderboard_count').val() ;
		for($i=5;$i<$leaderboard_count;$i++)
		{
			$('#leaderboard_div'+$i).hide() ;
		}
		$i =5 ;
		$('#loadmoreleaderboard').click(function(){
			$leaderboard_count = $('#leaderboard_count').val() ;
			for($j=$i;$j<($i+4);$j++)
			{
				$('#leaderboard_div'+$j).show("75000") ;
			}
			$i = $i + 4 ;
			if($i>=$leaderboard_count)
			{
				$('.hidethis').hide() ;
			}
		});
	

		
});


function submitForm() {

	$("#aweber").submit();
	}


function HideDivFirstShowSecond() {
	$('#blackoverlay-first').hide();
	$('#blackoverlay-second').show();
//	$(".form-search").addClass("index2");
	$("img.tweet").addClass("index3");
	$("img.fb").addClass("index4");
	$("div").removeClass( "index");
	$(".pasport h6").removeClass( "index");
	$("h1.name3").removeClass( "index");
}

function HideDivSecondAndShowThird() {
	$('#blackoverlay-second').hide();
	$('#blackoverlay-third').show();
	$(".form-search").removeClass("index2");
	$("img.tweet").removeClass( "index3");
	$("img.fb").removeClass("index4");
	$("div").removeClass( "index");
	$(".pasport h6").removeClass( "index");
	$("h1.name3").removeClass( "index");
}

function HideDivThirdAndlast() {
	$('#blackoverlay-third').hide();
	$('#blackoverlay-last').show();
}

function close_yellowdiv() {
	$(".yellowbg").hide();
	$(".cover-page").show("slow");
}

function hidemodal() {
	$('#coinbucketvideo').modal('hide');
}

function hidediv()
{
	$('.hidethis').hide();
}
//##############LOGIN FORM AND RESET FORM######################
