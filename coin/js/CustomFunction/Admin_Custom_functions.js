$(document).ready(function() {

	/*#############################
	 COMMISSION REPORT
	 ############################
	 */
	$('#example').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 25,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",
		"aoColumns" : [{
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]
	});

	/*#############################
	 USER LOG DATATABLES
	 ############################
	 */
	$('#userlogform_new').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 100,
		"aaSorting" : [[0,"asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",

		
	});
	
	$('#payment_report').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 100,
		"aaSorting" : [[0,"desc"]],
		"bLengthChange" : true,
		"bPaginate" : false,
		"sPaginationType" : "full_numbers",
		

		
	});
	
	$('#payment_report_user_list').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 100,
		"aaSorting" : [[0,"desc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",
	
		
	});
	
	$('#user_database_list').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 100,
		"aaSorting" : [[0,"desc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",
	
		
	});

	$('#program_password_list').dataTable({
		"bJQueryUI" : false,
		"bFilter" : false,
		"iDisplayLength" : 100,
		"aaSorting" : [[0,"desc"]],
		"bLengthChange" : false,
		"bPaginate" : false,
		"sPaginationType" : "full_numbers",
		"bInfo": false,
	
		
	});

	
	/*#############################
	 TUTORIAL REPORT
	 ############################
	 */

	$('#tutorial_report').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 25,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",

		"aoColumns" : [{
			"sWidth" : "auto"
		}, {
			"sWidth" : "auto"
		}, {
			"sWidth" : "auto",
			"bSortable" : false
		}, {
			"sWidth" : "auto",
			"bSortable" : false
		}]
	});

	/*#############################
	 PRO USER MANAGER REPORT
	 ############################
	 */

	$('#prousermanager').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 25,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",

		"aoColumns" : [{
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]

	});

	/*###############################
	 REPORTING PER LEVEL WISE REPORT
	 #################################
	 */
	$('#report_main').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 25,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",
		"aoColumns" : [{
			"sWidth" : "auto"
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto"
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]
	});

	/*#############################
	 FAQ REPORT
	 ############################
	 */

	$('#faqreport').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 25,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",
		"aoColumns" : [{
			"sWidth" : "auto"
		}, {
			"sWidth" : "auto"
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]
	});

	/*#############################
	 MISSION REPORT
	 ############################
	 */

	$('#mission').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 5,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",

		"aoColumns" : [{
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]
	});

	$(".inline").colorbox({
		inline : true,
		width : "50%"
	});

	/*#############################
	 DISCUSSION REPORT
	 ############################
	 */

	$('#discussionreport').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 5,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",

		"aoColumns" : [{
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]
	});

	/*#############################
	 BASIC USERMANAGER REPORT
	 
	 ############################
	 */

	$('#basicmanagerreport').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 10,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",

		"aoColumns" : [{
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]
	});
	
	
	$('#paymentcancelrecord').dataTable({
		"bJQueryUI" : true,
		"bFilter" : true,
		"iDisplayLength" : 25,
		"aaSorting" : [[0, "asc"]],
		"bLengthChange" : true,
		"bPaginate" : true,
		"sPaginationType" : "full_numbers",
		"aoColumns" : [{
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}, {
			"sWidth" : "auto",
			"bSortable" : true
		}]
	});
	
	
	$('#payment_withdraw').dataTable({
			"bJQueryUI" : true,
			"bFilter" : true,
			"iDisplayLength" : 25,
			"aaSorting" : [[0, "asc"]],
			"bLengthChange" : true,
			"bPaginate" : true,
			"sPaginationType" : "full_numbers",

			"aoColumns" : [{
				"sWidth" : "auto",
				"bSortable" : true
			}, {
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			}]
		});
		
		
		/* #########################
		 * Payment Approved Data Tables
		 ############################# 
		 */ 
		
		$('#payment_approved').dataTable({
			"bJQueryUI" : true,
			"bFilter" : true,
			"iDisplayLength" : 25,
			"aaSorting" : [[0, "asc"]],
			"bLengthChange" : true,
			"bPaginate" : true,
			"sPaginationType" : "full_numbers",

			"aoColumns" : [{
				"sWidth" : "auto",
				"bSortable" : true
			}, {
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			},{
				"sWidth" : "auto",
				"bSortable" : true
			}]
		});
		

});
 
/*#############################
 TUTORIAL REPORT
 for closing video in tutorial/mission report
 ############################
 */

function CloseVideo(id) {
	$(document).ready(function() {
		$('#popifrm' + id + ' iframe').attr('src', $('#popifrm' + id + ' iframe').attr('src'));
	});
}
/*#############################
 this is for permission area for
 admin area!!
 ############################
 */
function fun() {
		alert("Sorry You Can not give Permissions");
	}

