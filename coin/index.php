<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5VSQRN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5VSQRN');</script>
<!-- End Google Tag Manager -->
<?php 
header('Location: modules/home/aweber.php');
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Coin</title>
<meta property="og:image" content="http://www.comuniadcoin.com/coin/images/coin_fb_image.jpg" />
<meta property="og:description" content="Descubre como Vivir una Vida llena de Libertad, Diversión y Aventura. Únete al movimiento! " />
<link href="http://comunidadcoin.com/coin/css/style.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/font-awesome.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/bootstrap.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/bootstrap-responsive.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/jquery.bxslider.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/jcarousel.basic.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/flexslider.css" rel="stylesheet">
<link href="http://comunidadcoin.com/coin/css/colorbox.css" rel="stylesheet">
<link rel="shortcut icon" href="http://comunidadcoin.com/coin/images/favicon.ico" type="image/x-icon">
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://code.jquery.com/jquery.js"></script>
<script type="text/javascript" src="http://comunidadcoin.com/coin/js/All_Js_library/jquery.validationEngine.js"></script>
<script type="text/javascript" src="http://comunidadcoin.com/coin/js/All_Js_library/bootstrap.js"></script>
<script type="text/javascript" src="http://comunidadcoin.com/coin/js/All_Js_library/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="http://comunidadcoin.com/coin/js/All_Js_library/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://comunidadcoin.com/coin/js/All_Js_library/jquery.colorbox.js"></script>
<script type="text/javascript">
			$(document).ready(function() {
				$("#aweber").validationEngine('attach');
			});
		</script>
<!--                            -->
<script type="text/javascript">
			function submitForm() {

				$("#aweber").submit();
				$.colorbox({
					inline : true,
					fadeOut : 300,
					closeButton : false,
					escKey : false,
					trapFocus : true,
					overlayClose : false,
					href : "#view_msg"
				});
				setTimeout(function() {
					$.colorbox.close();
					window.location = "http://comunidadcoin.com/";
				}, 35000);
			
		
				return;
			}
		</script>
<script type="text/javascript">
			<!--(function() {
				var IE = /*@cc_on!@*/false;
				if (!IE) {
					return;
				}
				if (document.compatMode && document.compatMode == 'BackCompat') {
					if (document.getElementById("af-form-989030080")) {
						document.getElementById("af-form-989030080").className = 'af-form af-quirksMode';
					}
					if (document.getElementById("af-body-989030080")) {
						document.getElementById("af-body-989030080").className = "af-body inline af-quirksMode";
					}
					if (document.getElementById("af-header-989030080")) {
						document.getElementById("af-header-989030080").className = "af-header af-quirksMode";
					}
					if (document.getElementById("af-footer-989030080")) {
						document.getElementById("af-footer-989030080").className = "af-footer af-quirksMode";
					}
				}
			})();
			-->
		</script>
<script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] ||
				function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o), m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
			ga('create', 'UA-48279747-1', 'comunidadcoin.com');
			ga('send', 'pageview');
		</script>
</head>
<body>
<section> 
  <!--| Content Start |-->
  <div class="form_bg" style="top:0"> <img style="width:100%; height:100%;" src="http://comunidadcoin.com/coin/images/new-bg.jpg" alt=""> </div>
  <!--<div class="white-bg">
				<div class="container">
					<div class="row-fluid">
						<div class="span9">
							<div class="login-logo fast">
								<img width="80" src="http://comunidadcoin.com/coin/images/logo.png" alt="logo">

							</div>
						</div>
					</div>
				</div>
			</div>-->
  
  <div class="white-bg">
    <div class="container">
      <div class="row-fluid">
        <div class="span12">
          <div class="login-logo fast"> <img width="80" alt="logo" src="http://comunidadcoin.com/coin/images/logo-old.png">
            <div class="newlogin-but">
              <button class="btn btn-success" onClick="location.href='http://comunidadcoin.com/coin/modules/home/index.php?to=login'" type="button">Log in</button>
            </div>
            <!-- <div class="login_but ir">
            		<button type="button" onClick="location.href='http://comunidadcoin.com/coin/modules/home/index.php?to=login'">Log in</button>
        			</div> --> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="login-video green-bg-new">
    <div class="container">
      <div class="row-fluid">
        <div class="span12">
          <div class="span7"> <img class="index-coin" src="http://comunidadcoin.com/coin/images/coin-coin.png" alt="logo"> </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row-fluid">
        <div class="span12">
          <div class="span6">
            <div class="video-text"> Escribe tu sueño, Compartelo y Cumplelo ...así de fácil </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row-fluid">
        <div class="span12">
          <div class="span9" style="margin-top:10px !important;">
            <div style='display:none'>
              <div id='view_msg' class='irf-new-popup'>
                <div class="popup detail">
                  <div> &nbsp; </div>
                  <div style="text-align:center;"> <span style="text-align: center;"> <b>Se está procesando su solicitud...</b> </span> </div>
                  <div> <span> Hola! Dentro de poco te enviaremos nuestra invitación. Mantente al pendiente </span> </div>
                  <div> &nbsp; </div>
                </div>
              </div>
            </div>
            <form method="post" id="aweber" class="af-form-wrapper" accept-charset="iso-8859-1" action="http://www.aweber.com/scripts/addlead.pl"  >
              <div style="display: none;">
                <input type="hidden" name="meta_web_form_id" value="989030080" />
                <input type="hidden" name="meta_split_id" value="" />
                <input type="hidden" name="listname" value="default3376388" />
                <input type="hidden" name="redirect" value="http://comunidadcoin.com/" id="redirect_919b663cb895ad014a4aa00f4fbbcb39" />
                <input type="hidden" name="meta_adtracking" value="Home_page" />
                <input type="hidden" name="meta_message" value="1" />
                <input type="hidden" name="meta_required" value="name,email" />
                <input type="hidden" name="meta_tooltip" value="" />
              </div>
              <input id="awf_field-65380504" type="text" name="name" placeholder="Tu Nombre" class="" value="" onFocus="this.placeholder = ''" onBlur="this.placeholder = 'Tu Nombre'">
              <input id="awf_field-65380505" type="text" name="email" placeholder="Correo electrónico" class="inpbg validate[required,custom[email]]" value="" onFocus="this.placeholder = ''" onBlur="this.placeholder = 'Correo electrónico'">
              <input type="hidden" name="action"  value="aweberform"/>
              <button type="button" id="af-submit-image-989030080" class="btn btn-success" onClick="submitForm();"> Solicitar invitación </button>
            </form>
            <br/>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row-fluid">
          <div class="span12">
            <div class="login-logo-bottom"> <img width="40" src="http://comunidadcoin.com/coin/images/white-logo.png" alt="logo"> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
			.index-coin {
				margin: 50px 0 30px;
			}
			.login-video .span9 input[type="text"] {
				border-radius: 0;
				float: left;
				font-size: 18px;
				padding: 10px;
				width: 35%;
				margin-right: 10px;
				height: 42px;
			}
			.login-video .span9 button {
				color: #FFFFFF;
				font-size: 22px;
				padding: 10px 16px;
				text-shadow: -1px -1px 0 #46bcff;
				border-color: #c1f2e0 #025bad #2087bd #99d4fe;
				border-width: 1px;
				box-shadow: none;
				border-radius: 0;
				background-color: #88d2ff;
				background-image: -moz-linear-gradient(top, #88d2ff, #65c5ff);
				background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#88d2ff), to(#65c5ff));
				background-image: -webkit-linear-gradient(top, #88d2ff, #65c5ff);
				background-image: -o-linear-gradient(top, #88d2ff, #65c5ff);
				background-image: linear-gradient(to bottom, #88d2ff, #65c5ff);
				background-repeat: repeat-x;
				filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#88d2ff', endColorstr='#65c5ff', GradientType=0);
			}
			.video-text {
				color: #ffffff;
				font-size: 28px;
				line-height: 40px;
				text-align: center;
			}
			.green-bg-new .span6 {
				float: none !important;
				margin: 40px auto 50px !important;
				width: 380px;
			}
			.green-bg-new .span9 {
				float: none !important;
				margin: 30px auto 0 !important;
			}
		</style>
</body>
</html>
