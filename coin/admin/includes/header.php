<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!-- Required Stylesheets -->

		<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_CSS_URL; ?>/reset.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_CSS_URL; ?>/text.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_CSS_URL; ?>/stylesheet.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_CSS_URL; ?>/core/form.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_CSS_URL; ?>/core/login.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_CSS_URL; ?>/core/button.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_CSS_URL; ?>/mws.theme.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo CSS_URL; ?>/colorbox.css" media="screen" />
		<link rel="stylesheet" href="<?php echo ADMIN_CSS_URL; ?>/style.css">
		
		
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.colorbox.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.validationEngine-en.js"></script>
		<script type="text/javascript" src="<?php echo JS_URL; ?>/All_Js_library/jquery.dataTables.min.js"></script>
		<!-- For all dataTables js function and custom too -->
		<script type="text/javascript" src="<?php echo JS_URL; ?>/CustomFunction/Admin_Custom_functions.js">
		</script>
		<!------------------------------------>
		<link href="<?php echo CSS_URL; ?>/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ADMIN_CSS_URL; ?>/jquery.dataTables.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo CSS_URL; ?>/jquery-ui-1.8.20.blitzer.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo CSS_URL; ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		<?php if($pagename!="Bacic Manager"){ ?>
		<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
		<script type="text/javascript">
			tinymce.init({
			
				width: "800",
				height: "600",
				selector : "textarea ",
				plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
				toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
				relative_urls:false
				
			}); 
		</script> 
		<?php } ?>

	</head>
	<title>Coin Admin Manager</title>
<?php  
if(!isset($_SESSION['IS_admin_LoggedIn']))
{
echo "please wait";
echo "<script>window.location.href='http://www.proyectocoin.com/coin/admin/modules/login/login.php'</script>";
 
	exit;
}
?>
	<body>
		<div id="Container">
			<div class="bodybg">
				<img src="<?php echo ADMIN_IMAGE_URL; ?>/playa.png" alt="" class="HW100">
			</div>
			<header>
				<div id="">
					<div class="logo">
						<a href="#" class="logout">
							<img src="<?php echo ADMIN_IMAGE_URL; ?>/adminlogo.png" style="width:140px;margin-top: 5px;" alt="logo"></a>
					</div>
                    <div class="main-head">
                    	<?php if($imagename!=''){ ?>
                    	<img src="<?php echo ADMIN_IMAGE_URL."/".$imagename; ?>">
						<?php } ?>
				<?php if($pagename=='home'){ ?> 

				<div class="smart smartadmin">
                   <p><span class="text-green">S</span><span class="text-red">M</span><span class="text-skyblue">A</span><span class="text-yellow">R</span><span class="text-dark-red">T</span></p>
                   <p>CROWDFUNDING</p>
                   </div>				
			<?php } else{ echo '<h1>'.$pagename.'</h1>'; } ?>
						
                    </div>

					<div class="logoutbutton">
						<a href="<?php echo ADMIN_MODULE_URL . "/home/logout.php"; ?>" class="logout">
						<button type="button" class="btn active">
							SALIR
						</button></a>
					</div>
				</div>
				
			</header>
