<div id="Containt5" >
	<div class="table-top">
	<div class="table-left">
	 <h1 style="color:#8F8F8F;"><?php echo ucfirst($todo)?> Goal Category</h1>
		</div>
	</div>
	
	<section>
		<div class="MarA20">
			<div class="fl">
				<span style="color:red;"><?php if(!empty($_GET['error'])){ echo $_GET['error']; } ?></span>
				<form id="regform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" style="margin-left:20px;">

					<div class="MarT15">
						<div class="fl width100">
							Category Title :
						</div>
						<div class="fl">
							<input type="text" name="title" required class="form-control inpbg" placeholder="Enter Category title" value="<?php echo $record[0]['title']?>">
						</div>
						<div class=" cls"></div>
					</div>				
					<div class="MarT15">
						<div class="fl width100">
							Image :
						</div>
						<div class="fl">
							<?php if(!empty($record[0]['image'])){ ?>
								<input type="hidden" name="old_image" value="<?php echo $record[0]['image']; ?>" >
							<?php } ?>
							<input type="file" name="image" <?php if(empty($record[0]['image'])){ echo "required"; } ?> class="form-control inpbg" >
							<?php echo $record[0]['image']?>
						</div>
						<div class=" cls"></div>
					</div>					
					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
							<div class="form-actions">
								<?php if($todo == 'edit'){ ?>
								<input type="hidden" name="action" value="edit"/>
								<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
								<?php }else {?>
								<input type="hidden" name="action" value="add"/>
								<?php } ?>
								<button type="submit" class="btn btn-primary" id="add_department">
									Save changes
								</button>
								<button type="reset" class="btn" onclick="javascript:history.back()">
									Cancel
								</button>
							</div>
						</div>
						<div class=" cls"></div>
					</div>
					<div class="MarT15">
						<div class=" cls"></div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>