<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">Total Banners :<?php echo count($allrecord); ?></h4>
			<div style="float: left;">
				<a href="<?php echo ADMIN_MODULE_URL;?>/banner/?todo=add">
					<button type="button" class="btn btn-success">Add</button>
				</a>
			</div>
			<br/>
		</div>
	</div>
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC; margin-left:20px;" id="basicmanagerreport" >
		<thead>
			<tr style="background:#FFF;">
				<td>Sr.No</td>
				<td>Title</td>
				<td>Banner</td>
				<td>Url</td>
				<td>Actions</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="tdbor"></td>
				<td class="tdbor"></td>
				<td class="tdbor"></td>
				<td class="tdbor"></td>
				<td class="tdbor"></td>
			</tr>
			<?php
			if(!empty($allrecord)){ $i=1;
				foreach($allrecord as $value){
			?>
				<tr>	
					<td class="tdbor" style="padding-left: 30px;">
						<?php echo $i++;
						$id= $value['id'];
						?>
					</td>
					<td class="tdbor"><?php echo $value['title']; ?></td>
					<td class="tdbor"><img style="width:50px;" src="<?php echo DEFAULT_URL.'/banners/'.$value['banner']; ?>" ></td>
					<td class="tdbor"><?php echo $value['url']; ?></td>
					<td class="tdbor">
						<a  class="myButtonEdit" href="<?php echo ADMIN_MODULE_URL . "/banner/?id=$id&todo=edit"; ?>" title="Edit">Edit <img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Edit" /></a>
						&nbsp;&nbsp;&nbsp;
						<a  class="myButtonme" onclick="return confirm('Are you sure want to delete?')" href="<?php echo ADMIN_MODULE_URL . "/banner/?id=$id&todo=delete"; ?>" title="Delete">
						Delete <img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Edit" /></a>
					</td>
				</tr>
			<?php } } ?>
		</tbody>	
	</table>
</div>
