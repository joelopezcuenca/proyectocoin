<div id="Containt5" >
	<div class="table-top">
		<div class="table-left">
			<h1 style="color:#8F8F8F;"><?php echo ucfirst($todo)?> Bannner</h1>
		</div>
	</div>
	
	<section>
		<div class="MarA20">
			<div class="fl">
				<form id="regform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" style="margin-left:20px;">
					<div class="MarT15">
						<div class="fl width100">
							Title :
						</div>
						<div class="fl">
							<input type="text" name="title" required class="form-control inpbg" placeholder="Enter title" value="<?php echo $record['title']?>">
						</div>
						<div class=" cls"></div>
					</div>		
					<div class="MarT15">
						<div class="fl width100">
							Url :
						</div>
						<div class="fl">
							<input type="text" name="url" required class="form-control inpbg" placeholder="Enter url" value="<?php echo $record['url']?>">
						</div>
						<div class=" cls"></div>
					</div>		
					<div class="MarT15">
						<div class="fl width100">
							Banner Type :
						</div>
						<div class="fl">
							<select name="banner_type" class="form-control inpbg">
								<option <?php if($record['banner_type']==1){ echo "selected"; } ?>  value="1"><?php echo DEFAULT_URL.'/UNIQUEURL'; ?></option>
								<option <?php if($record['banner_type']==2){ echo "selected"; } ?>  value="2"><?php echo DEFAULT_URL.'/webinar/UNIQUEURL'; ?></option>
								<option <?php if($record['banner_type']==3){ echo "selected"; } ?>  value="3"><?php echo DEFAULT_URL.'/new/UNIQUEURL'; ?></option>
								<option <?php if($record['banner_type']==4){ echo "selected"; } ?>  value="4"><?php echo DEFAULT_URL.'/course/UNIQUEURL'; ?></option>
							</select>
						</div>
						<div class=" cls"></div>
					</div>		
					<div class="MarT15">
						<div class="fl width100">
							Banner :
						</div>
						<div class="fl">
							<?php if(!empty($record['banner'])){ ?>
								<input type="hidden" name="old_banner" value="<?php echo $record['banner']; ?>" >
							<?php } ?>
							<input type="file" name="banner" <?php if(empty($record['banner'])){ echo "required"; } ?> class="form-control inpbg" >
							<?php echo $record['banner']?>
						</div>
						<div class=" cls"></div>
					</div>					
					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
							<div class="form-actions">
								<?php if($todo == 'edit'){ ?>
								<input type="hidden" name="action" value="edit"/>
								<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
								<?php }else {?>
								<input type="hidden" name="action" value="add"/>
								<?php } ?>
								<button type="submit" class="btn btn-primary" id="add_department">
									Save changes
								</button>
								<button type="reset" class="btn" onclick="javascript:history.back()">
									Cancel
								</button>
							</div>
						</div>
						<div class=" cls"></div>
					</div>
					<div class="MarT15">
						<div class=" cls"></div>
					</div>
				</form>
			</div>
		</div>
	</section>
</div>