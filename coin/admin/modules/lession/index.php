<?php
include_once ("../../../conf/config.inc.php");
include_once (ADMIN_MODULE_PATH ."/lession/code/lession_code.php");
include_once (ADMIN_INCLUDE_PATH . "/header.php");
include_once (ADMIN_INCLUDE_PATH . "/left-menu.php");
?>
<section>
<?php
if ($todo == 'report') {
	include_once (ADMIN_MODULE_PATH . "/lession/report/lession_report.php");
} else if ($todo == 'add') {
	
	include_once (ADMIN_MODULE_PATH . "/lession/form/lession_form.php");
} else if ($todo == 'edit') {
	include_once (ADMIN_MODULE_PATH . "/lession/form/lession_form.php");
}
 else if ($todo == 'view') {
	include_once (ADMIN_MODULE_PATH . "/lession/form/lession_view.php");
}
?>
<section>
<?php
include_once (ADMIN_INCLUDE_PATH . "/footer.php");
?>