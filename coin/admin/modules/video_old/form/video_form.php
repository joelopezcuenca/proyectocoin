
<style>
input{
margin-left:14px;
}
</style>

<div id="content" class="span10">
<div style="color:red;text-align:center;">
		
</div>
<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Page</h2>
			</div>
			<div class="box-content">
				<form  action="<?php $_SERVER['PHP_SELF']; ?>" class="form-horizontal" name="pages" id="pages" method="post">
					<fieldset>
						
						<div class="control-group">
							<label class="control-label" for="typeahead">Page Title English<span class="required">*</span></label>
							<input type="text" name="page_title_english" class="form-control validate[required]" placeholder="Enter name" value="<?php echo $record[0]['page_title_english']?>">
							
						</div>
						<div class="control-group">
							<label class="control-label" for="typeahead">Page Title Urdu</label>
							<input type="text" name="page_title_urdu" class="form-control" placeholder="Enter name" value="<?php echo $record[0]['page_title_urdu']?>">
							
						</div>
						
						<div class="control-group">
							<label class="control-label" for="typeahead">Contents English
							</label>
							<div class="controls">
								<textarea  name="page_contents_english" rows="5"  cols="80" class="validate[required]"> <?php echo $record[0]['page_contents_english']?> </textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="typeahead">Contents Urdu
							</label>
							<div class="controls">
								<textarea  name="page_contents_urdu" rows="5"  cols="80"> <?php echo $record[0]['page_contents_urdu']?> </textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="typeahead">Status
							</label>
							<div class="controls">
								<select name="status"><option value="1" <?php if($record[0]['status']=='1'){ ?> selected="selected" <?php } ?> >Active</option><option value="0" <?php if($record[0]['status']=='0'){ ?> selected="selected" <?php } ?> >Inactive</option></select>
							</div>
						</div>
						<div class="form-actions">
							<?php if($todo == 'edit'){ ?>
							<input type="hidden" name="action" value="edit"/>
							<?php }else {?>
							
							<input type="hidden" name="action" value="add"/>
							<?php } ?>
							<button type="submit" class="btn btn-primary" id="add_department">
								Save changes
							</button>
							<button type="reset" class="btn" onclick="goBack()">
								Cancel
							</button>
						</div>
					</fieldset>
				</form>
		</div>
		</div>
</div>
</div>
</div>
</div>