<div id="Containt5" >
	<div class="table-top">
	<div class="table-left">
	 <h1 style="color:#8F8F8F;"><?php echo ucfirst($todo)?> Lession Video</h1>
		</div>
	</div>
	
	<section>
		<div class="MarA20">

			<div class="fl">

				<form id="regform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" style="margin-left:20px;">
					<div class="MarT15N">
						<div class="fl width150">
							Lession Title :
						</div>
						<div class="fl">
							<?php echo $allrecord_lession_val[0]['title'];  ?>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15N">
						<div class="fl width150">
							Title :
						</div>
						<div class="fl">
							<?php echo $record[0]['title']?>
						</div>
						<div class=" cls"></div>
					</div>					

					<div class="MarT15N">
						<div class="fl width150">
							Description :
						</div>
						<div class="fl">
							<?php echo $record[0]['description']?>
						</div>
						<div class=" cls"></div>
					</div>
					<?php if(!empty($record[0]['video'])){?>
						<div class="MarT15N">
							<div class="fl width150">
								Video Url :
							</div>
							<div class="fl">
								<?php echo $record[0]['video']?>
							</div>
							<div class=" cls"></div>
						</div>
					<?php } if(!empty($record[0]['password'])){?>
						<div class="MarT15N">
							<div class="fl width150">
								Video Password :
							</div>
							<div class="fl">
								<?php echo $record[0]['password']?>
							</div>
							<div class=" cls"></div>
						</div>
					<?php } if(!empty($record[0]['ref1'])){ ?>
						<div class="MarT15N">
							<div class="fl width150">
								Reference Url 1 :
							</div>
							<div class="fl">
								<?php echo $record[0]['ref1']?>
							</div>
							<div class=" cls"></div>
						</div>
					<?php } if(!empty($record[0]['ref1_title'])){ ?>
						<div class="MarT15N">
							<div class="fl width150">
								Reference Title 1 :
							</div>
							<div class="fl">
								<?php echo $record[0]['ref1_title']?>
							</div>
							<div class=" cls"></div>
						</div>
					<?php }  if(!empty($record[0]['ref2'])){  ?>
						<div class="MarT15N">
							<div class="fl width150">
								Reference Url 2 :
							</div>
							<div class="fl">
								<?php echo $record[0]['ref2']?>
							</div>
							<div class=" cls"></div>
						</div>
					<?php } if(!empty($record[0]['ref2_title'])){  ?>
						<div class="MarT15N">
							<div class="fl width150">
								Reference Title 2 :
							</div>
							<div class="fl">
								<?php echo $record[0]['ref2_title']?>
							</div>
							<div class=" cls"></div>
						</div>
					<?php } if(!empty($record[0]['ref3'])){  ?>
						<div class="MarT15N">
							<div class="fl width150">
								Reference Url 3 :
							</div>
							<div class="fl" style="margin-left:30px;">
								<?php echo $record[0]['ref3']?>
							</div>
							<div class=" cls"></div>
						</div>					
					<?php } if(!empty($record[0]['ref3_title'])){  ?>
						<div class="MarT15N">
							<div class="fl width150">
								Reference Title 3 :
							</div>
							<div class="fl" style="margin-left:30px;">
								<?php echo $record[0]['ref3_title']?>
							</div>
							<div class=" cls"></div>
						</div>					
					<?php } ?>	
					<div class="MarT15N">
						<div class="fl width150">
							Document file :
						</div>
						<div class="fl">
							<?php  echo $record[0]['doc'];  ?>
							<a class="green" href="<?php echo DEFAULT_URL.'/doc_files/'.$record[0]['doc']; ?>">Download Document file</a>
						</div>
						<div class=" cls"></div>
					</div>	
				
					<div class="MarT15N">
						<div class="fl width150">
							&nbsp;
						</div>
						<div class="fl">
							<div class="form-actions">
								<button type="reset" class="btn" onclick="javascript:history.back()">
									Back
								</button>
							</div>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15N">
						<div class=" cls"></div>
					</div>
				</form>
			</div>

		</div>
	</section>
</div>