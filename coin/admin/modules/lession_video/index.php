<?php
include_once ("../../../conf/config.inc.php");
include_once (ADMIN_MODULE_PATH ."/lession_video/code/lession_video_code.php");
include_once (ADMIN_INCLUDE_PATH . "/header.php");
include_once (ADMIN_INCLUDE_PATH . "/left-menu.php");
?>
<section>
<?php
if ($todo == 'report') {
	include_once (ADMIN_MODULE_PATH . "/lession_video/report/lession_video_report.php");
} else if ($todo == 'add') {
	
	include_once (ADMIN_MODULE_PATH . "/lession_video/form/lession_video_form.php");
} else if ($todo == 'edit') {
	include_once (ADMIN_MODULE_PATH . "/lession_video/form/lession_video_form.php");
}
 else if ($todo == 'view') {
	include_once (ADMIN_MODULE_PATH . "/lession_video/form/lession_video_view.php");
}
?>
</section>
<?php
include_once (ADMIN_INCLUDE_PATH . "/footer.php");
?>