<?php
include_once ("../../../conf/config.inc.php");
include_once (ADMIN_MODULE_PATH ."/email_message/code/email_message_code.php");
include_once (ADMIN_INCLUDE_PATH . "/header.php");
include_once (ADMIN_INCLUDE_PATH . "/left-menu.php");

?>
<section>
<?php include_once (ADMIN_MODULE_PATH . "/email_message/form/email_message_form.php");  ?>
</section>
<?php include_once (ADMIN_INCLUDE_PATH . "/footer.php");?>