<div id="Containt5" >
	<div class="table-top">
	<div class="table-left">
	 <h1 style="color:#8F8F8F;"><?php echo ucfirst($todo)?> Tutorials</h1>
		</div>
	</div>
	
	<section>
		<div class="MarA20">

			<div class="fl">

				<form id="regform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" style="margin-left:20px;">
					<div class="MarT15N">
						<div class="fl width150">
							Title :
						</div>
						<div class="fl">
							<?php echo $record[0]['title'];  ?>
						</div>
						<div class=" cls"></div>
					</div>
					<br/>
					<div class="MarT15N">
						<div class="fl width150">
							Description :
						</div>
						<div class="fl" style="width:850px;">
							<?php echo $record[0]['description']?>
						</div>
						<div class=" cls"></div>
						<div class=" cls"></div>
					</div>					
					<br/>
					<div class="MarT15N">
						<div class="fl width150">
							Blog Link :
						</div>
						<div class="fl">
							<?php echo $record[0]['blog_link']?>
						</div>
						<div class=" cls"></div>
					</div>
					<br/>
					<div class="MarT15N">
						<div class="fl width150">
							Image :
						</div>
						<div class="fl">
							<?php  if($record[0]['image']){  ?>
								<img class="green" src="<?php echo DEFAULT_URL.'/tutorial/'.$record[0]['image']; ?>">
							<?php } ?>
						</div>
						<div class=" cls"></div>
					</div>	
					<br/>
					<div class="MarT15N">
						<div class="fl width150">
							&nbsp;
						</div>
						<div class="fl">
							<div class="form-actions">
								<button type="reset" class="btn" onclick="javascript:history.back()">
									Back
								</button>
							</div>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15N">
						<div class=" cls"></div>
					</div>
				</form>
			</div>

		</div>
	</section>
</div>