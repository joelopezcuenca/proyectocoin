<div id="Containt5">
<div class="table-top" style="margin:0 0 10px 0;">
	<div  class="table-left">
		<h4 style="color:#8F8F8F;">Total Page :<?php echo count($allrecord); ?></h4>
		
		<div class="fl">
		
		<button type="button" class="btn btn-success" onclick="location.href='<?php echo ADMIN_MODULE_URL;?>/pagemanager/?todo=add'">
			Add
		</button> 
		</div>
		<?php if(isset($_SESSION['success_msg'])){ ?>
		<div class="fl" style="margin:5px 0 0 100px; text-align: right;">
			<font color="green" style="display: block; font-size: 14px; font-weight: bold"><?php echo $_SESSION['success_msg']; ?></font>
		</div>
		<?php 
		unset($_SESSION['success_msg']);
		} ?>
		
		</div>
		<div class="cls"></div>
		<br/>
	<table border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC; margin: 0; " id="basicmanagerreport" >
		<thead>
			<tr style="background:#FFF;">
				<td class="tdbor">Sr.No</td>
				<td class="tdbor">Title</td>
				<td class="tdbor">Description</td>
				<td class="tdbor">Created Date</td>
				<td class="tdbor">Actions</td>
				</tr>
		</thead>
				<tbody> 
				<?php
				
				if(!empty($allrecord))
				{
					$i=1;
				foreach($allrecord as $value)
					{
				?>
					<tr>
	
		<td class="tdbor" style="padding-left: 30px;">
			<?php echo $i++;
			$id=$value['Id'];
			?>
		</td>

		<td class="tdbor"><?php echo $value['page_title']; ?></td>
	    <td class="tdbor"><?php echo substr($value['page_content'],0,90);?></td>
	    <td class="tdbor"><?php echo $value['created_date']; ?></td>
	    <td class="tdbor">
	    	<a  class="myButtonEdit" href="<?php echo ADMIN_MODULE_URL . "/pagemanager/?id=$id&todo=edit"; ?>" title="Delete The User Account">
				Edit <img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Edit" />
		</a>
		<!--&nbsp;&nbsp;
			<a  class="myButtonEdit" href="<?php echo ADMIN_MODULE_URL . "/pagemanager/?id=$id&todo=view"; ?>" title="View The User Account">
				View <img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="View" />
		</a>-->
		&nbsp;&nbsp;
			<a  class="myButtonme" onClick="return confirm('Are you sure you want to delete this page?')" href="<?php echo ADMIN_MODULE_URL . "/pagemanager/?id=$id&todo=delete"; ?>" title="Delete The User Account">
				Delete <img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Delete" />
		</a>
		
	     </td>
	
		</tr>
		<?php }
			}
 ?>
 </tbody>
	</table>

</div>