<?php
extract($_POST);
extract($_GET);
$variables = array();
$page_Obj = new page;
$pagename='Page Manager';
$imagename='videotutorial.png' ;
switch($todo) {
	case 'report' :
		$condition="1=1 order by id Desc";
		$allrecord = $page_Obj -> SelectAll_Page($condition);
		break;
		
	case 'edit' :
		$condition="id=$id";
		$record = $page_Obj -> SelectAll_Page($condition);
		break;	
	
	case 'view' :
		$condition="id=$id";
		$page_val = $page_Obj -> SelectAll_Page($condition);
		break;		
		
	case 'delete' :
		$condition = "id=$id";
		$page_Obj -> DeleteCustomerById($condition);
		$_SESSION['success_msg'] = "Page has been deleted successfully.";
		header("location: " . ADMIN_MODULE_URL . "/pagemanager/?todo=report");
		exit;
		
}

if ($action == 'add') {

	$dataArray = array("page_title" =>$page_title, "page_content"=>$page_content,"created_date" =>date('Y-m-d'));
	$allrecord = $page_Obj ->PageDataInsert($dataArray);
	$_SESSION['success_msg'] = "Page has been Added successfully.";
	header("location: " . ADMIN_MODULE_URL . "/pagemanager/?todo=report");
}
if ($action == 'edit') {

	$dataArray = array("page_title" =>$page_title, "page_content"=>$page_content,"created_date" =>date('Y-m-d'));
	$condition = "id=$id";
	$allrecord = $page_Obj ->PageDataUpdateById($dataArray,$condition);
	$_SESSION['success_msg'] = "Page has been Updated successfully.";
	header("location: " . ADMIN_MODULE_URL . "/pagemanager/?todo=report");
}
?>