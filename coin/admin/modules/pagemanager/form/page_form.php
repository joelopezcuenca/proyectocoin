<div id="Containt5" >
	<div class="table-top">
	<div class="table-left">
	 <h1 style="color:#8F8F8F;"><?php echo ucfirst($todo)?> Page</h1>
		</div>
	</div>
	
	<section>
		<div class="MarA20">
			<div class="fl">
				<div style="color:red;text-align:center;"></div>
				<span style="color:red;"><?php if(!empty($_GET['error'])){ echo $_GET['error']; } ?></span>
				<form  action="<?php $_SERVER['PHP_SELF']; ?>" style="font-size: 14px; margin:0 0 0 20px; " class="form-horizontal" name="pages" id="pages" method="post">
					
						<div class="MarT15">
						<div class="fl width100">
							Page Title : 
						</div>
						<div class="fl">
							<input type="text" name="page_title" class="form-control inpbg validate[required]" placeholder="Enter name" value='<?php echo $record[0]['page_title']?>'>
						</div>
						<div class=" cls"></div>
					</div>
					<div class="MarT15">
						<div class="fl width100">
							Content : 
							</div>
						<div class="fl">
								<textarea  name="page_content" rows="5"  cols="80" class="validate[required] inpbg form-control"> <?php echo (isset($record[0]['page_content'])&& !empty($record[0]['page_content'])) ? $record[0]['page_content'] : "Enter Content" ; ?> </textarea>
							</div>
						<div class=" cls"></div>
					</div>
						
					<!--	<div class="MarT15">
						<div class="fl width100">
							Status : 
							</div>
							<div class="fl">
								<select name="status" class="inpbg"><option value="1" <?php if($record[0]['status']=='1'){ ?> selected="selected" <?php } ?> >Active</option><option value="0" <?php if($record[0]['status']=='0'){ ?> selected="selected" <?php } ?> >Inactive</option></select>
							</div>
						<div class=" cls"></div>
					</div>-->
					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
						<div class="form-actions">
							<?php if($todo == 'edit'){ ?>
							<input type="hidden" name="action" value="edit"/>
							<?php }else {?>
							
							<input type="hidden" name="action" value="add"/>
							<?php } ?>
							<input type="submit" class="btn btn-success" id="add_department" value="Save changes">
							<button type="reset" class="btn" onclick="javascript:history.back()">
								Cancel
							</button>
						</div>
						</div>
						</div>
						<div class="MarT15"></div>
					
				</form>
			</div>
		</div>
	</section>
</div>
<script>
	$("#pages").validationEngine({promptPosition : "bottomLeft"}); 
</script>