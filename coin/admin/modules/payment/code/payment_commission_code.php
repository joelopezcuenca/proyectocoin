<?php
/*Comision month Wise of All users*/
$getpagename = getMyPageName();
$ObjProuser = new UserDetail();
$objcommission = new commission();
$objPayment = new payment();
$ObjUser = new user();
$commision_report = $ObjProuser -> SelectAllProUser();
$imagename = "payments.png";
$pagename = "Payment manager";

###################################
/*
 * Withdraw Functionality
 * And Payment Approved Functionality
 * together in this code according to
 * Action events
 *
 */
##################################
if (($getpagename == 'view_commission.php' && $action == 'withdraw_report') || ($action == 'payment_approved')) {
	$imagename = "withdrawamount.png";
	$pagename = "Withdraw Manager";
	$condition = 'payment_approve_status=0';
	if ($action == 'withdraw_report') {
		$withdrawrequest = $objPayment -> GetAllPaymentRequestRecord($condition);
	}
	if ($action == 'payment_approved') {
		$condition = 'payment_approve_status=1';
		$paymentapprovedrequest = $objPayment -> GetAllPaymentRequestRecord($condition);
		$approvestatus = 'Payment Approved';
	}
}
if ($getpagename == 'view_commission.php' && $action == 'Ispayment_approve') {

	$condition = "userid=$userid";
	$data = array("payment_approve_status" =>'1',"payment_approved_date" =>date('Y-m-d'));
	$objPayment -> UpdatePaymentApproveData($userid);
	header("location: " . ADMIN_MODULE_URL . "/payment/view_commission.php?action=payment_approved");
	exit ;
}

##########################################
/*
 * Payment Cancelled Record Functionality
 */
##########################################

if ($getpagename == 'view_commission.php' && $action == 'cancel_report') {
	$cancelrecords = $objPayment -> GetAllCancelRecord();
}
?>

