<?php
include_once ("../../../conf/config.inc.php");
include_once (ADMIN_MODULE_PATH . "/payment/code/payment_commission_code.php");
include_once (ADMIN_INCLUDE_PATH . "/header.php");
include_once (ADMIN_INCLUDE_PATH . "/left-menu.php");
?>
<section>
	<?php
		if($action=='comision_report' ||$action=='')
		{
		    include_once (ADMIN_MODULE_PATH . "/payment/report_form/payment_commission_report.php");
		}
		else if($action=='cancel_report')
		{
			include_once (ADMIN_MODULE_PATH . "/payment/report_form/payment_cancelled_report.php");
		}
		else if($action=='withdraw_report')
		{
			include_once (ADMIN_MODULE_PATH . "/payment/report_form/paymentwithdraw_report.php");
		}
		else if($action=='payment_approved')
		{
			include_once (ADMIN_MODULE_PATH . "/payment/report_form/payment_approved_report.php");
		}
	?>
</section>
<?php
include_once (ADMIN_INCLUDE_PATH . "/footer.php");
?>