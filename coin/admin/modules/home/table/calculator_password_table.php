<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">Calculator Password List</h4>
			<br />
		</div>
	</div>
	
	<?php if($_SESSION['ERROR_MSG']!='') {?>
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:Red;"><?php echo $_SESSION['ERROR_MSG'];?></h4>
			<br />
		</div>
	</div>
	<?php $_SESSION['ERROR_MSG'] = '';} ?>
	
	<?php if($_SESSION['SUCCESS_MSG']!='') {?>
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:Green;"><?php echo $_SESSION['SUCCESS_MSG']?></h4>
			<br />
		</div>
	</div>
	<?php $_SESSION['SUCCESS_MSG'] = '';  } ?>

	<div class="table-div">
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;" id="program_password_list" >
		<thead>
			<tr>
				<th align="center">Step</th>
				<th align="center">Password</th>
				<th align="center">Action</th>				
				
				
			</tr>
		</thead>
		
		<tr>
		<form action="" name="step1_frm" id="step1_frm" method="POST">
			<td class="tdbor">1 </td>
			<td class="tdbor"> <input type="password" name="program_password" id="program_password1" value="<?php echo $first_step_password['program_password'];?>" placeholder="Program Password" class="form-control inpbg"/>&nbsp;&nbsp; <input type="checkbox" name="show_password" onclick="show_password2('program_password1')" >&nbsp; Show password </td>
			<td class="tdbor"> <a href="javascript:void(0)" onclick="jQuery('#step1_frm').submit()" >Edit</a> </td>	
			<input type="hidden" name="action" value="edit" />
			<input type="hidden" name="id" value="1" />
		</form>
		</tr>
		
		<tr>
		<form action="" name="step2_frm" id="step2_frm" method="POST">
			<td class="tdbor">2 </td>
			<td class="tdbor"> <input type="password" name="program_password" id="program_password2" value="<?php echo $second_step_password['program_password'];?>" placeholder="Program Password" class="form-control inpbg"/>&nbsp;&nbsp; <input type="checkbox" name="show_password" onclick="show_password2('program_password2')" >&nbsp; Show password </td>
			<td class="tdbor"> <a href="javascript:void(0)" onclick="jQuery('#step2_frm').submit()" >Edit</a> </td>	
			<input type="hidden" name="action" value="edit" />
			<input type="hidden" name="id" value="2" />
		</form>
		</tr>
		
		
		<tr>
		<form action="" name="step3_frm" id="step3_frm" method="POST">
			<td class="tdbor">3 </td>
			<td class="tdbor"> <input type="password" name="program_password" id="program_password3" value="<?php echo $third_step_password['program_password'];?>" placeholder="Program Password" class="form-control inpbg" />&nbsp;&nbsp; <input type="checkbox" name="show_password" onclick="show_password2('program_password3')" >&nbsp; Show password </td>
			<td class="tdbor"><a href="javascript:void(0)" onclick="jQuery('#step3_frm').submit()" >Edit</a></td>
			<input type="hidden" name="action" value="edit" />
			<input type="hidden" name="id" value="3" />
			</form>			
		</tr>
		
		<tr>
		<form action="" name="step4_frm" id="step4_frm" method="POST">
			<td class="tdbor">4 </td>
			<td class="tdbor"> <input type="password" name="program_password" id="program_password4" value="<?php echo $fourth_step_password['program_password'];?>" placeholder="Program Password" class="form-control inpbg" />&nbsp;&nbsp; <input type="checkbox" name="show_password" onclick="show_password2('program_password4')" >&nbsp; Show password </td>
			<td class="tdbor"> <a href="javascript:void(0)" onclick="jQuery('#step4_frm').submit()" >Edit</a> </td>
			<input type="hidden" name="action" value="edit" />
			<input type="hidden" name="id" value="4" />
			</form>			
		</tr>
		<tr>
		<form action="" name="step5_frm" id="step5_frm" method="POST">
			<td class="tdbor">5 </td>
			<td class="tdbor"> <input type="password" name="program_password" id="program_password5" value="<?php echo $fifth_step_password['program_password'];?>" placeholder="Program Password" class="form-control inpbg" />&nbsp;&nbsp; <input type="checkbox" name="show_password" onclick="show_password2('program_password5')" >&nbsp; Show password </td>
			<td class="tdbor"> <a href="javascript:void(0)" onclick="jQuery('#step5_frm').submit()" >Edit</a> </td>
			<input type="hidden" name="action" value="edit" />
			<input type="hidden" name="id" value="5" />		
		</form>
		</tr>
</div>
	</table>
	</div>
</div>
<script>
	function show_password2(val)
	{
		if(jQuery("#"+val).prop("type")=='text')
		{
			jQuery("#"+val).prop("type", "password");
		}else{
			jQuery("#"+val).prop("type", "text");
		}
		
		
	}
</script>