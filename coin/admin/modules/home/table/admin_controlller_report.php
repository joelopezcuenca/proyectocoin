<?php				/*
					 * Account creation success message
					 */
					
					 if(isset($_SESSION['successedit']) && $_SESSION['successedit']!=''){
					 	
						 unset($_SESSION['successmsg']);
					 	?>
					
					<span style="color: green; font-weight:bold;">
					<?php echo $_SESSION['successedit']; ?>
					</span>
					<?php }

						/*
						* New Account creation success message Flashing
						*/

						if(isset($_SESSION['successmsg']) && $_SESSION['successmsg']!=''){
	               ?>
					 <span style="color: green; font-weight:bold;">
					 	
					<?php
					unset($_SESSION['successedit']);
					echo $_SESSION['successmsg'];
					?>
					 </span>
					 <?php }

						if($_SESSION['Admin_Role']=='Superadmin')
						{
						?>
					
			<div id="Containt5">
			<div class="table-top">
           
            <div class="table-left">
		
		<h4 style="color:#8F8F8F;">Total Admin Manager :<?php echo count($useradminall); ?></h4>
		<br />
		
		<h4  style="color:#8F8F8F;">To Manage Admin Accounts</h4>
		<br />
		</div>
	<div class="table-right">
	<a href="<?php echo ADMIN_MODULE_URL . "/home/admincontroller.php?to=add"; ?>">
		<button class="btn btn-success" type="button">
			Add Account
		</button> </a>
		</div>
        </div>		
		
			
<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;padding-bottom:20px;margin-left:20px;" id="example2" align="center" >

		<tr>
			<td class="tdbor green" style="padding-left: 20px;font-weight: bold;font-size: 18px;">User Name
				<img src="<?php echo ADMIN_IMAGE_URL . '/admin12.png'; ?>" alt="user" width="32px" height="32px" />
				</td>
			<td class="tdbor green" style="padding-left: 20px;font-weight: bold;font-size: 18px;">
				<img src="<?php echo ADMIN_IMAGE_URL . '/mail.png'; ?>" alt="email"  width="32px" height="32px"/>
				Email
			</td>
			<td class="tdbor green" style="padding-left: 30px;font-weight: bold;font-size: 18px;">
				<img src="<?php echo ADMIN_IMAGE_URL . '/lock_12.png'; ?>" alt="user" width="32px" height="32px"/>
				Admin Type</td>
			<td class="tdbor green" style="text-align:center;font-weight: bold;font-size: 18px;">
				<img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="action"  />
				Action</td>
		</tr>
			
		<?php
		  for($i=0; $i<count($useradminall);$i++){ 
		  		$userid=$useradminall[$i]['id'];
		  	?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;font-weight: bold"><?php echo $useradminall[$i]['username']; ?></td>
			<td class="tdbor" style="padding-left: 20px;font-weight: bold"><?php echo $useradminall[$i]['email']; ?></td>
			<td class="tdbor" style="padding-left: 30px;font-weight: bold"><?php echo $useradminall[$i]['Admin_Role']; ?>
				<?php if($useradminall[$i]['Admin_Role']=='Superadmin')
				       {?>
				<span style="color:red">&nbsp;&nbsp;(Higher Authority)</span>
			   <?php }
					else {
			   	 ?>
			   	 
				   <span style="color:green">&nbsp;&nbsp;(Low Authority)</span>
			   <?php  } ?>
			  
			</td>
			<td class="tdbor" style="padding-left: 30px;font-weight: bold">
			<a class="myButtonEdit" href="<?php echo ADMIN_MODULE_URL . "/home/admincontroller.php?id=$userid&action=editinfo"; ?>" title="Edit Your Accounts">
				<img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Edit" />
				Edit Info
			</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<?php
				 if($_SESSION['Admin_Role']=='Superadmin'){
				 	if($useradminall[$i]['Admin_Role']!='Superadmin'){ ?>
		<a  class="myButtonme" href="<?php echo ADMIN_MODULE_URL . "/home/admincontroller.php?id=$userid&action=deleteinfo"; ?>" onclick="return confirm('Are you sure want to Delete This User?')"  title="Delete The User Account">
				<img src="<?php echo ADMIN_IMAGE_URL . '/delete.png'; ?>" alt="Edit" />
				Delete User
		</a>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<?php } } ?>
		
		
		<a class="myButtonEdit" <?php if($useradminall[$i]['Admin_Role']!="Superadmin"){ ?> href="<?php echo ADMIN_MODULE_URL . "/home/adminpermission.php?id=$userid&action=allowpermission"; ?>" <?php }else{ ?> href="javascript:void(0);" onclick="alert('You are already a Super Admin')"  <?php } ?> title="Allow Permission to access the Modules of Admin">
				<img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Edit" />
				Permission
		</a>
		
		</td>
		</tr>
		<?php } ?>
		</thead>
		</table>
	
</div>

<?php }

	else {

	/*
	*
	* Here We have Checked If SubAdmin Logins Then only Their particullar Data Will be shown
	* By Which he will not be able to edit or delete to someone's account
	*/
?>

	<div id="Containt5">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;padding-bottom:20px;" id="example2" >
		<tr>
			<td class="tdbor green" style="padding-left: 20px;font-weight: bold;font-size: 18px;">User Name
				<img src="<?php echo ADMIN_IMAGE_URL . '/admin12.png'; ?>" alt="user" width="32px" height="32px" />
				</td>
			<td class="tdbor green" style="padding-left: 20px;font-weight: bold;font-size: 18px;">
				<img src="<?php echo ADMIN_IMAGE_URL . '/mail.png'; ?>" alt="email"  width="32px" height="32px"/>
				Email
			</td>
			<td class="tdbor green" style="padding-left: 30px;font-weight: bold;font-size: 18px;">
				<img src="<?php echo ADMIN_IMAGE_URL . '/lock_12.png'; ?>" alt="user" width="32px" height="32px"/>
				Admin Type</td>
			<td class="tdbor green" style="text-align:center;font-weight: bold;font-size: 18px;">
				<img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="action"  />
				Action</td>
		</tr>
		
		
		
		<?php $user_id = $_SESSION[MAIN_admin_user_id];

			$userid = $ObjUserController -> selectAllAdminUserById($user_id);
		 ?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;font-weight: bold"><?php echo $userid[0]['username']; ?></td>
			<td class="tdbor" style="padding-left: 20px;font-weight: bold"><?php echo $userid[0]['email']; ?></td>
			<td class="tdbor" style="padding-left: 30px;font-weight: bold"><?php echo $userid[0]['Admin_Role']; ?>
				<?php if($useradminall[$i]['Admin_Role']=='Superadmin')
				       { ?>
				       	
				<span style="color:red">&nbsp;&nbsp;(Higher Authority)</span>
			   <?php }
					else {
			   	 ?>
			   	 
				   <span style="color:green">&nbsp;&nbsp;(Low Authority)</span>
			   <?php  } ?>
			  
			</td>
			<td class="tdbor" style="padding-left: 30px;font-weight: bold">
			<a class="myButtonEdit" href="<?php echo ADMIN_MODULE_URL . "/home/admincontroller.php?id=$user_id&action=editinfo"; ?>" title="Edit Your Accounts">
				<img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Edit" />
				Edit Info
			</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
			<?php	if($_SESSION['Admin_Role']=='Superadmin'){
				
				 ?>
		<a  class="myButtonme" href="<?php echo ADMIN_MODULE_URL . "/home/admincontroller.php?id=$user_id&action=deleteinfo"; ?>" onclick="return confirm('Are you sure want to Delete This User?')" title="Delete The User Account">
				<img src="<?php echo ADMIN_IMAGE_URL . '/delete.png'; ?>" alt="Edit" />
				Delete User
		</a>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<?php  } ?>
		
		
		<a  class="myButtonEdit" href="#" title="Allow Permission to access the Modules of Admin" onclick="fun();">
				<img src="<?php echo ADMIN_IMAGE_URL . '/edit.png'; ?>" alt="Edit" />
				Permission
		</a>
		
		</td>
		</tr>
		
		
		</table>
		</div>
	
	
<?php } ?>