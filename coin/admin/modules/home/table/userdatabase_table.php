<style>
table.dataTable td {
    padding: 7px 10px !important;
    border: 1px solid #ccc;
    font-weight: normal;
    margin: 0;
    text-align: left;
    vertical-align: middle;
    font-size: 13px;
    color: #000;
    text-shadow: none;
    white-space: nowrap;
    height: auto;
}
table.dataTable thead th {
    padding: 6px 10px !important;
    border: 1px solid #ccc !important;
    font-weight: bold !important;
    margin: 0;
    text-align: left;
    vertical-align: middle;
    font-size: 12px;
    color: #000;
    text-shadow: none;
    white-space: nowrap;
}
td.tdbor a:hover { 
    text-decoration: underline;
}
td.tdbor a{ color:#30BDFF;}
</style>
<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">
			<?php if($enroller!='') {
				echo "User List for the month of ".ucfirst($months_arr[$month])."";
			}else{
				echo "Monthly User List";
			}?>
			</h4>
			<br />
		</div>
	</div>
	
	<?php if($_SESSION['ERROR_MSG']!='') {?>
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:Red;"><?php echo $_SESSION['ERROR_MSG'];?></h4>
			<br />
		</div>
	</div>
	<?php $_SESSION['ERROR_MSG'] = '';} ?>
	
	<?php if($_SESSION['SUCCESS_MSG']!='') {?>
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:Green;"><?php echo $_SESSION['SUCCESS_MSG']?></h4>
			<br />
		</div>
	</div>
	<?php $_SESSION['SUCCESS_MSG'] = '';  } ?>

	<div class="table-div">
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;" id="user_database_list" >
		<thead>
			<tr>
				<th align="center"></th>
				<th align="center"><i class="fa fa-angle-right"></i> Dia - Año</th>
				<th align="center"><i class="fa fa-angle-right"></i> Name</th>
				<th align="center"><i class="fa fa-angle-right"></i> Email</th>
				<th align="center"><i class="fa fa-angle-right"></i> Username</th>				
				<th align="center"> Status </th>				
				<th align="center"></th>
				<th align="center">
				<form name="search_frm" id="search_frm" action="" >
					<select name="search" id="search" onchange="jQuery('#search_frm').submit();" >
						<option>-Select User-</option>
						<option value="recent" <?php if($search=='recent') { ?> selected="selected" <?php } ?>  >Most Recent</option>
						<option value="1" <?php if($search=='1') { ?> selected="selected" <?php } ?>>Only Active</option>
						<option value="0" <?php if($search=='0') { ?> selected="selected" <?php } ?>>Only Suspended</option>
					</select>
					<input type="hidden" name="action" value="search_user" />
				</form>	
				</th>
				
			</tr>
		</thead>
	<?php
		
	$trackCounter = 1;
	foreach($userList as $key=>$value)
	{
		
	?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;"><strong><i class="fa fa-angle-right"></i></strong></td>
			<td class="tdbor " style="padding-left: 20px;width: 20%;"><?php echo date('m/d-Y',$value['modified_date']);?></td>
			<td class="tdbor " style="padding-left: 20px;width: 20%;"><?php echo $value['name'];?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $value['email'];?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $value['username'];?></td>		
					
			<td class="tdbor" style="padding-left: 20px;width: 20%;">
			<?php if($value['active_status']=='0'){?>
				<a href="<?php echo ADMIN_MODULE_URL?>/home/userdatabase.php?action=active&id=<?php echo $value['id'];?>" style="color:Red">Suspend</a>
			<?php }else{ ?>
				<a href="<?php echo ADMIN_MODULE_URL?>/home/userdatabase.php?action=suspend&id=<?php echo $value['id'];?>" style="color:Green">Active</a>
			<?php } ?>
			</td>	
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><a href="<?php echo ADMIN_MODULE_URL?>/login/login_front_end.php?username=<?php echo base64_encode($value['username']);?>&password=<?php echo $value['password'];?>&<?php echo time()?>=<?php echo time()?>&action=login" target="_blank" >View Profile</a></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><a href="javascript:void()" onclick="delete_user('<?php echo $value['id']?>')" ><img src="<?php echo ADMIN_IMAGE_URL. '/delete.png'; ?>"></a></td>				
		</tr>

<?php $trackCounter++; } ?>

<div id="month_payment_alert" class="show-hide-main_new" style="display:none">
			<div class="table-shoe-hide-div">
				<h3>Are you sure you want to delete this user?</h3>
				
				<div>
				<input type="button" name="confirm_payment" value="Cancel" onclick="jQuery('#month_payment_alert').toggle()" />
				<input type="button" name="confirm_payment" value="Ok" onclick="jQuery('#month_payment_alert').toggle();jQuery('.show-hide-main').toggle()" /></div>
				</div>
				
			</div>
			
	<div  class="show-hide-main" style="display:none">
		<form name="payment_confirm_frm" id="payment_confirm_frm" action="" method="post" >
			<div class="table-shoe-hide-div">
				<h3>Please, Confirm who made the payout</h3>
				<div><label>Username:</label> <input type="text" name="username_admin" required /></div>
				<div><label>Password:</label> <input type="password" name="password_admin" required /></div>
				<div><input type="submit" name="confirm_payment" value="Confirm Delete" /> 
				<input type="button" name="confirm_payment" value="Cancel" onclick="jQuery('.show-hide-main').toggle()" /></div>	
			</div>
			<input type="hidden" name="action" value="confirm_delete" />
			<input type="hidden" name="user_id" id="user_id" value="" />

		</form>	
		</div>
			
</div>

	</table>
	</div>
</div>
<script>
function delete_user(val)
{
	jQuery('#user_id').val(val);
	jQuery('#month_payment_alert').show();
}
</script>
