<div id="Containt5">
	<div class="new-home">
    	<h5>Stats Overview</h5>
        <div class="row-fluid graph-page">
            <div class="span3">
                <div class="clicks">
                    <h3>590</h3>
                    <p>Total Prospects</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3>12,457</h3>
                    <p>Total  Users</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3>$104,900</h3>
                    <p>Total Comissions (80%)</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3 class="green">$244,246</h3>
                    <p>Total Earnings (20%)</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <h5>Users Overview</h5>
        <div class="row-fluid graph-page">
            <div class="span3">
                <div class="clicks">
                    <h3>1023</h3>
                    <p>Signups Sept</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3>457</h3>
                    <p>Signups  Last Week</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3>23</h3>
                    <p>Signups Yesterday</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3>19</h3>
                    <p>Signups Today</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="row-fluid graph-page">
          <div class="span9">
            <div class="graph-box">
              <h4>REGISTRO DE ACTIVIDAD</h4>
              <div class="graphs">
                <div id="curve_chart" style="width: 100%; height: 300px"></div>
              </div>
            </div>
          </div>
          <div class="span3 graph-right">
            <div class="clicks" style="text-align: left; padding: 20px 55px;">
              <h4><img style="display: inline-block; vertical-align: text-bottom; margin: 0px 10px 0px 0px;" src="<?php echo ADMIN_IMAGE_URL . '/green-dot.jpg'; ?>">Opt-Ins</h4>
            </div>
            <div class="clicks" style="text-align: left; padding: 20px 55px">
              <h4><img style="display: inline-block; vertical-align: text-bottom; margin: 0px 10px 0px 0px;" src="<?php echo ADMIN_IMAGE_URL . '/blue-dot.jpg'; ?>">New Users</h4>
            </div>
            <div class="clicks">
              <h3>12%</h3>
              <p>Conversion Rate</p>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
    </div>
</div>


<script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>