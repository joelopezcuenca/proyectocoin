<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">Total Transaction Record :</h4>
			<br />
		</div>
	</div>
	<div class="table-div">
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;" id="userlogform_new" >
		<thead>
			<tr>
				<th align="center">ENRTY</th>
				<th align="center">DATE</th>
				<th align="center" style="background:#ffff00;">SELLER</th>				
				<th align="center" style="background:#ffff00;">EMAIL</th>				
				<th align="center" style="background:#ffff00;">URL</th>				
				<th align="center" style="background:#ffff00;">TYPE</th>				
				<th align="center" style="background:#ffff00;">LEVEL</th>			
				<th align="center" style="background:#dbeef3;">BUYER</th>				
				<th align="center" style="background:#dbeef3;">EMAIL</th>
				<th align="center" style="background:#dbeef3;">URL</th>				
				<th align="center" style="background:#fde9d9;">DESCRIPTION</th>				
				<th align="center" style="background:#fde9d9;">COIN</th>			
				<th align="center" style="background:#fde9d9;">RECIPIENT</th>
				<th align="center" style="background:#fde9d9;">EMAIL</th>
				<th align="center" style="background:#fde9d9;">URL</th>
				<th align="center" style="background:#FFFFFF;">ACCOUNT STATUS</th>						
			</tr>
		</thead>
	<?php
	
	//echo "<pre>";
	//print_r($userLog_record);
	$counter = 0;
	foreach($userLog_record as $record)
	{ $counter++;
	
	//echo "<br/>".$counter.'    '.$record['view_order'];
	?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;width: 20%;">  <?php echo (int)$counter; ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= date('m/d-Y',$record['date']); ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['seller']; ?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['email']; ?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['url']; ?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['type']; ?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['seller_level']; ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['buyer']; ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['buyer_email']; ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['buyer_url']; ?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['description']; ?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['coin']; ?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['recipient']; ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['recipient_email']; ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['recipient_url']; ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?= $record['account_status']; ?></td>		
		</tr>

<?php } ?>
	</table>
	</div>

</div>