
<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">Monthly Payment Reports</h4>
			
		</div>
		<br />
			<button type="button" style="float: right; margin-right: 46px" class="btn btn-success" onclick="location.href='<?php echo ADMIN_MODULE_URL;?>/home/payment_record_generate_xls.php'">Export</button>
	</div>
	<div class="table-div">
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;" id="payment_report" >
		<thead>
			<tr>
				<th align="center">&nbsp;</th>
				<th align="center"><i class="fa fa-angle-right"></i> Month</th>
				<th align="center"><i class="fa fa-angle-right"></i> Total Generated</th>
				<th align="center"><i class="fa fa-angle-right"></i> Company Earnings</th>				
				<th align="center"><i class="fa fa-angle-right"></i> Total Comissions Paid</th>				
				<th align="center"><i class="fa fa-angle-right"></i> Paid out by </th>
			</tr>
		</thead>
	<?php

	$trackCounter = 1;
	foreach($paymentArr as $key=>$value)
	{
		//echo '<br/>'.$key;
		$paymentStatus = array();
		$userDetail = array();
		
			$condition = " year=".$currentYear." and month=".$key."";
			$paymentStatus = $ObjUser->get_monthly_comssion_status($condition);
	
		//----------get user detail-------
		$userDetail = $ObjUser->getOnId($paymentStatus['user_id']);
		
		
	?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;">
			<?php if(!empty($userDetail)){?>
			<img src="<?php echo ADMIN_IMAGE_URL?>/check_img.png" style="width:25px;" /></td>
			<?php } ?>
			<td class="tdbor " style="padding-left: 20px;width: 20%;"><input type="hidden" value="<?php echo (int)$i;?>" /><a href="<?php echo ADMIN_MODULE_URL . "/home/userlist.php?year=".date('Y')."&month=".$key.""; ?>" ><span class="set_month"><?php echo $key?><?php echo date('y')?></span></a></td>
			
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><a href="<?php echo ADMIN_MODULE_URL . "/home/userlist.php?year=".date('Y')."&month=".$key.""; ?>" >$<?php echo $value?></a></td>
			
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><a href="<?php echo ADMIN_MODULE_URL . "/home/userlist.php?year=".date('Y')."&month=".$key.""; ?>" > <?php echo '$'.number_format($total_comssion_earning[$key]['company_earning'],0);?></a></td>

			
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><a href="<?php echo ADMIN_MODULE_URL . "/home/userlist.php?year=".date('Y')."&month=".$key.""; ?>" ><?php echo '$'.$total_comssion_earning[$key]['comissions_paid'];?></a></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><a href="<?php echo ADMIN_MODULE_URL . "/home/userlist.php?year=".date('Y')."&month=".$key.""; ?>" ><?php if(!empty($userDetail)){
				echo $userDetail['username'];
			}else{
				echo 'N/A';
			}?></a></td>			
		</tr>

<?php $trackCounter++; } ?>
	</table>
	</div>
</div>
<script>
$( document ).ready(function() {
   $('.set_month').prepend('M');
});
</script>