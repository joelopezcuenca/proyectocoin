<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">User who will get Commission in the month of <?php echo ucfirst($months_arr[$month]);?></h4>
			<br />
		</div>
		<br />
			<button type="button" style="float: right; margin-right: 46px" class="btn btn-success" onclick="location.href='<?php echo ADMIN_MODULE_URL;?>/home/userlist_generate_xls.php?year=<?php echo $year?>&month=<?php echo $month?>'">Export</button>
	</div>

	<div class="table-div">
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;" id="payment_report_user_list" >
		<thead>
			<tr>
				<th align="center">
				<?php if(!empty($comssionRecord)){ ?>
				
				<a href="javascript:void(0)" /><img src="<?php echo ADMIN_IMAGE_URL?>/paid_icon.png"  /></a>
				<?php } else{ 
				?>
				<?php if($currentMonth==$month) { ?>
				<a href="javascript:void(0)" onclick="jQuery('#month_payment_alert').toggle()" /><img src="<?php echo ADMIN_IMAGE_URL?>/unpaid_icon.png"  /></a>
				<?php }else{ ?>

					<a href="javascript:void(0)" onclick="jQuery('.show-hide-main').toggle()" /><img src="<?php echo ADMIN_IMAGE_URL?>/unpaid_icon.png"  /></a>
				<?php } ?>
				
				
				
				
				<?php } ?>
				</th>
				<th align="center"><i class="fa fa-angle-right"></i> UserName</th>
				<th align="center"><i class="fa fa-angle-right"></i> Email</th>
				<th align="center"><i class="fa fa-angle-right"></i> Earnings</th>
				<th align="center"><i class="fa fa-angle-right"></i> Paypal email</th>				
				<th align="center"><i class="fa fa-angle-right"></i> Bank</th>				
				<th align="center"><i class="fa fa-angle-right"></i> CLABE</th>
				<th align="center">
				<form action="" method="GET" id="search_frm">
					<select name="search" id="search" onchange="jQuery('#search_frm').submit()">
						<option>-Select User-</option>
						<option value="1" <?php if($search=='1') {?> selected="selected" <?php } ?> >Active</option>
						<option value="0" <?php if($search=='0') {?> selected="selected" <?php } ?> >Suspended</option>
					</select>
					<input type="hidden" name="year" value="<?php echo $year;?>" />
					<input type="hidden" name="month" value="<?php echo $month?>" />
				</form>	
				</th>
			</tr>
		</thead>
		
		<div  class="show-hide-main" style="display:none">
		<form name="payment_confirm_frm" id="payment_confirm_frm" action="" method="post" >
			<div class="table-shoe-hide-div">
				<h3>Please, Confirm who made the payout</h3>
				<div><label>Username:</label> <input type="text" name="username_admin" required /></div>
				<div><label>Password:</label> <input type="password" name="password_admin" required /></div>
				<div><input type="submit" name="confirm_payment" value="Confirm Payment" /> 
				<input type="button" name="confirm_payment" value="Cancel" onclick="jQuery('.show-hide-main').toggle()" /></div>	
			</div>
			<input type="hidden" name="action" value="confirm_payment" />
			<input type="hidden" name="year" value="<?php echo $year?>" />
			<input type="hidden" name="month" value="<?php echo $month?>" />
		</form>	
		</div>
		
		<div id="month_payment_alert" class="show-hide-main_new" style="display:none">
			<div class="table-shoe-hide-div">
				<h3>Do not let us pay…until 1st of <?php echo $nextMonth?>.</h3>
				
				<div>
				<input type="button" name="confirm_payment" value="Ok" onclick="jQuery('#month_payment_alert').toggle()" /></div>	
			</div>		
		</div>
		
	<?php

	$trackCounter = 1;
	$user_comssion = USER_COMSSION;
	foreach($userList as $key=>$value)
	{
		
			if(!empty($comssionRecord))
			{
				//$imgPath = '<img src="'.ADMIN_IMAGE_URL.'/check_img.png" style="width:25px"  />';

				if($value['payment_status']=='0' && $search=='0' && $value['id']!='259')
					$imgPath  = '<img src="'.ADMIN_IMAGE_URL.'/sespended.png" style="width:25px"  />';
				else
					$imgPath = '<img src="'.ADMIN_IMAGE_URL.'/check_img.png" style="width:25px"  />';
				
			}else{
				if($value['payment_status']=='0' && $value['id']!='259')
					$imgPath  = '<img src="'.ADMIN_IMAGE_URL.'/sespended.png" style="width:25px"  />';
				else
					$imgPath = '';
			}
		 
		 $condition_detail = " user_id=".$value['id'];
		 $user_detail = $ObjUser->user_payment_gateway_detail($condition_detail);
		
		
		
		$bankDetail = unserialize($user_detail['payment_information']);
		

	?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;"><strong><i class="fa fa-angle-right"></i></strong></td>
			<td class="tdbor " style="padding-left: 20px;width: 20%;">
			<?php if($value['Admin_Role']=='Superadmin'){ ?>
			<a href="<?php echo ADMIN_MODULE_URL?>/login/login_front_end.php?username=<?php echo base64_encode($value['username']);?>&password=<?php echo $value['password'];?>&<?php echo time()?>=<?php echo time()?>&action=login" target="_blank" >Coin<a>
			<?php } else{?>
			<a href="<?php echo ADMIN_MODULE_URL?>/login/login_front_end.php?username=<?php echo base64_encode($value['username']);?>&password=<?php echo $value['password'];?>&<?php echo time()?>=<?php echo time()?>&action=login" target="_blank" ><?php echo $value['username'];?><a>
			
			<?php } ?>
			</td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;">
			<?php if($value['Admin_Role']=='Superadmin'){ ?>
			Coin Company
			<?php } else{ ?>
			<?php echo $value['email'];?>
			<?php } ?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo number_format($value['totalEarning']*($user_comssion/100),0);?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $user_detail['paypal_email'];?></td>			
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $bankDetail['bank_name'];?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $bankDetail['clabe_number'];?></td>	
		<td class="tdbor" style="padding-left: 20px;width: 20%;">
			<a href="<?php echo ADMIN_MODULE_URL?>/home/userdatabase.php?enroller=<?php echo $value['id'];?>&year=<?php echo $year?>&month=<?php echo $month?>" target="blank" >Veiw All User</a> &nbsp;&nbsp; <?php echo $imgPath;?>
		
		</td>				
		</tr>

<?php $trackCounter++; } ?>
	</table>
	</div>
</div>