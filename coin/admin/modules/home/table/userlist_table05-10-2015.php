
<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">Monthly User List</h4>
			<br />
		</div>
	</div>

	<div class="table-div">
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;" id="payment_report" >
		<thead>
			<tr>
				<th align="center"><a href="javascript:void(0)" onclick="jQuery('.show-hide-main').toggle()" /><img src="<?php echo ADMIN_IMAGE_URL?>/unpaid_icon.png"  /></a></th>
				<th align="center"><i class="fa fa-angle-right"></i> UserName</th>
				<th align="center"><i class="fa fa-angle-right"></i> Email</th>
				<th align="center"><i class="fa fa-angle-right"></i> Earnings</th>				
				<th align="center"><i class="fa fa-angle-right"></i> Bank</th>				
				<th align="center"><i class="fa fa-angle-right"></i> CLABE</th>
				<th align="center">
					<select name="search" id="search" >
						<option>-Select User-</option>
						<option>Active</option>
						<option>Suspended</option>
					</select>
				</th>
			</tr>
		</thead>
		<div class="show-hide-main" style="display:none">
		<form name="payment_confirm_frm" id="payment_confirm_frm" action="" method="post" >
			<div class="table-shoe-hide-div">
				<h3>Please, Confirm who made the payout</h3>
				<div><label>Username:</label> <input type="text" name="username_admin" required /></div>
				<div><label>Password:</label> <input type="password" name="password_admin" required /></div>
				<div><input type="submit" name="confirm_payment" value="Confirm Payment" /> 
				<input type="button" name="confirm_payment" value="Cancel" onclick="jQuery('.show-hide-main').toggle()" /></div>	
			</div>
			<input type="hidden" name="action" value="confirm_payment" />
			<input type="hidden" name="year" value="<?php echo $year?>" />
			<input type="hidden" name="month" value="<?php echo $month?>" />
		</form>	
		</div>
	<?php
	
	if(!empty($comssionRecord))
	{
		$imgPath = '<img src="'.ADMIN_IMAGE_URL.'/check_img.png" style="width:25px"  />';
	}else{
		$imgPath  = '';
	}
	
	$trackCounter = 1;
	foreach($userList as $key=>$value)
	{
		$bankDetail = unserialize($value['payment_information']);
	?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;"><strong><i class="fa fa-angle-right"></i></strong></td>
			<td class="tdbor " style="padding-left: 20px;width: 20%;"><?php echo $value['username'];?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $value['email'];?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $value['amount'];?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $bankDetail['bank_name'];?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $bankDetail['clabe_number'];?></td>	
		<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo $imgPath;?></td>				
		</tr>

<?php $trackCounter++; } ?>
	</table>
	</div>
</div>