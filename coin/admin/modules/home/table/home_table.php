<div id="Containt5">
	<div class="new-home">
    	<h5>Stats Overview</h5>
        <div class="row-fluid graph-page">
            <div class="span3">
                <div class="clicks">
                    <h3>$<?php echo number_format($Total_Comissions,0)?> </h3>
                    <p>A repartir en comisiones</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3>$<?php echo number_format($Total_Earnings,0);?> </h3>
                    <p>Ganancias del 20%</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3>$<?php echo number_format($Total_Comissions_Coin,0)?></h3>
                    <p>Comisiones p/ Coin</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
				<?php
						$total_user_comssion  = ($total_income['totalincome']/100)*20;
						$total_admin_comssion = $total_admin_income['admin_totalincome'];
				?>
                    <h3 class="green">$<?php echo  number_format($Company_Profit,0);?> </h3>
                    <p>Ganancias de la Empresa</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <h5>Users Overview</h5>
        <div class="row-fluid graph-page">
            <div class="span3">
                <div class="clicks">
                    <h3><?php echo $userDetail['total_prospects']; ?></h3>
                    <p>Total Prospects</p>
                </div>
            </div>
            <div class="span3" >
                <div class="clicks">
                    <h3  style="color:#f79646"><?php echo number_format($totalSeles,0); ?>%</h3>
                    <p>Conversion Rate</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3><?php echo $userDetail['total_user'];?></h3>
                    <p>Total Users</p>
                </div>
            </div>
            <div class="span3">
                <div class="clicks">
                    <h3><?php echo $userDetail['total_today_user'];?></h3>
                    <p>Signups Today</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="row-fluid graph-page">
          <div class="span9">
            <div class="graph-box">
              <h4>REGISTRO DE ACTIVIDAD</h4>
			<div class="year-month-box">
			  <form name="search_frm" action="" method="get" id="search_frm" >
				  Year: 
					<select name="year" id="year" onchange="jQuery('#search_frm').submit()">
						<?php for($y=2015;$y<=date('Y');$y++){?>
						<option <?php if($currentYear==$y) {?> selected="selected" <?php } ?> value="<?php echo $y;?>" > <?php echo $y;?></option>
						<?php } ?>
					</select> 
					Month: 
					   <select name="s" id="s" onchange="jQuery('#search_frm').submit()">
							<?php foreach($month_arr as $val=>$value){?>
							<option <?php if($currentMonth==$value) {?> selected="selected" <?php } ?> value="<?php echo $val;?>" > <?php echo $val;?></option>
							<?php } ?>
						</select>
			  </form>
			</div>
				
              <div class="graphs">
			  <?php if(count($chartDataarr)>0) {?>
                <div id="curve_chart" style="width: 100%; height: 300px"></div>
			  <?php } else{
					echo ' <div style="width: 100%; height: 300px; text-align: center; color: red; font-size: 13px">Hey! No tenemos resultados para ti en este mes, al parecer aún no eras miembro, por favor selecciona otro mes.</div>';
					
				}?>
              </div>
            </div>
          </div>
          <div class="span3 graph-right">
              <div class="clicks">
                    <h3><?php echo $userDetail['total_yesterday_user'];?></h3>
                    <p>Signups Yesterday</p>
                </div>
            <div class="clicks">
                    <h3><?php echo $userDetail['lastweek_registred_user'];?></h3>
                    <p>Signups  Last Week</p>
                </div>
            <div class="clicks">
              <h3><?php echo $userDetail['total_registred_user'];?></h3>
              <p>Signups <?php echo date('M');?></p>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
    </div>
</div>


			  
<script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

    <script type="text/javascript">
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Months', 'Prospectos', 'Ventas'],
		  <?php 
	
	for($i=1; $i<=count($chartDataarr['prospectos_arr']); $i++)
	{ ?>
		['<?php echo $i?>',<?php echo $chartDataarr['prospectos_arr'][$i]?>, <?php echo $chartDataarr['sale'][$i]?>],
		
	<?php } ?>		      
        ]);

        var options = {
          title: '',
		  width:950,
		 isStacked: true,
          curveType: 'function',
          legend: { position: 'top',x:10, y:20 },
		   colors: ['#3366cc', '#82cac7'],
		  min:0,
		  /* vAxis:{ textPosition: 'none'  } */
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
	<style>
		.graph-box .graphs {
    overflow: hidden;
	position:relative;
}
.graphs > div#curve_chart {
    margin-left: -85px;
}
	</style>