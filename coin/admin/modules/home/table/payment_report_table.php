<style>
.dataTables_filter {
    float: right;
    margin: 3px 40px 25px 0;
}
</style>
<div id="Containt5">
	<div class="table-top">
		<div  class="table-left">
			<h4 style="color:#8F8F8F;">Monthly Payment Reports</h4>
			<br />
		</div>
	</div>
	<div class="table-div">
	<table width="97%" border="0" cellspacing="0" cellpadding="0" style="border-top:1px solid #CCC; border-left:1px solid #CCC;" id="payment_report" >
		<thead>
			<tr>
				<th align="center">&nbsp;</th>
				<th align="center"><i class="fa fa-angle-right"></i> Month</th>
				<th align="center"><i class="fa fa-angle-right"></i> Total Generated</th>
				<th align="center"><i class="fa fa-angle-right"></i> Company Earnings</th>				
				<th align="center"><i class="fa fa-angle-right"></i> Total Comissions Paid</th>				
				<th align="center"><i class="fa fa-angle-right"></i> Status</th>
			</tr>
		</thead>
	<?php
	$trackCounter = 1;
	foreach($paymentArr as $key=>$value)
	{
		
		$paymentStatus = array();
		$condition = " year=".$currentYear." and month=".$key."";
		$paymentStatus = $ObjUser->get_monthly_comssion_status($condition);
		
		//echo "<br/>".$value;
	?>
		<tr>
			<td class="tdbor" style="padding-left: 20px;"><strong><i class="fa fa-angle-right"></i></strong></td>
			<td class="tdbor set_month" style="padding-left: 20px;width: 20%;"><input type="hidden" value="<?php echo (int)$i;?>" /><?php echo $key?><?php echo date('y')?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;">$<?php echo $value?></td>
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo '$'.number_format($total_comssion_earning[$key]['company_earning'],0);?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><?php echo '$'.number_format($total_comssion_earning[$key]['comissions_paid'],0);?></td>		
			<td class="tdbor" style="padding-left: 20px;width: 20%;"><a href="<?php echo ADMIN_MODULE_URL . "/home/userlist.php?year=".date('Y')."&month=".$key.""; ?>" />
			<?php if(!empty($paymentStatus)){?>
			<img src="<?php echo ADMIN_IMAGE_URL?>/paid_icon.png"  />
			<?php } else {?>
			<img src="<?php echo ADMIN_IMAGE_URL?>/unpaid_icon.png"  />
			<?php } ?>
			</a></td>			
		</tr>

<?php $trackCounter++; } ?>
	</table>
	</div>
</div>
<script>
$( document ).ready(function() {
   $('.set_month').prepend('M');
});
</script>