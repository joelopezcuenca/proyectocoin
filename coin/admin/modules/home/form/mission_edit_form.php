
<div id="Containt5" >
	
	<div class="table-top">
	<div class="table-left">
		
		<h1 style="color:#8F8F8F;">Edit Your Missions Here</h1>
		
		</div>
	</div>
	<section>
		<div class="MarA20">

			<div class="fl">
			<form id="mission_edit" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" style="margin-left:20px;" >
					
			  <div class="MarT15">
						<div class="fl width100">
							Mission-Level :
						</div>
						<div class="fl">
						 <input type="text" name="missionlevel" id="missionlevel"  class="inpbg validate[required,custom[onlyLetterNumber]]" value="<?php echo $mission['0']['Mission_Level'];?>" >
						</div>
						<div class=" cls"></div>
					</div>
					
					<div class="MarT15">
						<div class="fl width100">
							Title :
						</div>
						<div class="fl">
							<input type="text" name="title" id="title"  class="inpbg validate[required]" value="<?php echo $mission['0']['Title_Description'];?>">
						</div>
						<div class=" cls"></div>
					</div>
					<div class="MarT15">
						<div class="fl width100">
							Logo :
						</div>
						<div class="fl">
							<input type="file" name="missionlogo" id="missionlogo" class="validate[required]"  class="inpbg" value="<?php echo $mission['0']['Logo_Image'];?>">
						</div>
						<div class=" cls"></div>
					</div>
				<div class="MarT15">
						<div class="fl width100">
							VideoUrl :
						</div>
						<div class="fl">
							<textarea name="missionvideo_url" id="missionvideo_url" class="validate[required]"><?php echo $mission['0']['Video_Url'];?></textarea>																																																																																																																																																																																																																																																																																															
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							Type :
						</div>
						<div class="fl">
							<input type="radio" name="type" value="Basic" <?php if($mission[0]['Type']=='Basic') {?> checked="checked" <?php } ?>>
							&nbsp;Basic
							&nbsp; &nbsp; &nbsp; &nbsp;
							<input type="radio" name="type" value="Pro" <?php if($mission[0]['Type']=='Pro') {?> checked="checked" <?php }?>>
							&nbsp;Pro
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Submit Data
							</button>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class=" cls"></div>
					</div>

					<input name="action" type="hidden" value="update" />
					<input name="m_id" id="m_id" type="hidden" value="<?php echo $mission['0']['id'] ;?>" />
				
				</form>
			</div>

		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#mission_edit").validationEngine('attach');
	}); 
</script>

<script type="text/javascript">
	function submitForm() {
		$("#mission_edit").submit();
	}
</script>

