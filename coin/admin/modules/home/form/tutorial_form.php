
<div id="Containt5" >
	<div class="table-top">
	<div class="table-left">
	 <h1 style="color:#8F8F8F;">Add Your Tutorial From Here</h1>
		</div>
	</div>
	
	<section>
		<div class="MarA20">

			<div class="fl">

				<form id="regform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" style="margin-left:20px;">
					<div class="MarT15">
						<div class="fl width100">
							Title :
						</div>
						<div class="fl">
							<input type="text" name="title" id="title"  class="inpbg validate[required]" >
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							Description :
						</div>
						<div class="fl">
							<textarea name="description" class="validate[required]"></textarea>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							VideoUrl :
						</div>
						<div class="fl">
							<textarea name="video_url" id="video_url" class="validate[required]"></textarea>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							Type :
						</div>
						<div class="fl">
							<input type="radio" name="type" value="Basic">
							&nbsp;Basic
							&nbsp; &nbsp; &nbsp; &nbsp;
							<input type="radio" name="type" value="Pro">
							&nbsp;Pro
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Submit Data
							</button>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class=" cls"></div>
					</div>

					<input name="action" type="hidden" value="add" />
				</form>
			</div>

		</div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#regform").validationEngine('attach');
	}); 
</script>

<script type="text/javascript">
	function submitForm() {
		$("#regform").submit();
	}
</script>

