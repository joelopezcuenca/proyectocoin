<?php /*
 * ADD ADMIN ACCOUNT ACCESS ACCOUNTS FORM
 */
?>

<script>
	$(document).ready(function() {
		$('#selecctall').click(function(event) {//on click
			if (this.checked) {// check select status
				$('.checkbox1').each(function() {//loop through each checkbox
					this.checked = true;
					//select all checkboxes with class "checkbox1"
				});
			} else {
				$('.checkbox1').each(function() {//loop through each checkbox
					this.checked = false;
					//deselect all checkboxes with class "checkbox1"
				});
			}
		});

	}); 
</script>

<script type="text/javascript">
	function submitForm() {
		$("#permissionallow").submit();
	}
</script>
<style>
	
.alingedtogether {
    float: left;
    margin: 0 20px 10px 0;
    text-align: left;
    width: 150px;
}
.alingedtogether input[type="checkbox"]{
    float: right;
    margin:0;
}
</style>

<?php  
$userid=$_GET['id'];
$ObjPermission = new AdminUserPermission();
$usernumber = $ObjPermission -> AdminselectAllAdminUser($userid);
$permisingrant = $usernumber[0]['permission'];
$arraycheck = explode(',', $permisingrant);


?>
<div id="Containt5" style="border-bottom: 1px solid #ccc;" >

	<section>

		<div class="MarA20">

			<div>
				<form id="permissionallow" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" >

					<h1 style="color:green;">GRANT ACCESS PERMISSION CONTROL AREA</h1>
						<div class="MarT15">
							<div class="fl" style="width: 100%;padding-bottom: 15px;">
								<div class="checkbox" style="padding: 15px;">
								<div class="alingedtogether" style="margin: 0;">	
						  		 Select All:<input type="checkbox" id="selecctall"/></div>
						  		 <p style="clear: both;">&nbsp;</p>
						  		 <div class="alingedtogether">
								Tutorial Module: <input type="checkbox" value="tutorial_yes" class="checkbox1" name="permission[]" <?php if (in_array('tutorial_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								<div class="alingedtogether">
								User_Report Manager:<input type="checkbox" value="userreport_yes"  class="checkbox1" name="permission[]" <?php if (in_array('userreport_yes',$arraycheck)) {?> checked="checked" <?php } ?> />
								</div>
								
								
								<div class="alingedtogether">
								Transaction Log Manager:<input type="checkbox" value="userlog_yes"  class="checkbox1" name="permission[]" <?php if (in_array('userlog_yes',$arraycheck)) {?> checked="checked" <?php } ?> />
								</div>
								
								
								<div class="alingedtogether">
								Payment Module: <input type="checkbox" value="payment_yes" class="checkbox1" <?php if (in_array('payment_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								 
								
								<div class="alingedtogether">
								MissionVideo Module : <input type="checkbox" value="mission_yes" class="checkbox1" name="permission[]" <?php if (in_array('mission_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								
								 
								
								<div class="alingedtogether">
								FaqManager_Module: <input type="checkbox" value="faq_yes"  class="checkbox1" name="permission[]" <?php if (in_array('faq_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								
								 
								
								<div class="alingedtogether">
								 Coin Blog Manager: <input type="checkbox" value="coinblog_yes"  class="checkbox1" name="permission[]" <?php if (in_array('coinblog_yes',$arraycheck)) {?> checked="checked" <?php } ?> />
								</div>
								 
								 <div class="alingedtogether">
								 Community Guide: <input type="checkbox" value="coincommunity_yes"  class="checkbox1" name="permission[]" <?php if (in_array('coincommunity_yes',$arraycheck)) {?> checked="checked" <?php } ?> />
								</div>
								 
								 <div class="alingedtogether">
								About Us Manager: <input type="checkbox" value="aboutus_yes"  class="checkbox1" name="permission[]" <?php if (in_array('aboutus_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								
								 
								 <div class="alingedtogether">
								Preguntas Module: <input type="checkbox" value="preguntas_yes"  class="checkbox1" name="permission[]" <?php if (in_array('preguntas_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								
								 
								 <div class="alingedtogether">
								Broadcasting module: <input type="checkbox" value="broadcasting_yes"  class="checkbox1" name="permission[]" <?php if (in_array('broadcasting_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								
								 
								 	<div class="alingedtogether">
								Payment Edit Module: <input type="checkbox" value="paymentsetting_yes"  class="checkbox1" name="permission[]" <?php if (in_array('paymentsetting_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								
								<div class="alingedtogether">
								Discussion Manager: <input type="checkbox" value="discussion_yes"  class="checkbox1" name="permission[]" <?php if (in_array('discussion_yes',$arraycheck)) {?> checked="checked" <?php } ?>/>
								</div>
								
								<div class="alingedtogether">
								 Contact Manager: <input type="checkbox" value="contact_yes"  class="checkbox1" name="permission[]" <?php if (in_array('contact_yes',$arraycheck)) {?> checked="checked" <?php } ?> />
								</div>
								 
							<div style="clear: both"></div>
							</div>
							<br/>
							<div class=" cls"></div>
							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Allow Permission
							</button>
						</div>
						
					
						<input name="userid" type="hidden" value="<?php echo $id;?>" />
						<input name="action" type="hidden" value="updatepermission" name="permissionallow"/>
				</form>
			</div>
	</section>
</div>
</div>

