<script type="text/javascript">
	$(document).ready(function() {
		$("#topicedit").validationEngine('attach');
	}); 
</script>

<script type="text/javascript">
	function submitForm() {
		$("#topicedit").submit();
	}
</script>
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
   selector: "textarea",
   plugins: [
       "advlist autolink lists link image charmap print preview anchor",
       "searchreplace visualblocks code fullscreen",
       "insertdatetime media table contextmenu paste"
   ],
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>

<div id="Containt5" >
	
	<div class="table-top">
	<div class="table-left">
		
		<h1 style="color:#8F8F8F;">Edit Your Topic Here</h1>
		
		</div>
	</div>
	<section>
		<div class="MarA20">

			<div class="fl">
			<form id="topicedit" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" style="margin-left:20px;" >
					
			  <div class="MarT15">
						<div class="fl width100">
							Topic Name :
						</div>
						<div class="fl">
						 <input type="text" name="topic_name" id="topic_name"  class="inpbg validate[required]" value="<?php echo $discussion_topic_edit[0]['topic_name'];?>" >
						</div>
						<div class=" cls"></div>
					</div>
					
					<div class="MarT15">
						<div class="fl width100">
							Topic :
						</div>
						<div class="fl">
							<select name="topic_type">
							<option <?php if($discussion_topic_edit[0]['topic_img']=='love.png'){ ?> selected="selected"  <?php } ?> value="love.png">love</option>
							<option <?php if($discussion_topic_edit[0]['topic_img']=='flag-icon.png'){ ?> selected="selected"  <?php } ?>  value="flag-icon.png">flag</option>
							<option <?php if($discussion_topic_edit[0]['topic_img']=='Travel-train.png'){ ?> selected="selected"  <?php } ?> value="Travel-train.png">Travel-train</option>
							<option <?php if($discussion_topic_edit[0]['topic_img']=='plane.png'){ ?> selected="selected"  <?php } ?> value="plane.png">Plane</option>
							<option <?php if($discussion_topic_edit[0]['topic_img']=='Food2.png'){ ?> selected="selected"  <?php } ?> value="Food2.png">Food</option>
							<option <?php if($discussion_topic_edit[0]['topic_img']=='coins1.png'){ ?> selected="selected"  <?php } ?> value="coins1.png">Coin</option>
							<option <?php if($discussion_topic_edit[0]['topic_img']=='building3.png'){ ?> selected="selected"  <?php } ?> value="building3.png">Building</option>
							<option <?php if($discussion_topic_edit[0]['topic_img']=='beach2.png'){ ?> selected="selected"  <?php } ?> value="beach2.png">Beach</option>
							</select>
						</div>
						<div class=" cls"></div>
					</div>
				<div class="MarT15">
						<div class="fl width100">
							Description :
						</div>	
						<div class="fl">
							<textarea name="description" id="description" class="validate[required]"><?php echo $discussion_topic_edit[0]['topic_text'];?></textarea>																																																																																																																																																																																																																																																																																															
						</div>
						<div class=" cls"></div>
					</div>

					

					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Submit Data
							</button>
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class=" cls"></div>
					</div>
                    <input name="id_edit" type="hidden" value="<?php echo $discussion_topic_edit[0]['id'] ?>" />
					<input name="action" type="hidden" value="update" />
				</form>
			</div>

		</div>
	</section>
</div>

