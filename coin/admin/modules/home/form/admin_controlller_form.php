<?php /*
 * ADD ADMIN ACCOUNT ACCESS ACCOUNTS FORM
 */
?>

<script type="text/javascript">
	$(document).ready(function() {
		$("#adminuserform").validationEngine('attach');
	}); 
</script>

<script type="text/javascript">
	function submitForm() {
		$("#adminuserform").submit();
	}
</script>

<div id="Containt5" >

	<section>

		<div class="MarA20" style="margin:30px;">

			<div class="fl">
				<form id="adminuserform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" >

					<h1 style="color:green;">ADD AN ADMIN ACCOUNT ACCESS CONTROL AREA</h1>
					<div class="MarT15">
						<div class="fl width100">
							UserName :
						</div>
						<div class="fl">
							<input type="text" name="username" id="username"  class="inpbg validate[required]" >
						</div>

						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							Email :
						</div>
						<div class="fl">
							<input type="text" name="email" id="email"  class="search-query inpbg validate[required,custom[email]]" >
						</div>
						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							password :
						</div>
						<div class="fl">
							<input type="password" name="password" id="password"  class="inpbg validate[required]" >
						</div>

						<div class=" cls"></div>
					</div>
						
							<div class="MarT15">
						<div class="fl width100">
							Confirm password :
						</div>
						<div class="fl width100">
							<input type="password" name="cpassword" id="cpassword"  class="inpbg validate[required,equals[password]] text-input" >
						</div>

						<div class=" cls"></div>
					</div>
						
					<!-- <div class="MarT15">
						<div class="fl width100">
							Admin-Type :
						</div>

						<div class="fl">
							Super-Admin
							<input type="radio" name="admintype" id="superadmin" value="Superadmin">
						</div>

						<div class="fl">
							Sub-Admin
							<input type="radio" name="admintype" id="admin" value="admin">
						</div>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: red";>(*Please Specify Only One Admin Type)</span>
						<div class=" cls"></div>
					</div> -->

					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Submit Data
							</button>

							&nbsp;&nbsp;&nbsp;&nbsp;

							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Reset Data
							</button>
						</div>
					</div>
					<div class=" cls"></div>
			</div>

			<div class="MarT15">
				<div class=" cls"></div>
			</div>
			<input name="action" type="hidden" value="add" />
			</form>
		</div>

</div>
</section>
</div>

