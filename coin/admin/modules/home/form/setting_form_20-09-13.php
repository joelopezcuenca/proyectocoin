<script type="text/javascript">
$(document).ready(function(){
	$("#regform").validationEngine('attach');
});
</script>
<script>
    $(document).ready(function(){
        $("#password_field").removeAttr("placeholder").attr('disabled','disabled');
        $("#edit_password").change(function(){
            if($("#password_field").is(":disabled"))
                $("#password_field").removeAttr('disabled').attr('placeholder',"Enter New Password");
            else
                $("#password_field").removeAttr("placeholder").attr('disabled','disabled');
        });
    
    $("#media_form").validationEngine("attach",{binded:false});
    $(".customfile").attr("id","customfile");
});
</script>    
<div class="widthebg">
          <div class="font32 tl">My Account form</div>
          <div class="PdTop20">
          	<form id="regform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1" enctype="multipart/form-data" >
             <input type="hidden" name="action" value="insert">
            <div class="fl">
 
              <div class="PdTop20">
                <div class="fl width100">Name :</div>
                <div class="fl"><input type="text" class="inpbg validate[required]" name="admin_name" value="<?php echo $adminDetails['admin_name']; ?>"></div>
                <div class=" cls"></div>
              </div>
              
              <div class="PdTop20">
                <div class="fl width100">Email :</div>
                <div class="fl"><input type="text" class="inpbg validate[required,custom[email]]" name="admin_email" value="<?php echo $adminDetails['email']; ?>"></div>
                <div class=" cls"></div>
              </div>
              
              <div class="PdTop20">
                <div class="fl width100">User Name :</div>
                <div class="fl"><input type="text" class="inpbg validate[required]" name="admin_username" value="<?php echo $adminDetails['admin_username']; ?>"></div>
                <div class=" cls"></div>
              </div>
              
              <div class="PdTop20">
                <div class="fl width100">Password :</div>
                <div class="fl">
                	<input type="password" class="inpbg" name="password_field" id="password_field" disabled="disabled" placeholder="Enter New Password">
                	<input type="checkbox" name="edit_password" id="edit_password" value="1" /><label for="edit_password" id="change_password">&nbsp;&nbsp;Change Password?</label>
                	<label for="password_field" class="error" generated="true" style="display:none"></label>
                </div>
                <div class=" cls"></div>
              </div>
              
              <div class="PdTop20">
                <div class="fl width100">&nbsp;</div>
                <div class="fl">
                 <input name="action" type="hidden" value="add" />
                	<input name="user_login" id="user_login" class="btn btn-success" type="submit" value="Save" />
                </div>
                <div class=" cls"></div>
              </div>
              
            </div>
            
            <div class="fl MarL200"><img src="images/User-icon.png" alt=""></div>
            <div class="cls"></div>
           </form>
          </div>
      </div>
