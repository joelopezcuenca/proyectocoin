<?php /*
 *
 * Edit ADMIN ACCOUNT ACCESS ACCOUNTS FORM
 */
?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#adminuserform").validationEngine('attach');
	}); 
</script>

<script type="text/javascript">
	function submitForm() {
		$("#adminuserform").submit();
	}
</script>

<script>
	$(document).ready(function() {
		$("#password_field").removeAttr("placeholder").attr('disabled', 'disabled');
		$("#edit_password").change(function() {
			if ($("#password_field").is(":disabled"))
				$("#password_field").removeAttr('disabled').attr('placeholder', "Enter New Password");
			else
				$("#password_field").removeAttr("placeholder").attr('disabled', 'disabled');
		});

		$("#media_form").validationEngine("attach", {
			binded : false
		});
		$(".customfile").attr("id", "customfile");
	}); 
</script>

<div id="Containt5" >

	<section>
		<div class="MarA20">
			<div class="fl">

				<form id="adminuserform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1"  enctype="multipart/form-data" >
					<h1 style="color:green;">EDIT AN ADMIN ACCOUNT ACCESS CONTROL AREA</h1>
					<div class="MarT15">
						<div class="fl width100">
							UserName :
						</div>
						<div class="fl">
							<input type="text" name="username" id="username" value="<?php echo $useradminallid[0]['username']; ?>"  class="inpbg validate[required]" >
						</div>

						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							Email :
						</div>
						<div class="fl">
							<input type="text" name="email" id="email"  class="search-query inpbg validate[required,custom[email]]" value="<?php echo $useradminallid[0]['email']; ?>" >
						</div>

						<div class=" cls"></div>
					</div>

					<div class="MarT15">
						<div class="fl width100">
							password :
						</div>
						<div class="fl">
							<input type="password" class="inpbg" name="password_field" id="password_field" disabled="disabled" placeholder="Enter New Password">
							<input type="checkbox" name="edit_password" id="edit_password" value="1" />
							<label for="edit_password" id="change_password">&nbsp;&nbsp;Change Password?</label>
							<label for="password_field" class="error" generated="true" style="display:none"></label>
						</div>

						<div class=" cls"></div>
					</div>
					<div class="MarT15">
						<div class="fl width100">
							&nbsp;
						</div>
						<div class="fl">
							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Submit Data
							</button>

							&nbsp;&nbsp;&nbsp;&nbsp;

							<button class="btn btn-success" type="button" onclick="return submitForm();">
								Reset Data
							</button>
						</div>
					</div>
					<div class=" cls"></div>
			</div>

			<div class="MarT15">
				<div class=" cls"></div>
			</div>
			<input name="id" type="hidden" value="<?php echo $useradminallid[0]['id']; ?>" />
			<input name="action" type="hidden" value="update" />
			
			
			<?php if($useradminallid[0]['Admin_Role']!='Superadmin') {  ?>
			
			<input name="admintype" type="hidden" value="admin" />
			
			<?php  } 
		else {
			?>
			
			<input name="admintype" type="hidden" value="Superadmin" />
			<?php
			
		}
		?>
			</form>
		</div>

</div>
</section>
</div>

