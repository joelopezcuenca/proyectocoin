<script type="text/javascript">
$(document).ready(function(){
	$("#regform").validationEngine('attach');
});
</script>
<script>
    $(document).ready(function(){
        $("#password_field").removeAttr("placeholder").attr('disabled','disabled');
        $("#edit_password").change(function(){
            if($("#password_field").is(":disabled"))
                $("#password_field").removeAttr('disabled').attr('placeholder',"Enter New Password");
            else
                $("#password_field").removeAttr("placeholder").attr('disabled','disabled');
        });
    
    $("#media_form").validationEngine("attach",{binded:false});
    $(".customfile").attr("id","customfile");
});
</script>   

<script type="text/javascript">
function submitForm(){
$("#regform").submit();
}
</script>  
<?php //print_r($adminDetails);?>

<div id="Containt5" >
    <section>
      <div class="MarA20">

            <div class="fl">
            <form id="regform" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="post" name="f1" enctype="multipart/form-data" >
    
              <div class="MarT15">
                <div class="fl width100">Nombre :</div>
                <div class="fl"><input type="text" class="inpbg validate[required]" name="admin_name" value="<?php echo $adminDetails['name']; ?>"></div>
                <div class=" cls"></div>
              </div>

              <div class="MarT15">
                <div class="fl width100">Email :</div>
                <div class="fl"><input type="text" class="inpbg validate[required,custom[email]]" name="admin_email" value="<?php echo $adminDetails['email']; ?>"></div>
                <div class=" cls"></div>
              </div>
              
              
              
              <div class="MarT15">
                <div class="fl width100">Usuario :</div>
                <div class="fl">
                	<input type="text" class="inpbg validate[required]" name="admin_username" value="<?php echo $adminDetails['username']; ?>">
                </div>
                <div class=" cls"></div>
              </div>
                   
              <div class="MarT15">
                <div class="fl width100">Contraseña :</div>
                <div class="fl">
                	<input type="password" class="inpbg" name="password_field" id="password_field" disabled="disabled" placeholder="Enter New Password">
                	<input type="checkbox" name="edit_password" id="edit_password" value="1" /><label for="edit_password" id="change_password">&nbsp;&nbsp;Change Password?</label>
                	<label for="password_field" class="error" generated="true" style="display:none"></label>
                	
                </div>
              <div class=" cls"></div>
              </div>
               <div class="MarT15">
                <div class="fl width100">Contraseña :</div>
                <div class="fl">
                	<input type="file" class="inpbg" name="file" id="file" >
                	
                </div>
                <div class=" cls"></div>
              </div>
              <div class="MarT15">
                <div class="fl width100">&nbsp;</div>
                <div class="fl"><button class="btn btn-success" type="button" onclick="return submitForm();">Submit</button></div>
                <div class=" cls"></div>
              </div>
               <input name="action" type="hidden" value="add" />
             </form>
            </div>
            <?php $image=$userDetails['profile_pic'];
                  $uploads_dir = DEFAULT_URL."/profile_pic/";
                 ?>
            <div class="fr"><!-- <img src="<?php echo IMAGE_URL; ?>/User-icon.png" alt=""> -->
            	 <img src="<?php echo $uploads_dir.$adminDetails['profile_pic'];?>" alt="" width="193px" height="192px">
            	
            </div>
            <div class="cls"></div>
        
      </div>
    </section>
  </div>