<?php
$file = $_REQUEST['file'];

if (file_exists($file)) {
    header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="' . $file . '"');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Pragma: no-cache');
    readfile($file);
}
header('Location:'.ADMIN_MODULE_URL.'/home/payment_record.php');
exit();
?>