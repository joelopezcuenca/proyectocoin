<?php
include_once ("../../../conf/config.inc.php"); 
include '../../../classes/PHPExcel.php';
include_once (ADMIN_MODULE_PATH . "/home/code/userlist_code.php");



$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 

$rowCount = 1; 
$k=1;
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'S.No');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'UserName');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,'Email');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,'Earnings');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,'Paypal email');
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,'Bank');
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,'CLABE');
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount,'');


$trackCounter = 1;
	foreach($userList as $key=>$value)
	{
		$bankDetail = unserialize($value['payment_information']);
		
		if(!empty($comssionRecord))
		{
			$imgPath = ' Paid ';
		}else{
			$imgPath  = ' Unpaid ';
		}
		
		$rowCount++;
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$k);
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$value['username']);	
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$value['email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,$value['amount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,$value['paypal_email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,$bankDetail['bank_name']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$bankDetail['clabe_number']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount,$imgPath);

		$k++;
		$trackCounter++;
	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
	$objWriter->save('userlist.xlsx'); 
	$file_name ='userlist.xlsx';
	header("location:downloader_xls.php?file=$file_name");

?>
