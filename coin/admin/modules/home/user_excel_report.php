<?php
session_start();
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
extract($_GET);
include('../../../classes/PHPExcel.php');
$mainArr = array();
$mainArr[] = $_SESSION['$user_report'];
$objPHPExcel = new PHPExcel();
for ($col = 'A'; $col != 'J'; $col++) {
       $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                }
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
// Add cell formating
 $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
                $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setRGB('E4EAF4');
                $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				
				
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'S.no')
            ->setCellValue('B1', 'name')
            ->setCellValue('C1', 'Memebership')
            ->setCellValue('D1', 'email')
			->setCellValue('E1', 'paypal_email')
			->setCellValue('F1', 'Unique_Url')
			->setCellValue('G1', 'Account_Status');
			
	
for($k=0;$k<count($mainArr[0]);$k++)
			{
					
				
			$n = $k+2;
			if($mainArr[0][$k]['status'] == 1){
					$location = 'Pro User';
				
				}else {
					$location = 'Basic User';
				}
				
			if (($mainArr[0][$k]['Block_Status'] == 'blocked') || ($mainArr[0][$k]['Account_status'] == 'suspended')) {
				$accountstatus='Suspended';
				
			}
			else{
				$accountstatus='Active';
			}

					
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$n,$k+1)
		    ->setCellValue('B'.$n,$mainArr[0][$k]['name'])
		    ->setCellValue('C'.$n,$location)
		    ->setCellValue('D'.$n,$mainArr[0][$k]['email'])
		    ->setCellValue('E'.$n,$mainArr[0][$k]['paypal_email'])
		    ->setCellValue('F'.$n,$mainArr[0][$k]['url'])
			->setCellValue('G'.$n,$accountstatus);
		    }
	  
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');
if(empty($loc))
	$loc = "Total_";
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.$loc.date("Y/m/d").".xls");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
