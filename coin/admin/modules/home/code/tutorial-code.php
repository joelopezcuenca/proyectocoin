<?php
/*
 * Coder:Raghwendra
 * This Code Contains For Tutorial Section Handling
 */
 
if(isset($_SESSION['IS_admin_LoggedIn']))
 {
//$settingsObj = new settings();
$utilityObj = new utility();
$objtutorial = new tutorial();
extract($_POST);
extract($_GET);

$pagename="Tutorial Manager";
$imagename="videotutorial.png";
/*
 * ADD Action Defined Here
 */
$variable = $objtutorial -> selectAllTutorialData();

if ($action == 'add') {
	$dataArray = array("title" => $title, "description" => $description, "video_url" => $video_url, "type" => $type);
	$id = $objtutorial -> AdminTutorialInsert($dataArray);
	header("location: " . ADMIN_MODULE_URL . "/home/tutorial.php");
}
/*
 * Delete Action Defined Here
 */

if ($action == 'delete') {
	$condition = "id=$id";

	$id = $objtutorial -> deletetutorialOnGivenCondition($condition);
	header("location: " . ADMIN_MODULE_URL . "/home/tutorial.php");
}

/*
 * Update Action Defined Here
 */

if ($action == 'update') {

	$condition = "id='$id'";
	$data = array("title" => $title, "description" => $description, "video_url" => $video_url, "type" => $type, "id" => $id);
	$update_data = $objtutorial -> updateTutorialById($data, $condition);
	header("location: " . ADMIN_MODULE_URL . "/home/tutorial.php");
}

/*
 * Check if Id is not Equal to Blank!!
 */
if ($id != "")
	$edit_data = $objtutorial -> selectAllTutorialDataById($id);

 }
else
{
header("location:".ADMIN_MODULE_URL."/login/login.php");
}
?>