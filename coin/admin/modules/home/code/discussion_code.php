<?php
/*
 * Coder:Raghwendra
 * This Code Contains For Mission Section Handling
 */
 
 if(isset($_SESSION['IS_admin_LoggedIn']))
  {

$utilityObj = new utility();
$objMission = new Mission();
$discussionObj = new discussion() ;

$pagename='Discussion Forum';
$imagename='videotutorial.png' ;
/*
* Extracting ALL Post and get Data
*/

extract($_POST);
extract($_GET);

$condition ="discussion_id=1" ;
$discussion_topics =$discussionObj -> select_discussion_topic($condition);


/*
 * Add Action Defined Here for inserting
 */

if ($action == 'addtopic') {

 
	
	$dataArray = array("discussion_id"=>1,"topic_name" => $topic_name, "topic_text" =>$description, "topic_img" => $topic_type, "entry_date" => time());
	$Insertdiscussion = $discussionObj -> insertdiscussion_topic($dataArray);
	
	header("location: " . ADMIN_MODULE_URL . "/home/discussion.php");
}

// to edit topic information from admin
if($action== 'edit')
{
	$condition = "id=".$id ;
	$discussion_topic_edit =$discussionObj -> select_discussion_topic($condition);

}

/*
 * Delete Mission By Their Id
 */

if ($action == 'delete') {
	$condition = "id=$id";
	
	$discussionObj ->DeleteDiscussionById($condition);
	header("location: " . ADMIN_MODULE_URL . "/home/discussion.php");
}

/*
 * Update mission Action Defined Here
 */

if ($action == 'update') {

	$condition = "id='$id_edit'";
	
	$dataArray = array("topic_name"=>$topic_name,"topic_text" => $description, "topic_img" =>$topic_type);
	$update_data = $discussionObj -> UpdatetopicById($dataArray, $condition);
	header("location: " . ADMIN_MODULE_URL . "/home/discussion.php");
}
	}
else
{
header("location:".ADMIN_MODULE_URL."/login/login.php");
}
?>