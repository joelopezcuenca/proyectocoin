<?php
/*
 * Coder:Raghwendra
 * This Code Contains For Mission Section Handling
 */
 ob_start();
 if(isset($_SESSION['IS_admin_LoggedIn']))
  {

$utilityObj = new utility();
$objMission = new Mission();

$pagename='Mission Manager';
$imagename='videotutorial.png' ;
/*
* Extracting ALL Post and get Data
*/

extract($_POST);
extract($_GET);

if($id!='')
{
$mission =$objMission -> selectAllMissionById($id);
}
/*
 * Add Action Defined Here for inserting
 */

if ($action == 'addmission') {

 
	$logo_pic=$_FILES['file']['name'];
    $uploads_dir = SITE_ROOT."/missionlogo/";  //log directory defined here where all logo's will move on
    $tmp_name = $_FILES["file"]["tmp_name"];  //temporary file
	$dist = $uploads_dir."/".$logo_pic;
	move_uploaded_file($tmp_name, $dist);    //move uploading file
	$dataArray = array("Mission_Level"=>$missionlevel,"Title_Description" => $title, "Logo_Image" =>$logo_pic, "video_url" => $missionvideo_url, "type" => $type);
	$Insertmission = $objMission -> AdminMissionInsert($dataArray);
	
	header("location: " . ADMIN_MODULE_URL . "/home/mission.php");
}

/*
 * Delete Mission By Their Id
 */

if ($action == 'delete') {
	$condition = "id=$id";
	
	$objMission ->DeleteMissionById($condition);
	header("location: " .ADMIN_MODULE_URL . "/home/mission.php");
}

/*
 * Update mission Action Defined Here
 */

if ($action == 'update') {


	$condition = "id='$m_id'";
	$logo_pic=$_FILES['missionlogo']['name'];
    $uploads_dir = SITE_ROOT."/missionlogo/";  //log directory defined here where all logo's will move on
    $tmp_name = $_FILES["missionlogo"]["tmp_name"];  //temporary file
	$dist = $uploads_dir."/".$logo_pic;
	move_uploaded_file($tmp_name, $dist);    //move uploading file
	$dataArray = array("Mission_Level"=>$missionlevel,"Title_Description" => $title, "Logo_Image" =>$logo_pic, "video_url" => $missionvideo_url, "type" => $type);
	$update_data = $objMission -> UpdatemissionById($dataArray, $condition);
	header("location: " . ADMIN_MODULE_URL . "/home/mission.php");
	//die('aswwwwwwwww');
	exit ;
}
	}
else
{
header("location:".ADMIN_MODULE_URL."/login/login.php");
}
?>