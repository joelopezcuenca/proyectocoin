<?php
if (isset($_SESSION['IS_admin_LoggedIn'])) {

	/*
	 * CODE FOR ADMIN USER CONTROLLER AREA/MANAGING ACCOUNTS CREATION
	 */

	$utilityObj = new utility();

	/*
	 * OBJECT CREATION OF USER ADMIN ACCOUNT CLASSES
	 */

	$ObjUserController = new AdminUserController();
	$useradminall = $ObjUserController -> selectAllAdminUser();
	extract($_POST);
	extract($_GET);
	$useradminallid = $ObjUserController -> selectAllAdminUserById($id);
	$pagename = 'AdminUser Manager';
	$imagename = 'admin12.png';
	/*
	 * Adding a Account through this
	 */

	if ($action == 'add') {

	if(!isset($admintype))
		{
			$admintype ="admin" ;
		}
		$dataArray = array("email" => $email, "username" => $username, "password" => $utilityObj -> createHash($password), "Admin_Role" => $admintype);
		$ObjUserController -> AdminUserInsert($dataArray);
		$_SESSION['successmsg'] ="Your Account Has been Created Successfully";
		header("location: " .ADMIN_MODULE_URL."/home/admincontroller.php");
		exit;
	}

	/*
	 *check update action with given condition
	 */

	if ($action == 'update') {
		
		if ($password_field == '') {
			$data = array("email" => $email, "username" => $username, "Admin_Role" => $admintype);

		} else {
			$data = array("email" => $email, "username" => $username, "password" => $utilityObj -> createHash($password_field), "Admin_Role" => $admintype);

		}
		$condition = "id='" . $id . "'";
		$onj=$ObjUserController -> UpdateAdminUserData($data, $condition);
		$_SESSION['successedit'] = "Your Account Has been Updated Successfully";
		header("location: " . ADMIN_MODULE_URL . "/home/admincontroller.php");
		exit;

	}

	/*
	 * check delete action with given condition
	 */

	if ($action == 'deleteinfo') {
		$condition = "id='" . $id . "'";
		$ObjUserController -> DeleteAdminUserData($condition);
		//echo "<script type='text/javascript'>window.location='http://comunidadcoin.com/coin/admin/modules/home/admincontroller.php'</script>" ;
		header("location: " . ADMIN_MODULE_URL . "/home/admincontroller.php");
		exit;

	}
} else {
	header("location:" . ADMIN_MODULE_URL . "/login/login.php");
	exit;

}
/*
 * End
 */
?>