<?php
include_once ("../../../conf/config.inc.php"); 
include '../../../classes/PHPExcel.php';
include_once (ADMIN_MODULE_PATH . "/home/code/payment_record_code.php");



$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 

$rowCount = 1; 
$k=1;
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'S.No');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'Paid Status');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,'Month');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,'Total Generated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,'Company Earnings');
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,'Total Comissions Paid');
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,'Paid out by ');


$trackCounter = 1;
	foreach($paymentArr as $key=>$value)
	{
		$paymentStatus = array();
		$userDetail = array();
		$condition = " year=".$currentYear." and month=".$key."";
		$paymentStatus = $ObjUser->get_monthly_comssion_status($condition);
		//----------get user detail-------
		$userDetail = $ObjUser->getOnId($paymentStatus['user_id']);
		
		//-------------
		 if(!empty($userDetail)){
			$paidStatus = 'Paid';
		 }else{
			 $paidStatus = 'UnPaid';
		 }
		 
		 if(!empty($userDetail)){
				$paidBy =  $userDetail['username'];
		 }else{
				$paidBy =  'N/A';
		 }
		 
		 //------------------
		
		
		$rowCount++;
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$k);
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$paidStatus);	
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,'M'.$key);
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,'$'.$value);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,'$'.($value/100)*20);
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,'$'.($value/100)*80);
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$paidBy);

		$k++;
		$trackCounter++;
	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
	$objWriter->save('payment_record.xlsx'); 
	$file_name ='payment_record.xlsx';
	header("location:downloader_xls.php?file=$file_name");

?>
