<?php
include_once ("../../../conf/config.inc.php");
include_once (ADMIN_MODULE_PATH . "/home/code/tutorial-code.php");
include_once (ADMIN_INCLUDE_PATH . "/header.php");
include_once (ADMIN_INCLUDE_PATH . "/left-menu.php");
?>

<section>
	<?php
	if ($to == 'add') {
		include_once (ADMIN_MODULE_PATH . "/home/form/tutorial_form.php");
	} else if ($to == 'edit') {

		include_once (ADMIN_MODULE_PATH . "/home/form/tutorial_edit_form.php");
	} else {
		include_once (ADMIN_MODULE_PATH . "/home/table/tutorial_report.php");
	}
	?>
</section>
<?php
include_once (ADMIN_INCLUDE_PATH . "/footer.php");
?>