<?php
include_once ("../../../conf/config.inc.php"); 
include '../../../classes/PHPExcel.php';
include_once (ADMIN_MODULE_PATH . "/home/code/userlog_code.php");



$objPHPExcel = new PHPExcel(); 
$objPHPExcel->setActiveSheetIndex(0); 

$rowCount = 1; 
$k=1;
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'ENRTY');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'DATE');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,'SELLER');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,'EMAIL');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,'URL');
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,'TYPE');
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,'LEVEL');
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount,'BUYER');
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount,'EMAIL');
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount,'URL');
	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount,'DESCRIPTION');
	$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount,'COIN');
	$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount,'RECIPIENT');
	$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount,'EMAIL');
	$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount,'URL');
	$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount,'ACCOUNT STATUS');


$trackCounter = 1;
	$counter = 0;
	foreach($userLog_record as $record)
	{ $counter++;
		
		 //------------------
		
		$rowCount++;
		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$counter);
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,date('m/d-Y',$record['date']));	
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount,$record['seller']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount,$record['email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount,$record['url']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount,$record['type']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount,$record['seller_level']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $record['buyer']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount,$record['buyer_email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount,$record['buyer_url']);
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount,$record['description']);
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount,$record['coin']);
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount,$record['recipient']);
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount,$record['recipient_email']);
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount,$record['recipient_url']);
		$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount,$record['account_status']);

		$k++;
		$trackCounter++;
	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
	$objWriter->save('user_log_report.xlsx'); 
	$file_name ='user_log_report.xlsx';
	header("location:downloader_xls.php?file=$file_name");

?>
