<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Coin</title>
<!-- Included CSS Files -->
<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
<script type="text/javascript" src="js/modernizr.custom.72882.js"></script>
<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" media="all" href="css/ie.css" />
	<![endif]-->
</head>
<body>
<iframe src="https://player.vimeo.com/video/87042982?autoplay=1&loop=1" id="video_background" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<div class="wrap"> 
  
  <!-- Main -->
  <div id="main">
    <div class="inner"> 
      
      <!-- Header -->
      <header class="site-header"> <a href="#">
        <h1 class="site-title fade-in"><img src="images/coinlogo.png" alt="UPRISER" /></h1>
        </a> </header>
      
      <!-- Content -->
      <section class="content">
        <h1 class="section-title">Bienvenido al Proyecto Coin</h1>
        <p class="subtitle">RESERVA TU INVITACIÓN A UNA COMUNIDAD EXCLUSIVA DE PERSONAS QUE
QUIEREN ALCANZAR SUS SUEÑOS Y CREAR SU PROPIA REALIDAD.</p>
        
        <!-- Countdown timer -->
        <div id="timer"></div>
        
        <!-- Newsletter form -->
        <div id="newsletter" class="form-wrap">
  <form id="aweber_frm" method="post" class="af-form-wrapper" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"  >
            <div style="display: none;">
              <input type="hidden" name="meta_split_id" value="" />
			<input type="hidden" name="listname" value="awlist3953336" />
			<input type="hidden" name="redirect" value="http://www.proyectocoin.com/instruction.php" id="redirect_f25a5d95d2c0b615eb2ba445b6f96978" />

			<input type="hidden" name="meta_adtracking" value="Subscriber_form" />
			<input type="hidden" name="meta_message" value="1" />
			<input type="hidden" name="meta_required" value="email" />

			<input type="hidden" name="meta_tooltip" value="" />
            </div>
            <div id="af-form-719862157" class="af-form"><!--<div id="af-header-719862157" class="af-header"><div class="bodyText"><p>&nbsp;</p></div></div>-->
              <div id="af-body-719862157" class="af-body af-standards">
                <div class="af-element width75">
                  <div class="af-textWrap">
                    <input class="text" id="awf_field-74991821" type="text" name="email" value="" tabindex="500" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if (this.value == '') { this.value='';} " />
                  </div>
                  <div class="af-clear"></div>
                </div>
                <div class="af-element buttonContainer width25">
                  <input name="submit" id="af-submit-image-719862157" type="submit" name="ENTRER" class="image" alt="Submit Form"  tabindex="501" value="Registrarme" />
                  <div class="af-clear"></div>
                </div>
				
				<?php if($_REQUEST['action']=='success'){?>
				<div class="af-clear" style="color:Green; font-weight:bold">Subscription confirmed.</div>
				<?php } ?>
                <div class="af-clear"></div>
              </div>
              <!--<div id="af-footer-719862157" class="af-footer"><div class="bodyText"><p>&nbsp;</p></div></div>--> 
            </div>
            <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=7IycHGxMjKzs" alt="" /></div>
          </form>
        </div>
        
        <!-- Social links --> 
      </section>
      
      <!-- Modal page toggle -->
      <div class="modal-toggle"> <span class="modal-note">© 2014.coin. All Rights Reserved.</span></div>
    </div>
  </div>
</div>

<!-- Background overlay -->
<div class="body-bg"></div>

<!-- YouTube background video --> 
<!--<a id="bg-video" data-property="{videoURL:'https://vimeo.com/87042982', containment:'body', autoPlay:true, mute:true, startAt:0, showControls:false}" data-poster="images/poster.jpg"></a>-->
<div id="mute_id" style="display: none;">mute</div>
<div id="bg-video-controls"> <a id="bg-video-volume" title="ankesh" onclick="volum_func()" class="icon icon-mute" href="#" title="Unmute">
	<span class="screen-reader-text">Unmute</span></a> 
	
	
	<a id="bg-video-play" class="icon icon-pause" href="#" title="Pause"><span class="screen-reader-text">Pause</span></a> </div>

<!-- Loading... -->
<div id="preload">
  <div id="preload-content">
    <div class="preload-spinner"> <span class="bounce1"></span> <span class="bounce2"></span> <span class="bounce3"></span> </div>
    <div class="preload-text">Cargando...</div>
  </div>
</div>

<!-- Included JS Files --> 
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="js/plugins.js"></script> 
<script type="text/javascript" src="js/jquery.mb.YTPlayer.js"></script> 
<script type="text/javascript" src="js/scripts.js"></script> 
<!--vimeo video js--> 
<script type="text/javascript" src="js/bigvideo.js"></script> 
<script type="text/javascript">
$(function() {
    var BV = new $.BigVideo();
    BV.init();
    BV.show('http://vjs.zencdn.net/v/oceans.mp4');
});
</script> 
<!--vimeo video js end--> 
<!--awbeber form js--> 
<script type="text/javascript">
  
    (function() {
        var IE = /*@cc_on!@*/false;
        if (!IE) { return; }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-719862157")) {
                document.getElementById("af-form-719862157").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-719862157")) {
                document.getElementById("af-body-719862157").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-719862157")) {
                document.getElementById("af-header-719862157").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-719862157")) {
                document.getElementById("af-footer-719862157").className = "af-footer af-quirksMode";
            }
        }
    })();
  function volum_func()
  {
  	//alert(jQuery('#mute_id').html());
  	if(jQuery('#mute_id').html()=='mute')
  	{
  		jQuery('#mute_id').html('unmute');
  		var iframe = document.getElementsByTagName('iframe')[0];
		iframe.contentWindow.postMessage('{"method":"setVolume", "value":0}','*');
	}else{
		jQuery('#mute_id').html('mute');
		var iframe = document.getElementsByTagName('iframe')[0];
		iframe.contentWindow.postMessage('{"method":"setVolume","value":1}','*');
	}
  }
</script> 
<script type="text/javascript">document.getElementById('redirect_f98e0d273f98dca991d116c41e48a33b').value = document.location;</script>
</body>
</html>